% This function generates a BER transfer function
% from the additionnal BER array now included in the result files.
% One figure is generated for each SNR value.
% Note: This function is based on settlingBER2.m
%
% File format: See 'scripts/extractBERresult.pl' for details on the
% expected format.
%
% ARGUMENTS
% maxIterList: Vector containing the maximum number of decoding iteration
%              associated with each file in fileArray.
% fileArray: Cell Array containing the filesystem path to each result file
%            that should be plotted. The size of this array must agree with
%            the size of maxIterList.
% RETURNS: An array with the handles to all the curves

function curveHandles = EXITplot(maxIterList, fileArray)

numberOfFiles = size(fileArray,2);

% --- Global Parameters ---
useSeparateFigures = true; % plot each SNR in a different figure 
                           % NOTE: "True" is the only value tested.
twoColumnLegend = false; % currently only supported for useSeparateFigures==false
LINEWIDTH = 1.05;

% First parse each file and extract the #BERLIST: lines.
for fileCount=1:numberOfFiles
    filepath = [pwd '/' fileArray{fileCount}];
    filepathNew = [filepath '_tmpwork'];
    perl('scripts/extractBERresult.pl', filepath, filepathNew);
    fileArray{fileCount} = filepathNew;
end

% master SNR list taken from first file (only these SNRs will be plotted)
SNRListMaster = [];

% handle to each curve object
curveHandles = [];

% loop through the result files
for fileCount=1:1:numberOfFiles

    fid = fopen(fileArray{fileCount});
    % ignore SNR and just retrieve BER array
    M = textscan(fid, '%*f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f', 'CollectOutput', true, 'ReturnOnError', 0);
    BERmatrix = M{1};
    fclose(fid);
    % parse the same files again to retrieve the SNR values
    fid = fopen(fileArray{fileCount});
    M = textscan(fid, '%f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f', 'ReturnOnError', 0);
    SNRList = M{1};
    
    if fileCount == 1
        SNRListMaster = SNRList;
        if useSeparateFigures
            fighandles = zeros(length(SNRListMaster),1);
        else
            windowTitle = 'Settling';
            fig_h = figure('Color',[1 1 1], 'Name', windowTitle);
            fighandles = fig_h;
        end
    end
    
    curMaxIter = maxIterList(fileCount);
    
    % Determine the mapping between bins and iterations for the current
    % file
    binsize = curMaxIter / 20;
    if( binsize < 1 ) % less iter than bins => last ones are unused
        iterIndex = zeros(1,curMaxIter+1); % first element is BER-in
        iterIndex(1) = 0;
        for i=1:curMaxIter
            iterIndex(i+1) = i;
        end
    else
        iterIndex(1) = 0;
        for i=1:20
            iterIndex(i+1) = floor(i * binsize);
        end
    end
    
    % If the bins span more than one iteration each, create 
    % iteration-by-iteration BER matrix by interpolating
    if( binsize <= 1 )
        BERmatrixInterp = BERmatrix;
    else
        BERmatrixInterp = zeros(size(BERmatrix,1),curMaxIter+1);
        for i=1:size(BERmatrix,1)
            for j=1:size(BERmatrix,2)-1
                %curDiff = BERmatrix(i,j+1)-BERmatrix(i,j);
                %interpStep = curDiff / binsize;
                interpArray = logInterp(BERmatrix(i,j), BERmatrix(i,j+1), binsize);
                for k=1:binsize
                    %BERmatrixInterp(i,(j-1)*binsize+k) = BERmatrix(i,j)+(k-1)*interpStep;
                    BERmatrixInterp(i,(j-1)*binsize+k) = interpArray(k);
                end
            end
            BERmatrixInterp(i,curMaxIter+1) = BERmatrix(i,size(BERmatrix,2));
        end
    end
    
    % For each SNR
    for i=1:1:length(SNRList)
        
        curSNR = SNRList(i);
        
        % find the index of this SNR in the master list
        masterIndex=0;
        for j=1:length(SNRListMaster)
            if curSNR == SNRListMaster(j)
                masterIndex = j;
            end
        end
        % skip SNR if not found in first list
        if masterIndex==0
            continue;
        end
        
        if useSeparateFigures
            if( fighandles(masterIndex) == 0 )
                windowTitle = ['BER Transfer at SNR ' num2str(curSNR) 'dB'];
                fig_h = figure('Color',[1 1 1], 'Name', windowTitle);
                fighandles(masterIndex) = fig_h;
            else
                % activate figure
                figure(fighandles(masterIndex));
            end
        end
        
        if (fileCount > 1) || (~useSeparateFigures && i>1)
            hold all;
        end
        
        h = loglog(BERmatrixInterp(i,1:size(BERmatrixInterp,2)-1), ...
                     BERmatrixInterp(i,2:size(BERmatrixInterp,2)), ...
                     'LineWidth', LINEWIDTH);
        %set(h, 'Marker', 'o', 'MarkerSize', 4);
        curveHandles = [curveHandles h];
        % plot the inverse function
        if(fileCount==1) 
            hold on;
        end
        h = loglog(BERmatrixInterp(i,2:size(BERmatrixInterp,2)), ...
                   BERmatrixInterp(i,1:size(BERmatrixInterp,2)-1), ...
                   'LineWidth', LINEWIDTH, 'Color', get(h, 'Color'));
        %set(h, 'Marker', 'o', 'MarkerSize', 4);
        curveHandles = [curveHandles h];
        if(fileCount==1)
            hold off;
        end
        
    end
        
    fclose(fid);
end


% set some properties for each figure
for i=1:length(fighandles)
    figure(fighandles(i));
    
    grid on;
    fontname = 'Geneva';
    
    xlabel('BER In', 'FontSize', 14, 'FontName', fontname);
    ylabel('BER Out', 'FontSize', 14, 'FontName', fontname);
    
    % gca = Get Current Axes
    set(gca, 'FontSize', 16, 'FontName', fontname);
    
    % add a legend
    if(useSeparateFigures)
        % the number of curves per figure is twice the number of files
        % every pair of two curves represent the same function => omit one
        % from the legend
        legendLabels = {'1'};
        for j=2:numberOfFiles*2
            if mod(j,2)==0
                curveIndex = 2*i + (j/2-1)*numberOfFiles*2;
                hasbehavior(curveHandles(curveIndex), 'legend', false);
            else
                legendLabels = [legendLabels num2str((j+1)/2)];
            end
        end
        legend(legendLabels);
    else % (all curves on a single figure)
        legendLabels = {};
        for k=1:numberOfFiles
            for j=1:length(SNRListMaster)
                curLabel = [num2str(k) ' ' num2str(SNRListMaster(j)) 'dB'];
                legendLabels = [legendLabels curLabel];
            end
        end
        % order the legend entries by SNR
        order = [];
        for k=1:length(SNRListMaster)
            j=1;
            while j<=numberOfFiles && k+(j-1)*length(SNRListMaster) <= length(curveHandles)
                order = [order k+(j-1)*length(SNRListMaster)];
                j = j+1;
            end
        end
        if twoColumnLegend
            ah1 = gca;
            half = length(curveHandles)/2;
            legend(ah1, curveHandles(order(1:half)), legendLabels{order(1:half)});
            ah2 = axes('position',get(gca,'position'), 'visible','off');
            set(ah2, 'FontSize', 16, 'FontName', fontname);
            legend(ah2, curveHandles(order(half+1:length(order))), legendLabels{order(half+1:length(order))});
        else
            legend(curveHandles(order), legendLabels{order});
        end
    end
end

end

% Log-linear Interpolation
%@param delta is the distance between the two points, and also the number of 
% interpolation points returned. Must be an integer.
function interpArray = logInterp(y1,y2,delta)
    a = (log10(y2)-log10(y1)) / delta;
    b = log10(y1);
    interpArray = zeros(1,delta);
    for x=0:(delta-1)
        interpArray(x+1) = 10^(a*x+b);
    end
end