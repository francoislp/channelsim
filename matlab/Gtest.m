% Tests a G matrix expressed in systematic form (as only the matrix
% generating the (n-k) parity bits).

function Gtest(G, H, colorder)

fprintf('G is %d x %d\n', size(G,1), size(G,2));
k = size(G,1);
    
    
% Make sure that all columns of G contain some non-zero elements
% zerocol = 0; % number of zero columns
% for i=1:size(G,2)
%   sum = 0;
%   for j=1:size(G,1)
%     sum = sum + G(j,i);
%     % also check that all elements are either 1 or 0
%     if(G(j,i)~=0 && G(j,i)~=1)
%       fprintf('Invalid element in matrix: %d, ', G(j,i));
%       fprintf('at position (%d, %d)\n', j, i);
%       return;
%     end
%   end
%   if(sum == 0)
%     zerocol = zerocol + 1;
%   end
% end
% if( zerocol ~= 0 )
%   fprintf('Error: G has %d all-zero columns!\n', zerocol);
% end

% Generate some random codewords and check that they are codewords
% according to H.
NUMCW = 100;
failed = false;

for i=1:NUMCW
  % random message vector
  u = zeros(1, k);
  for j=1:k
    if rand() < 0.5
      u(j) = 0;
    else
      u(j) = 1;
    end
  end

  % generate the parity-bits
  p = mod(u*G, 2);
%p = zeros(1, size(G,2));
  
  % combine message and parity bits to form the codeword
  % bit ordering of p:
%  p = fliplr(p);
  cw = [p u];
%  cw = [u p];

  % re-order the columns as specified by Neal's format
  for i=1:length(cw)
    cw2(i) = cw(colorder(i));
  end
  
  % print all the codeword weights (for fun !)
  fprintf('%d, ', sum(cw2));
  if(mod(i, 10) == 0)
    fprintf('\n');
  end
  
  % compute the syndrom (it should be a zero vector)
  s = mod(cw2 * H', 2);
  % check if it's all zero
  comp = (s ~= zeros(1, size(H,1)));
  
  if any(comp)
    fprintf('(FAIL) Syndrom is non-zero\n');
    failed = true;
    break;
  end
end

if ~failed
  fprintf('\n');
  fprintf('(PASS)\n');
end

end %function