% This scripts assumes that a variable named "filename" exists in the
% workspace.
% The script will add a dataset to the last selected plot.

% --- Global Parameters ---
LINEWIDTH = 2;
PLOTBER = true;
PLOTFER = false;
color = 'g'; %One of the following letters: y,m,c,r,g,b,w,k
marker = '^'; %One of: +,o,*,.,x,s,d,^,v,>,<,p,hexagram

fid = fopen(filename);

try
    M = textscan(fid, '%f %f %f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f', 'CommentStyle', '#', 'ReturnOnError', 0);
catch
    fprintf('Using new file format for file 1.\n');
    fclose(fid);
    fid = fopen(filename);
    M = textscan(fid, '%f %f %f %*f %*f %*f %*s %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f', 'CommentStyle', '#', 'ReturnOnError', 0);
end

fclose(fid);

SNR = M{1};
BER = M{2};
FER = M{3};
hold on;
if( PLOTBER )
    semilogy(SNR, BER, ['-' color marker], 'LineWidth', LINEWIDTH, 'MarkerSize', 10);
end
% dotted line for FER
if( PLOTFER )
    h = semilogy(SNR, FER, ['-' color marker], 'LineWidth', LINEWIDTH, 'MarkerSize', 10);
end

if( PLOTBER && PLOTFER )
    hasbehavior(h, 'legend', false);
end