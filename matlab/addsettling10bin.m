% Adds (a) settling curve(s) to an existing figure. All SNRs in the file
% are added, unless a SNR index is specified.
%
% Arguments
% "maxiter"   Max decoding iteration for that result file.
% "filename"  Input file name

function addsettling10bin(maxiter, filename, figHandle, snrIndex)

% --- Global Parameters ---
LINEWIDTH = 1.05;

if nargin < 4
    snrIndex=0;
end

fid = fopen(filename);
% support both old and new file formats
try
    M = textscan(fid, '%f %*f %*f %*f %f %f %f %f %f %f %f %f %f %f %f %f', 'CommentStyle', '#', 'ReturnOnError', 0);
catch
    fprintf('Using new file format for file 1.\n');
    fclose(fid);
    fid = fopen(filename);
    M = textscan(fid, '%f %*f %*f %*f %f %f %*s %f %f %f %f %f %f %f %f %f %f', 'CommentStyle', '#', 'ReturnOnError', 0);
end

SNR = M{1};
trialcount = M{2};
baseErrorCnt = M{3};
%TODO: should add to baseErrorCnt the number of undetected errors
histogram = zeros(10, length(SNR));
for i=1:1:10
    histogram(i,:) = M{i+3};
end

iterAxis = zeros(10,1);
for i=1:1:10
    iterAxis(i) = maxiter/10 * i;
end

% make figure "current"
figure(figHandle);
grid on;
hold all;

if snrIndex==0
    % For each SNR
    %note: Code is written so it can be in a outter file loop
    for i=1:1:length(SNR)
        
        curSNR = SNR(i);
        curTrialCount = trialcount(i);
        curBaseErrorCnt = baseErrorCnt(i);
        curHistogram = histogram(:,i);
        
        curcurErrCnt = curBaseErrorCnt;
        for j=length(curHistogram):-1:1
            FERarray(j) = curcurErrCnt / curTrialCount;
            curcurErrCnt = curcurErrCnt + curHistogram(j);
        end
        
        if(i==2)
            hold all;
        end
        h = semilogy(iterAxis, FERarray, 'LineWidth', LINEWIDTH);
        set(h, 'Marker', 'o', 'MarkerSize', 8);
        
    end
else
    % almost same code as in the loop above
    curSNR = SNR(snrIndex);
    curTrialCount = trialcount(snrIndex);
    curBaseErrorCnt = baseErrorCnt(snrIndex);
    curHistogram = histogram(:,snrIndex);
    
    curcurErrCnt = curBaseErrorCnt;
    for j=length(curHistogram):-1:1
        FERarray(j) = curcurErrCnt / curTrialCount;
        curcurErrCnt = curcurErrCnt + curHistogram(j);
    end

    h = semilogy(iterAxis, FERarray, 'LineWidth', LINEWIDTH);
    set(h, 'Marker', 'o', 'MarkerSize', 8);
end

grid on;
fontname = 'Geneva';
%title('Error rate vs SNR', 'FontSize', 12, 'FontName', fontname);
xlabel('Iterations', 'FontSize', 14, 'FontName', fontname);
ylabel('FER', 'FontSize', 14, 'FontName', fontname);
    
% gca = Get Current Axes
set(gca, 'FontSize', 16, 'FontName', fontname);

end