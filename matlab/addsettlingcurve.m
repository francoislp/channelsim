function addsettlingcurve(fig_handle, file1)

blackandwhite = true; % use different line styles for each curve
LINEWIDTH = 1.5; % 2 on linux
showMarkers = false; %TODO: only implemented for first two curves
useLogScale = true; % use log scale for iterations

data = dlmread(file1);

figure(fig_handle); % make figure "current"
hold all;
if(useLogScale)
    h = loglog(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
else
    h = semilogy(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
end
if(showMarkers)
    set(h, 'Marker', 'o', 'MarkerSize', 10);
end