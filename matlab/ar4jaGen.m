% Generate the rate 1/2 AR4JA LDPC code of length k=1024 (n=2048)

function H = ar4jaGen()

    M=512;
    
    row1 = [zeros(M) zeros(M) eye(M) zeros(M) xor(eye(M),genPk(1,M))];
    
    row2 = [eye(M) eye(M) zeros(M) eye(M) xor(xor(genPk(2,M),genPk(3,M)),genPk(4,M))];
    
    row3 = [eye(M) xor(genPk(5,M),genPk(6,M)) zeros(M) xor(genPk(7,M),genPk(8,M)) eye(M)];
    
    H = [row1' row2' row3']';

end

% Generates matrix Pi_k found in the specifications
% Args:
% - k: index
% - M: matrix dimension (square matrix)
function P = genPk(k, M)

    P = zeros(M);
    
    for i=0:(M-1)
        colIndex = M/4*mod(thetaK(k)+floor(4*i/M),4);
        colIndex = colIndex + mod(phiK(k,floor(4*i/M),M)+i, M/4);
        P(i+1,colIndex+1) = 1; % indexes are zero-based in specifications
    end
end

function val = thetaK(k)
    switch k
        case 1
            val=3;
        case 2
            val=0;
        case 3
            val=1;
        case 4
            val=2;
        case 5
            val=2;
        case 6
            val=3;
        case 7
            val=0;
        case 8
            val=1;
        case 9
            val=0;
        case 10
            val=1;
        otherwise
            error('Function is currently undefined for argument value %d', k);
    end
end

function val = phiK(k, j, M)

% currently only M=512 is supported
if(M ~= 512)
    error('Only M=512 is supported in function phiK');
end

if(j==0)
    switch k
        case 1
            val=16;
        case 2
            val=103;
        case 3
            val=105;
        case 4
            val=0;
        case 5
            val=50;
        case 6
            val=29;
        case 7
            val=115;
        case 8
            val=30;
        case 9
            val=92;
        case 10
            val=78;
        otherwise
            error('Function is currently undefined for argument value %d',k);
    end
elseif(j==1)
        switch k
            case 1
                val=0;
            case 2 
                val=53;
            case 3
                val=74;
            case 4
                val=45;
            case 5
                val=47;
            case 6
                val=0;
            case 7
                val=59;
            case 8
                val=102;
            case 9
                val=25;
            case 10
                val=3;
            otherwise
                error('Function is currently undefined for argument value %d',k);
        end
elseif(j==2)
        switch k
            case 1
                val=0;
            case 2
                val=8;
            case 3 
                val=119;
            case 4
                val=89;
            case 5
                val=31;
            case 6
                val=122;
            case 7
                val=1;
            case 8
                val=69;
            case 9
                val=92;
            case 10
                val=47;
            otherwise
                error('Function is currently undefined for argument value %d',k);
        end
elseif(j==3)
        switch k
            case 1
                val=0;
            case 2
                val=35;
            case 3
                val=97;
            case 4
                val=112;
            case 5
                val=64;
            case 6
                val=93;
            case 7
                val=99;
            case 8
                val=94;
            case 9
                val=103;
            case 10
                val=91;
            otherwise
                error('Function is currently undefined for argument value %d',k);
        end
else
    error('Function is undefined for j>3');
end

end
               
            