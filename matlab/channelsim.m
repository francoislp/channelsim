% Author: Francois Leduc-Primeau

% define Q(a) = P(X > a)
Q = @(a) 1/2 * (1 - erf(a/sqrt(2)));

% error prob for BPSK (analytic expression)
SNR = 0.1:0.01:10; % ratio Eb/No
Perr = Q( sqrt(2*SNR) );
figure;
semilogy(10*log10(SNR), Perr, 'r');
title('Perr for BPSK (with AWGN)');
xlabel('SNR [dB]');
ylabel('Perr');
grid on;

% simulate the transmission for each noise power (Eb = 1)

SNR = [];
PerrVect = [];
errcntVect = []; %testing
POINTS = 500; %initial number of trials
MINERRORS = 100;

for No = 10:-0.01:0.1; % noise power
    SNR = [SNR 1/No]; %ratio Eb/No
    %TODO: for each noise power, simulate until we have n errors
    % generate random noise with exact mean and variance
    n = randn(POINTS, 1);
    n = (n - mean(n)) / sqrt(var(n)); % std normal with exact parameters
    n = sqrt(No/2) * n; % normal with var = No/2

    % say the signal S(t) = x*sigma(t)
    % with sqrt(Eb)=1, x=+/-1 with P(X=1) = P(X=-1) = 0.5
    x = heaviside(rand(POINTS, 1)-0.5) * 2 - 1;

    % received signal with the additive noise
    y = x + n;

    % count errors (y != x)
    errcnt=0;
    for i = 1:POINTS
        if (x(i) > 0 & y(i) < 0) | (x(i) < 0 & y(i) > 0)
            errcnt = errcnt + 1;
        end
    end

    % experimental error prob
    p = errcnt/POINTS;
    PerrVect = [PerrVect p];
    
    errcntVect = [errcntVect errcnt];
    % heurirstic: update the next number of trials based on current nb of
    % errors.
    if(errcnt < MINERRORS)
        POINTS = POINTS * 2;
    end
end

% plot Perr against the SNR (on top of previous graph)
hold on;
semilogy(10*log10(SNR), PerrVect, 'b');