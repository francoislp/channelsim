% This functions computes the G matrix corresponding to an H matrix, and 
% returns it. Optionally, if the "outfile" argument is provided, the matrix
% is also written to that file in a binary format.

function G = generateG(H, outfile)

if(nargin < 2)
  outfile='';
end

[matrixM, matrixN] = size(H);

% check if the left mxm matrix is the identity matrix
if( H(:, 1:matrixM) == eye(matrixM) )
    fprintf('H is in (left) systematic form\n');
    A = H(:, matrixM+1:matrixN);
    
    G = [A' eye(matrixN-matrixM)];
    
% else check if the right mxm matrix is the identity matrix
elseif( H(:, (matrixN-matrixM+1):matrixN) == eye(matrixM) )
    fprintf('H is in (right) systematic form\n');
    A = H(:, 1:(matrixN-matrixM));
    
    G = [eye(matrixN-matrixM) A'];

else
    fprintf('H not in systematic form, finding the nullspace...\n');
    
    Hrref = rref_mod2(H);
    
    % find free columns
    freecol = [];
    for i=1:size(Hrref,2) % column index
        curSum = sum( Hrref(:,i) );
        if( curSum==0 )
            fprintf('Error !\n');
            return;
        elseif( curSum ~= 1 )
            if(size(freecol,2)==0)
                freecol(1) = i;
            else
                freecol = [freecol i];
            end
        end
    end
    
    % build G-transposed row by row
    curIndepIndex = 1;
    dbg_freecount = 0;
    for i=1:size(Hrref,2) % column index
        curSum = 0;
        for j=1:size(Hrref,1) % row index
            curSum = curSum + Hrref(j,i);
            % assuming this is a pivot column, the index of the pivot
            if( Hrref(j,i) == 1 )
                pivotrow = j;
            end
        end
        
        if( curSum == 1 )  % => variable_i is dependent
            for j=1:size(freecol,2)
                G(i,j) = Hrref(pivotrow, freecol(j));
            end
        else % => variable_i is free (independent)
          dbg_freecount = dbg_freecount + 1;
          % first set all row-elements to zero
          for j=1:size(freecol,2)
            G(i,j) = 0;
          end
          G(i, curIndepIndex) = 1;
          curIndepIndex = curIndepIndex + 1;
        end
    end
    
    G = G';
    fprintf('G is %d x %d\n', size(G,1), size(G,2));
    fprintf('RREF of H has %d free columns\n', dbg_freecount);
    
    
    % Make sure that all columns of G contain some non-zero elements
    zerocol = 0; % number of zero columns
    for i=1:size(G,2)
        colSum = 0;
        for j=1:size(G,1)
            colSum = colSum + G(j,i);
            % also check that all elements are either 1 or 0
            if(G(j,i)~=0 && G(j,i)~=1)
                fprintf('Invalid element in matrix: %d, ', G(j,i));
                fprintf('at position (%d, %d)\n', j, i);
                return;
            end
        end
        if(colSum == 0)
            zerocol = zerocol + 1;
        end
    end
    if( zerocol ~= 0 )
        fprintf('Error: G has %d all-zero columns!\n', zerocol);
    end
    
end

% Make sure that G * transpose(H) = 0
G_sizeM = size(G,1);
if( G * H' ~= zeros(G_sizeM, matrixM) ) %TODO: this doesn't work as intended
    fprintf('Error: Matrix product not zero !\n');
    return;
end

fprintf('All tests passed\n');

if(strcmp(outfile, ''))
  return;
end

% Try to open the output file
outfile_fid = fopen(outfile, 'w');
if(outfile_fid < 0)
    fprintf('Error: Cannot open byte output file.\n');
    return;
end

% write G to the output file
% write format code
formatcode = 1;
count = fwrite(outfile_fid, formatcode, 'int32');
if(count~=1)
    fprintf('Error writing format code\n');
    return;
end
% write size "m" of the matrix
count = fwrite(outfile_fid, size(G,1), 'int32');
if(count~=1)
    fprintf('Error writing size M\n');
    return;
end
% write size "n"
count = fwrite(outfile_fid, size(G,2), 'int32');
if(count~=1)
    fprintf('Error writing size N\n');
    return;
end
% write matrix data by column
for i=1:size(G,2)
    count = fwrite(outfile_fid, G(:,i), 'int8');
    if(count~=size(G,1))
        fprintf('Error writing a column\n');
        return;
    end
end

% close the output file
fclose(outfile_fid);
