% Generates an all-zero generator matrix of the specified size
% and writes it to the file "outfile"

function generateZeroG(m, n, outfile)

% Try to open the output file
outfile_fid = fopen(outfile, 'w');
if(outfile_fid < 0)
    'Cannot open byte output file.'
    return;
end

% write G to the output file
% write format code
formatcode = 1;
count = fwrite(outfile_fid, formatcode, 'int32');
if(count~=1)
    'Error writing format code'
    return;
end
% write size "m" of the matrix
count = fwrite(outfile_fid, m, 'int32');
if(count ~= 1)
    'Error writing size M'
    return;
end
% write size "n"
count = fwrite(outfile_fid, n, 'int32');
if(count ~= 1)
    'Error writing size N'
    return;
end
% write matrix data by column
for i=1:n
    count = fwrite(outfile_fid, zeros(m,1), 'int8');
    if(count ~= m)
        'Error writing a column'
        return;
    end
end

% close the output file
fclose(outfile_fid);