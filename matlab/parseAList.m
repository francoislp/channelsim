% Author: Francois Leduc-Primeau, francoislp@gmail.com.

% This function parses a binary A-List file representing an H matrix and
% returns the matrix. The A-List file can contain line comments that 
% start with "#".

function H = parseAList(infile)

fid_in = fopen(infile);

wholefile = textscan(fid_in, '%s', 'delimiter', '\n', 'whitespace', '', ...
    'CommentStyle', '#');
lines = wholefile{1};

curLine = textscan(lines{1}, '%d %d');
% note: for the QA-List format, a 3rd %d token contains the field order
matrixN = curLine{1};
matrixM = curLine{2};

% list of # of non-zero elements in each column
colsizelist = textscan(lines{3}, '%d');
colsizelist = colsizelist{1};

%debug: print matrix size
fprintf('Matrix dimensions (mxn): %d x %d\n', matrixM, matrixN);
%debug: print colsizelist
%colsizelist

H = zeros(matrixM, matrixN); % generateG runs faster with dense format
% Using sparse matrix object: 
% (TODO: make sure to test with generateG because of comment below !)
%H = sparse(double(matrixM), double(matrixN));
%H = sparse(double(matrixM), double(matrixN)); NEEDS DEBUG, GET A DIFFERENT
% RESULT.

% place the 1 elements
colindex=1;
for i=5:4+matrixN
    curLine = textscan(lines{i}, '%d');
    curLine = curLine{1};
    
    if( colindex > size(colsizelist,1) )
        fprintf('Column size list is too short\n');
        return;
    end
    for j=1:colsizelist(colindex)
        rowindex = curLine(j);
        if(rowindex == 0)
            fprintf('Error, Invalid row index: (%d, %d)\n', i, j);
        else
            H(rowindex, colindex) = 1;
        end
    end
    colindex = colindex + 1;
end
% ---------- Done parsing ----------

fclose(fid_in);

end
