% Replaced by settling10bin2, but can be used as a shortcut to
% settling10bin2 when the iteration limit is the same for all simulation
% results.
function settling10bin(maxiter, file1, file2, file3, file4, file5, file6)

if nargin < 2
    fprintf('Need at least two arguments.\n');
    return;
end

% make sure first argument is a number
if ~isnumeric(maxiter)
    fprintf('First argument must be a number.\n');
    return;
end

numberOfFiles = nargin - 1;

if numberOfFiles == 1
    fileArray = {file1};
elseif numberOfFiles == 2
    fileArray = {file1 file2};
elseif numberOfFiles == 3
    fileArray = {file1 file2 file3};
elseif numberOfFiles == 4
    fileArray = {file1 file2 file3 file4};
elseif numberOfFiles == 5
    fileArray = {file1 file2 file3 file4 file5};
elseif numberOfFiles == 6
    fileArray = {file1 file2 file3 file4 file5 file6};
end

settling10bin2(maxiter*ones(1,numberOfFiles), fileArray);


end