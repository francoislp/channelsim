% Author: Francois Leduc-Primeau (francoislp@gmail.com)
%
% This function generates settling curves (FER vs max iterations) from the
% 10-bin iteration histogram included in the result files.
% One figure is generated for each SNR value.
%
% ARGUMENTS
% maxIterList: Vector containing the maximum number of decoding iteration
%              associated with each file in fileArray.
% fileArray: Cell Array containing the filesystem path to each result file
%            that should be plotted. The size of this array must agree with
%            the size of maxIterList.
% RETURNS: An array with the handles to all the curves
%
% TODO: Currently, undetected errors (if any) are not taken into account.

function curveHandles = settling10bin2(maxIterList, fileArray)

numberOfFiles = size(fileArray,2);

% --- Global Parameters ---
useSeparateFigures = true; % plot each SNR in a different figure
twoColumnLegend = false; % currently only supported for useSeparateFigures==false
LINEWIDTH = 1.05;


% master SNR list taken from first file (only these SNRs will be plotted)
SNRListMaster = [];

% handle to each curve object
curveHandles = [];

% loop through the result files
for fileCount=1:1:numberOfFiles

    fid = fopen(fileArray{fileCount});
    if(fid<0)
        error('Could not open file: %s', fileArray{fileCount});
    end
    % support both old and new file formats
    try
        M = textscan(fid, '%f %*f %*f %*f %f %f %f %f %f %f %f %f %f %f %f %f', 'CommentStyle', '#', 'ReturnOnError', 0);
    catch
        fprintf('Using new file format for file %d.\n', fileCount);
        fclose(fid);
        fid = fopen(fileArray{fileCount});
        M = textscan(fid, '%f %*f %*f %*f %f %f %*s %f %f %f %f %f %f %f %f %f %f', 'CommentStyle', '#', 'ReturnOnError', 0);
    end
    
    SNRList = M{1};
    trialcount = M{2};
    baseErrorCnt = M{3};
    %TODO: should add to baseErrorCnt the number of undetected errors
    histogram = zeros(10, length(SNRList));
    for i=1:1:10
        histogram(i,:) = M{i+3};
    end
    
    if fileCount == 1
        SNRListMaster = SNRList;
        if useSeparateFigures
            fighandles = zeros(length(SNRListMaster),1);
        else
            windowTitle = 'Settling';
            fig_h = figure('Color',[1 1 1], 'Name', windowTitle);
            fighandles = fig_h;
        end
    end
    
    % For each SNR
    for i=1:1:length(SNRList)
        
        curSNR = SNRList(i);
        
        % find the index of this SNR in the master list
        masterIndex=0;
        for j=1:length(SNRListMaster)
            if curSNR == SNRListMaster(j)
                masterIndex = j;
            end
        end
        % skip SNR if not found in first list
        if masterIndex==0
            continue;
        end
    
        curTrialCount = trialcount(i);
        curBaseErrorCnt = baseErrorCnt(i);
        curHistogram = histogram(:,i);
        
        if useSeparateFigures
            if( fighandles(masterIndex) == 0 )
                windowTitle = ['Settling at SNR ' num2str(curSNR) 'dB'];
                fig_h = figure('Color',[1 1 1], 'Name', windowTitle);
                fighandles(masterIndex) = fig_h;
            else
                % activate figure
                figure(fighandles(masterIndex));
            end
        end
        
        curcurErrCnt = curBaseErrorCnt;
        for j=length(curHistogram):-1:1
            FERarray(j) = curcurErrCnt / curTrialCount;
            curcurErrCnt = curcurErrCnt + curHistogram(j);
        end
        
        if (fileCount > 1) || (~useSeparateFigures && i>1)
            hold all;
        end
        iterAxis = maxIterList(fileCount)/10*(1:1:10); % x-axis
        h = semilogy(iterAxis, FERarray, 'LineWidth', LINEWIDTH);
        set(h, 'Marker', 'o', 'MarkerSize', 8);
        curveHandles = [curveHandles h];
        
    end
        
    fclose(fid);
end


% set some properties for each figure
for i=1:length(fighandles)
    figure(fighandles(i));
    
    grid on;
    fontname = 'Geneva';
    
    xlabel('Iterations', 'FontSize', 14, 'FontName', fontname);
    ylabel('FER', 'FontSize', 14, 'FontName', fontname);
    
    % gca = Get Current Axes
    set(gca, 'FontSize', 16, 'FontName', fontname);
    
    % add a legend
    if(useSeparateFigures)
        % the number of curves per figure is the number of files
        legendLabels = {'1'};
        for i=2:numberOfFiles
            legendLabels = [legendLabels num2str(i)];
        end
        legend(legendLabels);
    else % (all curves on a single figure)
        legendLabels = {};
        for i=1:numberOfFiles
            for j=1:length(SNRListMaster)
                curLabel = [num2str(i) ' ' num2str(SNRListMaster(j)) 'dB'];
                legendLabels = [legendLabels curLabel];
            end
        end
        % order the legend entries by SNR
        order = [];
        for i=1:length(SNRListMaster)
            j=1;
            while j<=numberOfFiles && i+(j-1)*length(SNRListMaster) <= length(curveHandles)
                order = [order i+(j-1)*length(SNRListMaster)];
                j = j+1;
            end
        end
        if twoColumnLegend
            ah1 = gca;
            half = length(curveHandles)/2;
            legend(ah1, curveHandles(order(1:half)), legendLabels{order(1:half)});
            ah2 = axes('position',get(gca,'position'), 'visible','off');
            set(ah2, 'FontSize', 16, 'FontName', fontname);
            legend(ah2, curveHandles(order(half+1:length(order))), legendLabels{order(half+1:length(order))});
        else
            legend(curveHandles(order), legendLabels{order});
        end
    end
end

end
