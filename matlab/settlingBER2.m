% This function generates BER settling curves (BER vs number of iterations)
% from the additionnal BER array now included in the result files.
% One figure is generated for each SNR value.
% Note: settlingBER2 allows to specify the max. number of iterations
%       independently for each file. When the number is the same,
%       settlingBER is a simpler front end.
%
% File format: See 'scripts/extractBERresult.pl' for details on the
% expected format.
%
% ARGUMENTS
% maxIterList: Vector containing the maximum number of decoding iteration
%              associated with each file in fileArray.
% fileArray: Cell Array containing the filesystem path to each result file
%            that should be plotted. The size of this array must agree with
%            the size of maxIterList.
% RETURNS: An array with the handles to all the curves

function curveHandles = settlingBER2(maxIterList, fileArray)

numberOfFiles = size(fileArray,2);

% --- Global Parameters ---
useSeparateFigures = true; % plot each SNR in a different figure
twoColumnLegend = false; % currently only supported for useSeparateFigures==false
LINEWIDTH = 1.05;

% First parse each file and extract the #BERLIST: lines.
for fileCount=1:numberOfFiles
    filepath = [pwd '/' fileArray{fileCount}];
    filepathNew = [filepath '_tmpwork'];
    perl('scripts/extractBERresult.pl', filepath, filepathNew);
    fileArray{fileCount} = filepathNew;
end

% master SNR list taken from first file (only these SNRs will be plotted)
SNRListMaster = [];

% handle to each curve object
curveHandles = [];

% loop through the result files
for fileCount=1:1:numberOfFiles

    fid = fopen(fileArray{fileCount});
    % ignore SNR and just retrieve BER array
    M = textscan(fid, '%*f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f', 'CollectOutput', true, 'ReturnOnError', 0);
    BERmatrix = M{1};
    fclose(fid);
    % parse the same files again to retrieve the SNR values
    fid = fopen(fileArray{fileCount});
    M = textscan(fid, '%f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f', 'ReturnOnError', 0);
    SNRList = M{1};
    
    if fileCount == 1
        SNRListMaster = SNRList;
        if useSeparateFigures
            fighandles = zeros(length(SNRListMaster),1);
        else
            windowTitle = 'Settling';
            fig_h = figure('Color',[1 1 1], 'Name', windowTitle);
            fighandles = fig_h;
        end
    end
    
    % Determine the mapping between bins and iterations for the current
    % file
    binsize = maxIterList(fileCount) / 20;
    if( binsize < 1 ) % less iter than bins => last ones are unused
        iterIndex = zeros(1,maxIterList(fileCount)+1); % first element is BER-in
        iterIndex(1) = 0;
        for i=1:maxIterList(fileCount)
            iterIndex(i+1) = i;
        end
    else
        iterIndex(1) = 0;
        for i=1:20
            iterIndex(i+1) = floor(i * binsize);
        end
    end
    
    % For each SNR
    for i=1:1:length(SNRList)
        
        curSNR = SNRList(i);
        
        % find the index of this SNR in the master list
        masterIndex=0;
        for j=1:length(SNRListMaster)
            if curSNR == SNRListMaster(j)
                masterIndex = j;
            end
        end
        % skip SNR if not found in first list
        if masterIndex==0
            continue;
        end
        
        if useSeparateFigures
            if( fighandles(masterIndex) == 0 )
                windowTitle = ['Settling at SNR ' num2str(curSNR) 'dB'];
                fig_h = figure('Color',[1 1 1], 'Name', windowTitle);
                fighandles(masterIndex) = fig_h;
            else
                % activate figure
                figure(fighandles(masterIndex));
            end
        end
        
        if (fileCount > 1) || (~useSeparateFigures && i>1)
            hold all;
        end
        
        h = semilogy(iterIndex, BERmatrix(i,:), 'LineWidth', LINEWIDTH);
        set(h, 'Marker', 'o', 'MarkerSize', 8);
        curveHandles = [curveHandles h];
        
    end
        
    fclose(fid);
end


% set some properties for each figure
for i=1:length(fighandles)
    figure(fighandles(i));
    
    grid on;
    fontname = 'Geneva';
    
    xlabel('Iterations', 'FontSize', 14, 'FontName', fontname);
    ylabel('BER', 'FontSize', 14, 'FontName', fontname);
    
    % gca = Get Current Axes
    set(gca, 'FontSize', 16, 'FontName', fontname);
    
    % add a legend
    if(useSeparateFigures)
        % the number of curves per figure is the number of files
        legendLabels = {'1'};
        for i=2:numberOfFiles
            legendLabels = [legendLabels num2str(i)];
        end
        legend(legendLabels);
    else % (all curves on a single figure)
        legendLabels = {};
        for i=1:numberOfFiles
            for j=1:length(SNRListMaster)
                curLabel = [num2str(i) ' ' num2str(SNRListMaster(j)) 'dB'];
                legendLabels = [legendLabels curLabel];
            end
        end
        % order the legend entries by SNR
        order = [];
        for i=1:length(SNRListMaster)
            j=1;
            while j<=numberOfFiles && i+(j-1)*length(SNRListMaster) <= length(curveHandles)
                order = [order i+(j-1)*length(SNRListMaster)];
                j = j+1;
            end
        end
        if twoColumnLegend
            ah1 = gca;
            half = length(curveHandles)/2;
            legend(ah1, curveHandles(order(1:half)), legendLabels{order(1:half)});
            ah2 = axes('position',get(gca,'position'), 'visible','off');
            set(ah2, 'FontSize', 16, 'FontName', fontname);
            legend(ah2, curveHandles(order(half+1:length(order))), legendLabels{order(half+1:length(order))});
        else
            legend(curveHandles(order), legendLabels{order});
        end
    end
end

end