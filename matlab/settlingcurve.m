function settlingcurve(file1, file2, file3, file4, file5, file6)

blackandwhite = true; % use different line styles for each curve
LINEWIDTH = 1.5; % 2 on linux
showMarkers = true;
useLogScale = false; % use log scale for iterations

if( nargin<1 || strcmp(file1,'') )
  fprintf('Must supply at least one file\n');
  return;
end

if nargin<6
    file6='';
end
if nargin<5
  file5='';
end
if nargin<4
  file4='';
end
if nargin<3
  file3='';
end
if nargin<2
  file2='';
end

numberOfCurves = 1;
data = dlmread(file1);

figure('Color',[1 1 1]);
if(useLogScale)
    h = loglog(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
else
    h = semilogy(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
end
if(showMarkers)
    set(h, 'Marker', 'o', 'MarkerSize', 10);
end

if( ~strcmp(file2, '') )
  hold all; % this enables color cycling
  numberOfCurves = numberOfCurves + 1;
  data = dlmread(file2);
  if(useLogScale)
    h = loglog(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
  else
    h = semilogy(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
  end
  if( blackandwhite )
    set(h, 'LineStyle', '--');
  end
  if( showMarkers )
      set(h, 'Marker', '+');
  end
end

if( ~strcmp(file3, '') )
  hold all; % this enables color cycling
  numberOfCurves = numberOfCurves + 1;
  data = dlmread(file3);
  if(useLogScale)
    h = loglog(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
  else
    h = semilogy(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
  end
  if( blackandwhite )
    set(h, 'LineStyle', ':');
  end
  if( showMarkers )
      set(h, 'Marker', '*');
  end
end

if( ~strcmp(file4, '') )
  hold all; % this enables color cycling
  numberOfCurves = numberOfCurves + 1;
  data = dlmread(file4);
  if(useLogScale)
    h = loglog(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
  else
    h = semilogy(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
  end
  if( blackandwhite )
    set(h, 'LineStyle', '-.');
  end
  if( showMarkers )
      set(h, 'Marker', 'x');
  end
end

if( ~strcmp(file5, '') )
  hold all; % this enables color cycling
  numberOfCurves = numberOfCurves + 1;
  data = dlmread(file5);
  if(useLogScale)
    h = loglog(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
  else
    h = semilogy(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
  end
  if( blackandwhite )
    set(h, 'LineWidth', 2);
  end
  if( showMarkers )
      set(h, 'Marker', '^');
  end
end

if( ~strcmp(file6, '') )
  hold all; % this enables color cycling
  numberOfCurves = numberOfCurves + 1;
  data = dlmread(file6);
  if(useLogScale)
    h = loglog(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
  else
    h = semilogy(data(:,1),data(:,2), 'LineWidth', LINEWIDTH);
  end
  if( blackandwhite )
    set(h, 'LineStyle', '--');
    set(h, 'LineWidth', 2);
  end
  if( showMarkers )
      set(h, 'Marker', 's');
  end
end

fontsize = 12;
interpreter = 'tex'; % 'tex', or 'none'

grid on;
%title('Decoding Convergence', 'FontSize', fontsize);
xlabel('Iterations', 'FontSize', fontsize);
ylabel('FER', 'FontSize', fontsize);

if(numberOfCurves < 2)
    h = legend(file1, file2);
    set(h, 'Interpreter', interpreter);
elseif(numberOfCurves ==3)
    h = legend(file1, file2, file3);
  set(h, 'Interpreter', interpreter);
elseif(numberOfCurves == 4)
  h = legend(file1, file2, file3, file4);
  set(h, 'Interpreter', interpreter);
elseif(numberOfCurves == 5)
  h = legend(file1, file2, file3, file4, file5);
  set(h, 'Interpreter', interpreter);
else
    h = legend(file1, file2, file3, file4, file5, file6);
  set(h, 'Interpreter', interpreter);
end

% gca = Get Current Axes
set(gca, 'FontSize', fontsize);