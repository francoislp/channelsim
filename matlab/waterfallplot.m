
function fig_h = waterfallplot(fileList, varargin)

argParser = inputParser;
% define possible optional key-value pairs
addParameter(argParser,'showFER',false);
addParameter(argParser,'showBER',true);
addParameter(argParser,'LineWidth',1.05);
addParameter(argParser,'MarkerSize',10);
addParameter(argParser,'MarkerList',{'p','+','^','*','X','o','>'});
addParameter(argParser,'ColorList',{'b','r','g','k','m','y','k'});
addParameter(argParser,'LegendText',cell(1,1));
addParameter(argParser,'BasePath',''); % base path for resolving filenames
addParameter(argParser,'showBPSK',false);
% option to have x-axis in terms of Es/No instead of Eb/No
addParameter(argParser,'Es_No',false);
addParameter(argParser,'Es_No_rate',0);
addParameter(argParser,'Es_No_log2M',0);
% do the parsing
parse(argParser, varargin{:});
showFER= argParser.Results.showFER;
showBER= argParser.Results.showBER;
lineWidth= argParser.Results.LineWidth;
markerSize= argParser.Results.MarkerSize;
markers= argParser.Results.MarkerList;
curveColors= argParser.Results.ColorList;
LegendText= argParser.Results.LegendText;
if(~isempty(find(strcmp(argParser.UsingDefaults,'LegendText'),1)))
    % arg 'LegendText' was not supplied
    LegendText= cell(length(fileList),1);
    for i=1:length(fileList)
        LegendText{i}= int2str(i);
    end
end
basePath= argParser.Results.BasePath;
showBPSK = argParser.Results.showBPSK;
Es_No= argParser.Results.Es_No;
% check rate and log2M arguments
if(Es_No)
    Es_No_rate= argParser.Results.Es_No_rate;
    Es_No_log2M= argParser.Results.Es_No_log2M;
    if(length(Es_No_rate)<length(fileList))
        error('Length of Es_No_rate array must match file list');
    end
    if(length(Es_No_log2M) < length(fileList))
        error('Length of Es_No_log2M array must match file list');
    end
end

plothandles = [];
fig_h = figure('Color',[1 1 1]);

for i=1:length(fileList)
    curPath= strcat(basePath, fileList{i});
    fid = fopen(curPath);
    if fid<0
        error('Could not open file: %s', curPath);
    end
    try
        M = textscan(fid, '%f %f %f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f', 'CommentStyle', '#', 'ReturnOnError', 0);
        fclose(fid);
    catch
        fprintf('Using new file format for file %d.\n',i);
        fclose(fid);
        fid = fopen(curPath);
        M = textscan(fid, '%f %f %f %*f %*f %*f %*s %*f %*f %*f %*f %*f %*f %*f %*f %*f %*f', 'CommentStyle', '#', 'ReturnOnError', 0);
        fclose(fid);
    end
    
    SNR = M{1};
    BER = M{2};
    FER = M{3};
    
    if Es_No
        if(Es_No_rate(i)==0 || Es_No_log2M(i)==0)
            error('Must provide Es_No_rate and Es_No_log2M');
        end
        SNR= SNR + 10*log10(Es_No_rate(i)*Es_No_log2M(i));
    end
    
    curMarkerSize= markerSize;
    if markers{i}=='.'
        curMarkerSize= markerSize*2;
    elseif markers{i}=='p'
        curMarkerSize= markerSize*1.1;
    end
    
    if( showBER )
        h = semilogy(SNR, BER, '-', 'LineWidth', lineWidth, ...
            'MarkerSize', curMarkerSize,...
            'Color',curveColors{i},'Marker',markers{i});
        plothandles = [plothandles h];
        hold on;
    end
    % dotted line for FER
    if( showFER )
        h = semilogy(SNR, FER, '--', 'LineWidth', lineWidth, ...
            'MarkerSize', curMarkerSize,...
            'Color',curveColors{i},'Marker',markers{i});
        plothandles = [plothandles h];
    end
    if i==1
        hold on;
    end
end


% define Q(a) = P(X > a)
Q = @(a) 1/2 * (1 - erf(a/sqrt(2)));

% BER for BPSK (analytic expression)
if(showBPSK)
    SNR_bpsk = 0:0.1:14.3;
    % or SNR_bpsk = SNR to use the same SNR values as the last curve
    bpskPerr = zeros(1,length(SNR_bpsk));
    
    for i=1:length(SNR_bpsk)
        % convert to non-dB
        SNRlin = 10^(SNR_bpsk(i)/10);
        bpskPerr(i) = Q( sqrt(2*SNRlin) );
    end
    % add an extra point
    %delta = SNR(size(SNR,1)) - SNR(size(SNR,1)-1);
    %SNR(size(SNR,1)+1) = SNR(size(SNR,1))+delta;
    %SNRlin = 10^(SNR(size(SNR,1))/10);
    %bpskPerr(size(SNR,1)) = Q( sqrt(2*SNRlin) );

    h = semilogy(SNR_bpsk, bpskPerr, '-.k', 'LineWidth', 2);
    %hasbehavior(h, 'legend', false);
end

grid on;
fontname = 'Arial';
%title('Error rate vs SNR', 'FontSize', 12, 'FontName', fontname);
if Es_No
    xlabel('Es/No [dB]', 'FontSize', 14, 'FontName', fontname);
else
    xlabel('Eb/No [dB]', 'FontSize', 14, 'FontName', fontname);
end
if( showBER && ~showFER )
    ylabel('BER', 'FontSize', 14, 'FontName', fontname);
elseif( ~showBER && showFER )
    ylabel('FER', 'FontSize', 14, 'FontName', fontname);
else
    ylabel('BER -- FER', 'FontSize', 14, 'FontName', fontname);
end
    
% gca = Get Current Axes
set(gca, 'FontSize', 16, 'FontName', fontname);

% add a legend
% only one legend entry for both BER & FER curves:
if( showBER && showFER )
  for i=2:2:length(plothandles)
    hasbehavior(plothandles(i), 'legend', false);
  end
end
legend(LegendText);


