% This function writes the binary matrix "H" to a file in "A-List" format.
% Author: Francois Leduc-Primeau (francoislp@gmail.com)

function writeAList(H, outfile)

out_fid = fopen(outfile, 'w');
if(out_fid < 0)
    fprintf('Error: Cannot open output file.\n');
    return;
end

% write matrix size
[matrixM, matrixN] = size(H);
fprintf(out_fid, '%d  %d\n', matrixN, matrixM);

% make a list of the number of non-zero elements in each column
colsize_list = zeros(matrixN, 1);
for i=1:matrixN
  colsize_list(i) = sum(H(:,i)); % since non-zero elements are always 1
end

% make a list of the number of non-zero elements in each row
rowsize_list = zeros(matrixM, 1);
for i=1:matrixM
  rowsize_list(i) = sum(H(i,:)); % since non-zero elements are always 1
end

% write maximum numbers of non-zero elements
fprintf(out_fid, '%d  %d\n', max(colsize_list), max(rowsize_list));

% write number of non-zero elements in each col
fprintf(out_fid, '%d', colsize_list(1));
for i=2:matrixN
  fprintf(out_fid, ' %d', colsize_list(i));
end
fprintf(out_fid, '\n');

% write number of non-zero elements in each row
fprintf(out_fid, '%d', rowsize_list(1));
for i=2:matrixM
  fprintf(out_fid, ' %d', rowsize_list(i));
end
fprintf(out_fid, '\n');

% write: for each column, index (one based) of the non-zero elements
for i=1:matrixN
  for j=1:matrixM
    if( H(j,i) ~= 0 )
      fprintf(out_fid, '%d ', j);
%      fprintf(out_fid, '%d ', j-1); % zero-based
    end
  end
  fprintf(out_fid, '\n');
end

% write: for each row, index (one based) of the non-zero elements
for i=1:matrixM
  for j=1:matrixN
    if( H(i,j) ~= 0 )
      fprintf(out_fid, '%d ', j);
%      fprintf(out_fid, '%d ', j-1); % zero-based
    end
  end
  fprintf(out_fid, '\n');
end


fclose(out_fid);

end
