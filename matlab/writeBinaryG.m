% This function writes a matrix to file in the binary format expected by channelsim 
% for the G matrix.
% Author: Francois Leduc-Primeau

function writeBinaryG(G, outfile, systematic_flag, n)

if nargin == 2
  systematic_flag = false;
  n= size(G, 2);
else
  if nargin ~= 4
    fprintf('Error: Number of arguments must be 2 or 4\n');
    return;
  end
end

% Try to open the output file
outfile_fid = fopen(outfile, 'w');
if(outfile_fid < 0)
    fprintf('Error: Cannot open output file.\n');
    return;
end

% write G to the output file

% write format code
formatcode = 1;
count = fwrite(outfile_fid, formatcode, 'int32');
if(count~=1)
    fprintf('Error writing format code\n');
    return;
end

% write size "m" of the matrix (the code "k")
count = fwrite(outfile_fid, size(G,1), 'int32');
if(count~=1)
    fprintf('Error writing size M\n');
    return;
end

% write size of the code "n"
count = fwrite(outfile_fid, n, 'int32');
if(count~=1)
    fprintf('Error writing size N\n');
    return;
end

% note: the parity-bit part of G has been moved after the I part

if systematic_flag
  % add the identity matrix of size such that G will have n columns
  I_size = n - size(G,2);
  curColumn = zeros(size(G,1), 1);
  for i=1:I_size
    if i>1
      curColumn(i-1) = 0;
    end
    curColumn(i) = 1;
    count = fwrite(outfile_fid, curColumn, 'int8');
    if(count~=size(G,1))
        fprintf('Error writing a column\n');
        return;
    end
  end
%   for i=1:I_size
%     if i>1
%       curColumn(I_size+2-i) = 0;
%     end
%     curColumn(I_size+1-i) = 1;
%     count = fwrite(outfile_fid, curColumn, 'int8');
%     if(count~=size(G,1))
%         fprintf('Error writing a column\n');
%         return;
%     end
%   end
end % end if systematic_flag

% write matrix data by column
for i=1:size(G,2)
    count = fwrite(outfile_fid, G(:,i), 'int8');
    if(count~=size(G,1))
        fprintf('Error writing a column\n');
        return;
    end
end

% close the output file
fclose(outfile_fid);

end
