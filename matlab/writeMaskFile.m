% Writes a binary vector to a file, in the format expected by C++ class
% MaskParser.
function writeMaskFile(pattern, filePath)

out_fid = fopen(filePath, 'w');
if(out_fid < 0)
    fprintf('Error: Cannot open output file.\n');
    return;
end

% Write puncturing pattern in reverse order
for i=length(pattern):-1:1
    if pattern(i)<0 || pattern(i)>1
        error('Invalid pattern');
    end
    fprintf(out_fid, '%d ', pattern(i));
end
fprintf(out_fid, '\n');

% Write pattern weight
fprintf(out_fid, '%d\n', length(pattern)-sum(pattern));

fclose(out_fid);