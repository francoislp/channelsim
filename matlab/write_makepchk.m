% writes a matrix as a script that invokes make-pchk to create a pchk file

function write_makepchk(H, k, basename)

script_filename = strcat(basename, '.sh');
pchk_filename = strcat(basename, '.pchk');
data_filename = strcat(basename, '_data');

out_fid = fopen(script_filename, 'w');
if(out_fid < 0)
    fprintf('Error opening output file\n');
    return;
end

out_fid_data = fopen(data_filename, 'w');
if(out_fid_data < 0)
    fprintf('Error opening data output file\n');
    return;
end

[m n] = size(H);

fprintf(out_fid, '#!/bin/bash\n');
fprintf(out_fid, 'make-pchk %s %d %d %s\n', pchk_filename, k, n, data_filename);
fclose(out_fid);

% write the data file with the coordinates of all 1-elements
for i=1:m
    for j=1:n
        if(H(i,j) ~= 0)
            fprintf(out_fid_data, '%d:%d\n', i-1, j-1);
        end
    end
end
fclose(out_fid_data);

end