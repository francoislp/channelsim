#!/usr/bin/perl -w

# This scripts converts a (binary) A-List matrix description to the
# QA-List format.
# Note that interpretations differ as to whether a "0" or a "1" needs
# to be appended to each entry.

$#ARGV==1 || die("Usage: A2QA-List.pl <input file> <output file>\n");

$infile = $ARGV[0];
$outfile = $ARGV[1];

open(INFILE, $infile) || die("Could not open file $infile\n");
open(OUTFILE, ">".$outfile);

# copy any comment at the top of the file
$line = <INFILE>;
while( defined($line) && $line =~ /^\s*#/ ) {
  $line =~ s/\r//; # remove windows extra EOL character
  print OUTFILE $line;
  $line = <INFILE>;
}

# the header is mostly the same in both files
# matrix size
# (has been done already: $line = <INFILE>; )
defined($line) || die("Invalid input file");
chomp($line);
# remove any windows newline extra character
$line =~ s/\r//;
print OUTFILE $line . " 2\n"; # add field order = 2

# max column and row weight
# row weight list
# column weight list
for( $i=0; $i<3; $i++ ) {
  $line = <INFILE>;
  defined($line) || die("Invalid input file");
  $line =~ s/\r//; # remove windows extra EOL character
  print OUTFILE $line;
}

# matrix data
while( defined($line = <INFILE>) ) {
  $line =~ s/\r//; # remove windows extra EOL character
  # append "1" to each element
  $line =~ s/(\d+)/$1 1/g;
  print OUTFILE $line;
}

close(INFILE);
close(OUTFILE);
