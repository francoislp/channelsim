#!/usr/bin/perl -w

# This script removes comment lines (lines starting with "#") in a file.
# The comment-less file is written to the output file.

$#ARGV==1 || die("Usage: AList_strip.pl <input file> <output file>\n");

$infile = $ARGV[0];
$outfile = $ARGV[1];

open(INFILE, $infile) || die("Could not open file $infile\n");
open(OUTFILE, ">".$outfile);

while( defined($line = <INFILE>) ) {
  if( $line !~ /^\s*#/ ) {
    print OUTFILE $line;
  }
}

close(INFILE);
close(OUTFILE);
