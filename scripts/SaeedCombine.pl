#!/usr/bin/perl -w

# This script combines many single-line result files (all having the
# same No) produced by prof. Gross / Saeed's decoder.

$#ARGV==1 || 
  die("Usage: SaeedCombine.pl <base file name> <number of files>\n");

# hard-coded parameters:
$codesize = 1024;

$basename = $ARGV[0];
$size = $ARGV[1];

$bitsum = 0;
$errorsum = 0;

for($i=1; $i <= $size; $i++) {
  $filename = $basename . $i . ".txt";
  open(FILE, $filename) || die("Could not open $filename\n");

  # skip to last line
  while( defined( $line = <FILE> ) ) {
    $lastLine = $line;
  }

  # retrieve number of words and bit errors
  if( $lastLine !~ /^\s*[\d\.]+\s+([\d\.]+)\s+[\d\.]+\s+[\d\.]+\s+([\d\.]+)/ )
    {
      die("Invalid line: $lastLine in file $filename\n");
    }
  $words = $1;
  $biterr = $2;

  $bitsum+= $words * $codesize;
  $errorsum+= $biterr;
}

# Average BER
print "Average BER = ". ($errorsum / $bitsum) . "\n";
