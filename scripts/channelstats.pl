#!/usr/bin/perl -w

# TODO: proof read and test ****************

use strict;

$#ARGV==0 || die("Usage channelstats.pl <channel file>\n");

my $infile = $ARGV[0];

open(INFILE, $infile) || die("Could not open file $infile\n");

my $line;
my $linecount = 0;
while( defined($line = <INFILE>) && $linecount<10 ) { #debug
  my @chvalList;
  while( $line =~ /([-\d\.]+)\s+/g ) {
    push(@chvalList, $1);
  }
  # assuming a mean of 1, compute the variance
  my $var = 0;
  for(my $i=0; $i<scalar(@chvalList); $i++) {
    $var+= ($chvalList[$i] - 1)^2;
  }
  $var/= (scalar(@chvalList) - 1);

  $linecount++;
  print "variance = ".$var."\n";
}

close(INFILE);
