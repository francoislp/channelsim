#!/usr/bin/perl
#
# This script creates (or overwrites) a file named COMMIT_ID.h and
# adds a define constant containing the SHA1 ID of the latest git commit.
#

die("Usage: create_git_header.pl <header filepath>\n") if $#ARGV<0;
$definename="#define GIT_COMMIT_SHA1";
$id=`git log -1 --pretty=format:%H`;
open(OUTFILE, ">$ARGV[0]") || die("Could not open output file.\n");
print OUTFILE $definename." \"".$id."\"\n";
print "Don't forget to run make!\n";
