#!/usr/bin/perl -w

# This scripts makes a list of hard-decision errors in a received
# values file (compared to the corresponding transmitted codewords).

$#ARGV==1 || die("Usage: cwCheck.pl <tx file> <rx file>\n");

open(TXFILE, $ARGV[0]) || die("Could not open file $ARGV[0]\n");
open(RXFILE, $ARGV[1]) || die("Could not open file $ARGV[1]\n");


while( defined($rxline = <RXFILE>) ) {

  my @hdlist;
  my @txlist;

  while( $rxline =~ /([-\d\.]+)/g ) {
    if($1 >= 0) {
      $hd = 1;
    } else {
      $hd = 0;
    }
    push(@hdlist, $hd);
  }

  # transmitted values
  defined($txline = <TXFILE>) || die("Ay caramba!");
  while( $txline =~ /([01])\s+/g ) {
    push(@txlist, $1);
  }

  print "Size of HD list is ". scalar(@hdlist)."\n";  # debug
  print "Size of TX list is ". scalar(@txlist). "\n"; # debug

  # identify bit errors
  my @errlist;
  for my $i (0 .. $#hdlist) {
    if( $hdlist[$i] != $txlist[$i] ) {
      push(@errlist, $i);
    }
  }

  # print number of HD errors
  $nberrors = $#errlist+1;
  print "$nberrors hard-decision errors\n";

  # print error indexes
  print "Error indexes: ";
  foreach (@errlist) {
    print $_.", ";
  }
  print "\n";
  print "Error indexes (reversed, *my bit order*): ";
  foreach (@errlist) {
    $_ = $#hdlist - $_; # (this updates the list)
  }
  @errlist = sort {$a <=> $b} @errlist;
  foreach (@errlist) { print $_ .", "; }
  print "\n";

}
