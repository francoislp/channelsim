#!/usr/bin/perl -w

# WILL NEED TO RE-STRUCTURE THE CODE

use strict;

my $infile = $ARGV[0];
my $outfile = $ARGV[1];

my $No = "0.427905";

open(INFILE, $infile) || die("Could not open file $infile\n");
open(OUTFILE, ">".$outfile) || die("Could not open file $outfile\n");

my $line;
while( defined($line = <INFILE>) ) {

  # find a channel value line
  if( $line !~ /^\s*#/ ) {
    my $chvalLine = $line;

    # get the next line
    $line = <INFILE>;
    defined($line) || die("No info after channel value line !");
    # check the value of No
    if( $line =~ /\(No=([\d.]+)\)/ ) {
      if( $1 eq $No ) {
        #if the value of No matches, copy the channel value line
        print OUTFILE $chvalLine;
        print OUTFILE $line;
      }

      # copy all subsequent "comment" lines
      $line = <INFILE>;
#blablabla
    } else {
      die("Invalid line after channel value line");
    }
  }
}
