#!/usr/bin/perl -w

# This script duplicates the first line of a text file.

$#ARGV==0 || die("Usage: dupline1.pl <file>\n");

$file = $ARGV[0];

open(INFILE, $file) || die("Could not open file $file\n");

# first line
$line = <INFILE>;
defined($line) || die("File is empty !\n");
$content = $line.$line; # duplicated !

while( defined($line = <INFILE>) ) {
  $content.= $line;
}

close(INFILE);

# overwrite the input file
open(OUTFILE, ">".$file);
print OUTFILE $content;
close(OUTFILE);
