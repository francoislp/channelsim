#!/usr/bin/perl -w

$#ARGV==0 || die ("Usage: errstats.pl <cw log file>\n");

open(CWFILE, $ARGV[0]) || die("Could not open file $ARGV[0]\n");

my @curlist;
my @prevlist;

my $i=0;
while( defined($line = <CWFILE>) ) {

  # copy cur into previous
  @prevlist = ();
  foreach (@curlist) { push(@prevlist, $_); }
  # clear current
  @curlist = ();

  while( $line =~ /(\d+),/g ) {
    push(@curlist, $1);
  }

  # current number of bit errors
  print "i=".$i.": ";
  print $#curlist+1 . " errors";

  # number of bits that were corrected
  my $count = 0;
  foreach my $oldbit (@prevlist) {
    foreach my $newbit (@curlist) {
      $count++ if($newbit == $oldbit);
    }
  }
  my $corrected = $#prevlist + 1 - $count;
  print ", $corrected corrected";

  # number of bits that became wrong
  $count = 0;
  foreach my $newbit (@curlist) {
    foreach my $oldbit (@prevlist) {
      $count++ if($newbit == $oldbit);
    }
  }
  my $wrong = $#curlist + 1 - $count;
  print ", $wrong new errors";
  print "\n";

  $i++;
}
