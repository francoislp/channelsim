#!/usr/bin/perl -w

# Extracts the lines starting with "#BERLIST:" from a file and copies
# them (without the initial marker) into a second file.

use strict;

die("Usage: extractBERresult.pl <filepath> <outputpath>\n") if $#ARGV<1;

# parse arguments
my $inputfilepath = $ARGV[0];
my $outputfilepath = $ARGV[1];

open(INFILE, $inputfilepath) || die("Cannot open input file.\n");
open(OUTFILE, ">".$outputfilepath) || die("Cannot open output file.\n");

my $line;
my $prevLine;
while( defined($line = <INFILE>) ) {
  if( $line =~ /^#BERLIST: (.*)$/ ) {
    my $berdata = $1;
    # Get the SNR from the previous line
    my $snr;
    if( defined($prevLine) && $prevLine =~ /^\s*(\d+[\.]?\d*)/ ) {
      $snr = $1;
    } else {
      die("Invalid file format");
    }
    print OUTFILE $snr." ".$berdata."\n";
  }
  $prevLine = $line;
}

close(INFILE);
close(OUTFILE);
