#!/usr/bin/perl -w

use strict;

$#ARGV>=1 || die("Usage: logparser.pl <log file> <output file>\n");

my $infile = $ARGV[0];
my $outfile = $ARGV[1];

open(INFILE, $infile) || die("Could not open file $infile\n");
open(OUTFILE, ">".$outfile) || die("Could not open file $outfile\n");

# PROGRAM PARAMETERS
# value of No of interest
my $No = "0.416019";

# the mode of a frame error is defined in terms of the avg no-update count
my $MODE1_MIN_NU = 900;
# degree of all VNs
my $VNDEG = 6;
# degree of all CNs
my $CNDEG = 32;

# statistics variables
# occurrences of mode 1 and 2 frame errors
my $mode1Cnt=0;
my $mode2Cnt=0;
# average no-update counts for modes 1 and 2
my $mode1AvgNU=0;
my $mode2AvgNU=0;
# total number of bit errors for each mode
my $mode1BitCnt=0;
my $mode2BitCnt=0;

# bit error count histogram
my %biterrhist;

my %NUvsbitcnt;
my $totalNU=0;

# N-U count histogram for good bits
my @goodBitsHist;
# for bad bits
my @badBitsHist;

my $line;
while( defined($line = <INFILE>) ) {

  # find a channel value line
  if( $line !~ /^\s*#/ ) {
    my $chvalLine = $line;

    # get the next line
    $line = <INFILE>;
    defined($line) || die("No info after channel value line !");
    # check the value of No
    if( $line =~ /\(No=([\d.]+)\)/ ) {
      if( $1 eq $No ) {

        # get bit errors indexes
        my @errIndexes;
        if( $line =~ /Err indexes.*:\s*([\d,\s]+)\s*$/ ) {
          my $list = $1;
          # split the list into tokens
          while( $list =~ /(\d+)\s*,/g ) {
            push(@errIndexes, $1);
          }
          @errIndexes = sort {$a <=> $b} @errIndexes;
        } else {
          die("Expecting \"Err indexes\" line");
        }

        # get the corresponding channel values
        my @chValList = getChValues($chvalLine, \@errIndexes);

        # get the output counter values
        my @outCounters;
        $line = <INFILE>;
        defined($line) || die("Expecting \"Reliabilities\" line");
        if( $line =~ /Reliabilities:\s*([-\d,\s]+)\s*$/ ) {
          my $list = $1;
          # split into tokens
          while( $list =~ /(-?\d+)\s*,/g ) {
            push(@outCounters, $1);
          }
        } else {
          die("Expecting \"Reliabilities\" line");
        }

        $line = <INFILE>;
        defined($line) || die("Expecting \"no-update\" line");
        if( $line =~ /No update counts:\s*([\d,\s]+)\s*$/ ) {
          my $list = $1;
      
          # split the list into tokens
          my @tokens;
          while( $list =~ /(\d+)\s*,/g ) {
            push(@tokens, $1);
          }

          my $biterrcnt = scalar(@tokens);
          $biterrcnt > 0 || die("No bit errors in bad frame ?");

          # compute average no-update count for that frame
          my $nuAvg=0;
          for(my $i=0; $i < $biterrcnt; $i++) {
            $nuAvg+= $tokens[$i];
            $totalNU+= $tokens[$i];
          }
          $nuAvg/= $biterrcnt;
          
          if( $nuAvg >= $MODE1_MIN_NU ) {
            $mode1Cnt++;
            $mode1BitCnt+= $biterrcnt;
            for(my $i=0; $i < $biterrcnt; $i++) {
              $mode1AvgNU+= $tokens[$i];
            }
          } else {
            $mode2Cnt++;
            $mode2BitCnt+= $biterrcnt;
            for(my $i=0; $i < $biterrcnt; $i++) {
              $mode2AvgNU+= $tokens[$i];
            }
          }

          # bit errors histogram
          $biterrhist{$biterrcnt} = 0 if !defined($biterrhist{$biterrcnt});
          $biterrhist{$biterrcnt}++;
          
          # NU cycles vs bit err cnt
          $NUvsbitcnt{$biterrcnt} = 0 if !defined($NUvsbitcnt{$biterrcnt});
          $NUvsbitcnt{$biterrcnt}+= $nuAvg;
        } else {
          die("Expecting \"no-update\" line");
        }

        # print the channel values of error bits along with the sent value
        print OUTFILE "Channel values for error bits:\n";
        for(my $j=0; $j<scalar(@errIndexes); $j++) {
          print OUTFILE ", " if($j!=0);
          print OUTFILE $errIndexes[$j]." => ";
          print OUTFILE $chValList[$j]." ";
          #TODO: should log the sent bits and use that instead
          my $sentval=0;
          $sentval=1 if($outCounters[$j] <= 0); # (counter has wrong value)
          print OUTFILE "(".$sentval." sent)";
        }
        print OUTFILE "\n";

        # get the neighbors lists for each error bit
        for(my $j=0; $j<=$#errIndexes; $j++) {
          $line = <INFILE>;
          defined($line) || die("Expecting a neighbor line");

          print OUTFILE "Neighbors of error bit $j:\n";

          if( $line =~ /Neighbors $j.*:\s*([\d,\s]+)\s*$/ ) {
            my $neighText = $1;

            # group the neighbors into parity check groups
            for(my $k=0; $k<$VNDEG; $k++) {
              my @curList;
              for(my $l=0; $l<$CNDEG; $l++) {
                if( $neighText =~ /(\d+)\s*,/g ) {
                  push(@curList, $1);
                } else {
                  die("Error in neighbor list");
                }
              }
              
              # get the channel values
              print OUTFILE "Parity-check group $k:\n";
              printChValues($chvalLine, \@curList);
            }


          } else {
            die("Invalid line");
          }
        }

      }
    } else {
      die("Invalid line after channel value line");
    }
  } # end "frame error" block

  # also look for N-U histogram lines to sum them up
  elsif( $line =~ /^\s*#.*N-U hist good bits/ ) {
    # check the value of No
    if( $line =~ /\(No=([\d.]+)\)/ ) {
      if( $1 eq $No ) {
        # read the histogram
        my @curHist;
        if( $line =~ /bits\s*:\s*([\d,\s]+)\s*$/ ) {
          my $list = $1;
          # split the list into tokens
          while( $list =~ /(\d+)\s*(,|$)/g ) {
            push(@curHist, $1);
          }
        } else {
          die("Invalid line");
        }

        # add up the values
        for(my $j=0; $j<scalar(@curHist); $j++) {
          $goodBitsHist[$j]+= $curHist[$j];
        }
      }
    } else {
      die("Invalid \"good bits\" line: no No value");
    }
  }
  elsif( $line =~ /^\s*#.*N-U hist bad bits/ ) {
    # (same code as previous block)
    # check the value of No
    if( $line =~ /\(No=([\d.]+)\)/ ) {
      if( $1 eq $No ) {
        # read the histogram
        my @curHist;
        if( $line =~ /bits\s*:\s*([\d,\s]+)\s*$/ ) {
          my $list = $1;
          # split the list into tokens
          while( $list =~ /(\d+)\s*(,|$)/g ) {
            push(@curHist, $1);
          }
        } else {
          die("Invalid line");
        }

        # add up the values
        for(my $j=0; $j<scalar(@curHist); $j++) {
          $badBitsHist[$j]+= $curHist[$j];
        }
      }
    } else {
      die("Invalid \"bad bits\" line: no No value");
    }
  }

}


# complete average calculation
# if( $mode1BitCnt != 0 ) {
#   $mode1AvgNU/= $mode1BitCnt;
# }
# if( $mode2BitCnt != 0 ) {
#   $mode2AvgNU/= $mode2BitCnt;
# }

# print the results
# print "--- Mode 1 ---\n";
# print "   frame err count                : ".$mode1Cnt."\n";
# print "   average bit error count        : ".($mode1BitCnt/$mode1Cnt)."\n";
# print "   average N-U cycles for err bits: ".$mode1AvgNU."\n";

# print "--- Mode 2 ---\n";
# print "   frame err count                : ".$mode2Cnt."\n";
# print "   average bit error count        : ".($mode2BitCnt/$mode2Cnt)."\n";
# print "   average N-U cycles for err bits: ".$mode2AvgNU."\n";

# global average NU cycles among error bits
print "Average NU cycles for error bits: ";
print ($totalNU / ($mode1BitCnt+$mode2BitCnt));
print "\n";

# print the bit errors histogram
print "\n";
print "- Bit error count histogram -\n";
for my $key (sort {$a <=> $b} (keys %biterrhist)) {
  my $errcnt = $biterrhist{$key};
  my $nuavg = $NUvsbitcnt{$key} / $errcnt;
  print $key." => ".$errcnt."  (NU avg = ".$nuavg.")\n";
}

# print the N-U count histograms
print "\n";
print "Occurrences of good bits with the given N-U count:\n";
for(my $i=0; $i<scalar(@goodBitsHist); $i++) {
  print $i." => ".$goodBitsHist[$i]."\n";
}
print "\n";
print "Occurrences of bad bits with the given N-U count:\n";
for(my $i=0; $i<scalar(@badBitsHist); $i++) {
  print $i." => ".$badBitsHist[$i]."\n";
}

# parses a channel value line and prints the values corresponding to
# certain indexes
# Arg 1: the channel value line
# Arg 2: ref to the list of indexes
sub printChValues {
  my $chvalLine = shift;
  my $indexListRef = shift;

  my $index=0;
  my $i=0;
  while( $i<scalar(@$indexListRef) && $chvalLine =~ /([-\d\.]+)\s+/g ) {
    if( $index == $$indexListRef[$i] ) {
      # this channel value corresponds to an error bit
      # => print it out
      print OUTFILE $$indexListRef[$i]." => ".$1.", ";
      $i++;
    }
    $index++;
  }
  print OUTFILE "\n";
}

# same as printChValues, but returns the values in an array instead or
# printing them
sub getChValues {
  my $chvalLine = shift;
  my $indexListRef = shift;
  my @list;

  my $index=0;
  my $i=0;
  while( $i<scalar(@$indexListRef) && $chvalLine =~ /([-\d\.]+)\s+/g ) {
    if( $index == $$indexListRef[$i] ) {
      # this channel value corresponds to an error bit
      # => add it to the list
      push(@list, $1);
      $i++;
    }
    $index++;
  }

  return @list;
}
