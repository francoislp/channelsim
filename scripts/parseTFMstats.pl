#!/usr/bin/perl -w

use strict;

$#ARGV==0 || die("Usage: parseTFMstats.pl <log file>\n");

my $infile = $ARGV[0];

open(INFILE, $infile) || die("Could not open file $infile\n");

my $line;
my $satcount = 0;
my $cyclecount = 0;
while( defined($line = <INFILE>) ) {
  if( $line =~ /\s*\((\d+)\s*,\s*(\d+)\s*\)/ ) {
    $satcount+= $1;
    $cyclecount+= $2;
  }
}

close(INFILE);

# display result
print "Total # of saturation cycles: ". $satcount."\n";
print "Total # of cycles: ".$cyclecount."\n";
print "Average time in saturation: ".($satcount/$cyclecount)."\n";
