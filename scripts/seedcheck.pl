#!/usr/bin/perl -w

use strict;

die("Usage: seedcheck.pl <group size> [directories]\n") if $#ARGV<0;
# if several directories are specified, the script will look for
# <group size> log files in each directory. If no directory is
# specified, the script will use the current directory.

my $groupsize = $ARGV[0];
my @directoryList;
if($#ARGV>0) {
  for(my $i=1; $i <= $#ARGV; $i++) {
    push(@directoryList, $ARGV[$i]);
  }
}
else {
  my $dir = `pwd`;
  chomp($dir);
  push(@directoryList, $dir);
}

my @seedlist;

foreach my $curDir (@directoryList) {
  for(my $i=1; $i < $groupsize; $i++) {
    my $filename = $curDir."/logM".$i;
    my $openok = 0;
    $openok = 1 if open(INFILE, $filename);
    
    if($openok) {
      my $line;
      my $found=0;
      while( $found==0 && defined($line = <INFILE>) ) {
        if( $line =~ /Using seed = (\d+)/ ) {
          $found = 1;
          push(@seedlist, $1);
        }
      }
      
      close(INFILE);
    }
    else {
      warn("Could not open file $filename\n");
    }
  }
}

# sort the seed list
@seedlist = sort {$a <=> $b} @seedlist;

# check if each seed is different from the previous one
my $ok=1;
for(my $i=1; $i <= $#seedlist; $i++) {
  if( $seedlist[$i] == $seedlist[$i-1] ) {
    print "Equal seeds ! (".$seedlist[$i].")\n";
    $ok = 0;
  }
}

if( $ok==1 ) {
  print "All ".scalar(@seedlist)." seeds are different\n";
}
