#!/usr/bin/perl -w

# This scripts outputs the total number of trials performed by each
# machine as reported in the log files.

use strict;

my $index=1; #note: CPU #0 is the head node
my $filename = "logM".$index;
my @trialList;

while( -e $filename )  {

  open(INFILE, $filename) || die("Could not open $filename\n");

  my $line;
  $trialList[$index] = 0;
  while( defined($line = <INFILE>) ) {
    if( $line =~ /,\s*trials=(\d+)/ ) {
      $trialList[$index]+= $1;
    }
  }

  $index++;
  $filename = "logM".$index;

  close(INFILE);
}

# note: the first computation CPU has index 1, and the number of CPUs
# is $#trialList.

# calculate the average
my $avg=0;
for(my $i=1; $i<=$#trialList; $i++) {
  $avg+= $trialList[$i];
}
$avg/= $#trialList;
print "Average trial count / CPU: $avg \n";

# calculate the std dev
my $stddev=0;
for(my $i=1; $i<=$#trialList; $i++) {
  $stddev+= abs($trialList[$i] - $avg);
}
$stddev/= $#trialList;
print "Std dev: $stddev \n";

# print result
my $cur;
print "\n--- Trial count by CPU ---\n";
for(my $i=1; $i<=$#trialList; $i++) {
  print "CPU $i: $trialList[$i] \n";
}
