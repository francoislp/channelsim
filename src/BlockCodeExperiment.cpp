//Author: Francois Leduc-Primeau

#include "BlockCodeExperiment.hpp"
#include "util/Exception.hpp"
#include "Logging.hpp"
#include "source/ZeroSource.hpp"
#include "source/ExtendedSource.hpp"
#include "modulation/BPSKMod.hpp"
#include "modulation/BPSKDemod.hpp"
#include "modulation/QPSKMod.hpp"
#include "modulation/QPSKDemod.hpp"
#include "channel/AdditiveChannel.hpp"
#include "channel/CAdditiveChannel.hpp"

#include <iostream>
#include <sstream>
#include <vector>
#include <cmath>
#include <string>

#include "compileflags.h"

using std::endl;
using std::string;
using std::stringstream;
using std::vector;

BlockCodeExperiment::BlockCodeExperiment(int n, int k, int Hm,
                                         BitBlock* G, BitBlock* H, Config& conf)
  : m_n(n),
    m_k(k)
{
  int srcType = conf.getSrcType();

  string errorType= conf.getPropertyByName<string>("error_type");
  if(errorType== "ber_info") m_doCWBitErrors= false;
  else if(errorType=="ber_cw") m_doCWBitErrors= true;
  else throw Exception("BlockCodeExperiment", "Invalid error count type");

  string modTypeStr= conf.getPropertyByName<string>("mod_type");

  // source
  if( srcType == SRCTYPE_EXTENDED ) m_psrc = new ExtendedSource(m_k);
  else if( srcType == SRCTYPE_ZERO ) m_psrc = new ZeroSource(m_k);
  else throw Exception("BlockCodeExperiment", "Invalid source type");
  // coder
  m_pcoder = new BlockCoder(m_psrc, m_k, m_n, Hm, G, H, true);

  m_pCh= 0;
  m_pChCplx= 0;
  
  if(modTypeStr=="bpsk" || modTypeStr=="BPSK") {
	  // modulator
	  BPSKMod* mod= new BPSKMod(m_pcoder);
	  m_pmod = mod;
	  // channel
	  m_pCh = new AdditiveChannel(m_pmod, &m_ngen, m_n, conf);
	  // demodulator
	  m_pdemod = new BPSKDemod(mod, m_pCh, 0);
  } else if(modTypeStr=="qpsk" || modTypeStr=="QPSK") {
	  // modulator
	  m_pmod= new QPSKMod(m_pcoder);
	  // channel
	  if(m_n/2*2 != m_n) {
		  throw Exception("BlockCodeExperiment", "block size must be even");
	  }
	  m_pChCplx= new CAdditiveChannel(m_pmod, &m_ngen, m_n/2, conf);
	  // demodulator
	  m_pdemod= new QPSKDemod(m_pChCplx);
  } else {
	  throw Exception("BlockCodeExperiment", "Invalid modulation type");
  }

  // decoder
  m_pdecoder = new BlockDecoder(m_pcoder, m_pdemod, conf, m_doCWBitErrors);
  // noise: (no arg constructor)

  // measure decoder statistics
  m_pstats = new DecodingStats(m_pcoder, m_pmod->getPolarity(), m_pdemod,
                               m_pdecoder);
}

BlockCodeExperiment::~BlockCodeExperiment() {
  delete m_psrc;
  delete m_pcoder;
  delete m_pmod;
  delete m_pdemod;
  delete m_pdecoder;
  if(m_pCh!=0) delete m_pCh;
  if(m_pChCplx!=0) delete m_pChCplx;
}

void BlockCodeExperiment::run(Task& task) {

  double    No         = task.No();
  uint      minTrials  = task.minTrials();
  u64_t     maxTrials  = task.maxTrials();
  uint      minBErrors = task.minBErrors();
  uint      minWErrors = task.minWErrors();

  uint      nbtrials   = 0; // number of trials per iteration
  u64_t     trialcnt   = 0;
  uint      berrcnt    = 0; // number of bit errors encountered
  uint      werrcnt    = 0; // number of word errors encountered
  uint      MLerrcnt   = 0; // number of ML word errors
  uint      werrUndetCnt=0; // number of undetected word errors

  // number of trials done at once (defines amount of memory used)
  if(minTrials*m_n < BLOCKSIZELIMIT && minTrials>0) nbtrials = minTrials;
  else if(m_n > BLOCKSIZELIMIT) nbtrials = 1;
  else nbtrials = BLOCKSIZELIMIT / m_n;

  // we are given No in terms of the information bits
  // convert it to the equivalent No over all coded bits
  double scaledNo = No*m_n/m_k; // No/R, where R=k/n is the code rate

  //debug: record the number of errors in each codeword to do stats
  //std::vector<int> errorLog;

  try {
    // initialization
    m_pdemod->setNo(scaledNo);
    if(m_pdemod->hasNext()) {
      throw Exception("BlockCodeExperiment::run",
                          "Assertion failed: m_pdemod should be empty");
    }
    // noise
    // variance is No/2 (using the scaled No)
    m_ngen.setGaussian(0, scaledNo/2/m_pmod->getNbBitsPerSymbol());
    // decoder
    // special initialization for a stochastic decoder
    if( task.isStochastic() ) {
      m_pdecoder->setStochNDSCst(task.NDSScaling());
    }
    
    // keep doing trials until we reach the desired number of errors
    while((trialcnt < minTrials) || 
          (berrcnt < minBErrors) ||
          (werrcnt < minWErrors)) {
      
      // generate source block
      if( m_psrc->genBlock(nbtrials) != 1 ) {
        // mem alloc failed
        std::ostringstream ss;
        ss << "Source mem alloc failed for size=" << nbtrials;
        throw Exception(ss.str());
      }

      // compare the demodulated-decoded signal with the source
      // note: decoder is emptied in the process
      vector<uint> errors; // bit [0], word [1] and ML [2] error counts
      u64_t trialCheck = m_pstats->processAll(errors);
      berrcnt+=  errors[0];
      werrcnt+=  errors[1];
      MLerrcnt+= errors[2];
      werrUndetCnt+= errors.at(3);

      // assertions
      if(trialCheck != nbtrials)
        throw Exception("BlockCodeExperiment::run", "Assertion failed: trial count mismatch");
      if(errors[1] > nbtrials)
        throw Exception("BlockCodeExperiment::run", "Assertion failed: more symbol errors than symbols.");
      if(m_pdemod->hasNext()) {
        throw Exception("BlockCodeExperiment::run",
                            "Assertion failed: m_demod should be empty #2");
      }
      
      trialcnt+= nbtrials;

      if( (maxTrials>0) && (trialcnt > maxTrials) ) {
        stringstream ss;
        ss << "Trial count reached max value (" << maxTrials << ")";
        throw MaxTrialsException(ss.str());
      }

    } //end while
    
  } catch(Exception& e) {
    std::ofstream* pLog = Logging::getstream();
    *pLog << e << endl;
    std::ostringstream ss;
    ss << "Experiment failed for noise power " << No;
    throw Exception(ss.str());
  }

  std::ofstream* pLog = Logging::getstream();
  *pLog << "Finished running noise power " << No;
  *pLog << " (scaled="<< scaledNo << ")" << endl;
  *pLog << "    bit";
  *pLog << " errors="<<berrcnt;
  *pLog << ", frame errors="<<werrcnt;
  *pLog <<", trials="<<trialcnt << endl;    

  // set the results
  if(werrUndetCnt > werrcnt) {
	  throw Exception("BlockCodeExperiment::run", "should not happen");
  }
  uint werrDetectedCnt= werrcnt - werrUndetCnt;
  task.setIterStats(m_pdecoder->getIterCount(), m_pdecoder->getItHistogram(),
                    werrDetectedCnt, m_pdecoder->getBErrHistogram());
  m_pdecoder->resetCounters();

  u64_t bitTrialsOut;
  if(m_doCWBitErrors) bitTrialsOut = trialcnt * m_n;
  else bitTrialsOut = trialcnt * m_k;
  task.setResult(berrcnt, bitTrialsOut, werrcnt, trialcnt, MLerrcnt);
}
