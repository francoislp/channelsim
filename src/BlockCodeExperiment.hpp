//Author: Francois Leduc-Primeau

#ifndef _BlockCodeExperiment_h_
#define _BlockCodeExperiment_h_

#include "source/ISource.hpp"
#include "coding/BlockCoder.hpp"
#include "modulation/IModulator.hpp"
#include "modulation/IDemodulator.hpp"
#include "channel/IChannel.hpp"
#include "channel/IChannelCplx.hpp"
#include "channel/NoiseGen.hpp"
#include "coding/BlockDecoder.hpp"
#include "engine/TaskList.hpp"
#include "DecodingStats.hpp"

class BlockCodeExperiment {

 public:

  /**
   * Sets-up a new block-code experiment. We assume that the energy
   * per information bit is 1.
   *@param n  The number of bits / codeword (the code rate being R=n/k).
   *@param k  The number of information bits / codeword.
   *@param Hm The number of parity checks (rows in H).
   *@param G  The k x n code generator matrix. G[i] should return the 
   *          i-th column vector (transposed storage).
   *@param H  The (n-k) x n parity check matrix. H[i] should return the i-th
   *          row vector.
   *@param conf The Config object containing the simulation parameters.
   */
  BlockCodeExperiment(int n, int k, int Hm, BitBlock* G, BitBlock* H,
                      Config& conf);

  ~BlockCodeExperiment();


  /**
   * Runs the experiment and returns the error rate.
   *@param task The task object describing the experiment. The task object
   *            is updated with the results when the experiment completes.
   *@throws MaxTrialsException If the maximum number of trials is reached
   *                           without satisfying the stopping conditions.
   */
  void run(Task& task);

 private:
  // parameters
  int m_n;
  int m_k;

  // whether to count bit errors on the full codeword, or just on the
  // source symbol
  bool m_doCWBitErrors;

  // persistent simulation objects
  ISource*         m_psrc;
  BlockCoder*      m_pcoder;
  IModulator*      m_pmod;
  IChannel*        m_pCh;
	IChannelCplx*    m_pChCplx;
  IDemodulator*    m_pdemod;
  BlockDecoder*    m_pdecoder;
  NoiseGen         m_ngen;

  // persistent statistics object
  DecodingStats*   m_pstats;

};

#endif
