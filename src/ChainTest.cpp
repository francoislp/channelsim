//Author: Francois Leduc-Primeau

#include "ChainTest.hpp"
#include "util/Exception.hpp"
#include "Logging.hpp"

#include "source/ZeroSource.hpp"
#include "source/ExtendedSource.hpp"
#include "modulation/BPSKMod.hpp"
#include "modulation/BPSKDemod.hpp"
#include "modulation/QPSKMod.hpp"
#include "modulation/QPSKDemod.hpp"
#include "channel/AdditiveChannel.hpp"
#include "channel/CAdditiveChannel.hpp"

#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;


ChainTest::ChainTest(Config& conf) {
	int srcType = conf.getSrcType();
	string modTypeStr= conf.getPropertyByName<string>("mod_type");
	m_bitBlockSize= 1000;
	
	// source
  if(srcType == SRCTYPE_EXTENDED) m_psrc = new ExtendedSource(m_bitBlockSize);
  else if(srcType == SRCTYPE_ZERO) m_psrc = new ZeroSource(m_bitBlockSize);
  else throw Exception("BlockCodeExperiment", "Invalid source type");

  m_pCh= 0;
  m_pChCplx= 0;
  
  if(modTypeStr=="bpsk" || modTypeStr=="BPSK") {
	  // modulator
	  BPSKMod* mod= new BPSKMod(m_psrc);
	  m_pmod = mod;
	  // channel
	  m_pCh = new AdditiveChannel(m_pmod, &m_ngen, m_bitBlockSize, conf);
	  // demodulator
	  m_pdemod = new BPSKDemod(mod, m_pCh, 0);
  } else if(modTypeStr=="qpsk" || modTypeStr=="QPSK") {
	  // modulator
	  m_pmod= new QPSKMod(m_psrc);
	  // channel
	  if(m_bitBlockSize/2*2 != m_bitBlockSize) {
		  throw Exception("BlockCodeExperiment", "block size must be even");
	  }
	  m_pChCplx= new CAdditiveChannel(m_pmod, &m_ngen, m_bitBlockSize/2, conf);
	  // demodulator
	  m_pdemod= new QPSKDemod(m_pChCplx);
  } else {
	  throw Exception("BlockCodeExperiment", "Invalid modulation type");
  }

}

ChainTest::~ChainTest() {
	delete m_psrc;
	delete m_pmod;
	delete m_pdemod;
	if(m_pCh!=0) delete m_pCh;
  if(m_pChCplx!=0) delete m_pChCplx;
}

void ChainTest::run(Task& task, bool doLLROut, std::ofstream& ofsLLR) {
  double   No         = task.No();
  int      minBErrors = task.minBErrors();
  uint     minTrials  = task.minTrials();
  

  uint     berrcnt    = 0; // number of bit errors encountered
  uint     bitTrials   = 0;

  // Note: might want to replicate the noise scaling that would be
  // used with coding, but currently it is not possible to specify a
  // code rate.
  
  // // we are given No in terms of the information bits
  // // convert it to the equivalent No over all coded bits
  // double scaledNo = No*m_n/m_k; // No/R, where R=k/n is the code rate

  double* dataout = new double[m_bitBlockSize];

  try {
    // initialization
    m_pdemod->setNo(No);
    if(m_pdemod->hasNext()) {
      throw Exception("ChainTest::run",
                      "Assertion failed: m_pdemod should be empty");
    }
    // noise
    // variance is No/2 (using the scaled No)
    m_ngen.setGaussian(0, No/2/m_pmod->getNbBitsPerSymbol());

    // keep doing trials until we reach the desired number of errors
    while(berrcnt < minBErrors || bitTrials < minTrials) {

	    // generate source block
      if( m_psrc->genBlock(1) != 1 ) {
        // mem alloc failed
        throw Exception("ChainTest::run", "Source mem alloc failed");
      }

      if( !m_pdemod->hasNext() ) {
	      throw Exception("ChainTest::run",
	                      "Assertion failed: m_pdemod should have data");
      }

      m_pdemod->nextLLR(m_bitBlockSize, dataout);
      // output LLR values if a file is specified
      if(doLLROut) {
	      for(int i=0; i<m_bitBlockSize; i++) ofsLLR<< dataout[i] <<endl;
      }
      // hard decision
      BitBlock rx(m_bitBlockSize);
      for(int i=0; i<m_bitBlockSize; i++) {
	      if(dataout[i] < 0.0) rx.setbit(i,1);
      }

      BitBlock const& tx= m_psrc->getSym(0);
      berrcnt+= tx.biterrorcnt(rx);
      bitTrials+= m_bitBlockSize;
    }

  } catch(Exception& e) {
    std::ofstream* pLog = Logging::getstream();
    *pLog << e << endl;
    std::ostringstream ss;
    ss << "Experiment failed for noise power " << No;
    throw Exception(ss.str());
  }

  delete[] dataout;

  // set the results
  task.setResult(berrcnt,bitTrials,0,bitTrials,0);
}

