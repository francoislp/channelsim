//Author: Francois Leduc-Primeau

#include "Config.hpp"
#include "util/Exception.hpp"
#include "Logging.hpp"
#include "coding/DecoderTypes.h" // for the MSGTYPE_* constants
#include "source/ISource.hpp" // for the SRCTYPE_* constants
#include "coding/stoch/IEdgeMem.hpp" // for the EMTYPE_* constants

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string.h>
#include <sys/stat.h>

#include <boost/filesystem.hpp>

// Character indicating a line comment in the config file
#define COMMENTCHAR '#'

using std::vector;
using std::map;
using std::string;
using std::ifstream;
using std::cout;
using std::cerr;
using std::endl;
using std::getline;
using std::stringstream;
using std::ostringstream;

using boost::filesystem::path;
using boost::filesystem::absolute;

// static data
Config* Config::s_instance = 0;

Config::Config()
  : m_machineIndex(0)
{
  init();
}

Config::Config(int machineIndex)
  : m_machineIndex(machineIndex)
{
  init();
}

// (private)
void Config::init() {
  if(s_instance == NULL) {
    s_instance = this;
  }
  else {
    throw Exception("Config::ctor", 
                    "Singleton has already been instatiated");
  }

  m_initialized = false;
}

void Config::parseFile(char const* filepath) {
  ifstream inFile;
  string inLine;
  std::ofstream* pLog = Logging::getstream();

  inFile.open(filepath);
  if(!inFile.good()) 
    throw Exception("Config::parseFile", "Error reading file");

  while(!inFile.eof()) {
    // parse each line
    stringstream ss;
    vector<string> tokens;
    MyGetline(inFile, inLine);
    ss << inLine;
    ss >> std::ws; // extract whitespace (necessary for empty lines)
    // tokenize the line
    while(!ss.eof()) {
      string curToken;
      ss >> curToken;
      tokens.push_back(curToken);
    }

    if( tokens.size() > 0 ) { // (skip empty lines)
      if( tokens.size() < 3 ) {
        cerr << "Less than 3 tokens on line:" << endl;
        cerr << inLine << endl;
        throw Exception("Config::parseFile", "File format error");
      }
      if( tokens[1] != "=" ) {
        cerr << "2nd token is not \"=\" on line:" << endl;
        cerr << inLine << endl;
        throw Exception("Config::parseFile", "File format error");
      }

      // check if key was already seen before
      if(m_configmap.find(tokens[0]) != m_configmap.end()) {
        throw Exception("Config::parseFile", "Duplicate key in config");
      }

      m_configmap[tokens[0]] = tokens[2];
    }
  }

  // now get the stuff we're looking for

  // Make sure the config file version is acceptable
  int confVersion = getPropertyByName<int>("config_version");
  if(confVersion < CONFIG_VERSION_MIN || confVersion > CONFIG_VERSION_MAX) {
    throw Exception("Config::parseFile", "Invalid \"config_version\"");
  }

  // optional elements:


  try {
    m_stochNDS = getPropertyByName<double>("stoch_NDS");
  } catch(InvalidKeyException&) {
    *pLog << "Warning: \"stoch_NDS\" undefined" << endl;
    m_stochNDS = 0;
  }

  try {
    m_stochNDSSweepIncr = getPropertyByName<float>("stoch_NDS_sweep_incr");
  } catch(InvalidKeyException&) {
    *pLog << "Warning: \"stoch_NDS_sweep_incr\" undefined" << endl;
    m_stochNDSSweepIncr = 0;
  }

  try {
    m_stochNDSSweepEnd = getPropertyByName<float>("stoch_NDS_sweep_end");
  } catch(InvalidKeyException&) {
    *pLog << "Warning: \"stoch_NDS_sweep_end\" undefined" << endl;
    m_stochNDSSweepEnd = 0;
  }

  try {
    string strEMType = getPropertyByName<string>("stoch_EM_type");
    if( strEMType == "shiftreg" ) m_stochEMType = EMTYPE_SHIFTREG;
    else if( strEMType == "tfm" ) m_stochEMType = EMTYPE_TFM;
    else if( strEMType == "s_tfm" ) m_stochEMType = EMTYPE_STFM;
    else if( strEMType == "stfm" )  m_stochEMType = EMTYPE_STFM; // (same)
    else if( strEMType == "feedback_tfm" ) m_stochEMType = EMTYPE_FDBCKTFM;
    else throw Exception("Config::parseFile", "Invalid value for \"stoch_EM_type\"");

  } catch(InvalidKeyException&) {
    m_stochEMType = -1;
  }

  try {
    m_Gk = getPropertyByName<int>("Gk");
  } catch(Exception&) {
    *pLog << "Warning: \"Gk\" undefined" << endl;
    m_Gk = 0;
  }

  try {
    ostringstream tmp;
    tmp << getPropertyByName<string>("iter_count_log_file");
    // differentiate filenames on each machine
    if( m_machineIndex != 0 ) tmp << "_" << m_machineIndex;
    m_itCountFile = tmp.str();
  } catch(InvalidKeyException&) {
    *pLog << "Warning: \"iter_count_log_file\" undefined" << endl;
    m_itCountFile = "";
  }

  inFile.close();

  m_initialized = true;
}

string Config::getOutputFileByName(string key) const {
  string value = getPropertyByName<string>(key);
  stringstream ss;
  ss << value << "_" << m_machineIndex;
  string filepath = ss.str();
  validateOutputPath(filepath);
  return filepath;
}

string Config::getInputFileByName(string key) const {
  string value = getPropertyByName<string>(key);
  if( m_machineIndex==0 ) { // not an MPI environment
    // check if file exists
    struct stat stFileInfo;
    int stCode = stat(value.c_str(), &stFileInfo);
    if(stCode!=0) {
      stringstream ss;
      ss << "File \""<<value<<"\" not found.";
      throw Exception("Config::getInputFileByName", ss.str());
    }

    return value;
  }
  else {
    // try different formats for the suffix, e.g. "_1", "_01", "_001", etc.
    bool fileExists = false;
    string filepath;
    for(uint i=0; i<5 && !fileExists; i++) {
      stringstream ssFilePath;
      ssFilePath << value << "_";
      for(uint j=0; j<i; j++) ssFilePath << "0";
      ssFilePath << m_machineIndex;
      // check if file exists
      struct stat stFileInfo;
      int stCode = stat(ssFilePath.str().c_str(), &stFileInfo);
      if(stCode==0) {
        fileExists = true;
        filepath = ssFilePath.str();
      }
    }
    if(!fileExists) {
      stringstream ss;
      ss << "File \""<<value<<"_[0*]"<<m_machineIndex<<"\" not found.";
      throw Exception("Config::getInputFileByName", ss.str());
    }

    return filepath;
  }
}

string Config::getMatrixFileName() {
  if(!initialized()) throw Exception("Config not initialized");
  return getPropertyByName<string>("matrix_basename");
}

string Config::getResultFileName() {
  if(!initialized()) throw Exception("Config not initialized");
  return getPropertyByName<string>("result_filename");
}

double Config::getSnrStart() {
  if(!m_initialized) throw Exception("Config not initialized");
  return getPropertyByName<double>("snr_start");
}

double Config::getSnrIncr() {
  if(!m_initialized) throw Exception("Config not initialized");
  return getPropertyByName<double>("snr_incr");
}

double Config::getSnrEnd() {
  if(!m_initialized) throw Exception("Config not initialized");
  return getPropertyByName<double>("snr_end");;
}

int Config::getMinBitErrors() {
  if(!m_initialized) throw Exception("Config not initialized");
  return getPropertyByName<int>("min_biterrors");;
}

int Config::getMinWordErrors() {
  if(!m_initialized) throw Exception("Config not initialized");
  return getPropertyByName<int>("min_worderrors");;
}


int Config::getMinTrials() {
  if(!m_initialized) throw Exception("Config not initialized");
  return getPropertyByName<int>("min_trials");;
}

long long Config::getMaxTrials() {
  if(!m_initialized) throw Exception("Config not initialized");
  return getPropertyByName<long long>("max_trials");;
}

int Config::getSrcType() {
	int srcType;

	string strSrcType = getPropertyByName<string>("srctype");
  if( strSrcType == "zero" ) srcType = SRCTYPE_ZERO;
  else if( strSrcType == "extended" ) srcType = SRCTYPE_EXTENDED;
  else throw Exception("Config::parseFile", "Invalid value for \"srctype\"");
	
  return srcType;
}

int Config::getMsgType() {
  if(!m_initialized) throw Exception("Config not initialized");
  string strMsgType = getPropertyByName<string>("msg_type");
  int msgType;
  if( strMsgType == "stochastic" ) msgType = MSGTYPE_STOCH;
  else if( strMsgType == "parallel-stochastic" ) msgType = MSGTYPE_PARALSTOCH;
  else if( strMsgType == "LLR" ) msgType = MSGTYPE_LLR;
  else if( strMsgType == "LR" ) msgType = MSGTYPE_LR;
  else if( strMsgType == "hd-stoch" ) msgType = MSGTYPE_HDSTOCH;
  else if( strMsgType == "hard_decision" ) msgType = MSGTYPE_HD;
  else if( strMsgType == "hd-mc" ) msgType = MSGTYPE_HDMC;
  else if( strMsgType == "min-sum" ) msgType = MSGTYPE_MINSUM;
  else if( strMsgType == "dd-bmp" ) msgType = MSGTYPE_DDBMP;
  else if( strMsgType == "dd_bmp" ) msgType = MSGTYPE_DDBMP;
  else if( strMsgType == "ddbmp" ) msgType = MSGTYPE_DDBMP;
  else throw Exception("Config::parseFile", "Invalid value for key \"msg_type\"");
  return msgType;
}

int Config::getMaxDecodeIter() {
  if(!m_initialized) throw Exception("Config not initialized");
  return getPropertyByName<int>("max_decode_iter");
}


// Private

// Calls getline but skips comment lines
// (same as AListParser::MyGetline)
void Config::MyGetline(ifstream& inFile, string& inLine) {
  char firstchar = COMMENTCHAR;

  while(firstchar == COMMENTCHAR) {
    if(!inFile.eof()) getline(inFile, inLine);
    else inLine = "";
    stringstream sstream;
    sstream << inLine;
    sstream >> std::ws; // extract whitespace
    firstchar = sstream.peek();
  }
}

void Config::validateOutputPath(std::string filepath) const {
  path p = absolute(filepath);
  p.remove_filename();
  try {
    create_directory(p); //TODO: this will only create a directory for
                         //the last element in the path !
  } catch(...) {  //TODO: of course this could be improved...
    std::ofstream* pLog = Logging::getstream();
    *pLog << "Could not create directories in path: " << filepath << endl;
  }
}
