//Author: Francois Leduc-Primeau

#ifndef _Config_hpp_
#define _Config_hpp_

#include "util/TypeConverter.hpp"
#include "util/Exception.hpp"
#include <fstream>
#include <string>
#include <map>

class InvalidKeyException : public Exception {
public:
  InvalidKeyException(std::string msg)
    : Exception("InvalidKeyException", msg)
  {}
};

/**
 * This class is used to access the information in a configuration file.
 */
class Config {
public:
  /**
   * Calling this constructor is the same as calling Config(int) with
   * a value of 0.
   */
  Config();

  Config(int machineIndex);

	~Config() {}

  /**
   * Returns the single instance.
   */
  static Config* getInstance() { return s_instance; }

  void parseFile(char const* filepath);

  /**
   * Allows overridding the config file value for the result output file.
   */
  void setOutputFilename(std::string outfilename) {
	  m_configmap["result_filename"]= outfilename;
  }

  /// Whether parseFile has been called successfully.
	bool initialized() { return m_initialized; }

  template<typename T> T getPropertyByName(std::string key) const {
    MapType::const_iterator it;
    it = m_configmap.find(key);
    if( it == m_configmap.end() ) {
      std::string msg = "Key " + key + " doesn't exist";
      throw InvalidKeyException(msg);
    }
    T ret;
    try {
      TypeConverter::convert(&ret, it->second);
    } catch(...) {
      throw Exception("Config::getPropertyByName", "Parsing Error");
    }
    return ret;
  }

  /**
   * Checks if a key is defined in the configuration.
   */
  bool propertyExists(std::string key) const {
    MapType::const_iterator it;
    it = m_configmap.find(key);
    if( it == m_configmap.end() ) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Retrieves the property corresponding to "key" in the
   * configuration, and appends to it the index of the MPI
   * instance. Also calls validateOutputPath() on the value (this
   * creates missing directories).
   *@throws Exceptions thrown by getPropertyByName(std::string) and
   *        validateOutputPath(std::string).
   */
  std::string getOutputFileByName(std::string key) const;

  /**
   * Retrieves the filepath property corresponding to "key" in the
   * configuration. In an MPI environment, the index of the MPI
   * instance is appended with a "_<index>" format. This method will
   * check that the file exists and throw an Exception otherwise. If
   * an index needs to be appended, some leading zeros will be added
   * to the index suffix before the file is reported as not found.
   *@throws Exceptions thrown by getPropertyByName() and a plain Exception 
   *        if the file is not found.
   */
  std::string getInputFileByName(std::string key) const;

  /**
   * The base name for the matrix files. The corresponding key in the
   * configuration is "matrix_basename".
   */
  std::string  getMatrixFileName();

  /**
   * The name of the output file. This value is actually passed as a command
   * line parameter and must be given to Config via setFileNames().
   */
  std::string  getResultFileName();

  /**
   * The first SNR value to be simulated (in dB). The corresponding
   * key in the configuration is "snr_start".
   */
  double getSnrStart();

  /**
   * The increment value the SNR (can be negative). The corresponding key in
   * the configuration is "snr_incr".
   */
  double getSnrIncr();

  /**
   * The last SNR value to be simulated (in dB). The corresponding key in the
   * configuration is "snr_end".
   */
  double getSnrEnd();

  /**
   * The minimum number of bit errors to be observed before ending the
   * simulation. The corresponding key in the configuration is
   * "min_biterrors".
   */
  int    getMinBitErrors();

  /**
   * The minimum number of word errors to be observed before ending the
   * simulation. The corresponding key in the configuration is
   * "min_worderrors".
   */
  int    getMinWordErrors();

  /**
   * The minimum number of trials to be performed before ending the
   * simulation. The corresponding key in the configuration is "min_trials".
   */
  int    getMinTrials();

  /**
   * The maximum number of trials to be performed by each CPU, after which the
   * simulation is aborted. The corresponding key in the configuration is
   * "max_trials".
   */
  long long   getMaxTrials();

  /**
   * The type of source to be used in the simulation. The corresponding
   * key in the configuration is "srctype". 
   *@return One of the SRCTYPE_* constants defined in ISource.h.
   */
  int getSrcType();

  /**
   * The type of message used in the decoder. The corresponding
   * key in the configuration is "msg_type".
   *@return One of the MSGTYPE_* constants defined in Node.h.
   */
  int getMsgType();

  /**
   * The maximum number of decoding iterations. The corresponding key in the
   * configuration is "max_decode_iter".
   */
  int    getMaxDecodeIter();

  /**
   * Name of the a file where the number of iterations used to decode
   * each codeword is logged. The corresponding key in the
   * configuration is "iter_count_log_file".
   * DEPRECATED: getOutputFileByName should be used instead
   */
  std::string& itCountFile() { 
    if(m_itCountFile!="") validateOutputPath(m_itCountFile);
    return m_itCountFile;
  }

  // (Stochastic)

  /**
   * Scaling constant used in the stochastic decoder. The corresponding
   * key in the configuration is "stoch_NDS".
   */
  double getStochNDS() { return m_stochNDS; }

  /**
   * Type of edge-memory used in the stochastic decoder. The
   * corresponding key in the configuration file is
   * "stoch_EM_type". Acceptable values are "shiftreg" or "tfm".
   *@return One of the EMTYPE_* constants in IEdgeMem.h
   */
  int getStochEMType() { 
    if(m_stochEMType < 0) throw InvalidKeyException("Key stoch_EM_type was not provided.");
    return m_stochEMType; 
  }

  /**
   * Number of rows in the G matrix (for cases where G is not available). The
   * corresponding key in the configuration is "Gk".
   */
  int getGk() { return m_Gk; }

  /**
   * The increment to be used when doing an NDS sweep. The corresponding
   * key in the configuration is "stoch_NDS_sweep_incr".
   */
  float getStochNDSSweepIncr() { return m_stochNDSSweepIncr; }

  /**
   * When doing an NDS sweep, this corresponds to the end of the sweep
   * range. The corresponding key in the configuration is
   * "stoch_NDS_sweep_end".
   */
  float getStochNDSSweepEnd() { return m_stochNDSSweepEnd; }

private:
  // ----- Methods -----

  /// Called by the constructors
  void init();

  void MyGetline(std::ifstream& inFile, std::string& inLine);

  /**
   * Creates any missing directories mentioned in "filepath" (currently,
   * only handles 1 missing directory).
   *@throws (Check source code)
   */
  void validateOutputPath(std::string filepath) const;

  // ----- Data -----

	/// Minimum required configuration file version.
	static const int CONFIG_VERSION_MIN = 2;

	/// Maximum known configuration file version
	static const int CONFIG_VERSION_MAX = 2;
	
  /**
   * The single instance.
   */
  static Config* s_instance;

  typedef std::map<std::string, std::string> MapType; 
  MapType m_configmap;

  /**
   * The index of the MPI machine we're running on. This index can be
   * used to customize parameters for each machine in the group.
   */
  int m_machineIndex;

  bool m_initialized;

  int m_maxDecIter;

  std::string m_itCountFile;

  double m_stochNDS;

  // A negative value indicates "undefined".
  int m_stochEMType;

  // Number of rows in the G matrix (for cases where G is not available)
  int m_Gk;

  float m_stochNDSSweepIncr;
  float m_stochNDSSweepEnd;

};

#endif
