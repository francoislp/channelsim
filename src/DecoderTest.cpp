//Author: Francois Leduc-Primeau

#include "DecoderTest.hpp"
#include "GlobalDefs.h"
#include "util/Exception.hpp"
#include "Logging.hpp"
#include "source/ZeroSource.hpp"
#include "coding/BlockCoder.hpp"
#include "coding/FileInputCoder.hpp"
#include "channel/FileChannel.hpp"

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

using std::endl;
using std::string;
using std::vector;
using std::ofstream;

DecoderTest::DecoderTest(int n, int k, int Hm,
                         BitBlock* G, BitBlock* H, Config& conf)
  : m_conf(conf),
    m_n(n),
    m_k(k)
{
  std::ofstream* pLog = Logging::getstream();

  // open channel input file
  if( !conf.propertyExists("channel_input_file") )
    throw Exception("DecoderTest", "No channel file specified in config");

  // check if we are using a CW input file
  m_usingCWInput = conf.propertyExists("cw_input_file");

  // coder
  if( m_usingCWInput ) {
    string filename = conf.getInputFileByName("cw_input_file");
    m_pcoder = new FileInputCoder(m_k, m_n, H, Hm, filename);
    *pLog << "Reading tx codewords from: "<< filename << endl;
  } 
  else { // no CW input file => comparing against the zero CW
    m_pcoder = new FileInputCoder(m_k, m_n, H, Hm, "");
    *pLog << "No tx CW file supplied, assuming the zero CW was sent." <<endl;
  }

  // channel
  // passing a pter to encoder so that channel can sync with encoder
  m_pchannel = new FileChannel(conf.getInputFileByName("channel_input_file"), 
                               m_n, m_pcoder);

  // demodulator
  m_pdemod = new BPSKDemod(m_pchannel, 0);
  // decoder
  m_pdecoder = new BlockDecoder(m_pcoder, m_pdemod, conf, true);

  // decoder stats
  // NOTE: WE ARE ASSUMING MODULATOR POLARITY IS -1
  m_pstats = new DecodingStats(m_pcoder, -1, m_pdemod, m_pdecoder);
}

DecoderTest::~DecoderTest() {
  delete m_pcoder;
  delete m_pchannel;
  delete m_pdemod;
  delete m_pdecoder;
  delete m_pstats;
}

void DecoderTest::run(Task& task) {

  uint berrcnt = 0;
  uint werrcnt = 0;
  uint MLerrcnt = 0;

  // this is required for proper computation of LLR's or NDS'ed channel values
  double scaledNo = task.No()*m_n/m_k;
  m_pdemod->setNo(scaledNo);

  // special initialization for a stochastic decoder
  if( task.isStochastic() ) {
    m_pdecoder->setStochNDSCst(task.NDSScaling());
  }

  // check the CWs outputted by the decoder
  vector<uint> errors;
  u64_t trialcnt = m_pstats->processAll(errors);
  berrcnt+=  errors[0];
  werrcnt+=  errors[1];
  MLerrcnt+= errors[2];

  std::ofstream* pLog = Logging::getstream();
  *pLog << "Finished running task: ";
  *pLog << " bit errors="<<berrcnt;
  *pLog << ", frame errors="<<werrcnt;
  *pLog <<", trials="<<trialcnt << endl;

  // set the results
  u64_t bitTrialsOut;
  bitTrialsOut = trialcnt * m_n;
  task.setResult(berrcnt, bitTrialsOut, werrcnt, trialcnt, MLerrcnt);

  task.setIterStats(m_pdecoder->getIterCount(), m_pdecoder->getItHistogram(),
                    m_pdecoder->getFailedCount(), m_pdecoder->getBErrHistogram());
  m_pdecoder->resetCounters();
}

void DecoderTest::writeRedecodeProbs(ofstream& ofs, double infoNo) {
  double scaledNo = infoNo * m_n / m_k;
  m_pdemod->setNo(scaledNo);

  //TODO: not doing the NDS scaling initialization

  vector<float> results;
  float redecodeCount = m_conf.getPropertyByName<float>("redecodetest_trials");
  m_pstats->redecodingStats(results, redecodeCount);

  // sort the results
  sort(results.begin(), results.end());

  // write out the sorted results
  for(uint i=0; i<results.size(); i++) {
    ofs << results[i] << endl;
  }
}
