//Author: Francois Leduc-Primeau

#ifndef _DecoderTest_h_
#define _DecoderTest_h_

//#include "coding/ICoder.hpp"
#include "coding/FileInputCoder.hpp"
#include "channel/IChannel.hpp"
#include "modulation/BPSKDemod.hpp"
#include "coding/BlockDecoder.hpp"
#include "engine/TaskList.hpp"
#include "DecodingStats.hpp"
#include "Config.hpp"

#include <fstream>

/**
 * This class is used to test a decoder with specific channel
 * values. The code is similar to the one in BlockCodeExperiment, with
 * some simplifications.
 */
class DecoderTest {
public:
  DecoderTest(int n, int k, int Hm, BitBlock* G, BitBlock* H,
              Config& conf);

  ~DecoderTest();

  /**
   * Decodes the channel values from the file specified in the
   * configuration file and returns the results via the Task
   * object. This method assumes that the channel values in the file
   * were generated from the zero codeword.
   */
  void run(Task& task);

  void writeRedecodeProbs(std::ofstream&, double);

private:

  Config& m_conf;

  int m_n;
  int m_k;

  // persistent simulation objects
  FileInputCoder* m_pcoder;
  IChannel*       m_pchannel;
  BPSKDemod*      m_pdemod;
  BlockDecoder*   m_pdecoder;

  DecodingStats* m_pstats;

  bool m_usingCWInput;
};

#endif
