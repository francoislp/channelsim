//Author: Francois Leduc-Primeau

#include "DecodingStats.hpp"
#include "util/Exception.hpp"
#include "Logging.hpp"
#include "util/BitBlock.hpp"

#include <iostream>
#include <string>

using std::endl;
using std::ofstream;
using std::vector;
using std::string;

DecodingStats::DecodingStats(ICoder* coder, int modPolarity,
                             IDemodulator* demod, BlockDecoder* decoder)
  : m_coder(coder),
    m_modPolarity(modPolarity),
    m_demod(demod),
    m_decoder(decoder)
{
  // open file streams to log CWs (transmitted and received) if required
  ofstream* pLog = Logging::getstream();
  Config* conf = Config::getInstance();
  if(conf == NULL) {
    throw Exception("DecodingStats::ctor", 
                    "Cannot get handle to configuration");
  }

  string errorType= conf->getPropertyByName<string>("error_type");
  if(errorType== "ber_info") m_checkInfo= true;
  else if(errorType=="ber_cw") m_checkInfo= false;
  else throw Exception("DecodingStats", "Invalid error count type");  
  
  m_cwOutputStream = openFile(conf, pLog, "cw_output_file", false);
  m_chValOutputStream = openFile(conf, pLog, "failed_cw_logging", false);
  m_LLROutputStream = openFile(conf, pLog, "failed_llr_logging", false);
  m_undetectedChValOutputStream = openFile(conf, pLog, "undetectederror_rx_logging", false);
  m_undetectedCWOuputStream = openFile(conf, pLog, "undetectederror_tx_logging", false);
  m_undetectedLLROutputStream = openFile(conf, pLog, "undetectederror_llr_logging", false);

#ifdef LOG_ALL_CW
  m_goodCWOutputStream = openFile(conf, pLog, "good_tx_logging", false);
  m_goodChValOutputStream = openFile(conf, pLog, "good_rx_logging", false);

  m_goodLLROutputStream = openFile(conf, pLog, "good_llr_logging", false);
#endif
}

DecodingStats::~DecodingStats() {
  if(m_cwOutputStream != 0) {
    m_cwOutputStream->close();
    delete m_cwOutputStream;
  }
  if(m_chValOutputStream != 0) {
    m_chValOutputStream->close();
    delete m_chValOutputStream;
  }
  if(m_LLROutputStream != 0) {
    m_LLROutputStream->close();
    delete m_LLROutputStream;
  }
  if(m_undetectedCWOuputStream != 0) {
    m_undetectedCWOuputStream->close();
    delete m_undetectedCWOuputStream;
  }
  if(m_undetectedChValOutputStream != 0) {
    m_undetectedChValOutputStream->close();
    delete m_undetectedChValOutputStream;
  }
  if(m_undetectedLLROutputStream != 0) {
    m_undetectedLLROutputStream->close();
    delete m_undetectedLLROutputStream;
  }
#ifdef LOG_ALL_CW
  if(m_goodCWOutputStream != 0) {
    m_goodCWOutputStream->close();
    delete m_goodCWOutputStream;
  }
  if(m_goodChValOutputStream != 0) {
    m_goodChValOutputStream->close();
    delete m_goodChValOutputStream;
  }
  if(m_goodLLROutputStream != 0) {
    m_goodLLROutputStream->close();
    delete m_goodLLROutputStream;
  }
#endif
}

u64_t DecodingStats::processAll(vector<uint>& errCount) {

  uint berrcnt = 0;
  uint werrcnt = 0;
  uint MLerrcnt = 0;
  uint werrUndetCnt = 0;
  u64_t trials = 0;
 
  while(m_decoder->hasNext()) {

    // pointers to the appropriate log file depending on the event
    ofstream* curCWOutputStream = 0;
    ofstream* curChValOutputStream = 0;
    ofstream* curLLROutputStream = 0;

    //Note: Call to decoder must be issued before tx CW is retrieved from coder!
    BitBlock const& b1 = m_decoder->nextSymbol(); // decoded cw
    trials++;

    if(m_checkInfo) { // measure bit errors on info bits
	    BitBlock const& bTx= m_coder->nextSentInfo();

	    int curBERcount= b1.biterrorcnt(bTx);

	    if(curBERcount > 0) {
		    werrcnt++;
		    berrcnt+= curBERcount;

		    bool undetected = m_decoder->isLastValid();
		    if(undetected) werrUndetCnt++;
		    
		    //TODO: check for ML errors
	    }

    } else { // measure bit errors on entire CW
	    BitBlock const& b2 = m_coder->nextSentSymbol(); // transmitted cw

	    int curBERcount = b1.biterrorcnt(b2);
	    //note: for punctured codes, it is possible to have a decoding
	    //      failure but no bit errors.
	    if(curBERcount>0 || !m_decoder->isLastValid()) { // word error !
		    werrcnt++;
		    berrcnt+= curBERcount;

		    bool undetected = m_decoder->isLastValid();

		    if(undetected) {
			    werrUndetCnt++;
			    
			    // determine whether the error is an ML error
			    vector<double> const& chvals = m_demod->lastChValues();
			    BitBlock b11= b1;
			    BitBlock b22= b2;
			    if(m_modPolarity > 0) {
				    b11.toggleAll();
				    b22.toggleAll();
			    }
			    double distToTx = b22.euclideanDistanceSq(chvals);
			    double distToDec = b11.euclideanDistanceSq(chvals);
			    if(distToTx > distToDec) {
				    // error was an ML error
				    MLerrcnt++;
			    }
		    }

		    // Set the log files
		    // if error was undetected, print to separate log if it exists
		    curCWOutputStream = m_cwOutputStream;
		    curChValOutputStream = m_chValOutputStream;
		    curLLROutputStream = m_LLROutputStream;
		    if(undetected) {
			    if(m_undetectedCWOuputStream != 0) {
				    curCWOutputStream = m_undetectedCWOuputStream;
			    }
			    if(m_undetectedChValOutputStream != 0) {
				    curChValOutputStream = m_undetectedChValOutputStream;
			    }
			    if(m_undetectedLLROutputStream != 0) {
				    curLLROutputStream = m_undetectedLLROutputStream;
			    }
		    }

		    // ask the decoder to print out further stats
		    m_decoder->printFailureStats(b2);
	    }
#ifdef LOG_ALL_CW
	    else { // codeword succesfully decoded
		    // set the codeword log pointers
		    curCWOutputStream = m_goodCWOutputStream;
		    curChValOutputStream = m_goodChValOutputStream;
		    curLLROutputStream = m_goodLLROutputStream;
	    }
#endif

	    // Print data to the logs (the pointers have been set to the
	    // appropriate log files above)
	    uint n = b2.size();

	    // Print tx codeword
	    if(curCWOutputStream != 0) {
#ifdef LOG_CW_REVERSE
		    for(int i=(n-1); i>=0; i--) //note: must use a signed type for index!
#else
			    for(uint i=0; i<n; i++)
#endif
				    {
					    if(b2[i]==0) *curCWOutputStream << "0 ";
					    else *curCWOutputStream << "1 ";
				    }
		    *curCWOutputStream << endl;
	    }

	    // Print the received channel values
	    if(curChValOutputStream != 0) {
		    vector<double> const& chvals = m_demod->lastChValues();
#ifdef LOG_CW_REVERSE
		    for(int i=(n-1); i>=0; i--) //note: must use a signed type for index!
#else
			  for(uint i=0; i<n; i++)
#endif
				  {
					  *curChValOutputStream << chvals[i] << " ";
				  }
		    *curChValOutputStream << endl;
	    }

	    if(curLLROutputStream != 0) {
		    vector<double> const& llrvals = m_demod->lastLLRValues();
		    if(llrvals.size() != n) {
			    throw Exception("DecodingStats::processAll",
			                    "Invalid size for LLR vector");
		    }
#ifdef LOG_CW_REVERSE
		    for(int i=(n-1); i>=0; i--) //note: must use a signed type for index!
#else
			  for(uint i=0; i<n; i++)
#endif
				  {
					  *curLLROutputStream << llrvals[i] << " ";
				  }
		    *curLLROutputStream << endl;
	    }
    } // end: measure bit errors on codeword

  } // end while

  errCount.resize(4);
  errCount[0] = berrcnt;
  errCount[1] = werrcnt;
  errCount[2] = MLerrcnt;
  errCount[3] = werrUndetCnt;
  return trials;

} // end DecodingStats::processAll

u64_t DecodingStats::redecodingStats(vector<float>& probList, 
                                     uint redecodeCount) {
  // configure the demodulator so that it duplicates each frame
  m_demod->duplicateFrames(redecodeCount, m_coder->getN());
  // put the decoder in "redecode only" mode
  m_decoder->setAlwaysRedecode(true);
  // normally the decoder RNGs are reset for each new CW. This must be
  // deactivated.
  m_decoder->setNoReseed(true);

  u64_t distinctFrameCount = 0;

  while(m_decoder->hasNext()) {

    uint werrcnt = 0;
    uint trials = 0;

    // first of a batch is a special case (we need to get the tx frame)
    BitBlock const& b1 = m_decoder->nextSymbol();
    BitBlock const& tx = m_coder->nextSentSymbol();
    distinctFrameCount++;

    trials++;
    if(b1 != tx) werrcnt++;

    for(uint i=0; i<(redecodeCount-1); i++) {

      if( ! m_decoder->hasNext() ) {
        throw Exception("DecodingStats::redecodingStats", "Frame duplication is not miraculous enough");
      }

      BitBlock const& b11 = m_decoder->nextSymbol();
      trials++;
      if(b11 != tx) werrcnt++;

    }

    // probability of successful redecoding
    float prob = static_cast<float>(trials - werrcnt) / trials;
    probList.push_back(prob); 
  }

  return distinctFrameCount;
}

ofstream* DecodingStats::openFile(Config* conf, ofstream* pLog, 
                                  string config_key, bool mandatory) {
  ofstream* ofs = 0;
  try {
    string filename = conf->getOutputFileByName(config_key);
    ofs = new ofstream(filename.c_str());
    if(!ofs->good()) {
      ofs = 0;
      *pLog << "(DecodingStats) ";
      *pLog << "Failed to open output file " <<filename << endl;
    }
  } catch(InvalidKeyException&) {
    if(mandatory) throw;
    ofs = 0;
  }
  if(mandatory && ofs==0) {
    throw Exception("DecodingStats::openFile",
                    "Failed to open mandatory output file");
  }
  return ofs;
}
