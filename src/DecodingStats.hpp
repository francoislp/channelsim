//Author: Francois Leduc-Primeau

#ifndef _DecodingStats_h_
#define _DecodingStats_h_

#include "GlobalDefs.h"
#include "coding/ICoder.hpp"
#include "channel/IChannel.hpp"
#include "modulation/IDemodulator.hpp"
#include "coding/BlockDecoder.hpp"
#include "Config.hpp"

#include <fstream>
#include <vector>

/**
 * Computes statistics on the operation of the system.
 */
class DecodingStats {
public:

	DecodingStats(ICoder*, int, IDemodulator*, BlockDecoder*);

  ~DecodingStats();

  /**
   * Retrieves codewords from the decoder until the queue is
   * empty. Statistics are computed for all the codewords
   * processed. Some are written to local log files, while the number
   * of bit and word errors are returned in <code>errCount</code>.
   *@param errCount   Return array that will be written with the following 
   *                  data (in this order): number of bit errors, number of 
   *                  word errors, number of those word errors that were ML
   *                  errors, number of undetected word errors.
   *@return The number of codewords processed.
   */
	u64_t processAll(std::vector<uint>& errCount);

  /**
   * For each frame in the queue, computes the probability of
   * successful decoding (in a randomized decoder) by decoding the
   * same frame <code>redecodeCount</code> times.
   *@param probList Decoding probabilities of each frame are added to this list.
   *@param redecodeCount (see above)
   *@return The number of distinct frames processed (i.e. the number of items added to <code>probList</code>).
   */
  u64_t redecodingStats(std::vector<float>& probList, uint redecodeCount);

private:

  // --- Methods ---

  /**
   * Opens the file specified by the config key.
   *@param conf Config object that resolves the configuration keys.
   *@param pLog Output stream to the log file (for printing warning and 
   *            error messages).
   *@param config_key The config key that provides the filepath.
   *@param mandatory Whether to throw an Exception if an error occurs.
   *@return A pointer to the output file stream, or NULL on error.
   *@throws InvalidKeyException
   *@throws Exception
   */
  std::ofstream* openFile(Config* conf, std::ofstream* pLog, std::string config_key,
                          bool mandatory);

  // --- Data ---

  ICoder*       m_coder;
  IDemodulator* m_demod;
  BlockDecoder* m_decoder;

	int m_modPolarity;

	/// If true, bit errors are only checked on info bits.
	bool m_checkInfo;

  /**
   * Output stream where transmitted values corresponding to failed
   * codewords are written. (Can be NULL)
   */
  std::ofstream* m_cwOutputStream;

  /**
   * Output stream where channel values of failed codewords are
   * written. (Can be NULL)
   */
  std::ofstream* m_chValOutputStream;

  /**
   * Output stream for LLR values (possibly quantized) corresponding
   * to failed codewords. (Can be NULL)
   */
  std::ofstream* m_LLROutputStream;

  /**
   * Output stream where transmitted values corresponding to
   * undetected failed codewords are written. (Can be NULL).
   */
  std::ofstream* m_undetectedCWOuputStream;

  /**
   * Output stream where channel values corresponding to undetected
   * failed codewords are written. (Can be NULL)
   */
  std::ofstream* m_undetectedChValOutputStream;

  /**
   * Output stream for LLR values (possibly quantized) corresponding
   * to undetected failed codewords. (Can be NULL)
   */
  std::ofstream* m_undetectedLLROutputStream;

#ifdef LOG_ALL_CW
  /**
   * Output stream where transmitted values corresponding to
   * successfully decoded codewords are written.
   */
  std::ofstream* m_goodCWOutputStream;

  /**
   * Output stream where channel values corresponding to successfully
   * decoded codewords are written.
   */
  std::ofstream* m_goodChValOutputStream;

  /**
   * Output stream for LLR values corresponding to successfully
   * decoded codewords.
   */
  std::ofstream* m_goodLLROutputStream;
#endif
};

#endif
