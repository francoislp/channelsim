//Author: Francois Leduc-Primeau

#include "Logging.hpp"

std::ofstream Logging::m_logstream;

void Logging::init(int uniqueID)
{
	std::ostringstream tmp;
  tmp << "logM" << uniqueID;
  std::string filename = tmp.str();
  const char* filename_c = filename.c_str();
  m_logstream.open(filename_c);

}


void Logging::writeln(std::string str)
{
	Logging::m_logstream << str << std::endl;
}

void Logging::write(int nb)
{
  Logging::m_logstream << nb;
}
