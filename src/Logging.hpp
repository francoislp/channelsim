//Author: Francois Leduc-Primeau

#ifndef _Logging_h_
#define _Logging_h_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

class Logging
{
 public:
  //Logging(); //constructor
  static void writeln(std::string str);
  static void write(int nb);
  static std::ofstream* getstream() {return &m_logstream;}

  static void init(int uniqueID);

 private:
  static std::ofstream m_logstream;

};

#endif
