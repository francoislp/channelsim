//Author: Francois Leduc-Primeau

// the message sending the filename containing the matrix
#define TAG_INIT_MATRIXNAME 1
// message sending general parameters
#define TAG_INIT_PARAM 2 //deprecated
// message sending the name of the config file
#define TAG_INIT_CONFIGNAME 3

// message signaling that a node is ready to do computations
#define TAG_SIMREADY  10 // not used
// message to transmist experiment parameters
#define TAG_SIMPARAM  11
// message to start an experiment at a specified noise power
#define TAG_SIMNOISE  12
// experiment result
#define TAG_SIMRESULT 13

/**
 * Used to signal that a Task failed. Message content: 1 MPI_INT.
 */
#define TAG_SIMFAILED 14

/**
 * Terminate the program. Message content: 1 MPI_INT.
 */
#define TAG_TERMINATE 30
