//Author: Francois Leduc-Primeau

#ifndef ISequential_h_
#define ISequential_h_

/**
 * Interface for a class that looks like sequential hardware.
 */
class ISequential {
public:

  virtual ~ISequential() {}

  /// Go to the next clock cycle.
  virtual void nextTick() = 0;

  /**
   * Reset the state. The object must then be ready to operate:
   * <code>nextTick()</code> is only called after the first cycle has
   * completed.
   */
  virtual void reset() = 0;

protected:
  ISequential() {} // prevent direct instantiation
};

#endif
