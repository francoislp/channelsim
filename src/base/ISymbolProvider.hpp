//Author: Francois Leduc-Primeau

#ifndef _ISymbolProvider_h_
#define _ISymbolProvider_h_

#include "util/BitBlock.hpp"

/**
 * Classes that provide symbols (sources, encoders, demodulators and
 * decoders) should implement this interface. Symbols can be provided
 * as hard symbols (represented by a symbol index), as a probability
 * array, or both.
 */
class ISymbolProvider {
 public:

  /**
   * Returns the number of bits/symbol (which is the log2 of the
   * alphabet size).
   */
  virtual int getAlphaBitsize() = 0;

  /**
   * Returns the next available symbol. TODO: This interface is bad 
   * because not thread safe, and only allows one pass through the data.
   * Should have a method that returns a proper iterator.
   *@throws EndOfFeedException If no other symbol is available.
   */
  virtual BitBlock const& nextSymbol() = 0;

  /**
   * Checks if there are further symbols to be read.
   */
  virtual bool hasNext() = 0;


  virtual ~ISymbolProvider() {}

 protected:
  ISymbolProvider() {}
};

#endif 
