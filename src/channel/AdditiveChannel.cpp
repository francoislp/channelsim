//Author: Francois Leduc-Primeau

#include "AdditiveChannel.hpp"
#include "util/Exception.hpp"
#include <cmath>

AdditiveChannel::AdditiveChannel(IModulator* mod, NoiseGen* noisegen, int n,
                                 Config& conf) 
  : m_n(n),
    m_pmod(mod),
    m_pnoise(noisegen),
    m_ISon(false)
{
  m_noisevect = new double[m_n];
  m_lastValues = new double[m_n];

  m_doFading= false;
  try {
	  m_doFading= conf.getPropertyByName<bool>("fading_amp_rayleigh");
  } catch(InvalidKeyException&) {}

  m_ampFading= new double[m_n];
  for(int i=0; i<m_n; i++) m_ampFading[i]= 1.0;
}

AdditiveChannel::~AdditiveChannel() {
  delete[] m_noisevect;
  delete[] m_lastValues;
  delete[] m_ampFading;
}

int AdditiveChannel::nextValues(int n, double* array) {
  if(n != m_n) throw Exception("AdditiveChannel::nextValues",
                                   "Ay caramba !");
  // get the noise
  m_noisevect = m_pnoise->getBlock(n, m_noisevect);
  if(m_doFading) {
	  m_ampFading= m_pnoise->getRayleighBlock(n, m_ampFading);
  }

  int i;
  for(i=0; i<n && m_pmod->hasNext(); i++) {
	  double curSym= m_pmod->nextElem1D();
	  if(m_doFading) {
		  if(m_ampFading[i] <= 0.0) throw Exception("AdditiveChannel::nextValues", "invalid fading coefficient");
		  curSym*= m_ampFading[i];
	  }
    array[i] = curSym + m_noisevect[i];
    m_lastValues[i] = array[i]; // keep a copy
  }

  return i;
}

double AdditiveChannel::nextISCoeff() {
  double sum=0;
  for(int j=0; j<m_n; j++) sum+= (m_noisevect[j]*m_noisevect[j]);
  double term1 = m_n * ( log(m_ISt) - log(m_ISs) );
  double term2 = sum * ( 0.5/m_ISt/m_ISt - 0.5/m_ISs/m_ISs );
  return exp(term1 + term2);
}
