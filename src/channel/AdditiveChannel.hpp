//Author: Francois Leduc-Primeau

#ifndef AdditiveChannel_hpp_
#define AdditiveChannel_hpp_

#include "IChannel.hpp"
#include "modulation/IModulator.hpp"
#include "NoiseGen.hpp"
#include "Config.hpp"

class AdditiveChannel : public IChannel {
public:

  /**
   * Constructor
   *@param mod      The modulator that is sending through this channel.
   *@param noisegen Source of noise to be added.
   *@param n        The size of a codeword.
   *@param conf     Configuration object.
   */
	AdditiveChannel(IModulator* mod, NoiseGen* noisegen, int n, Config& conf);

  ~AdditiveChannel();

  bool hasNext() { return m_pmod->hasNext(); }

  /**
   * Returns the next n values received through the channel.
   *@param n  The number of values to return.
   *@param array  A pre-allocated array of size >= n.
   *@return The number of values actually written to the array (may be <n).
   */
  int nextValues(int n, double* array);

  // Implements IChannel
  double const* lastReceivedValues() { return m_lastValues; }

	double const* lastFading() { return m_ampFading; }

  /**
   * Enables the channel object to compute the IS coefficients for the
   * codewords (which is then obtained using nextISCoeff()).
   *@param s  The std dev for the nominal distribution (the one that is the
   *          target of the experiment).
   *@param t  The std dev for the distribution that is actually used to
   *          generate the noise.
   */
  void useIS(double s, double t) {
    m_ISon = true;
    m_ISs = s;
    m_ISt = t;
  }

  /**
   * Computes and returns the IS coefficient that corresponds to the
   * codeword returned by the last call to nextValues().
   */
  double nextISCoeff();

private:

  /// The codeword length.
  int m_n;

  IModulator* m_pmod;

  NoiseGen* m_pnoise;
  double* m_noisevect;

  double* m_lastValues;

	bool m_doFading;
	double* m_ampFading;

  /// Whether the IS functions are "on".
  bool m_ISon;

  double m_ISs;

  double m_ISt;
};

#endif
