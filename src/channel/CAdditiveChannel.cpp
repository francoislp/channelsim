//Author: Francois Leduc-Primeau

#include "CAdditiveChannel.hpp"
#include "util/Exception.hpp"

CAdditiveChannel::CAdditiveChannel(IModulator* mod, NoiseGen* ngen, int n,
                                   Config& conf)
	: m_n(n),
	  m_pmod(mod),
	  m_pnoise(ngen)
{
	m_noiseVectRe= new double[m_n];
	m_noiseVectIm= new double[m_n];
	m_lastValues= new std::complex<double>[m_n];

	// make sure the modulator produces 2-D symbols
	if(m_pmod->getDim() != 2) {
		throw Exception("CAdditiveChannel::ctor", "Invalid modulator");
	}

	m_doFading= false;
	try {
		m_doFading= conf.getPropertyByName<bool>("fading_amp_rayleigh");
	} catch(InvalidKeyException&) {}

	m_ampFading= new double[m_n];
	for(int i=0; i<m_n; i++) m_ampFading[i]= 1.0;
}

CAdditiveChannel::~CAdditiveChannel() {
	delete[] m_noiseVectRe;
	delete[] m_noiseVectIm;
	delete[] m_lastValues;
	delete[] m_ampFading;
}

int CAdditiveChannel::nextValues(int n,
                                 std::vector<std::complex<double>>& array) {
	if(n > m_n) throw Exception("CAdditiveChannel::nextValues",
	                            "block size too large");

	// retrieve the Gaussian noise
	m_noiseVectRe= m_pnoise->getBlock(n, m_noiseVectRe);
	m_noiseVectIm= m_pnoise->getBlock(n, m_noiseVectIm);
	if(m_doFading) {
		// retrieve amplitude fading
		m_ampFading= m_pnoise->getRayleighBlock(n, m_ampFading);
	}

	std::vector<double> curSym;
	curSym.resize(2);
	int i;
  for(i=0; i<n && m_pmod->hasNext(); i++) {
	  m_pmod->nextElem(curSym);

	  if(m_doFading) {
		  curSym[0]*= m_ampFading[i];
		  curSym[1]*= m_ampFading[i];
	  }
	  curSym[0]+= m_noiseVectRe[i];
	  curSym[1]+= m_noiseVectIm[i];

	  array[i]= std::complex<double>(curSym[0], curSym[1]);
	  m_lastValues[i]= array[i]; // keep a copy
  }

  return i;
}
