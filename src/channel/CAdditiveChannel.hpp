//Author: Francois Leduc-Primeau

#ifndef CAdditiveChannel_hpp_
#define CAdditiveChannel_hpp_

#include "IChannelCplx.hpp"
#include "modulation/IModulator.hpp"
#include "NoiseGen.hpp"
#include "Config.hpp"

#include <complex>

class CAdditiveChannel : public IChannelCplx {
public:

  /**
   * Constructor
   *@param mod      The modulator that is sending through this channel.
   *@param noisegen Source of noise to be added.
   *@param n        Maximum number of symbols that can be handled in a block.
   *@param conf     Configuration object.
   */	
	CAdditiveChannel(IModulator* mod, NoiseGen* ngen, int n, Config& conf);

	~CAdditiveChannel();

	bool hasNext() { return m_pmod->hasNext(); }

  /**
   *@see IChannelCplx::nextValues(int,std::complex<double>*)
   */
	int nextValues(int n, std::vector<std::complex<double>>& array);

	std::complex<double> const* lastReceivedValues() { return m_lastValues; }

	double const* lastFading() { return m_ampFading; }

private:

  /// Max. number of complex symbols in a block
  int m_n;

  IModulator* m_pmod;

  NoiseGen* m_pnoise;

	std::complex<double>* m_lastValues;

	double* m_noiseVectRe;
	double* m_noiseVectIm;

	/// Whether amplitude scaling is applied on the complex symbols
	bool m_doFading;

	double* m_ampFading;
};

#endif
