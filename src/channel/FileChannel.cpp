//Author: Francois Leduc-Primeau

#include "compileflags.h"
#include "FileChannel.hpp"
#include "util/Exception.hpp"

#include <iostream>
#include <sstream>


using std::string;
using std::ifstream;
using std::stringstream;
using std::ws;
using std::cerr;
using std::endl;

FileChannel::FileChannel(string filepath, int n) {

  // common ctor code
  m_pFileStream = new ifstream(filepath.c_str());
  if( !m_pFileStream->good() ) 
    throw Exception("FileChannel::ctor", "Could not open channel file");

  m_lastValues = new double[n];

  m_ampFading= new double[n];
  for(int i=0; i<n; i++) m_ampFading[i]= 1.0;    
  // end common ctor code

  m_pcoder = 0;
}

FileChannel::FileChannel(string filepath, int n, FileInputCoder* coder) {
  // common ctor code
  m_pFileStream = new ifstream(filepath.c_str());
  if( !m_pFileStream->good() ) 
    throw Exception("FileChannel::ctor", "Could not open channel file");

  m_lastValues = new double[n];

  m_ampFading= new double[n];
  for(int i=0; i<n; i++) m_ampFading[i]= 1.0;    
  // end common ctor code

  m_pcoder = coder;
}

FileChannel::~FileChannel() {
  m_pFileStream->close();
  delete m_pFileStream;

  delete[] m_lastValues;
  delete[] m_ampFading;
}

bool FileChannel::hasNext() {
  if(m_curLine != "") return true;
  *m_pFileStream >> ws;
  MyGetline(*m_pFileStream, m_curLine);
  if(m_curLine == "") return false;
  return true;
}

int FileChannel::nextValues(int n, double* array) {
  // note: calling hasNext() also pre-fetches the next line into
  // m_curLine if necessary
  if( !hasNext() ) {
    throw EndOfFeedException("All the file content has been received");
  }

  // send dummy codeword request to encoder so that it remains in sync
  //note: we're assuming that the caller of this method is retrieving
  //   exactly one codeword, even though this is not guaranteed or
  //   even suggested by the interface.
  if(m_pcoder != 0) {
    m_pcoder->nextSymbol(); // (discard the return value)
  }

  string cwText = m_curLine;
  m_curLine = ""; // signals that another line needs to be fetched
  stringstream cwStream;
  cwStream << cwText;

  int i;
  for(i=0; i<n; i++) {
    cwStream >> ws;
    if(cwStream.eof()) {
      cerr << "Invalid line: "<< cwText << endl;
      throw Exception("FileChannel::nextValues", "Invalid line (printed to stderr)");
    }

    double sample;
    cwStream >> sample;
#ifdef FILECHANNEL_REVERSE_BITORDER
    array[n-1-i] = sample;
    m_lastValues[n-1-i] = sample;
#else
    array[i] = sample;
    m_lastValues[i] = sample;
#endif
  }

  return i;
}

void FileChannel::useIS(double s, double t) {
  throw Exception("FileChannel::useIS", "not supported on this type of channel");
}

double FileChannel::nextISCoeff() {
  throw Exception("FileChannel::nextISCoeff", "not supported on this type of channel");
}

//private

// Calls getline but skips comment lines (same code as
// AListParser::MyGetline()).
void FileChannel::MyGetline(ifstream& inFile, string& inLine) {
  char firstchar = COMMENTCHAR;

  while(firstchar == COMMENTCHAR) {
    if(!inFile.eof()) getline(inFile, inLine);
    else inLine = "";
    stringstream sstream;
    sstream << inLine;
    sstream >> std::ws; // extract whitespace
    firstchar = sstream.peek();
  }
}
