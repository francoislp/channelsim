//Author: Francois Leduc-Primeau

#ifndef _FileChannel_h_
#define _FileChannel_h_

#include "IChannel.hpp"
#include "coding/FileInputCoder.hpp"

#include <string>
#include <fstream>

class FileChannel : public IChannel {
public:

  /**
   * Constructor 
   *@param filepath Path to the channel file. The expected file format
   *                is one line per codeword. Full line comments
   *                starting with COMMENTCHAR are supported.
   *@param n        The codeword length.
   */
  FileChannel(std::string filepath, int n);

  /**
   * Constructor 
   *@param filepath Path to the channel file. The expected file format
   *                is one line per codeword. Full line comments
   *                starting with COMMENTCHAR are supported.
   *@param n        The codeword length.
   *@param coder    If non-null, will send dummy codeword requests to that encoder so that it remains synchronized with the channel values.
   */
  FileChannel(std::string filepath, int n, FileInputCoder* coder);

  ~FileChannel();

  bool hasNext();

  int nextValues(int n, double* buf);

  double const* lastReceivedValues() { return m_lastValues; }

	double const* lastFading() { return m_ampFading; }	

  /// (not supported, throws an Exception)
  void useIS(double, double);

  /// (not supported, throws an Exception)
  double nextISCoeff();

private:

  void MyGetline(std::ifstream& inFile, std::string& inLine);

  std::ifstream* m_pFileStream;

  /**
   * The next line to be processed, or an empty string if the next
   * line still needs to be fetched.
   */
  std::string m_curLine;

  double* m_lastValues;

  // remain in sync with that encoder if non-NULL
  FileInputCoder* m_pcoder;

	double* m_ampFading;	
};

#endif
