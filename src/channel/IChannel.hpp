//Author: Francois Leduc-Primeau

#ifndef IChannel_hpp_
#define IChannel_hpp_

/**
 * Interface for binary channel models. See AdditiveChannel for
 * function documentation.
 */
class IChannel {
public:

  virtual ~IChannel() {}

  virtual bool hasNext() = 0;

  virtual int nextValues(int, double*) = 0;

  /**
   * Returns the noisy channel values from the last call to nextValues().
   */
  virtual double const* lastReceivedValues() = 0;

	virtual double const* lastFading() = 0;

  virtual void useIS(double, double) = 0;

  virtual double nextISCoeff() = 0;

protected:
  IChannel() {} // prevent direct instantiation
};

#endif
