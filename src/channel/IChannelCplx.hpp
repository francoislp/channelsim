//Author: Francois Leduc-Primeau

#ifndef IChannelCplx_hpp_
#define IChannelCplx_hpp_

#include <vector>
#include <complex>

/**
 * Interface for complex channel models.
 */
class IChannelCplx {
public:

	virtual ~IChannelCplx() {}

	virtual bool hasNext() = 0;

  /**
   * Returns the next n complex values received through the channel.
   *@param n  The number of values to return.
   *@param array  A pre-allocated array of size >= n.
   *@return The number of values actually written to the array (may be <n).
   */	
	virtual int nextValues(int n,
	                       std::vector<std::complex<double>>& array) = 0;

  /**
   * Returns the noisy channel values from the last call to nextValues(...).
   */	
	virtual std::complex<double> const* lastReceivedValues() = 0;

	/**
	 * Returns the scaling coefficients that were applied on the last
	 * call to nextValues(...). If no fading is applied, returns an
	 * all-one vector.
	 */
	virtual double const* lastFading() = 0;

protected:
	IChannelCplx() {} // prevent direct instantiation
};

#endif
