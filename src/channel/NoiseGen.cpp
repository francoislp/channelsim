//Author: Francois Leduc-Primeau

#include "NoiseGen.hpp"
#include "Logging.hpp"
#include "util/Exception.hpp"
#include <cmath>
#include <unistd.h>
#include <sys/time.h>

unsigned long NoiseGen::s_seed = 0;
gsl_rng*      NoiseGen::s_prndgen = 0;
uint          NoiseGen::s_objCount = 0;

NoiseGen::NoiseGen() {

	s_objCount++;

	if(s_prndgen==0) {
		//gsl_rng_ranlxd2: double precision (48 bits) output from the RANLUX
		//random number generator, with "luxury level" 2 (slower, better
		//de-correlation).
		s_prndgen = gsl_rng_alloc(gsl_rng_ranlxd2);
		// seed the random number generator
		pid_t mypid = getpid();
		struct timeval tp;
		int returnval = gettimeofday(&tp, NULL);
		std::ofstream* pLog = Logging::getstream();
		if(returnval < 0) { // error getting the time
			(*pLog) << "WARNING: Failed to get the time, only the pid is used as the NoiseGen seed" << std::endl;
			s_seed = mypid;
		} else {
			s_seed = mypid * (tp.tv_usec % 10000); // assume doesn't overflow
			s_seed++; // avoid using 0 since it is a special value that indicates
			       // the default seed (and might therefore be the same as another seed)
		}
		gsl_rng_set(s_prndgen, s_seed);
		// log the seed value
		(*pLog) << "Using seed = " << s_seed << std::endl;
	}

  m_mean = 0;
  m_var = -1; // a negative value indicates the parameters were not set
}

// destructor
NoiseGen::~NoiseGen() {
	if(s_prndgen!=0 && s_objCount==1) {
		gsl_rng_free(s_prndgen);
		s_prndgen= 0;
	}
	s_objCount--;
}

void NoiseGen::setGaussian(double mean, double var) {
  m_mean = mean;
  m_var = var;
}

double* NoiseGen::getBlock(int size, double* array) {
	return getNormBlock(size, array, m_mean, m_var);
}

double* NoiseGen::getNormBlock(int size, double* array, double mean, double var) {
	if(var<0) throw Exception("NoiseGen::getNormBlock", "Distr param not set");
	double stddev= sqrt(var);
  for(int i=0; i<size; i++) {
    array[i] = mean + gsl_ran_gaussian(s_prndgen, stddev);
  }
  
  return array;
}

double* NoiseGen::getRayleighBlock(int size, double* array) {
	for(int i=0; i<size; i++) {
		array[i]= gsl_rng_uniform_pos(s_prndgen); // uniform in ]0,1[
		array[i]= sqrt(-log(array[i]));
	}
	return array;
}

double NoiseGen::nextSample() {
	return nextNormSample(m_mean, m_var);
}

double NoiseGen::nextNormSample(double mean, double var) {
	if(var<0) throw Exception("NoiseGen::nextSample", "Distr param not set");
	return mean + gsl_ran_gaussian(s_prndgen, sqrt(var));
}

uint NoiseGen::nextBinSample(double prob) {
	if( gsl_rng_uniform(s_prndgen) < prob ) return 1;
	return 0;
}
