//Author: Francois Leduc-Primeau

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <string>
#include <string.h>
#include <sys/stat.h>

#include "GlobalDefs.h"
#include "Logging.hpp"
#include "util/Exception.hpp"
#include "MPITags.h"
#include "BlockCodeExperiment.hpp"
#include "ChainTest.hpp"
#include "DecoderTest.hpp"
#include "parsers/AListParser.hpp"
#include "parsers/AListElem.hpp"
#include "parsers/GParser.hpp"
#include "parsers/MaskParser.hpp"
#include "Config.hpp"
#include "config/config.hpp" // not the same as Config! :)

// Data Structures
#include <map>
#include <vector>

using std::endl;
using std::cout;
using std::cerr;
using std::ofstream;
using std::map;
using std::vector;
using std::string;
using std::ws;
using std::stringstream;

#include "compileflags.h"

// Function prototypes
void regularRun(int, int, int, BitBlock*, BitBlock*, Config&, ofstream&);
void decoderTestRun(int, int, int, BitBlock*, BitBlock*, Config&, ofstream&);
void redecodeTestRun(int, int, int, BitBlock*, BitBlock*, Config&, ofstream&);
int chainTestRun(Config&, ofstream&, bool);

// Print usage info and exit.
void usageExit(int exitCode) {
  cout << "Usage: channelsim config=<conf file> [--chainTest | --chainTestPMF]" << endl;
  exit(exitCode);
}

// define authorized command-line keys
void setAuthorizedKeys(config& conf) {
	// key-value pairs
	conf.addValidKey("config");
	// options
	conf.addValidOption("chainTest");
	conf.addValidOption("chainTestPMF");
}

int main(int argc, char** argv) {
  char* configfile = 0;
  
  if(argc == 1) {
    usageExit(0);
  }
  config cliConf;
  setAuthorizedKeys(cliConf);

  try {
    cliConf.initCL(argc, argv);
  } catch(syntax_exception& e) {
    cerr << "Invalid syntax in cmd-line arguments: "<<e.what()<<endl;
    usageExit(1);
  } catch(invalidkey_exception& e) {
    cerr << "Invalid key or option: "<<e.what()<<endl;
    usageExit(1);
  }  
  
  try {

    Config conf;
    try {
	    conf.parseFile(cliConf.getParamString("config").c_str());
    } catch(key_not_found&) {
	    cerr << "Configuration file is missing!" << endl;
	    usageExit(1);
    } catch(Exception& e) {
      cerr << "Exception occured while parsing config file:" << endl;
      cerr << e << endl;
      return 1;
    }

    // make sure config is happy
    if( !conf.initialized() ) {
      cerr << "The configuration is not happy" << endl;
      exit(1);
    }
    
    // init the log file
    Logging::init(0); //(only one machine)

    // output file for the results (same code as channelsimMPI.cpp)
    string outfileName = conf.getResultFileName();
    // check if file exists
    struct stat stFileInfo;
    int outfileIndex = 1;
    int stCode = stat(outfileName.c_str(), &stFileInfo);
    while(stCode==0) {
      // increment a suffix in the filename until it doesn't exist
      std::stringstream outfileName2;
      outfileName2 << conf.getResultFileName() << "_" << outfileIndex;
      outfileName = outfileName2.str();
      // check if that file exists
      stCode = stat(outfileName.c_str(), &stFileInfo);
      outfileIndex++;
    }
    ofstream ofsResult(outfileName.c_str());
    if( ofsResult.fail() ) {
      cerr << "Failed to open the result output file at ";
      cerr << outfileName.c_str() << endl;
      return 1;
    }

    if(cliConf.checkOption("chainTest")) {
	    return chainTestRun(conf, ofsResult, false);
    } else if(cliConf.checkOption("chainTestPMF")) {
	    return chainTestRun(conf, ofsResult, true);
    }
    
    // Load the H matrix
    std::string filenameH = conf.getMatrixFileName();
    filenameH = filenameH+"_H";
    cout << "Reading H matrix from " << filenameH << endl;
    AListParser parserH;
    try {
      parserH.ParseFile(filenameH);
    } catch(Exception& e) {
      cerr << "Exception occured while parsing H matrix file:" << endl;
      cerr << e << endl;
      return 1;
    }
    vector<AListElem> hElemList;
    parserH.GetElements(hElemList);
    int Hm = parserH.GetRowCount();
    int Hn = parserH.GetColCount();
    
    // copy to dense format
    char** H_tmp = new char*[Hm];
    for(int i=0; i<Hm; i++) H_tmp[i] = new char[Hn];
    // initialize all elements to 0
    for(int i=0; i<Hm; i++) {
      for(int j=0; j<Hn; j++) {
        H_tmp[i][j] = 0;
      }
    }
    // set some elements to 1
    for(uint i=0; i<hElemList.size(); i++) {
      int row = hElemList[i].GetRow();
      int col = hElemList[i].GetCol();
      H_tmp[row][col] = 1;
    }
    // pack into BitBlocks
    BitBlock* H = new BitBlock[Hm];
    for(int i=0; i<(Hm); i++) {
      H[i].init_rev(Hn, H_tmp[i]);
      delete[] H_tmp[i];
    }
    delete[] H_tmp;
    
    // code rate (will be overwritten if we are reading a G matrix)
    int n = Hn;
    // k is taken 1st from the G matrix, 2nd from Config, 3rd using
    // Hn-Hm (assuming H is full rank)
    int k = 0;
    
    // Load the G matrix
    GParser gp;
    string matrixfilename = conf.getMatrixFileName();
    matrixfilename = matrixfilename + "_G";
    
    BitBlock* G = NULL;
    if(conf.getSrcType() != SRCTYPE_ZERO) {
      cout << "Reading G matrix from " << matrixfilename << endl;
      try {
        G = gp.parseFile_block(matrixfilename.c_str());
      } catch(Exception& e) {
        cerr << "Failed to parse G matrix from "<<matrixfilename << endl;
        cerr << e << endl;
        exit(1);
      }
      // get k from G
      k = gp.getK();
    }

    if(k==0) {
      k = conf.getGk();
    }
    if(k==0) {
      cerr << "Warning: Assuming full-rank H to determine k" << endl;
      k = Hn-Hm;
    }

    // if the code is punctured, the codeword length is not equal to the
    // number of columns in the H matrix
    try {
      string punctureFilePath = conf.getPropertyByName<string>("puncture_file");
      MaskParser mp;
      mp.parseFile(punctureFilePath);
      n= mp.getWeightComp();
    
      // check that n >= k
      if(n < k) {
        throw Exception("channelsim::main", 
                        "Invalid code length read from puncture file");
      }
    } catch(InvalidKeyException&) {}

    // sanity check for size of G and H
    if( G != NULL && gp.getN() != Hn && gp.getN() != n ) {
	    cerr << "(channelsim) Assertion failed: G dimensions" << endl;
    }    

    cout << "H dimensions: " << Hm << " x " << Hn << endl;
    cout << "Code rate: (" << n << ", " << k << ")" << endl;
    
    if( !conf.propertyExists("channel_input_file") ) {
      regularRun(n, k, Hm, G, H, conf, ofsResult);
    } else {
      bool redecodeMode = false;
      try {
        redecodeMode = conf.getPropertyByName<bool>("mode_redecodeprob");
      } catch(InvalidKeyException&) { }
      cout << "Performing a decoder test using the channel values from:" <<endl;
      cout << conf.getInputFileByName("channel_input_file") << endl;
      if(redecodeMode) {
        redecodeTestRun(n, k, Hm, G, H, conf, ofsResult);
      } else {
        decoderTestRun(n, k, Hm, G, H, conf, ofsResult);
      }
    }

    ofsResult.close();

    // delete matrices
    delete[] H;
    delete[] G;

  } catch(Exception& e) {
    cerr << "Exception catched at top-level: " << endl;
    cerr << e << endl;
    return 1;
  }

  return 0;
}

void regularRun(int n, int k, int Hm, BitBlock* G, BitBlock* H, Config& conf,
                ofstream& ofs) {
  try {
    // instantiate experiment engine
    BlockCodeExperiment exp(n, k, Hm, G, H, conf);
    
    // calculate nb of points (nb of experiments)
    // round values to 6 decimals
    double snrdb_start = conf.getSnrStart();
    double snrdb_end = conf.getSnrEnd();
    double snrdb_incr = conf.getSnrIncr();
    double snrdb_start_tmp = round( snrdb_start*1000000 );
    double snrdb_end_tmp = round( snrdb_end*1000000 );
    double snrdb_incr_tmp = round( snrdb_incr*1000000 );
    int nbpoints = floor((snrdb_end_tmp - snrdb_start_tmp) / snrdb_incr_tmp) + 1;
    if(nbpoints<=0) {
      cerr << "Invalid noise stepping parameters, exiting." << endl;
      return;
    }
    cout << "Simulating "<<nbpoints<<" SNR points"<<endl;
    
    TaskList tl(ofs, conf);
    
    // create all Tasks
    double curSNRdB = snrdb_start; 
    for(int i=0; i < nbpoints; i++) {
      // convert SNR in dB into linear
      double No = 1 / pow(10, curSNRdB/10);

      if( ISSTOCHASTICDECODER(conf.getMsgType()) ) {
        tl.addExperimentStoch(No,
                              conf.getMinBitErrors(),
                              conf.getMinWordErrors(),
                              conf.getMinTrials(),
                              conf.getMaxTrials(),
                              1,
                              conf.getStochNDS() );
      } else {
        tl.addExperiment(No,
                         conf.getMinBitErrors(),
                         conf.getMinWordErrors(),
                         conf.getMinTrials(),
                         conf.getMaxTrials(),
                         1 );
      }

      curSNRdB+= snrdb_incr;
    }

    // run all Tasks in the TaskList
    while(tl.hasNext()) {
      Task* t = tl.next();
      cout << "Running task with noise power " << t->No() << endl;
      exp.run( *t );
      tl.writeResults(false);
    }

    tl.writeResults();

  } catch(Exception& e) {
    cerr << e << endl;
  }
}

/**
 * This method is used to run the decoder with pre-computed noisy
 * channel values. The SNR operating point is taken from
 * Config::getSnrStart.
 */
void decoderTestRun(int n, int k, int Hm, BitBlock* G, BitBlock* H, 
                    Config& conf, ofstream& ofs) {
  // Create a TaskList
  TaskList tl(ofs, conf);

  // how many times the codeword file should be used (default: 1)
  int repeat = 1;
  try {
    repeat = conf.getPropertyByName<int>("repeat_codeword_file");
  } catch(InvalidKeyException&) {
  }
  if( repeat < 1 ) {
    Logging::writeln("Invalid value for config key \"repeat_codeword_file\"");
    repeat = 1;
  }

  double snrdb = conf.getSnrStart();
  double No = 1 / pow(10, snrdb/10);
  
  if( ISSTOCHASTICDECODER(conf.getMsgType()) ) {
    tl.addExperimentStoch(No, 0, 0, 0, conf.getMaxTrials(), repeat,
                          conf.getStochNDS() );
  } else {
    tl.addExperiment(No, 0, 0, 0, conf.getMaxTrials(), repeat);
  }

  for(int i=0; i < repeat; i++) {
    try {
      DecoderTest dt(n, k, Hm, G, H, conf);

      dt.run( *(tl.next()) );

    } catch(Exception& e) {
      cerr << e << endl;
    }
  }

  tl.writeResults();

}

void redecodeTestRun(int n, int k, int Hm, BitBlock* G, BitBlock* H, 
                     Config& conf, ofstream& ofs) {
 
  double snrdb = conf.getSnrStart();
  double No = 1 / pow(10, snrdb/10);

  try {
    DecoderTest dt(n, k, Hm, G, H, conf);
    dt.writeRedecodeProbs(ofs, No);
  } catch(Exception& e) {
    cerr << "Exception caught in redecodeTestRun(...)" << endl;
    cerr << e << endl;
  }
}

int chainTestRun(Config& conf, ofstream& ofs, bool doLLRPMF) {

	try {
		ChainTest chain(conf);
	
		// calculate nb of points (nb of experiments)
		// round values to 6 decimals
		double snrdb_start = conf.getSnrStart();
		double snrdb_end = conf.getSnrEnd();
		double snrdb_incr = conf.getSnrIncr();
		double snrdb_start_tmp = round( snrdb_start*1000000 );
		double snrdb_end_tmp = round( snrdb_end*1000000 );
		double snrdb_incr_tmp = round( snrdb_incr*1000000 );
		int nbpoints = floor((snrdb_end_tmp - snrdb_start_tmp) / snrdb_incr_tmp) + 1;
		if(nbpoints<=0) {
			cerr << "Invalid noise stepping parameters, exiting." << endl;
			return 1;
		}
		cout << "Simulating "<<nbpoints<<" SNR points"<<endl;
		
		TaskList tl(ofs, conf);
		
		// create all Tasks
		double curSNRdB = snrdb_start; 
		for(int i=0; i < nbpoints; i++) {
			// convert SNR in dB into linear
			double No = 1 / pow(10, curSNRdB/10);
			
			tl.addExperiment(No,
			                 conf.getMinBitErrors(),
			                 0,
			                 conf.getMinTrials(),
			                 0,
			                 1 );
			
			curSNRdB+= snrdb_incr;
		}
  
		// run all Tasks in the TaskList
    while(tl.hasNext()) {
      Task* t = tl.next();
      cout << "Running task with noise power " << t->No() << endl;
      if(doLLRPMF) chain.run( *t, ofs);
      else chain.run( *t );
    }

    if(!doLLRPMF) tl.writeResults();
	} catch(Exception& e) {
    cerr << e << endl;
    return 1;
  }

	return 0;
}
