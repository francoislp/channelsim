//Author: Francois Leduc-Primeau

#include <mpi.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <string.h>
#include <string>
#include <sstream>
#include <sys/stat.h>

#include "GlobalDefs.h"
#include "Logging.hpp"
#include "util/Exception.hpp"
#include "MPITags.h"
#include "BlockCodeExperiment.hpp"
#include "DecoderTest.hpp"
#include "parsers/AListParser.hpp"
#include "parsers/AListElem.hpp"
#include "parsers/GParser.hpp"
#include "parsers/MaskParser.hpp"
#include "Config.hpp"
#include "engine/TaskList.hpp"
#include "source/ISource.hpp" // for the SRCTYPE_* constants

// Data Structures
#include <vector>

using std::endl;
using std::cout;
using std::cerr;
using std::ws;
using std::ofstream;
using std::stringstream;
using std::vector;
using std::string;

#include "compileflags.h"

// function prototypes
int  headroutine(char*, string);
int  slaveroutine(int);
void regularSlaveRun(int,int,int,BitBlock*,BitBlock*, Config&);
void decoderTestSlaveRun(int,int,int,BitBlock*,BitBlock*, Config&);
void generateLLRTasks_dyn(TaskList&, Config&, int);
void generateStochTasks_dyn(TaskList&, Config&, int);
void generateNDSSweepTasks(TaskList&, Config&);

// Print usage info and exit.
void usageExit(int exitCode) {
  cout << "Usage: channelsim --config <conf file> [<output file>]" << endl;
  exit(exitCode);
}

int main(int argc, char** argv) {
  char* configfile;
  string outfilename = "";

  MPI_Init(&argc, &argv);

  int machineIndex = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &machineIndex);

  // set error handler so that MPI doesn't abort the program on error
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);

  if(argc != 3 && argc != 4) {
    usageExit(0);
  }

  if( strcmp(argv[1], "--config") == 0 ) {
    configfile = argv[2];
  }
  else {
    usageExit(1);
  }

  if(argc == 4) {
    outfilename = argv[3];
  }
    
  // init the log file
  Logging::init(machineIndex);
  char hostname[256];
  gethostname(hostname, sizeof(hostname));
  pid_t mypid = getpid();
  ofstream* pLog = Logging::getstream();
  (*pLog) << "Running on "<<hostname<<" with pid "<<mypid << endl;


  int code = 1;
  if(machineIndex == 0) {
    try {
      // (outfilename is "" if not specified on command line)
      code = headroutine(configfile, outfilename);
      Logging::writeln("head routine completed successfully");

    } catch(Exception& e) {
      *pLog << "Catched exception at top-level:" << endl;
      *pLog << e << endl;
      cerr << "Head node catched an exception at top-level, see log file";
      cerr << endl;
      MPI_Abort(MPI_COMM_WORLD, 1);
    }    
  }
  else {
    try {
      code = slaveroutine(machineIndex);
      Logging::writeln("slave routine completed successfully");

    } catch(Exception& e) {
      *pLog << "Catched exception at top-level:" << endl;
      *pLog << e << endl;
      cerr << "Node "<<machineIndex<<" catched an exception at top-level, ";
      cerr << "see log file" << endl;
      MPI_Abort(MPI_COMM_WORLD, 1);
    }
  }

  return code;
}

int headroutine(char* configfile, string outfilename) {

  time_t rawtime;
  ofstream* pLog = Logging::getstream();

#ifdef MPIDEBUG_HEAD
  int dbgflag = 0;
  char hostname[256];
  gethostname(hostname, sizeof(hostname));
  cout << "PID "<< getpid() << " on "<< hostname <<" ready for attach"<<endl;
  while (0 == dbgflag)
    sleep(5);
#endif

  Config config;
  try {
    config.parseFile(configfile);
    if(outfilename!="") config.setOutputFilename(outfilename);
  } catch(Exception& e) {
    cerr << "Exception occured while parsing config file:" << endl;
    cerr << e << endl;
    *pLog << "Exception occured while parsing config file:" << endl;
    *pLog << e << endl;
    MPI_Abort(MPI_COMM_WORLD, 1);
    return 1;
  }
  // make sure config is happy
  if( !config.initialized() ) {
    cerr << "The configuration is not happy" << endl;
    *pLog << "The configuration is not happy" << endl;
    MPI_Abort(MPI_COMM_WORLD, 1);
    return 1;
  }

  int mpigroupsize = -1;
  MPI_Comm_size(MPI_COMM_WORLD, &mpigroupsize);
  if(mpigroupsize < 0) {
    cout << "Could not get MPI group size" << endl;
    *pLog << "Could not get MPI group size" << endl;
    MPI_Abort(MPI_COMM_WORLD, 1);
    return 1;
  }
  *pLog << "MPI group size: " << mpigroupsize << endl;

  // output file for the results
  string outfileName = config.getResultFileName();
  // check if file exists
  struct stat stFileInfo;
  int outfileIndex = 1;
  int stCode = stat(outfileName.c_str(), &stFileInfo);
  while(stCode==0) {
    // increment a suffix in the filename until it doesn't exist
    stringstream outfileName2;
    outfileName2 << config.getResultFileName() << "_" << outfileIndex;
    outfileName = outfileName2.str();
    // check if that file exists
    stCode = stat(outfileName.c_str(), &stFileInfo);
    outfileIndex++;
  }
  ofstream ofs(outfileName.c_str());
  if( ofs.fail() ) {
    cerr << "Failed to open the output file" << endl;
    *pLog << "Failed to open the result output file at ";
    *pLog << outfileName.c_str() << endl;
    MPI_Abort(MPI_COMM_WORLD, 1);
    return 1;
  }

  // Generate the tasks
  TaskList tasklist(ofs, config);
  if( config.getStochNDSSweepIncr() != 0 ) {
    generateNDSSweepTasks(tasklist, config);
  }
  else if( config.propertyExists("channel_input_file") ) { 
    // "decoder test" mode
    double snrdb = config.getSnrStart();
    double No = 1 / pow(10, snrdb/10);
  
    // create one task for each "slave" MPI process
    if( ISSTOCHASTICDECODER(config.getMsgType()) ) {
      tasklist.addExperimentStoch(No, 0, 0, 0, config.getMaxTrials(), 
                                  mpigroupsize-1, config.getStochNDS() );
      
    } else {
      tasklist.addExperiment(No, 0, 0, 0, config.getMaxTrials(), mpigroupsize-1);
    }
  }
  else if( ISSTOCHASTICDECODER(config.getMsgType()) || 
           config.getMsgType() == MSGTYPE_HDSTOCH ) {
    generateStochTasks_dyn(tasklist, config, mpigroupsize);
  } 
  else {
    generateLLRTasks_dyn(tasklist, config, mpigroupsize);
  }

  // broadcast the initialization messages
  for(int i=1; i<mpigroupsize; i++) { // (don't send to head)
    // send name of configuration file
    MPI_Send(configfile, strlen(configfile)+1, MPI_CHAR, i, TAG_INIT_CONFIGNAME,
             MPI_COMM_WORLD);
  }

  // write the (empty) result file (so user can read the header)
  tasklist.writeResults(false);
  time_t timeLastWrite = time(NULL);

  // array to keep track of which machine is computing which Task
  // note: index 0 is wasted but this simplifies indexing
  Task** machinearray = new Task*[mpigroupsize];

  // dispatch experiments to everyone (except head)
  int running = 0; //number of running tasks
  for(int i=0; (i < (mpigroupsize-1)) && tasklist.hasNext(); i++) {
    Task* curTask = tasklist.next();
#ifdef DEBUG
	*pLog << "Sending task #" << curTask->ID() << " to node #";
	*pLog << i+1 << "...";
#endif
    curTask->dispatch(i+1); // dispatch Task to machine i+1
#ifdef DEBUG
    time( &rawtime );
    *pLog << " Done! "<< ctime(&rawtime); // ctime adds a new line
#endif
    machinearray[i+1] = curTask;
    running++;
  }

  // wait for results, dispatch new experiments
  while(running>0) {
    // wait for results
    MPI_Status status;
#ifdef DEBUG
    if(running < (mpigroupsize-1)) {
      *pLog << "Manager waiting for results ("<<running<<" running)" << endl;
    }
#endif
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    int machineIndex = status.MPI_SOURCE;

    if(status.MPI_TAG == TAG_SIMRESULT) {
      Task* curTask = machinearray[machineIndex];
      try {
        curTask->receiveResult(machineIndex);
#ifdef DEBUG
        *pLog << "Received a result from node #" << machineIndex << endl;
#endif
      } catch(Exception& e) {
        *pLog << "*** Error receiving result from node #"<<machineIndex<<endl;
      }
    } 
    else if(status.MPI_TAG == TAG_SIMFAILED) {
      *pLog << "Experiment failed on node #" << machineIndex << endl;
      // receive (discard) the message
      int tmp;
      int code = MPI_Recv(&tmp, 1, MPI_INT, MPI_ANY_SOURCE, TAG_SIMFAILED,
                          MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      if(code != MPI_SUCCESS) {
        *pLog << "MPI_Recv failed in channelsimMPI.cpp:"<<(__LINE__ - 3);
        *pLog << endl;
      }
      // Abort program
      MPI_Abort(MPI_COMM_WORLD, 1);
      return 1;
    }
    else {
      *pLog << "Received invalid tag from node #"<<machineIndex << endl;
    }
    running--;
    
    // give a new task to that machine
    if(tasklist.hasNext()) {
      Task* curTask = tasklist.next();
#ifdef DEBUG
      *pLog << "Sending task #" << curTask->ID() << " to node #";
      *pLog << machineIndex << "...";
#endif
      curTask->dispatch(machineIndex);
#ifdef DEBUG
        time( &rawtime );
        *pLog << " Done! "<< ctime(&rawtime); // ctime adds a new line
#endif
      machinearray[machineIndex] = curTask;
      running++;
    }
    else { // no new task => terminate
      int tmp=0;
      MPI_Send(&tmp, 1, MPI_INT, machineIndex, TAG_TERMINATE, MPI_COMM_WORLD);
    }

    // update the result file every 30 seconds
    if( time(NULL) - timeLastWrite > 30 ) {
      tasklist.writeResults(false);
      timeLastWrite = time(NULL);
    }
  }


  // write the sorted results to the output file
  //TODO: this is not pretty
  ofs.close(); // delete the backup made by TaskList
  ofs.open(outfileName.c_str());
  if( ofs.fail() ) {
    *pLog << "Failed to open the output file for last writeup" << endl;
  }

  try{ tasklist.writeResults(); }
  catch( Exception& e ) {
    *pLog << "Exception occured while writing results: " << e << endl;
  }
  ofs.close();

  MPI_Finalize();

  return 0;
}

int slaveroutine(int machineIndex) {
  ofstream* pLog = Logging::getstream();

#ifdef MPIDEBUG_SLAVE
  int dbgflag = 0;
  char hostname[256];
  gethostname(hostname, sizeof(hostname));
  cout << "PID "<< getpid() << " on "<< hostname <<" ready for attach"<<endl;
  while (0 == dbgflag)
    sleep(5);
#endif

  // wait for initialization messages

  MPI_Status status;
  // name of config file
  MPI_Probe(MPI_ANY_SOURCE, TAG_INIT_CONFIGNAME, MPI_COMM_WORLD, &status);
  // get string size and allocate buffer
  int filename_len = 0;
  MPI_Get_count(&status, MPI_CHAR, &filename_len);
  char* configfile = new char[filename_len];
  int code = MPI_Recv(configfile, filename_len, MPI_CHAR, MPI_ANY_SOURCE, 
                      TAG_INIT_CONFIGNAME, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if(code != MPI_SUCCESS) {
    *pLog << "MPI_Recv failed in channelsimMPI.cpp:"<<(__LINE__ - 3);
    *pLog << endl;
  }

  // parse the config file ((almost) same code as in headroutine)
  Config config(machineIndex);
  try {
    config.parseFile(configfile);
  } catch(Exception& e) {
    if(machineIndex==1) {
      cerr << "Exception occured while parsing config file:" << endl;
      cerr << e << endl;
    }
    *pLog << "Exception occured while parsing config file:" << endl;
    *pLog << e << endl;
    MPI_Abort(MPI_COMM_WORLD, 1);
    return 1;
  }
  // make sure config is happy
//   if( !config.initialized() ) {
//     if(machineIndex==1) cerr << "The configuration is not happy" << endl;
//     MPI_Abort(MPI_COMM_WORLD, 1);
//     return 1;
//   } (can't do this because the output filename is not set)

  int srcType = config.getSrcType();

  *pLog << "Source type:      " << srcType << endl;
  *pLog << "Error count type: " << config.getPropertyByName<string>("error_type") << endl;

  // Load the H matrix
  std::string filenameH = config.getMatrixFileName();
  filenameH = filenameH+"_H";
  *pLog << "Reading H matrix (A-List) from " << filenameH << endl;
  AListParser parserH;
  try {
    parserH.ParseFile(filenameH);
  } catch(Exception& e) {
    if(machineIndex==1) {
      cerr << "Exception occured while parsing H matrix file:" << endl;
      cerr << e << endl;
    }
    *pLog << "Exception occured while parsing H matrix file:" << endl;
    *pLog << e << endl;
    MPI_Abort(MPI_COMM_WORLD, 1);
    return 1;
  }
  vector<AListElem> hElemList;
  parserH.GetElements(hElemList);
  int Hm = parserH.GetRowCount();
  int Hn = parserH.GetColCount();

  // create an array of (empty) BitBlocks to store the matrix
  BitBlock* H = new BitBlock[Hm];
  for(int i=0; i<Hm; i++) H[i].init(Hn);
  // set some elements to 1
  for(uint i=0; i<hElemList.size(); i++) {
    int row = hElemList[i].GetRow();
    int col = hElemList[i].GetCol();
    H[row].setbit(Hn-1-col, 1); // note: bit order is "reversed"
  }

  // code rate k/n
  int n = Hn;
  // k is taken 1st from the G matrix, 2nd from Config, 3rd using
  // Hn-Hm (assuming H is full rank)
  int k=0;

  // Load the G matrix
  GParser gp;
  BitBlock* G = NULL;
  if( srcType != SRCTYPE_ZERO ) {
    string filenameG = config.getMatrixFileName();
    filenameG = filenameG + "_G";
    *pLog << "Reading G matrix from " << filenameG << endl;

    G = gp.parseFile_block(filenameG.c_str());
    // get k from G
    k = gp.getK();
  }

  if(k==0) {
    k = config.getGk();
  }
  if(k==0) {
    *pLog << "Warning: Assuming full-rank H to determine k" << endl;
    k = Hn-Hm;
  }

  // if the code is punctured, the codeword length is not equal to the
  // number of columns in the H matrix
  try {
    string punctureFilePath = config.getPropertyByName<string>("puncture_file");
    MaskParser mp;
    mp.parseFile(punctureFilePath);
    n= mp.getWeightComp();

    // check that n >= k
    if(n < k) {
      throw Exception("channelsimMPI::main", 
                      "Invalid code length read from puncture file");
    }
  } catch(InvalidKeyException&) {}

  // sanity check for size of G and H
  if( G != NULL && gp.getN() != Hn && gp.getN() != n ) {
      *pLog << "(channelsimMPI) The matrix G has invalid dimensions" << endl;
  }

  *pLog << "H dimensions: " << Hm << " x " << Hn << endl;
  *pLog << "Code rate: (" << n << ", " << k << ")" << endl;

  if( config.propertyExists("channel_input_file") ) {
    decoderTestSlaveRun(n,k,Hm,G,H,config);
  }
  else {
    regularSlaveRun(n,k,Hm,G,H,config);
  }

  MPI_Finalize();

  // delete matrices
  delete[] H;
  delete[] G;


  return 0;
}

void regularSlaveRun(int n, int k, int Hm, BitBlock* G, BitBlock* H,
                     Config& config) {
  ofstream* pLog = Logging::getstream();
  int code;
  MPI_Status status;

  // instantiate experiment engine
  BlockCodeExperiment bce(n, k, Hm, G, H, config);

  int runcount = 0; // debug
  while(1) {
    // wait for a command message
    // get message tag (blocking)
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    if( status.MPI_TAG == TAG_TERMINATE ) {
      // get the message anyway, don't be lazy !
      int temp;
      code = MPI_Recv(&temp, 1, MPI_INT, MPI_ANY_SOURCE, TAG_TERMINATE,
                      MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      if(code != MPI_SUCCESS) {
        *pLog << "MPI_Recv failed in channelsimMPI.cpp:"<<(__LINE__ - 3);
        *pLog << endl;
      }
      Logging::writeln("Terminated");
      break;
    }
    else if( status.MPI_TAG == TAG_SIMPARAM ) {
      try {
        Task task;
        task.receive(status.MPI_SOURCE);
        
        // perform experiment
        // print Task parameters
        *pLog << "Executing task: "<< task << endl;

#ifdef _DBGSPECIAL
        // debug !
        if(runcount>0) {
          int dbgflag = 0;
          *pLog << "*** Ready for attach ***" << endl;
          while (0 == dbgflag)
            sleep(5);
        }
#endif

        bce.run(task);
        runcount++;

        // send result back to head machine (#0)
        task.sendResult(0);
        
      } catch(Exception& e) {
        cerr << e << endl;
        int tmp = 0;
        MPI_Send(&tmp, 1, MPI_INT, 0, TAG_SIMFAILED, MPI_COMM_WORLD);
      }
    }
    else { // invalid tag
      *pLog << "!!! Received invalid tag: " << status.MPI_TAG << endl;
      break;
    }
  } // end infinite loop
}

void decoderTestSlaveRun(int n, int k, int Hm, BitBlock* G, BitBlock* H,
                         Config& config) {
  ofstream* pLog = Logging::getstream();
  int code;
  MPI_Status status;

  DecoderTest dt(n, k, Hm, G, H, config);

  while(1) {
    // wait for a command message
    // get message tag (blocking)
    MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    if( status.MPI_TAG == TAG_TERMINATE ) {
      // get the message anyway, don't be lazy !
      int temp;
      code = MPI_Recv(&temp, 1, MPI_INT, MPI_ANY_SOURCE, TAG_TERMINATE,
                      MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      if(code != MPI_SUCCESS) {
        *pLog << "MPI_Recv failed in channelsimMPI.cpp:"<<(__LINE__ - 3);
        *pLog << endl;
      }
      Logging::writeln("Terminated");
      break;
    }
    else if( status.MPI_TAG == TAG_SIMPARAM ) {
      try {
        Task task;
        task.receive(status.MPI_SOURCE);
        
        // perform experiment
        // print Task parameters
        *pLog << "Executing task: "<< task << endl;

        dt.run(task);
        // send result back to head machine (#0)
        task.sendResult(0);
        
      } catch(Exception& e) {
        cerr << e << endl;
        int tmp = 0;
        MPI_Send(&tmp, 1, MPI_INT, 0, TAG_SIMFAILED, MPI_COMM_WORLD);
      }
    }
    else { // invalid tag
      *pLog << "!!! Received invalid tag: " << status.MPI_TAG << endl;
      break;
    }
  } // end infinite loop
}

void generateLLRTasks_dyn(TaskList& tasklist, Config& conf, int mpisize) {
  // calculate nb of SNR points
  // round values to 6 decimals
  double snrdb_start = conf.getSnrStart();
  double snrdb_end = conf.getSnrEnd();
  double snrdb_incr = conf.getSnrIncr();
  double snrdb_start_tmp = round( snrdb_start*1000000 );
  double snrdb_end_tmp = round( snrdb_end*1000000 );
  double snrdb_incr_tmp = round( snrdb_incr*1000000 );
  int nbpoints = floor((snrdb_end_tmp - snrdb_start_tmp) / snrdb_incr_tmp) + 1;
  if(nbpoints<=0) {
    cerr << "Invalid noise stepping parameters, exiting." << endl;
    MPI_Abort(MPI_COMM_WORLD, 1);
    exit(1);
  }
  cout << "Simulating "<<nbpoints<<" SNR points"<<endl;

  int mpiTrialMultiplier = 1;
  try {
	  mpiTrialMultiplier= conf.getPropertyByName<int>("mpi_trial_multiplier");
  } catch(InvalidKeyException&) {}
  int mpiMinTrials = 100;
  try {
	  mpiMinTrials= conf.getPropertyByName<int>("mpi_trial_min");
  } catch(InvalidKeyException&) {}

  int tasksize = mpiTrialMultiplier*mpisize;
  if(tasksize < mpiMinTrials) tasksize = mpiMinTrials;
  
  double curSNRdB = snrdb_start;
  for(int i=0; i < nbpoints; i++) {
    // convert SNR in dB into linear
    double No = 1 / pow(10, curSNRdB/10);

    tasklist.addExperiment_dyn(No, conf.getMinBitErrors(), 
                               conf.getMinWordErrors(),
                               conf.getMinTrials(),
                               tasksize);
    curSNRdB+= snrdb_incr;
  }
}

void generateStochTasks_dyn(TaskList& tasklist, Config& conf, int mpisize) {
  // calculate nb of SNR points
  // round values to 6 decimals
  double snrdb_start = conf.getSnrStart();
  double snrdb_end = conf.getSnrEnd();
  double snrdb_incr = conf.getSnrIncr();
  double snrdb_start_tmp = round( snrdb_start*1000000 );
  double snrdb_end_tmp = round( snrdb_end*1000000 );
  double snrdb_incr_tmp = round( snrdb_incr*1000000 );
  int nbpoints = floor((snrdb_end_tmp - snrdb_start_tmp) / snrdb_incr_tmp) + 1;
  if(nbpoints<=0) {
    cerr << "Invalid noise stepping parameters, exiting." << endl;
    MPI_Abort(MPI_COMM_WORLD, 1);
    exit(1);
  }
  cout << "Simulating "<<nbpoints<<" SNR points"<<endl;

  int mpiTrialMultiplier = 1;
  try {
	  mpiTrialMultiplier= conf.getPropertyByName<int>("mpi_trial_multiplier");
  } catch(InvalidKeyException&) {}
  int mpiMinTrials = 100;
  try {
	  mpiMinTrials= conf.getPropertyByName<int>("mpi_trial_min");
  } catch(InvalidKeyException&) {}
  
  int tasksize = mpiTrialMultiplier*mpisize;
  if(tasksize < mpiMinTrials) tasksize = mpiMinTrials;
    
  double curSNRdB = snrdb_start;
  for(int i=0; i < nbpoints; i++) {
    // convert SNR in dB into linear
    double No = 1 / pow(10, curSNRdB/10);
    
    tasklist.addExperimentStoch_dyn(No, 
                                    conf.getMinBitErrors(), 
                                    conf.getMinWordErrors(),
                                    conf.getMinTrials(),
                                    conf.getStochNDS(),
                                    tasksize);
    curSNRdB+= snrdb_incr;
  }
}

void generateNDSSweepTasks(TaskList& tasklist, Config& conf) {

  tasklist.setOutputFormat(OUTPUT_NDSSWEEP);

  double No = conf.getSnrStart();
  int split = conf.getPropertyByName<int>("expsplit_size");

  double start = conf.getStochNDS();
  double incr = conf.getStochNDSSweepIncr();
  double end = conf.getStochNDSSweepEnd();
  double start_tmp = round( start*1000000 );
  double incr_tmp = round( incr*1000000 );
  double end_tmp = round( end*1000000 );
  int nbpoints = floor((end_tmp - start_tmp) / incr_tmp) + 1;

  double curNDS = start;
  for(int i=0; i < nbpoints; i++) {
    tasklist.addExperimentStoch(No, conf.getMinBitErrors(), 
                                conf.getMinWordErrors(),
                                conf.getMinTrials(),
                                conf.getMaxTrials(),
                                split,
                                curNDS );
    curNDS+= incr;
  }
}
