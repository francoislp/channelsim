//Author: Francois Leduc-Primeau

#include "GlobalDefs.h"
#include "Logging.hpp"
#include "BlockCoder.hpp"
#include "util/Exception.hpp"
#include "Config.hpp"
#include "compileflags.h"
#include "parsers/MaskParser.hpp"

#include <cmath>
#include <string>

using std::string;

// See header for function descriptions

BlockCoder::BlockCoder(ISource* src, uint k, uint n, uint Hm,
                       BitBlock* G, BitBlock* H, bool saveCWs) 
  : m_k(k),
    m_n(n),
    m_Hm(Hm),
    m_G(G),
    m_H(H),
    m_psrc(src),
    m_saveCWs(saveCWs)
{
  init();
}

void BlockCoder::init() {
  // make sure that the source's bit-size divides k
  if( m_psrc->getAlphaBitsize() % m_k != 0) {
    throw Exception("k not a multiple of the source bit-size");
  }
  // allocate temp buffer
  m_bufcodeword = new char[m_n];

  Config* conf= Config::getInstance();
  if(conf==NULL) throw Exception("Ay Caramba!");
  try {
	  string infoLocFile= conf->getPropertyByName<string>("infobits_loc_file");
	  loadInfoBitData(infoLocFile);
  } catch(InvalidKeyException&) {}
  
  if(m_infoBitLocation.size() > 0) {
	  m_curSentSymbolInfo= BitBlock(m_k);
  }
}

BlockCoder::~BlockCoder() {
  delete[] m_bufcodeword;
}

void BlockCoder::setSource(ISource* src) {
  // make sure that the source's bit-size divides k
  if( src->getAlphaBitsize() % m_k != 0) {
    throw Exception("Invalid source: k not a multiple of the source bit-size");
  }

  m_psrc = src;
}


BitBlock const& BlockCoder::nextSymbol() {
  if(m_transmitQueue.empty()) encodeNext(); //throws EndOfFeedException
  BitBlock* cwOut = m_transmitQueue.front();

  if(m_saveCWs) {
    m_sentQueue.push(cwOut);
    m_transmitQueue.pop();
    return *cwOut;
  }
  else {
    m_curSymbol = *cwOut; // note: invalidates previously returned reference
    delete cwOut;
    m_transmitQueue.pop();
    return m_curSymbol;
  }
}

inline bool BlockCoder::hasNext() {
  if( !m_transmitQueue.empty() ) return true;
  return m_psrc->hasNext();
}

BitBlock const& BlockCoder::lastSentSymbol() {
  if( m_sentQueue.empty() ) 
    throw EndOfFeedException("BlockCoder::lastSentSymbol");

  return *(m_sentQueue.front());
}

BitBlock const& BlockCoder::nextSentSymbol() {
  if( m_sentQueue.empty() ) 
    throw EndOfFeedException("BlockCoder::nextSentSymbol");

  m_curSentSymbol = *(m_sentQueue.front());
  delete m_sentQueue.front();
  m_sentQueue.pop();
  return m_curSentSymbol;
}

BitBlock const& BlockCoder::nextSentInfo() {
	if(m_infoBitLocation.size()==0) {
		throw Exception("BlockCoder::lastSentInfo", "unknown info bit location");
	}

	BitBlock const& cw= nextSentSymbol();
	int j=0;
	for(int i=0; i<cw.size(); i++) {
		if(m_infoBitLocation.at(i)) m_curSentSymbolInfo.setbit(j++, cw.getbit(i));
	}
	if(j != m_k) {
		throw Exception("BlockCoder::nextSentInfo", "Invalid info bit location vect");
	}
	return m_curSentSymbolInfo;
}

void BlockCoder::encodeNext() {
  BitBlock const& srcsym = m_psrc->nextSymbol(); //throws EndOfFeedException
  
  if( m_psrc->getType() == SRCTYPE_ZERO ) {
    for(uint i=0; i < m_n; i++) m_bufcodeword[i] = 0;
  }
  else { // Multiply the source bit-vector with the generator matrix
    for(uint i=0; i < m_n; i++) {
      m_bufcodeword[i] = srcsym.dotprod(m_G[m_n-1-i]);
    }
  }

  BitBlock* b = new BitBlock(m_n, m_bufcodeword);
  m_transmitQueue.push(b);

}

// note: same code in BlockDecoder
void BlockCoder::loadInfoBitData(string filepath) {
	MaskParser mp;
	mp.parseFile(filepath);
	// make sure vector has the right length
	if(mp.getMask().size() != m_n) {
		throw Exception("BlockCoderr::loadInfoBitData", "Invalid vector length");
	}
	// make sure mask weight matches expected number of info bits
	if(mp.getWeight() != m_k) {
		throw Exception("BlockCoderr::loadInfoBitData", "Invalid number of info bits in pattern");
	}
	m_infoBitLocation= mp.getMask();
}
