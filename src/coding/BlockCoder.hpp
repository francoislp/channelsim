//Author: Francois Leduc-Primeau

#ifndef _BlockCoder_h_
#define _BlockCoder_h_

#include "source/ISource.hpp"
#include "coding/ICoder.hpp"
#include "coding/BlockDecoder.hpp" // temporary, maybe

#include <queue>
#include <fstream>

typedef unsigned int uint; 

/**
 * A block-code (Hamming, LDPC) encoder.
 */
class BlockCoder : public ICoder {
 public:

  /**
   * Creates a block-code (Hamming, LDPC) encoder for the specified
   * source. R=k/n is the code rate, where k is the size of the
   * message block and n is the size of the codeword. For simplicity
   * it is required that the k be a multiple of the source's bit-size.
   *@param src The source that will provide symbols for the encoder.
   *@param k   The number of information bits per codeword.
   *@param n   The size of transmitted codewords (not equal to the number of columns in the H matrix when some bits are punctured).
   *@param Hm  The number of rows in the H matrix.
   *@param G   The k x n code generator matrix. G[i] should return the 
   *           i-th column vector (transposed storage).
   *@param H   The parity check matrix, stored such that H[i] returns the 
   *           i-th row-vector.
   *@param saveCWs Whether to save the codewords that are computed.
   */
  BlockCoder(ISource* src, uint k, uint n, uint Hm,
             BitBlock* G, BitBlock* H, bool saveCWs);

  ~BlockCoder();


  // ----- Implements ICoder -----

  ///@see ICoder::getK()
  uint getK() {return m_k;}

  ////@see ICoder::getN()
  uint getN() {return m_n;}
  
  ///@see ICoder::setSource()
  void setSource(ISource* src);

  ///@see ICoder::getAlphaBitsize()
  int getAlphaBitsize() {return m_n;}

  /**
   * Returns the next coded source symbol, *while invalidating the
   * reference returned by the previous call*.  
   *@see ICoder::nextSymbol()
   *@throws EndOfFeedException If there are no more symbols.
   *@throws Exception          If the matrix multiplication fails (the source
   *                           symbols bit size doesn't match the code).
   */
  BitBlock const& nextSymbol();

  ///@see ICoder::hasNext()
  bool hasNext();

  /**
   * Returns the last codeword that was returned by
   * <code>nextSymbol()</code>. Doesn't influence the codewords
   * returned by <code>nextSentSymbol()</code>, although the reverse
   * is not true.
   *@throws EndOfFeedException If the sent queue is empty.
   */
  BitBlock const& lastSentSymbol();

  /**
   * Returns sequentially the codewords that were sent.
   *@throws EndOfFeedException If all the sent codewords have been retrieved.
   */
  BitBlock const& nextSentSymbol();

	BitBlock const& nextSentInfo();

  /**
   * Returns the H matrix that was passed to the constructor.
   *@return Assuming H is the return value, H[i] corresponds to the ith
   *        row-vector.
   */
  BitBlock* getH() {return m_H;}

  /// The number of rows in the H matrix (returned by getH()).
  uint getHm() {return m_Hm;}

  // ----- End ICoder -----

 private:
  /// Called by the constructors
  void init();

  /// Encodes a codeword and places it in the transmit queue.
  void encodeNext();

	/**
	 * Loads data specifying the location of info bits, and performs
	 * some checks.
	 */
	void loadInfoBitData(std::string filepath);	

  // -------- Data Members --------
	
  uint       m_k;
  uint       m_n;
  /// Number of rows in H.
  uint       m_Hm;
  BitBlock*  m_G; // generator matrix
  BitBlock*  m_H; // parity-check matrix
  ISource*   m_psrc;
  char*      m_bufcodeword; // temp buffer

  bool       m_saveCWs; // whether all the codewords should be saved

  /// Symbol (CW) currently being accessed
  BitBlock   m_curSymbol;

  /// Sent CW currently being accessed
  BitBlock  m_curSentSymbol;

	/// Sent CW currently being accessed (info bits only)
	BitBlock  m_curSentSymbolInfo;

  /// Codewords waiting to be transmitted
  std::queue<BitBlock*> m_transmitQueue;

  /// If m_saveCWs, list of all the sent codewords
  std::queue<BitBlock*> m_sentQueue;

	/// Vector indicating for each codeword bit, whether it corresponds
	/// to an info bit.
	std::vector<bool> m_infoBitLocation;	

};

#endif
