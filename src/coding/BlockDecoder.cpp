//Author: Francois Leduc-Primeau

#include "GlobalDefs.h"
#include "Logging.hpp"
#include "BlockDecoder.hpp"
#include "Node.hpp"
#include "util/Exception.hpp"
#include "stoch/StochDRE.hpp"
#include "stoch/RandomLLRFloat.hpp"
#include "stoch/RandomLLR_LFSR.hpp"
#include "stoch/RandomLLR_Cycle0Adaptor.hpp"
#include "stoch/RandomLLR_CycleAdaptor.hpp"
#include "channel/NoiseGen.hpp" // used to retrieve the RNG seed
#include "parsers/MaskParser.hpp"

#include <vector>
#include <iostream>
#include <sstream>
#include <fstream> //debug
#include <unistd.h> //debug

#include "compileflags.h"

using std::vector;
using std::set;
using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::ws;
using std::stringstream;

void printBits(int, BitBlock*); //debug

// ----- Static Data -----
// (none)

// ----- Public Methods -----

BlockDecoder::BlockDecoder(ICoder* encoder, IDemodulator* demod, Config& conf,
                           bool returnCW) 
  : m_conf(&conf),
    m_encoder(encoder),
    m_demod(demod),
    m_curIterCount(0),
    m_lastIterCount(0),
    m_lastIterCountLastTry(0),
    m_lastValid(false),
    m_iterCount(0),
    m_failedCount(0),
    m_returnCW(returnCW),
    m_pSentCW(0)
{
  m_maxIter = conf.getMaxDecodeIter();
  m_msgtype = conf.getMsgType();
  init(encoder, conf);
}

// (called by the constructors)
void BlockDecoder::init(ICoder* encoder, Config& conf) {
  ofstream* pLog = Logging::getstream();
  Config* pconf = Config::getInstance();

  // some default values
  m_redecodeMode = false;
  m_alwaysRedecode = false;
  m_noReseed = false;

  // get H from the encoder (stored as H[row][column])
  m_H = encoder->getH();
  m_Hm = encoder->getHm();
  m_vnCount = m_H[0].size();

  // code rate (k, n)
  m_k = encoder->getK();
  m_n = encoder->getN();

#if defined DECODER_COLLAYERSCHED
  m_layerCount = pconf->getPropertyByName<uint>("decoder_layercount");
  m_layerSize = ceilf(static_cast<float>(m_vnCount) / 
                      static_cast<float>(m_layerCount));
#elif defined DECODER_ROWLAYERSCHED
  m_layerCount = pconf->getPropertyByName<uint>("decoder_layercount");
  m_layerSize = ceilf(static_cast<float>(m_Hm)/ static_cast<float>(m_layerCount));
#else
  m_layerCount = 1;
  m_layerSize = m_vnCount;
#endif

  // allocate some buffers
  m_bufvarinit = new double[m_n];
  m_curCW = new BitBlock(m_n, true); // excluding punctured bits
  m_curVNHDVect = new BitBlock(m_vnCount, true); // including punctured bits
  m_infoBits= new BitBlock(m_k, false);

  // load the puncture list
  try {
    string punctureFilePath = conf.getPropertyByName<string>("puncture_file");
    loadPunctureData(punctureFilePath); // into m_punctureList
    m_puncturedCode = true;
  } catch(InvalidKeyException&) {
	  m_punctureList.resize(m_vnCount);
    for(uint i=0; i<m_vnCount; i++) m_punctureList[i] = 0;
    m_puncturedCode = false;
  }

  if(!m_returnCW) { // returning info bits => must know where they are
	  string infoLocFile= conf.getPropertyByName<string>("infobits_loc_file");
	  loadInfoBitData(infoLocFile);
  }

  // Early termination
  m_earlyTerm= true;
  try {
	  m_earlyTerm= conf.getPropertyByName<bool>("decoder_ET");
  } catch(InvalidKeyException&) {}
  
  // Check if deviations are applied
  m_applyDeviations= false;
  m_devNbIter= -1;
  string devPMFFile;
  string devVNTotPMFFile;
  m_pDevGen= 0;
  m_pDevGenTot= 0;
  try {
	  devPMFFile= conf.getPropertyByName<string>("LLRDeviation::dev_pmf_file");
	  devVNTotPMFFile= conf.getPropertyByName<string>("LLRDeviation::dev_vntotal_pmf_file");
	  m_applyDeviations= true; // if both keys above are present
	  // "LLRDeviation::nbIter" is optional
	  m_devNbIter= conf.getPropertyByName<int>("LLRDeviation::nbIter");
  } catch(InvalidKeyException&) {}

  if(m_applyDeviations) {
	  uint fractMult= conf.getPropertyByName<uint>("LLRDeviation::fract_mult");
	  uint nbErrPtsPerDec= 1000;
	  try {
		  nbErrPtsPerDec= conf.getPropertyByName<uint>("LLRDeviation::nbErrPtsPerDec");
	  } catch(InvalidKeyException&) {}

	  *pLog << "Deviations: fractMult="<<fractMult<<", nbErrPts="<<nbErrPtsPerDec;
	  *pLog << ", nbIter="<<m_devNbIter;
	  *pLog << endl;
	  
	  // create a config/config object, which is used by LLRDeviationPMF
	  // (since we are not using namespaces, mind the lowercase class
	  // name!)
	  config devConf;
	  devConf.addConfElem("LLRDeviation::dev_pmf_file", devPMFFile);
	  stringstream ssFractMult;
	  ssFractMult << fractMult;
	  devConf.addConfElem("LLRDeviation::fract_mult", ssFractMult.str());
	  stringstream ssNbPts;
	  ssNbPts << nbErrPtsPerDec;
	  devConf.addConfElem("LLRDeviation::nbErrPtsPerDec", ssNbPts.str());
	  // create a NoiseGen object to retrieve the RNG seed
	  NoiseGen tmpNG;
	  stringstream ssSeed;
	  ssSeed << tmpNG.getSeed();
	  devConf.addConfElem("LLRDeviation::seed", ssSeed.str());

	  m_pDevGen= new LLRDeviationPMF(devConf);

	  // create a second deviation generator used for the VN total
	  config devConfTot;
	  devConfTot.addConfElem("LLRDeviation::dev_pmf_file", devVNTotPMFFile);
	  devConfTot.addConfElem("LLRDeviation::fract_mult", ssFractMult.str());
	  devConfTot.addConfElem("LLRDeviation::nbErrPtsPerDec", ssNbPts.str());
	  stringstream ssSeed2;
	  ssSeed2 << (tmpNG.getSeed() + 1); //TODO: not very safe
	  devConfTot.addConfElem("LLRDeviation::seed", ssSeed2.str());

	  m_pDevGenTot= new LLRDeviationPMF(devConfTot);
  }

  // Setup the variable and check nodes
  m_varnodelist = new VariableNode*[m_vnCount];
  generateVarNodes(m_msgtype);
  
  m_chknodelist = new CheckNode*[m_Hm];

  // each row of H specifies a set of variable nodes connected to the
  // same check node
  for(uint i=0; i < m_Hm; i++) {
#ifdef DECODER_ROWLAYERSCHED
    uint curLayer = i / m_layerSize;
    if(i % m_layerSize == 0) { // entering a new layer
      set<VariableNode*> vnSet;
      m_vnLayerGroups.push_back(vnSet);
    }
#endif

    // collect all the variable nodes in the current row
    BitBlock Hrow = m_H[i];
    vector<VariableNode*> curNodeList;
    for(uint j=0; j<m_vnCount; j++) {
      if(Hrow[j]==1) {
        curNodeList.push_back(m_varnodelist[j]);
#ifdef DECODER_ROWLAYERSCHED
        m_vnLayerGroups[curLayer].insert(m_varnodelist[j]);
#endif
      }
    }

    // create a check node connected to those var nodes
    CheckNode* chknode = new CheckNode( curNodeList.size(),
                                        m_msgtype,
                                        curNodeList,
                                        this );
    // add check node to the list
    m_chknodelist[i] = chknode;

  }
  // the graph is now set-up. some post-graph-setup initialization might be required
  for(uint i=0; i<m_vnCount; i++) m_varnodelist[i]->baseInit();

  // Initialize the iteration histogram
  for(uint i=0; i<10; i++) m_itHistogram[i] = 0;
  // and the bit error histogram
  for(uint i=0; i<21; i++) {
    m_tempBErrHistogram[i] = 0;
    m_bErrHistogram[i] = 0;
  }

  // Try opening the decoding info output file
  string decInfoFileName = "";
  try {
     decInfoFileName = pconf->getOutputFileByName("decoder_infofile");
  } catch(InvalidKeyException&) {
    *pLog << "Warning: \"decoder_infofile\" not specified." << endl;
    decInfoFileName = "";
  }
  m_pDecInfoStream = 0; // default
  if( decInfoFileName != "" ) {
    m_pDecInfoStream = new ofstream(decInfoFileName.c_str());
    if(!m_pDecInfoStream->good()) {
      m_pDecInfoStream = 0;
      *pLog << "(BlockDecoder) Could not open decoding info output file ";
      *pLog << "(filename="<<decInfoFileName<<")"<< endl;
    }
  }

  // Try opening the iteration count output file
  string itCountFileName = pconf->itCountFile();
  m_pItCountStream = 0; // default
  if(itCountFileName != "") {
    m_pItCountStream = new ofstream(itCountFileName.c_str());
    if(!m_pItCountStream->good()) {
      *pLog << "(BlockDecoder) Could not open output file for iteration ";
      *pLog << "count output (filename=" << itCountFileName << ")" << endl;
      m_pItCountStream = 0;
    }
  }

  // get the re-decode property from the config
  m_maxDecodeCount = 1;
  try {
    m_maxDecodeCount = conf.getPropertyByName<int>("decoder_trialcount");
  } catch(InvalidKeyException&) {
    *pLog << "Warning: Defaulting to \"decoder_trialcount\" = 1" << endl;
  }

  m_redecodeMaxIter = m_maxIter;
  if( m_maxDecodeCount > 1 ) {
    try {
      m_redecodeMaxIter = 
        conf.getPropertyByName<int>("decoder_redecode_max_iter");
    } catch(InvalidKeyException&) {
      *pLog << "Warning: Defaulting to \"decoder_redecode_max_iter\" = ";
      *pLog << m_maxIter << endl;
    }
  }

  // m_maxIterTotal is the total iteration budget, which doesn't
  // necessarily need to correspond to the "max_decode_iter" property
  // (but now it does)
  m_maxIterTotal = m_maxIter;

  // Load constants for trap detection
  m_doTrapDetect = false;
  try {
    m_doTrapDetect = conf.getPropertyByName<bool>("decoder_trapdetect_enable");
    m_doTrapDetect = m_doTrapDetect && (m_maxDecodeCount > 1);
  } catch(InvalidKeyException&) {}
#ifndef DECODER_RANDOMRESET
  if( m_doTrapDetect ) {
#endif
    // mandatory items:
    m_trapCheckThreshold = conf.getPropertyByName<int>("decoder_trap_check_threshold");
    m_trapStableCount = conf.getPropertyByName<int>("decoder_trap_stable_count");
    m_trapDetectionPeriod = conf.getPropertyByName<int>("decoder_trap_detection_period");
    // optional:
    try {
      m_trapCheckThresholdMin = conf.getPropertyByName<int>("decoder_trap_check_threshold_min");
    } catch(InvalidKeyException&) {
      *pLog << "Warning: Using decoder_trap_check_threshold_min = 1" << endl;
      m_trapCheckThresholdMin = 1;
    }
    m_trapDetectionOnlyOnce = false;
    try {
      m_trapDetectionOnlyOnce = conf.getPropertyByName<bool>("decoder_trap_detection_onceonly");
    } catch(InvalidKeyException&) {
      *pLog << "Warning: Using decoder_trap_detection_onceonly = false" <<endl;
    }
#ifndef DECODER_RANDOMRESET
  }
#endif

  // Check if "dithering" mode (always randomize) is turned on
  try {
    setAlwaysRedecode(conf.getPropertyByName<bool>("decoder_dithering"));
  } catch(InvalidKeyException&) {
    *pLog << "Warning: Assuming decoder_dithering = false" << endl;
  }

  // Get iteration index at which to start logging the errors in the decoder
  try {
    m_errorLogIterIndex = conf.getPropertyByName<uint>("log_errors_at_iter");
  } catch(InvalidKeyException&) {
    m_errorLogIterIndex = m_maxIterTotal+1; // (deactivated)
  }

  // Post-Processing Algorithms
  try {
    m_doBerkeleyPP = conf.getPropertyByName<bool>("decoder_berkeleyPP");
  } catch(InvalidKeyException&) {
    m_doBerkeleyPP = false;
  }
  bool doVNHarmonization = false;
  try {
    doVNHarmonization = conf.getPropertyByName<bool>("decoder_vnharmonization");
  } catch(InvalidKeyException&) {}
  // make sure that only one post-processing algorithm is activated
  if(m_doBerkeleyPP && doVNHarmonization) {
    throw Exception("BlockDecoder::init", "Only one post-processing algorithm can be used at a time");
  }

  bool explicitNoReset = false;
  try {
    explicitNoReset = conf.getPropertyByName<bool>("redecode_noreset");
  } catch(InvalidKeyException&) {}

  // do not reset the decoder after Phase-I
  if(m_doBerkeleyPP || doVNHarmonization || explicitNoReset) {
    m_noRedecodeReset = true;
  } else {
    m_noRedecodeReset = false;
  }

  // (RHS decoder) "Gear-shift scaling" feature
  try {
    m_rhsGSScaleIter = pconf->getPropertyByName<int>("rhs_statescale_iter_1");
    m_rhsGSScaling = pconf->getPropertyByName<float>("rhs_statescale_factor_1");
  } catch(InvalidKeyException&) {
    *pLog << "Warning: Key \"rhs_statescale_iter_1\" not found." << endl;
    m_rhsGSScaleIter = -1;
    m_rhsGSScaling = 0;
  }
  // (RHS decoder) "State Normalization" feature
  try {
    m_rhsStateNormIter = pconf->getPropertyByName<int>("rhs_statenorm_iter_1");
  } catch(InvalidKeyException&) {
    m_rhsStateNormIter = -1;
  }
}

BlockDecoder::~BlockDecoder() {
  // delete variable nodes
  for(uint k=0; k < m_vnCount; k++) {
    delete m_varnodelist[k];
  }
  // delete check nodes
  for(uint k=0; k < (m_Hm); k++) {
    delete m_chknodelist[k];
  }
  delete[] m_varnodelist;
  delete[] m_chknodelist;

  delete[] m_bufvarinit;
  delete m_curVNHDVect;
  delete m_curCW;
  delete m_infoBits;

  // delete StochDRE objects
  for(uint i=0; i<m_dreList.size(); i++) delete m_dreList[i];

  if(m_pDecInfoStream != 0) m_pDecInfoStream->close();
  delete m_pDecInfoStream;

  if(m_pItCountStream != 0) m_pItCountStream->close();
  delete m_pItCountStream;

  if(m_pSentCW != 0) delete m_pSentCW;

  if(m_pDevGen != 0) delete m_pDevGen;
  if(m_pDevGenTot != 0) delete m_pDevGenTot;
}

// Decodes the next symbol
BitBlock const& BlockDecoder::nextSymbol() {

  // retrieve all the bits in the block
  uint size=0;
  // retrieve the channel output
  if( ISSTOCHASTICDECODER(m_msgtype) ) {
#ifdef STOCH_USELLR
    size = m_demod->nextLLR(m_n, m_bufvarinit);
#else
    size = m_demod->nextValues(m_n, m_bufvarinit);
#endif
  }
  else if(m_msgtype == MSGTYPE_LR) size = m_demod->nextLR(m_n, m_bufvarinit);
  // m_msgtype == MSGTYPE_LLR or MSGTYPE_HDSTOCH
  else size = m_demod->nextLLR(m_n, m_bufvarinit);

  // make sure we have the required number of bits
  if( size!=m_n ) {
    std::ostringstream ss;
    ss << "Insufficient number of bits returned by demodulator (expected ";
    ss << m_n << ", got " << size << ")";
    throw Exception(ss.str());
  }

  // remember the hard-decisions before decoding
  m_inputHD= m_demod->lastHD();

  // try to get the actual transmitted codeword
  bool haveSentCW = true;
  try {
    if(m_pSentCW != 0) delete m_pSentCW;
    //note: this call must be made after the call to the demod (which
    //calls the channel which calls the encoder)
    m_pSentCW = new BitBlock(m_encoder->lastSentSymbol()); //TODO: not clear why we 
                                                         //need to make a copy of this
  } catch(EndOfFeedException&) {
    haveSentCW = false;
  }

  if( m_applyDeviations ) {
	  // turn on deviations in each VN, and inform each VN of its tx bit
	  // so it can sample the proper deviation
	  BitBlock const& curTx= m_encoder->lastSentSymbol();
	  for(uint i=0; i < m_vnCount; i++) {
		  m_varnodelist[i]->applyDeviations(m_pDevGen);
		  m_varnodelist[i]->applyTotalDeviations(m_pDevGenTot);
		  if( m_punctureList[i] ) {
			  throw Exception("BlockDecoder::nextSymbol",
			                  "puncturing not supported with deviations");
		  }
		  m_varnodelist[i]->devSetTxBit(curTx[i]);
	  }
  }  

  // reset the decoder RNG unless we're in the mode where we don't do that
  if(!m_noReseed) {
    // (stochastic decoders, and possibly others) reset the decoder RNG
    for(uint i=0; i<m_dreList.size(); i++) m_dreList[i]->reset();

    // reset the "random()" RNG
    //TODO: obviously this is bad, as we have no guarantee this RNG is only used in the decoder
    srandom(1); // (default seed)
  }

  // Initialize the result vector (in case the decoder performs 0 iterations)
  uint j=0;
  for(uint i=0; i < m_vnCount; i++) {
	  if( !m_punctureList[i] ) {
		  double initval = m_bufvarinit[j++];
		  if(initval < 0) m_curVNHDVect->setbit(i,1);
		  else            m_curVNHDVect->setbit(i,0);
	  } else { // this vn is punctured
		  m_curVNHDVect->setbit(i,0);
	  }
  }
        
  int itercount;
  int totalitercount=0; // total for all decoding attempts
  bool valid = false;
  uint trials;
  for(trials=0; 
      totalitercount<m_maxIter && trials<m_maxDecodeCount && !valid; 
      trials++) {

    // Max number of iterations for the current trial
    int curMaxIter = m_maxIter;
    if(m_maxDecodeCount>1) {
      if(trials==0 && m_doTrapDetect) curMaxIter = m_maxIter;
      // always use the redecoding max iter if not performing trap detection
      else curMaxIter = m_redecodeMaxIter;
      // make sure we don't go over budget !
      if( (curMaxIter+totalitercount) >= m_maxIter ) {
        curMaxIter = m_maxIter - totalitercount;
      }
    }

    // The algorithms might have special modes for redecoding trials
    if(trials==0 && !m_alwaysRedecode) m_redecodeMode = false;
    else m_redecodeMode = true;

    bool curDoTrapDetect = m_doTrapDetect;
    if(m_trapDetectionOnlyOnce && trials>0) curDoTrapDetect = false;

    // don't reset the decoder after the first trial when using the
    // "post-processing" algorithm
    bool doDecoderReset = true;
    if(trials>0 && m_noRedecodeReset) doDecoderReset = false;

    // Run the decoder
    valid = decoderLoop(curMaxIter, &itercount, 
                        haveSentCW, 
                        m_errorLogIterIndex,
#ifdef DECODER_RANDOMRESET
                        true, // always perform trap detect in this case
#else
                        curDoTrapDetect,
#endif
                        doDecoderReset);

    totalitercount+= itercount;

    // post-processing: perform an operation after the first trial
    // then continue decoding without re-setting the decoder
    if(m_doBerkeleyPP) {
      for(uint k=0; k < m_Hm; k++) {
        CheckNode* curChk = m_chknodelist[k];
        // unsat. CNs send a "true" token to neighbor VNs
        curChk->binaryTokenBcst(curChk->curParity() != 0);
      }
      for(uint k=0; k < m_vnCount; k++) {
        m_varnodelist[k]->berkeleyPPInit();
      }
      for(uint k=0; k < m_Hm; k++) {
        m_chknodelist[k]->broadcast();
      }
      for(uint k=0; k < m_vnCount; k++) {
        m_varnodelist[k]->berkeleyPPSend();
      }
      for(uint k=0; k < m_Hm; k++) {
        m_chknodelist[k]->broadcast();
      }
      //note: these iterations are not counted
    }

  } // LOOP: decoding attempts
  m_lastIterCount = totalitercount;
  m_lastIterCountLastTry = itercount;
  m_iterCount+= totalitercount; // cumulative iteration count
  m_lastValid = valid;
  if(valid) addToItHist(totalitercount); // add to iteration histogram
  if(!valid) m_failedCount++; // cumulative failed CW count
  // log iteration count to a file
  if(m_pItCountStream != 0) {
    *m_pItCountStream << "No=" << m_demod->getNo() << ", ";
    if(!valid) *m_pItCountStream << "x";
    *m_pItCountStream << totalitercount << endl;
  }
#ifdef DECODER_BERHIST
  // add current bit error histogram to the cumulative one
  for(uint i=0; i<21; i++) {
    m_bErrHistogram[i]+= m_tempBErrHistogram[i];
    m_tempBErrHistogram[i] = 0;
  }
#endif

  if(valid && trials>1 && m_pDecInfoStream != 0) {
    *m_pDecInfoStream << "#(No="<< m_demod->getNo()<<") ";
    *m_pDecInfoStream << "CW decoded after "<< trials << " tries";
    *m_pDecInfoStream << " (last try-> " << itercount<<" iter)"<< endl;
  }

#ifdef DEBUG2 // ---------- DEBUG2 section ----------
    std::ofstream* pLog = Logging::getstream();
    if(!valid) { // iteration limit reached but codeword not valid
      *pLog << "No codeword match after " << itercount <<" iterations" << endl;
    } 
    else {
      *pLog << "Codeword decoded after " << itercount << " iterations" << endl;
    }
    *pLog << "   ";
    printBits(m_n, decodedTxBits());

#endif // ---------- End DEBUG2 section ----------

  BitBlock* returnVect;
  if( m_returnCW ) {
    returnVect= decodedTxBits();
  }
  else {
	  returnVect= infoBits();
  }

  return *returnVect;
}

void BlockDecoder::setStochNDSCst(double val) {
  if( !ISSTOCHASTICDECODER(m_msgtype) ) return;
  for(uint i=0; i<m_n; i++) {
    m_varnodelist[i]->setStochNDS(val);
  }
}

int* BlockDecoder::getItHistogram() {
  // make a copy of the histogram
  int* hist = new int[10];
  for(uint i=0; i<10; i++) {
    hist[i] = m_itHistogram[i];
    m_itHistogram[i] = 0;
  }

  return hist;
}

u64_t* BlockDecoder::getBErrHistogram() {
  // make a copy of the histogram
  u64_t* hist = new u64_t[21];
  for(uint i=0; i<21; i++) {
    hist[i] = m_bErrHistogram[i];
    m_bErrHistogram[i] = 0;
  }

  return hist;
}

void BlockDecoder::printFailureStats(BitBlock const& txCW) {
  if( m_pDecInfoStream == 0 ) return;

  // indexes of failed bits
  vector<int> failedbitsList;
  txCW.errorLoc(decodedTxBits(), failedbitsList);

  vector<int> inputErrList;
  txCW.errorLoc(m_inputHD.data(), inputErrList);

  if(m_lastValid) {
    *m_pDecInfoStream<<"#(No="<<m_demod->getNo()<< ") Undetected error!"<<endl;
  }
  printFailedBits(m_pDecInfoStream, inputErrList, 0);
  printFailedBits(m_pDecInfoStream, failedbitsList, m_lastIterCountLastTry);

  if( ISSTOCHASTICDECODER(m_msgtype) ) {
    // value of output counters
    *m_pDecInfoStream << "#(No="<<m_demod->getNo()<< ") Reliabilities: ";
    for(uint j=0; j<failedbitsList.size(); j++) {
      int index = failedbitsList[j];
      IVNStoch* vn = static_cast<IVNStoch*>(m_varnodelist[index]->getImplementation());
      *m_pDecInfoStream << vn->curReliability() << ", ";
    }
    *m_pDecInfoStream << endl;
  }
}

// Private:

// Initializes m_varnodelist
void BlockDecoder::generateVarNodes(int msgtype) {

  // Have the option for VNs to share their RNGs with previous ones
  uint rngShareCount = 1; // default
  try {
    rngShareCount = m_conf->getPropertyByName<uint>("stoch_rn_share_count");
  } catch(InvalidKeyException&) {}

  // only one of these is actually used:
  vector<IStochDRE*> curDreListStoch; // used for non-RHS stoch decoders
  vector<IRandomLLR*> curDreListRHS; // used for RHS decoders

  // Is this a RHS decoder?
  bool isRHS = false;
  try {
    if(m_conf->getPropertyByName<string>("decoder_subtype") == "half-stochastic") {
      isRHS = true;
    }
  } catch(InvalidKeyException&) {}
  // If it is a RHS decoder, which DRE type are we using?
  bool usingHWDRE = false;
  bool usingHWFloorDist = false;
  if(isRHS) {
    try {
      string dreType = m_conf->getPropertyByName<string>("stoch_dretype");
      if(dreType == "float") usingHWDRE = false;
      else if(dreType == "hwlfsr") {
        usingHWDRE = true;
        usingHWFloorDist = false;
      } else if(dreType == "hwlfsr_floor") {
        usingHWDRE = true;
        usingHWFloorDist = true;
      }
      else {
        throw Exception("BlockDecoder::generateVarNodes", "Invalid DRE type");
      }
    } catch(InvalidKeyException&) {}
  }

  int curSeed = 0; // for a special case only
  vector<IRandomLLR*> baseDreList; // also a special case
  vector<IRandomLLR*> baseDreList2;
  vector<IRandomLLR*> baseDreList3;
  // for each variable node
  for(uint i=0; i < m_vnCount; i++) {

    // compute its degree
    uint degree = 0;
    for(uint j=0; j < (m_Hm); j++) {
      degree+= m_H[j][i];
    }

    //Note: VariableNode() constructors vary depending on the decoder type.

    // Build a DRE list if the node type needs it.
    // However skip this if the current VN is sharing its RNGs with the 
    // previous one.
    if( (i%rngShareCount) == 0 &&
        (ISSTOCHASTICDECODER(msgtype) || msgtype == MSGTYPE_HDSTOCH 
         || msgtype == MSGTYPE_HDMC) ) {

      uint drelistSize = 0;
      if(isRHS) curDreListRHS.clear();
      else curDreListStoch.clear();

      // Determine the number of DRE objects to pass (to one VN)
#ifdef MULTIPLE_DRE // we're assuming this flag is only used for
                    // specific classes
      if( msgtype == MSGTYPE_PARALSTOCH ) {
        uint msg_bitwidth = m_conf->getPropertyByName<uint>("stoch_parallel_width");
        drelistSize = degree+1+2*msg_bitwidth;
      }
      else {
        drelistSize = degree+3; //TODO: this is really for degree 6 VNs
      }
#else
      drelistSize = 1;
      try {
        drelistSize = m_conf->getPropertyByName<uint>("stoch_parallel_width");
      } catch(InvalidKeyException&) {}
#ifdef VNSTOCHB_SIGNEDMSG
      //drelistSize--; // the first message bit is deterministic
      // second msg gen rule uses additionnal thresholds
      if(drelistSize==4) drelistSize = 5;
      else {
        throw Exception("BlockDecoder::generateVarNodes", 
                        "Unsupported case at line 638");
      }
#endif
#endif

      // populate the DRE list
      if(isRHS) {
        if(usingHWDRE) {
#ifdef VNSTOCHB_SIGNEDMSG
          IRandomLLR* basedre;
          IRandomLLR* dreinst;
          IRandomLLR* dreinst2;
          IRandomLLR* dreinst3;
          if(i >= m_layerSize) {
            // after first layer, re-use existing RNGs
            basedre = baseDreList[(i/rngShareCount) % baseDreList.size()];
            IRandomLLR* basedre2 = baseDreList2[(i/rngShareCount) 
                                                % baseDreList2.size()];
            IRandomLLR* basedre3 = baseDreList3[(i/rngShareCount) 
                                                % baseDreList3.size()];
            // the first RNG does not represent the first clock cycle in this case
            dreinst = new RandomLLR_CycleAdaptor(basedre);
            curDreListRHS.push_back(dreinst);
            m_dreList.push_back(dreinst);
            dreinst2 = new RandomLLR_CycleAdaptor(basedre2);
            dreinst3 = new RandomLLR_CycleAdaptor(basedre3);
          } else { // create a new RNG
            //note: (TEMP) use a lfsr length of 10 even though we only need 9
            //      because the seeds are programmed so that the first
            //      threshold is 1 or 2 (configurable), rather than 0.
            basedre = new RandomLLR_LFSR(LfsrPrng::FIBO, 10, curSeed,
                                         usingHWFloorDist, true);
            baseDreList.push_back(basedre);
            IRandomLLR* basedre2 = new RandomLLR_LFSR(LfsrPrng::FIBO, 10, curSeed,
                                                      false, true, 1);
            baseDreList2.push_back(basedre2);
            IRandomLLR* basedre3 = new RandomLLR_LFSR(LfsrPrng::FIBO, 10, curSeed,
                                                      false, true, 2);
            baseDreList3.push_back(basedre3);
            curSeed++;
            // the first clock cycle is a special case
            dreinst = new RandomLLR_Cycle0Adaptor(basedre);
            curDreListRHS.push_back(dreinst);
            m_dreList.push_back(dreinst);
            dreinst2 = new RandomLLR_Cycle0Adaptor(basedre2);
            dreinst3 = new RandomLLR_Cycle0Adaptor(basedre3);
          }

          // two more RNGs using 1st distribution
          dreinst = new RandomLLR_CycleAdaptor(basedre);
          curDreListRHS.push_back(dreinst);
          m_dreList.push_back(dreinst);
          dreinst = new RandomLLR_CycleAdaptor(basedre);
          curDreListRHS.push_back(dreinst);
          m_dreList.push_back(dreinst);
          // RNG with 2nd distribution
          curDreListRHS.push_back(dreinst2);
          m_dreList.push_back(dreinst2);
          // RNG with 3rd distribution
          curDreListRHS.push_back(dreinst3);
          m_dreList.push_back(dreinst3);
#else
          IRandomLLR* basedre;
          if(usingHWFloorDist) {
            basedre = new RandomLLR_LFSR(LfsrPrng::FIBO, 10, curSeed++, true);
          } else {
            basedre = new RandomLLR_LFSR(LfsrPrng::FIBO, 9, curSeed++, false);
          }
          // the first "cycle" needs a special adaptor
          IRandomLLR* dreinst = new RandomLLR_Cycle0Adaptor(basedre);
          curDreListRHS.push_back(dreinst);
          m_dreList.push_back(dreinst);
          for(uint j=1; j<drelistSize; j++) {
            // Other DREs represent the clk cycle after the previous
            // DRE in the list
            dreinst = new RandomLLR_CycleAdaptor(basedre);
            curDreListRHS.push_back(dreinst);
            m_dreList.push_back(dreinst);
          }
#endif
        } else {
          for(uint j=0; j<drelistSize; j++) {
#ifdef VNSTOCHB_SIGNEDMSG
            IRandomLLR* dreinst;
            if(j<=2)      dreinst = new RandomLLRFloat(0.25);
            else if(j==3) dreinst = new RandomLLRFloat(0.0001);
            else          dreinst = new RandomLLRFloat(0.00001);
#else
            IRandomLLR* dreinst = new RandomLLRFloat();
#endif
            curDreListRHS.push_back(dreinst);
            m_dreList.push_back(dreinst); // keep a pointer to each object
          }
        }
      } else { // not a RHS decoder
        for(uint j=0; j<drelistSize; j++) {
          IStochDRE* dreinst = new StochDRE();
          curDreListStoch.push_back(dreinst);
          m_dreList.push_back(dreinst); // keep a pointer to each object
        }
      }
    } //end: if build DRE list

    // Create the current variable node object
    VariableNode* varnode;
    if( isRHS ) {
      varnode = new VariableNode(i, degree, curDreListRHS, this);
    }
    else if( ISSTOCHASTICDECODER(msgtype) || msgtype == MSGTYPE_HDSTOCH 
        || msgtype == MSGTYPE_HDMC) {
      varnode = new VariableNode(i, degree, msgtype, curDreListStoch, this);
    }
    else {
      varnode = new VariableNode(i, degree, msgtype, this);
    }

    // add to the list
    m_varnodelist[i] = varnode;

  } //end: loop through all variable nodes
}

bool BlockDecoder::decoderLoop(int maxIter, int* itercount,
                               bool logBitErrors, int logIter,
                               bool doTrapDetect, bool doReset) {

#ifdef DECODER_BERHIST
  // log the number of bit errors before decoding
  for(uint i=0; i<m_n; i++) {
    char curHD = 0;
    if(m_bufvarinit[i] < 0) curHD = 1;
    m_curCW->setbit(i, curHD);
  }
  int berrCnt = m_curCW->biterrorcnt(m_encoder->lastSentSymbol());
  setTempBerrHist(0, berrCnt);
#endif

  // Initialize the variable nodes
  if(doReset) {
    uint j=0;
    for(uint i=0; i < m_vnCount; i++) {
      if( !m_punctureList[i] ) {
        double initval = m_bufvarinit[j++];
        if(initval!=initval) 
          throw Exception("BlockDecoder::nextSymbol", 
                              "Invalid initial likelihood value");
      
        m_varnodelist[i]->init(initval);
      }
      else { // this vn is punctured
        m_varnodelist[i]->init(0);        
      }
    }
  }
    
  // keep processing the codeword until it matches a valid codeword,
  // or until the iteration limit has been reached
  bool valid = false;
  int minFailedChecks = m_n; // min number of failed checks seen so far
  int trappedCycleCount = 0;
  bool trapped = false;

  // stochastic: turn off training mode (so it's basically never used?)
  if( ISSTOCHASTICDECODER(m_msgtype) ) {
	  for(uint k=0; k < m_vnCount; k++) {
		  IVNStoch* vn = static_cast<IVNStoch*>(m_varnodelist[k]->getImplementation());
		  vn->setTrainingMode(false);
	  }
  }  

  // broadcast messages iteratively
#ifndef DECODER_RANDOMRESET
  for(m_curIterCount=0; m_curIterCount < maxIter && !trapped && !valid; 
      m_curIterCount++) {
#else // ---------- "Random Reset" algorithm ----------
  int resetCount = 0;
  for(m_curIterCount=0; m_curIterCount < maxIter && !valid; m_curIterCount++) {
    // randomly reset some variable nodes if we're trapped
    if(trapped) {
      minFailedChecks = m_n;
      trappedCycleCount = 0;
      trapped = false;
      resetCount++;
      if(m_pDecInfoStream!=0) {
        *m_pDecInfoStream << "Resetting some VNs" << endl;
      }
      for(uint k=0; k < m_n; k++) {
        if( (random() & 1) == 0 ) {
          VariableNodeStoch_B* vn = dynamic_cast<VariableNodeStoch_B*>(m_varnodelist[k]->getImplementation());
          vn->resetState();
        }
      }
    }
#endif // ---------- End "Random Reset" algorithm ----------

#ifdef DECODER_COLLAYERSCHED
    if(m_applyDeviations) {
	    throw Exception("BlockDecoder::decoderLoop",
	                    "deviations unimplemented for this schedule");
    }
    if(m_curIterCount==0) {
      for(uint k=0; k < m_vnCount; k++) {
        m_varnodelist[k]->broadcast();
      }
    } else {
      uint partitionStart = 0;
      for(uint partIndex=0; partIndex < m_layerCount; partIndex++) {
        // Update VNs in the current partition
        for(uint k=partitionStart; 
            (k < partitionStart+m_layerSize) && k < m_vnCount;
            k++) {
          m_varnodelist[k]->broadcast();
        }
        // Update all the CNs
        for(uint k=0; k < m_Hm; k++) {
          // Note: this should not update the state of the neighboring VNs
          // apart from replacing any pending messages.
          m_chknodelist[k]->broadcast();
        }
        // Get the current hard decisions for the current VN partition
        //note: in RHS this updates the trackers
        //note: Need to do this here because otherwise the decisions could be 
        //      affected by subsequent layers, which is not allowed. (?)
        for(uint k=partitionStart; 
            (k < partitionStart+m_layerSize) && k < m_vnCount;
            k++) {
          char curHD = m_varnodelist[k]->currentDecision();
          m_curVNHDVect->setbit(k, curHD);
        }
        partitionStart+= m_layerSize;
      }
    }
#elif defined DECODER_ROWLAYERSCHED
    if(m_applyDeviations) {
	    throw Exception("BlockDecoder::decoderLoop",
	                    "deviations unimplemented for this schedule");
    }    
    uint partitionStart = 0;
    for(uint l=0; l < m_vnLayerGroups.size(); l++) { // loop through row layers
      // update VNs connected to CNs in the current layer
      set<VariableNode*> curVNGroup = m_vnLayerGroups[l];
      set<VariableNode*>::iterator it;
      for(it=curVNGroup.begin(); it!=curVNGroup.end(); it++) {
        (*it)->broadcast();
      }
      // update CNs in the current layer
      for(uint k=partitionStart; 
          (k < partitionStart+m_layerSize) && (k < m_Hm); k++) {
        m_chknodelist[k]->broadcast();
      }
      partitionStart+= m_layerSize;
    }
    // get the hard decision at the end of the iteration (same code as #else case)
    for(uint j=0; j<m_vnCount; j++) {
      char curHD = m_varnodelist[j]->currentDecision();
      m_curVNHDVect->setbit(j, curHD);
    }
#else // Flooding Schedule (aka fully-parallel schedule)

    // turn off deviations if necessary
    if(m_curIterCount==m_devNbIter) {
	    // turn off deviations in variable nodes (we've reached the part
	    // of the iterations that should be reliable)
	    for(uint j=0; j<m_vnCount; j++) {
		    m_varnodelist[j]->applyDeviations(0);
		    m_varnodelist[j]->applyTotalDeviations(0);
	    }
    }    
    
    // variable nodes send messages to check nodes (deviations are
    // applied, except on first iteration)
    for(uint k=0; k < m_vnCount; k++) {
      m_varnodelist[k]->broadcast();
    }

    // check nodes send messages to variable nodes
    int cnFailCount = 0; // count number of failed checks
    for(uint k=0; k < (m_Hm); k++) {
      if( doTrapDetect && m_chknodelist[k]->curParity() != 0 ) cnFailCount++;

      m_chknodelist[k]->broadcast();
    }

    // Update the p_e parameter of the deviation generator (if in use)
    if(m_applyDeviations &&
       (m_devNbIter<0 || m_curIterCount < (m_devNbIter-1))) {
	    // compute current average error rate *of VN->CN messages*
	    for(uint j=0; j<m_vnCount; j++) {
		    char prevExtrDecision= m_varnodelist[j]->samplePrevExtrinsicDecision();
		    // using the same buffer as for decisions (should not hurt
		    // caching performance since content is similar)
		    m_curVNHDVect->setbit(j, prevExtrDecision);
	    }
	    if(m_puncturedCode) throw Exception("BlockDecoder::decoderLoop", "deviations + punctured code not supported");
	    //Note: To support punctured codes, could compare the estimates with a vector that includes the known values of the punctured bits.
	    
	    BitBlock const& curSent= m_encoder->lastSentSymbol();
	    double errCount= m_curVNHDVect->biterrorcnt(curSent);
	    
	    // set error rate parameter of the deviation generators
	    double errRate= errCount / m_vnCount;
	    m_pDevGen->setInputErrorRate(errRate);
	    m_pDevGenTot->setInputErrorRate(errRate);
    }
    
    // get the current hard decisions (deviations are applied)
    for(uint j=0; j<m_vnCount; j++) {
#ifdef DECODER_EXTRINSIC_OUTPUT
	    char curHD = m_varnodelist[j]->sampleExtrinsicDecision();
#else
      char curHD = m_varnodelist[j]->currentDecision();
#endif
      m_curVNHDVect->setbit(j, curHD);
    }
#endif // DECODER_COLLAYERSCHED

    if(m_earlyTerm) {
#ifndef DECODER_HWEARLYTERM
	    // check if codeword is valid (syndrome checking)
	    //TODO: Might be necessary to avoid checking in the first
	    //      iteration when using DECODER_COLLAYERSCHED?
	    valid = checkCodeword(*m_curVNHDVect);
#elif defined VNSTOCHB_SIGNEDMSG // "Speculative Early Termination"
	    valid = true;
	    for(uint j=0; j < m_Hm; j++) {
		    // check sign bit of total parity
		    if((m_chknodelist[j]->curParity() & 1) != 0) valid = false;
	    }
#else // emulation of possible HW ET mechanism
	    // check if all CNs have even parity
	    valid= true;
	    for(uint j=0; j < m_Hm; j++) {
		    if(m_chknodelist[j]->curParity() != 0) valid= false;
	    }
#endif
    }

#ifdef DECODER_BERHIST
    // set the current number of bit errors in the temp bit error histogram
    berrCnt = decodedTxBits()->biterrorcnt(m_encoder->lastSentSymbol());
    setTempBerrHist(m_curIterCount+1, berrCnt);
#endif

    // stuff to be done on the clock transition (only useful for the
    // stochastic decoder)
    if(((m_curIterCount+1) % RNDVAL_HOLD) == 0) {
      nextTick();
    }

    // log the current bit errors
    if( m_pDecInfoStream!=0 && logBitErrors && m_pSentCW != NULL 
        && !valid && (m_curIterCount >= logIter) ) {
      vector<int> failedbitsList;
      m_pSentCW->errorLoc(decodedTxBits(), failedbitsList);
      printFailedBits(m_pDecInfoStream, failedbitsList, m_curIterCount);
      printUnsatCN(m_pDecInfoStream, m_curIterCount);
    }

#if !defined(DECODER_COLLAYERSCHED) && !defined(DECODER_ROWLAYERSCHED)
    //          (don't want to bother supporting this for layered schedules)
    if(!valid && doTrapDetect) {
      // keep track of the min number of failed checks
      if(cnFailCount < minFailedChecks) {
        minFailedChecks = cnFailCount;
        trappedCycleCount = 0;
      }
      // count the number of cycles the minimum has been stable and
      // in the range of interest
      else if(minFailedChecks <= m_trapCheckThreshold &&
              minFailedChecks >= m_trapCheckThresholdMin) {
        trappedCycleCount++;
      }
      // give up if we think we're trapped
      if(trappedCycleCount >= m_trapStableCount && 
         m_curIterCount < m_trapDetectionPeriod) {
        if(m_pDecInfoStream!=0) {
          *m_pDecInfoStream <<"Giving up at "<<(m_curIterCount+1)<< " iterations ";
          *m_pDecInfoStream <<"with "<<minFailedChecks<<" failed checks"<<endl;
        }
        trapped = true;
      }
    }
#endif

    // (RHS) Scale state if turned on
    if(m_curIterCount==(m_rhsGSScaleIter-1) && !valid) {
      for(uint k=0; k < m_vnCount; k++) {
        VariableNodeStoch_B* rhsVN = dynamic_cast<VariableNodeStoch_B*>(m_varnodelist[k]->getImplementation()); // returns NULL pointer if cast fails
        rhsVN->scaleState(m_rhsGSScaling);
      }
    }

    // (RHS) Normalize state if turned on
    if(m_curIterCount==(m_rhsStateNormIter-1) && !valid) {
      for(uint k=0; k < m_vnCount; k++) {
        VariableNodeStoch_B* rhsVN = dynamic_cast<VariableNodeStoch_B*>(m_varnodelist[k]->getImplementation()); // returns NULL pointer if cast fails
        rhsVN->normalizeState();
      }
    }

#ifdef DECODER_RANDOMRESET
    if(valid && resetCount>0 && m_pDecInfoStream!=0) {
      *m_pDecInfoStream << "Successful decode after "<<resetCount;
      *m_pDecInfoStream << " resets and "<<(m_curIterCount+1)<<" iterations"<< endl;
    }
#endif

  } // --- End decoding iterations loop ---

  // return iteration count
  *itercount = m_curIterCount; //TODO: since iter count is now a
                               //member this mechanism is not required
                               //anymore

  // if early termination is turned off, we haven't checked whether
  // the output vector is a valid codeword yet
  if(!m_earlyTerm) {
#ifndef DECODER_HWEARLYTERM
	    // check if codeword is valid (syndrome checking)
	    valid = checkCodeword(*m_curVNHDVect);
#elif defined VNSTOCHB_SIGNEDMSG // "Speculative Early Termination"
	    valid = true;
	    for(uint j=0; j < m_Hm; j++) {
		    // check sign bit of total parity
		    if((m_chknodelist[j]->curParity() & 1) != 0) valid = false;
	    }
#else // emulation of possible HW ET mechanism
	    // check if all CNs have even parity
	    valid= true;
	    for(uint j=0; j < m_Hm; j++) {
		    if(m_chknodelist[j]->curParity() != 0) valid= false;
	    }
#endif
  }
  
  return valid;
}

bool BlockDecoder::checkCodeword(BitBlock& codeword) {
  // perform all the parity checks
  for(uint i=0; i<(m_Hm); i++) {
    BitBlock& check = m_H[i];
    if( check.dotprod(codeword) != 0) return false;
  }

  return true;
}

// Takes care of things that need to be updated on the clock
// (stochastic decoder).
void BlockDecoder::nextTick() {
  for(uint i=0; i<m_dreList.size(); i++) m_dreList[i]->nextTick();
}

void BlockDecoder::addToItHist(int itVal) {
  int index;
  if(itVal == m_maxIterTotal) index = 9;
  else {
    float binsize = static_cast<float>(m_maxIterTotal) / 10; // size of a bin
    index = floor(static_cast<float>(itVal) / binsize);
  }
  m_itHistogram[index]++;
}

void BlockDecoder::setTempBerrHist(uint itIndex, uint berrCount) {
  float binsize = static_cast<float>(m_maxIterTotal) / 20;
  uint binIndex = ceil(static_cast<float>(itIndex) / binsize);
  m_tempBErrHistogram[binIndex] = berrCount;
}

void printBits(int n, BitBlock* array) {
  std::ofstream* pLog = Logging::getstream();
  array->printbits(*pLog);
  *pLog << endl;
}

void BlockDecoder::loadPunctureData(string filepath) {
	MaskParser mp;
	mp.parseFile(filepath);
	// make sure puncturing vector has the right length
	if(mp.getMask().size() != m_vnCount) {
		throw Exception("BlockDecoder::loadPunctureData", "Invalid vector length");
	}	
	m_punctureList= mp.getMask();
}

void BlockDecoder::loadInfoBitData(string filepath) {
	MaskParser mp;
	mp.parseFile(filepath);
	// make sure vector has the right length
	if(mp.getMask().size() != m_n) {
		throw Exception("BlockDecoder::loadInfoBitData", "Invalid vector length");
	}
	// make sure mask weight matches expected number of info bits
	if(mp.getWeight() != m_k) {
		throw Exception("BlockDecoder::loadInfoBitData", "Invalid number of info bits in pattern");
	}
	m_infoBitLocation= mp.getMask();
}

BitBlock* BlockDecoder::decodedTxBits() {
  if(!m_puncturedCode) return m_curVNHDVect;

  uint j=0;
  for(uint i=0; i < m_vnCount; i++) {
    if( !m_punctureList[i] ) { // this bit is not punctured
      if(j >= m_n) {
        throw Exception("BlockDecoder::decodedTxBits", 
                            "Invalid puncture pattern");
      }
      m_curCW->setbit(j++, m_curVNHDVect->getbit(i));
    }
  }

  if(j != m_n) {
    throw Exception("BlockDecoder::decodedTxBits", 
                        "Invalid puncture pattern");
  }

  return m_curCW;
}

BitBlock* BlockDecoder::infoBits() {
	BitBlock* txVect= decodedTxBits();
  uint j=0;
  for(uint i=0; i < m_n; i++) {
	  if(m_infoBitLocation.at(i)) m_infoBits->setbit(j++, txVect->getbit(i));
  }
  if(j != m_k) {
	  throw Exception("BlockDecoder::infoBits()",
	                  "Invalid info bit location vector");
  }
  return m_infoBits;
}
 
void BlockDecoder::printFailedBits(ofstream* pofs, vector<int>& failedbitsList, 
                                   int iterIndex) {
  *pofs << "#(No="<<m_demod->getNo();
  if(iterIndex>0) {
    *pofs << ") Err indexes ";
    *pofs << "at i=" << iterIndex << ": ";
  }
  else {
    *pofs << ") Input errors: ";
  }
  for(uint j=0; j<failedbitsList.size(); j++) {
    *pofs << failedbitsList[j] << ", ";
  }
  *pofs << endl;
}

void BlockDecoder::printUnsatCN(ofstream* pofs, int iterIndex) {
  vector<int> cnList;
  for(uint k=0; k < (m_Hm); k++) {
    if( m_chknodelist[k]->curParity() != 0 ) cnList.push_back(k);
  }
  *pofs << "#(No="<< m_demod->getNo() <<") Unsat CN at i="<< iterIndex << ": ";
  for(uint i=0; i<cnList.size(); i++) {
    *pofs << cnList[i] << ", ";
  }
  *pofs << endl;
}

// vector<VariableNodeStoch*> getVNNeighbors(VariableNodeStoch* vn) {
//   vector<VariableNodeStoch*> neighbors;
//   vector<SingleBitCN*> level1 = vn->getNeighbors();
//   for(uint i=0; i<level1.size(); i++) {
//     vector<SingleBitVN*> tmp = level1[i]->getNeighbors();
//     for(uint j=0; j<tmp.size(); j++) {
//       VariableNodeStoch* tmp2 = static_cast<VariableNodeStoch*>(tmp[j]);
//       neighbors.push_back(tmp2);
//     }
//   }
//   return neighbors;
// }

