//Author: Francois Leduc-Primeau

#ifndef _BlockDecoder_h_
#define _BlockDecoder_h_

#include "GlobalDefs.h"
#include "base/ISymbolProvider.hpp"
#include "coding/ICoder.hpp"
#include "coding/Node.hpp"
#include "modulation/IDemodulator.hpp"
#include "base/ISequential.hpp"
#include "Config.hpp"
#include "deviation/LLRDeviationPMF.hpp"

#include <vector>
#include <set>
#include <fstream>
#include <string>

/**
 * Implements belief-propagation decoding of linear block codes using
 * a "flooding" schedule.
 */
class BlockDecoder : public ISymbolProvider {
public:

  /**
   * Creates a new decoder that matches the specified encoder and
   * retrieves demodulated bits from demod.
   *@param encoder
   *@param demod
   *@param msgtype The type of message to be used in the decoder (one of 
   *               the MSGTYPE_* constants defined in Node.hpp).
   *@param maxIter The number of iterations before giving up. 
   *@param returnCW Whether to return the complete codeword (true), or
   *                just the source symbol (false).
   */
  BlockDecoder(ICoder* encoder, IDemodulator* demod, Config& conf,
               bool returnCW);

  ~BlockDecoder();

  // Implements ISymbolProvider:

  ///@see ISymbolProvider::getAlphaBitsize()
  int getAlphaBitsize() { if(m_returnCW) return m_n; else return m_k; }

  /**
   * Decodes the next noisy codeword and returns a source symbol. This call
   * invalidates the reference that was returned previously.
   *@see ISymbolProvider::nextSymbol()
   */
  BitBlock const& nextSymbol();

  ///@see ISymbolProvider::hasNext()
  bool hasNext() { return m_demod->hasNext(); }

  // End ISymbolProvider

  u64_t getIterCount() { return m_iterCount; }
  int   getFailedCount() { return m_failedCount; }
  void  resetCounters() { m_iterCount = 0; m_failedCount=0; }

  void setStochNDSCst(double val);

  /// Returns the current iteration count while decoding.
  int getCurIterCount() { return m_curIterCount; }

  /**
   * Returns the current iteration count histogram, *and resets the
   * histogram*. The caller is reponsible for deleting the array.
   *@return A newly allocated array of length 10.
   */
  int* getItHistogram();

  /**
   * Returns the current bit error count histogram, *and resets the
   * histogram*. The caller is reponsible for deleting the array.
   *@return A newly allocated array of length 21.
   */
  u64_t* getBErrHistogram();

  /**
   * Returns whether the last codeword processed was decoded
   * successfully (resulted in a valid codeword being output). Returns
   * false if no codeword has been processed.
   */
  bool isLastValid() { return m_lastValid; }

  /**
   * Prints statistics on the last decoded codeword, assuming a
   * decoding failure.
   *@param txCW   The transmitted codeword.
   */
  void printFailureStats(BitBlock const& txCW);

  /**
   * This value can be checked by decoding algorithms that have a
   * special variant for "redecoding trials", to know whether to
   * activate it or not.
   */
  bool getRedecodeMode() { return m_redecodeMode; }

  /**
   * Activates / deactivates a special mode where the redecoding mode
   * is always on.
   */
  void setAlwaysRedecode(bool value) { m_alwaysRedecode = value; }

  /**
   * Calling this method with a true argument means that the random
   * number generators will not be re-seeded at the start of a new
   * codeword (the default is to re-seed).
   */
  void setNoReseed(bool value) { m_noReseed = value; }

  /**
   * Returns the noise power currently registered in the demodulator
   * that feeds this decoder.
   */
  double getNoisePower() { return m_demod->getNo(); }

private: /* ------------------------------------------------------------ */

  /// (Called by the constructors)
  void init(ICoder* encoder, Config& conf);

  /// Initializes m_varnodelist
  void generateVarNodes(int msgtype);

  BitBlock const& nextSymbol(bool haveSentCW);

  /**
   * Implements the decoding process. The decoded bits are returned via
   * the m_bufcodeword member. This method relies on other member variables
   * as well.
   *@param maxIter   Maximum number of decoding iterations.
   *@param itercount (Output) The number of iterations used.
   *@param logBitErrors Whether to write the indexes of bits that are in error
   *                    starting at iteration logIter.
   *@param logIter      The iteration at which to start logging bit errors
   *                    (ignored if logBitErrors is false).
   *@param doTrapDetect Whether to abort if a trapping condition is detected.
   *@param doReset      Whether to reset the decoder before the first decoding
   *                    iteration (should usually be true).
   *@return Returns true if the decoding was successful, false otherwise.
   */
  bool decoderLoop(int maxIter, int* itercount, bool logBitErrors,
                   int logIter, bool doTrapDetect, bool doReset);

  /// Checks if a codeword is valid
  bool checkCodeword(BitBlock& codeword);

  /**
   * (Stochastic decoder) Simulates a clock tick by updating values
   * that depend on the clock.
   */
  void nextTick();

  /// Add an iteration value to the histogram.
  void addToItHist(int itVal);

  /**
   * Sets the temporary bit error histogram (corresponding to the current frame)
   *@param itIndex The index of the iteration that just ended. This determins the 
   *               appropriate bin.
   *@param berrCount Number of bit errors. This value will overwrite the previous
   *                 content of the bin.
   */
  void setTempBerrHist(uint itIndex, uint berrCount);

  /**
   * Copies data on punctured bits from the file specified to the
   * member m_punctureList, and performs some checks.
   */
  void loadPunctureData(std::string filepath);

	/**
	 * Loads data specifying the location of info bits, and performs
	 * some checks.
	 */
	void loadInfoBitData(std::string filepath);

  /**
   * Returns the bit vector containing the tx bits, and updates
   * m_curCW from m_curVNHDVect if necessary.
   *
   *@return A pointer to a vector that contains only the tx bits (not
   *        necessarily m_curCW).
   */
  BitBlock* decodedTxBits();

	BitBlock* infoBits();

  void printChVal(std::ofstream* pofs, double* array);
  void printFailedBits(std::ofstream*, std::vector<int>&, int);
  void printUnsatCN(std::ofstream*, int);

  // ----- Data Members -----

  Config* m_conf;

  /// Number of information bits per codeword.
  uint m_k;

  /**
   * Number of transmitted bits per codeword (not necessarily equal to
   * the number of variable nodes when some bits are "punctured").
   */
  uint m_n;

  /// Number of variable nodes in the Tanner graph
  uint m_vnCount;

  /// Number of parity checks (or number or rows in H)
  uint m_Hm;

  /// Maximum number of iterations before giving up a given decoding attempt.
  int m_maxIter;

  /// Number of decoding attempts of the same codeword (re-decoding)
  int m_maxDecodeCount;

  /// Maximum number of iterations that applies to re-decoding attempts.
  int m_redecodeMaxIter;

  /// Maximum total number of iterations that can be spent on a codeword.
  int m_maxIterTotal;

	/// Whether the decoder stops as soon as a codeword is found
	bool m_earlyTerm;

  /**
   * Parity-check matrix H (dimensions (m_n-m_k) x m_n). H[i] is the
   * i-th parity check.
   */
  BitBlock* m_H;

  /** 
   * List of variable node pointers. Array index corresponds to bit index.
   */
  VariableNode** m_varnodelist;

  /// List of check node pointers
  CheckNode** m_chknodelist;

#ifdef DECODER_ROWLAYERSCHED
  std::vector<std::set<VariableNode*> > m_vnLayerGroups;
#endif

  /// The encoder (used to get the actual transmitted CW)
  ICoder* m_encoder;

  /// The demodulator that provides the bit estimates
  IDemodulator* m_demod;

  /// Type of messages being exchanged in the graph.
  int m_msgtype;

  /// Buffer used when initializing (non-punctured) variable nodes.
  double* m_bufvarinit;

  /// Vector of the current hard decisions for each VN (including
  /// punctured VNs).
  BitBlock* m_curVNHDVect;

  /// Current codeword estimate, excluding punctured VNs. Note that
  /// this vector should not usually be accessed directly but rather
  /// through decodedTxBits().
  BitBlock* m_curCW;

	/// Information part of the codeword
	BitBlock* m_infoBits;

  /// Vector indicating whether a codeword bit is punctured.
	std::vector<bool> m_punctureList;

	/// Vector indicating for each VN, whether it corresponds to an info bit.
	std::vector<bool> m_infoBitLocation;

  /// Shortcut to know whether a code has punctured VNs or not
  /// (instead of going through m_punctureList).
  bool m_puncturedCode;

	/// Hard bit decisions before decoding
	std::vector<char> m_inputHD;

  /// Current iteration (updated at each decoding iteration)
  int m_curIterCount;

  /// Number of iterations used for the last decoded codeword
  uint m_lastIterCount;

  /// Number of iterations used for the last decoded codeword in the last try.
  uint m_lastIterCountLastTry;

  /// Whether the last decoded frame had converged.
  bool m_lastValid;

  /// Cumulative number of iterations used
  u64_t m_iterCount;

  /**
   * Iteration histogram. Bins are 10% of m_maxIter, with each bin's
   * right boundary rounded to the next integer.
   */
  int m_itHistogram[10];

  /**
   * Bit error histogram. The first bin contains the number of bit
   * errors before the first iteration, and the other bins have a size
   * 5% of m_maxIter, with each bin's right boundary rounded to the
   * next integer.
   */
  u64_t m_bErrHistogram[21];

  /// Temporary bit error histogram for the current codeword.
  u64_t m_tempBErrHistogram[21];

  /// Cumulative number of failed codewords (reached max. iterations)
  int m_failedCount;

  /** Whether to return the complete codeword (true), or just the
   *  source symbol (false).
   */
  bool m_returnCW;

  /// List of DRE objects to be deleted later
  std::vector<ISequential*> m_dreList;

  /**
   * File output stream for writing additionnal information about the
   * decoded codewords (NULL if the function is not activated).
   */
  std::ofstream* m_pDecInfoStream;

  /**
   * File output stream for writing the iteration count of every
   * decoded codeword (NULL if the function is not activated).
   */
  std::ofstream* m_pItCountStream;

  /**
   * The true codeword that was sent (for statistics and debug).
   */
	BitBlock* m_pSentCW; //TODO: as mentioned in BlockDecoder.cpp, storing a separate copy of the tx codeword in the decoder seems superflous.

  /// Whether or not to perform trap detection
  bool m_doTrapDetect;

  /**
   * For trap detection, threshold (inclusive maximum) on the number of
   * unsatisfied check nodes.
   */
  int m_trapCheckThreshold;

  /**
   * For trap detection, minimum number (inclusive) of unsatisfied
   * check nodes.
   */  
  int m_trapCheckThresholdMin;

  /**
   * For trap detection, number of cycles the number of unsatisfied
   * check nodes has to be stable.
   */
  int m_trapStableCount;

  /**
   * Deactivate trap detection after that many cycles.
   */
  int m_trapDetectionPeriod;

  /**
   * Whether to only perform trap detection in the first trial.
   */
  bool m_trapDetectionOnlyOnce;

  /// True when the decoding algorithm should use its "redecoding" variant.
  bool m_redecodeMode;

  /// When true, the "redecoding" mode remains always activated.
  bool m_alwaysRedecode;

  /// When true, the RNGs are not re-seeded when starting a new CW.
  bool m_noReseed;

  /// Iteration index at which to start logging errors in the decoder.
  uint m_errorLogIterIndex;

  /**
   * Whether to run the Berkeley post-processing algorithm after the
   * first trial (and then continue decoding).
   */
  bool m_doBerkeleyPP;
  
  /**
   * Whether to reset the decoder when starting new trials. Setting
   * this to true would only make sense if the number of trials is 2
   * (or 1).
   */
  bool m_noRedecodeReset;

  /**
   * (RHS decoder) Iteration at which to perform "Gear-Shift
   * scaling". The scaling is performed at the *end* of the iteration
   * specified (i.e. a value of 1 would mean perform 1 iteration, then
   * apply the scaling). A negative value indicates scaling is not
   * performed. The value must be negative if the VN class is not
   * VariableNodeStoch_B.
   */
  int m_rhsGSScaleIter;

  /**
   * (RHS decoder) Iteration at which to perform "State
   * Normalization". Performed at the end of the iteration specified
   * ("1" means at the end of the first iteration).
   */
  int m_rhsStateNormIter;

  /// (RHS decoder) Scaling factor for "Gear-Shift scaling"
  float m_rhsGSScaling;

  // Number of layers.
  uint m_layerCount;

  // Number of VNs in each layer.
  uint m_layerSize;

	/// Whether deviations are being applied to VN->CN messages
	bool m_applyDeviations;

	/** 
	 * Number of iterations for which to apply deviations, after which
	 * the iterations become reliable (no deviations are applied). A
	 * negative value indicates no limit (deviations are applied to all
	 * iterations).
	 */
	int m_devNbIter;
	
	/// Deviation Generator (null pointer when unused)
	LLRDeviationPMF* m_pDevGen;

	/// Deviation generator for VN totals (null pointer when unused)
	LLRDeviationPMF* m_pDevGenTot;
  
};

#endif
