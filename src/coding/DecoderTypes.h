/*
 * The MSGTYPE_ prefix is now misleading, this really refers to the
 * various types of decoders (which may or may not use different
 * message types). ***Bit #2 is reserved for children of
 * IVNStoch.***
 */
#define MSGTYPE_LR         1
#define MSGTYPE_LLR        2  // SPA based on LLR messages
#define MSGTYPE_STOCH      4
#define MSGTYPE_LLRQUANT   10
#define MSGTYPE_HDSTOCH    16  // hard decision coated with stochastic sugar
#define MSGTYPE_HD         48  // hard decision
#define MSGTYPE_HDMC       80  // "Monte-Carlo" hard decision
#define MSGTYPE_MINSUM     130  // min-sum SPA (LLR messages)
#define MSGTYPE_PARALSTOCH 260 // parallel stochastic decoding
#define MSGTYPE_DDBMP      512

#define ISSTOCHASTICDECODER(x) (((x) & 4) != 0)
