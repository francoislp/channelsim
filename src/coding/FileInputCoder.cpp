//Author: Francois Leduc-Primeau

#include "FileInputCoder.hpp"
#include "BlockDecoder.hpp"

#include <sstream>
#include <iostream>

using std::string;
using std::ifstream;
using std::ws;
using std::stringstream;
using std::cerr;
using std::endl;

FileInputCoder::FileInputCoder(uint k, uint n, BitBlock* H, uint Hm, 
                               string filename)
  : m_k(k),
    m_n(n),
    m_H(H),
    m_Hm(Hm),
    m_cwSent(false)
{
  if(filename == "") {
    m_useZeroCW = true;
    m_pInStream = 0;
    m_pInStream2 = 0;
  }
  else {
    m_useZeroCW = false;
    // open the input file
    m_pInStream = new ifstream(filename.c_str());
    if(!m_pInStream->good())
      throw Exception("FileInputCoder", "Could not open input file");
    // open a second stream used by nextSymbol()
    m_pInStream2 = new ifstream(filename.c_str());
    if(!m_pInStream2->good()) {
      cerr << "FileInputCoder: Could not open second ifstream" << endl;
      m_pInStream2 = 0;
    }
  }

  m_cwbuf = new char[m_n];
  for(uint i=0; i<m_n; i++) m_cwbuf[i] = 0;
}

FileInputCoder::~FileInputCoder() {
  delete m_cwbuf;
  if(m_pInStream != 0)  delete m_pInStream;
  if(m_pInStream2 != 0) delete m_pInStream2;
}

bool FileInputCoder::hasNext() {
  if(m_pInStream2==0) return false;
  *m_pInStream2 >> ws;
  if(m_pInStream2->eof()) return false;
  return true;
}

BitBlock const& FileInputCoder::nextSymbol() {

  if(m_useZeroCW) {
    m_curCW.init(m_n, m_cwbuf); // m_cwbuf is all zeroes in this case
  }
  else {

	  if(m_pInStream2 == 0) {
		  throw Exception("FileInputCoder::nextSymbol", 
		                  "No file input stream");
	  }

	  readNext(m_pInStream2, m_cwbuf);
	  m_curCW.init(m_n, m_cwbuf);
  }
  
  m_cwSent = true;
  return m_curCW;
}

BitBlock const& FileInputCoder::lastSentSymbol() {

  if( !m_cwSent ) {
    throw Exception("FileInputCoder::lastSentSymbol()",
                        "nextSymbol() was never called");
  }

  // return the value that was set by nextSymbol()
  return m_curCW;
}

BitBlock const& FileInputCoder::nextSentSymbol() {
  //TODO: this doesn't implement the functionality intended in ICoder
  return m_curCW;
}

// (private)
void FileInputCoder::readNext(ifstream* ifs, char* buffer) {
  // read a CW from the file
  string line;
  *ifs >> ws;
  if(ifs->eof()) throw Exception("FileInputCoder::biterrors",
                                     "Premature end of input file");
  getline(*ifs, line);
  stringstream linestr;
  linestr << line;
  for(uint i=0; i<m_n; i++) {
    int tmp;
    linestr >> ws;
    if(linestr.eof()) throw Exception("FileInputCoder::biterrors",
                                          "Missing bits in CW");
    linestr >> tmp;
    if(tmp!=0 && tmp!=1) throw Exception("FileInputCoder::biterrors",
                                             "Invalid value for bit");
#ifdef FILEINPUTCODER_REVERSE_BITORDER
    buffer[m_n-1-i] = static_cast<char>(tmp);
#else
    buffer[i] = static_cast<char>(tmp);
#endif
  }
}
