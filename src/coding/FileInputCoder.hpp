//Author: Francois Leduc-Primeau

#ifndef _FileInputCoder_h_
#define _FileInputCoder_h_

#include "ICoder.hpp"
#include "util/Exception.hpp"

#include <fstream>
#include <string>

class FileInputCoder : public ICoder {

public:

  /**
   * Constructor
   *@param filename Location of the transmitted CWs file. Can be the empty
   *                string to indicate that we are comparing against the
   *                zero-CW.
   */
  FileInputCoder(uint k, uint n, BitBlock* H, uint Hm, std::string filename);

  ~FileInputCoder();

  // Implements ICoder

  ///@see ICoder::getK()
  uint getK() {return m_k;}

  ////@see ICoder::getN()
  uint getN() {return m_n;}
  
  /// Not used (does nothing).
  void setSource(ISource* src) { }

  ///@see ICoder::getAlphaBitsize()
  int getAlphaBitsize() {return m_n;}

  /**
   * Returns the codeword that appears next in the file (with respect
   * to the last codeword returned by this function). Note that the
   * previously returned reference is invalidated.
   */
  BitBlock const& nextSymbol();

  ///@see ICoder::hasNext()
  bool hasNext();

  /**
   * Returns the last codeword that was returned by
   * <code>nextSymbol()</code>. However in the context of file inputs,
   * the codeword might not be retrieved by the channel object since
   * it might also be reading channel values from a file. In this case
   * we just read a new codeword from the file and return that, which
   * is misleading with respect to the method name !
   *TODO: Interface and expected functionality must be revised.
   */
  BitBlock const& lastSentSymbol();

  /**
   * Returns the last codeword that was returned by either <code>nextSymbol()</code> or <code>lastSentSymbol()</code>.
   *TODO: Interface and expected functionality must be revised. Code only works in the specific context that it is currently used in.
   */
  BitBlock const& nextSentSymbol();

	BitBlock const& nextSentInfo() { throw Exception("FileInputCoder::nextSentInfo", "unimplemented"); }

  /**
   * Returns the H matrix that was passed to the constructor. The matrix has
   * dimension (n-k) x n. 
   *@return Assuming H is the return value, H[i] corresponds to the ith
   *        row-vector.
   */
  BitBlock* getH() {return m_H;}

  /// The number of rows in the H matrix (returned by getH()).
  uint getHm() {return m_Hm;}

  // End ICoder

private:

  void readNext(std::ifstream* ifs, char* buffer);

  // ---------- Data ----------

  uint m_k;
  uint m_n;

  BitBlock* m_H;
  uint m_Hm;

  std::ifstream* m_pInStream;
  std::ifstream* m_pInStream2; // keeps track of a different cursor position

  /// Buffer for reading codewords
  char* m_cwbuf;

  /// Flag indicating that we are comparing against the zero-CW.
  bool m_useZeroCW;

  BitBlock m_curCW;

  /// Whether nextSymbol() was called at least once.
  bool m_cwSent;

};

#endif
