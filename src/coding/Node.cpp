//Author: Francois Leduc-Primeau

#include "Node.hpp"
#include "util/Exception.hpp"
#include "Logging.hpp"
#include "BlockDecoder.hpp"
#include "stoch/ShiftReg.hpp"
#include "stoch/TFM.hpp"
#include "stoch/TFMFP.hpp"
//#include "TFMDP.hpp"
#include "stoch/S_TFMFP.hpp"
#include "stoch/FeedbackTFMFP.hpp"
#ifdef VNSTOCHB_LLRTRACKER
#include "stoch/LLRTracker.hpp"
#elif defined VNSTOCHB_LLRTRACKER_2
#include "stoch/LLRTracker2.hpp"
#elif defined VNSTOCHB_LLRTRACKER_FIXED
#include "stoch/LLRTrackerFixed2.hpp"
#elif defined VNSTOCHB_LLRRELAX
#include "stoch/FPLLREMA.hpp"
#elif defined(VNSTOCHB_LLRTRACKER_HW) || defined(VNSTOCHB_LLRTRACKER_HWOPTIM)
#include "stoch/RHS_models/LLRTrackerHWAdaptor.hpp"
#elif defined VNSTOCHB_LLRTRACKER_ADAPTIVE
#include "stoch/LLRTrackerAdaptive.hpp"
#elif defined VNSTOCHB_LLRTRACKER_SIGNED
#include "stoch/LLRTrackerSigned.hpp"
#else
#include "stoch/LLRTrackerFP.hpp"
#endif

#include "stoch/StochDRE.hpp" // ideal DRE
#ifdef VNSTOCHB_SIGNEDMSG
#include "stoch/LLRtoBitsSigned.hpp"
#else
#include "stoch/LLRtobits.hpp"
#endif

#include <math.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <algorithm>

#include "compileflags.h"

using std::vector;
using std::string;
using std::ofstream;


/* Class Node */

Node::Node(uint degree, int msgtype, BlockDecoder* pdecoder) 
  : m_degree(degree),
    m_msgtype(msgtype),
    m_pDecoder(pdecoder)
{
  m_config = Config::getInstance();
}

/* ----- Class VariableNode ----- */

VariableNode::VariableNode(int index, uint degree, int msgtype,
                           BlockDecoder* pdec) 
  : m_msgtype(msgtype) 
{
  Config* config = Config::getInstance();

  if( msgtype == MSGTYPE_LLR || msgtype == MSGTYPE_MINSUM ) {
    // check if a subtype is specified
    string subtype = "";
    try {
      subtype = config->getPropertyByName<string>("decoder_subtype");
    } catch(InvalidKeyException&) {}
    if( subtype == "" && index == 0) {
      Logging::writeln("Warning: Using default subtype of the SPA/min-sum decoder");
    }

    if( subtype == "" || subtype == "default" ) {
      m_implementation = new VariableNodeLLR(index, degree, pdec);
    }
    else if( subtype == "relaxed" ) {
      m_implementation = new VNRelaxedLLRSPA(index, degree, pdec);
    }
    else if( subtype == "input-relaxed" ) {
      m_implementation = new VNRelaxedInLLRSPA(index, degree, pdec);
    }
    else {
      throw Exception("VariableNode::ctor", "Invalid decoder subtype");
    }
  }
  else if( msgtype == MSGTYPE_HD ) {
    m_implementation = new VNHD(index, degree, pdec);
  }
  else if( msgtype == MSGTYPE_DDBMP ) {
    m_implementation = new VNDDBMP(index, degree, pdec);
  }
  else {
    throw Exception("VariableNode::ctor", "Invalid message type");
  }
}

VariableNode::VariableNode(int index, uint degree, int msgtype, 
                           vector<IStochDRE*> drelist, 
                           BlockDecoder* pdec)
  : m_msgtype(msgtype)
{
  Config* config = Config::getInstance();

  if( msgtype == MSGTYPE_HDSTOCH ) {
    m_implementation = new VNHDStoch(index, degree, drelist[0], pdec); //TODO
  }
  else if( msgtype == MSGTYPE_HDMC ) {
    m_implementation = new VNHDMC(index, degree, drelist[0], pdec); //TODO
  }
  else if( msgtype == MSGTYPE_PARALSTOCH ) {
    if( degree != 6) {
      throw Exception("VariableNode::ctor", 
                          "Invalid node degree for this decoder type");
    }
    uint msg_bitwidth= config->getPropertyByName<uint>("stoch_parallel_width");
    m_implementation = new VariableNodeStochParallel6(index, drelist,
                                                      msg_bitwidth,
                                                      pdec);
  }
  else if( msgtype == MSGTYPE_STOCH ) {
    // check if the configuration has more to say concerning the decoder type
    string subtype = "";

    try {
      subtype = config->getPropertyByName<string>("decoder_subtype");
    } catch(InvalidKeyException&) {}

    // the old key is still supported for now
    if( subtype == "" ) {
      try {
        subtype = config->getPropertyByName<string>("stochastic_subtype");
      } catch(InvalidKeyException&) {}
    }

    if( subtype == "" && index == 0) {
      Logging::writeln("Warning: Using default subtype of the stochastic decoder");
    }

    if( subtype == "" || subtype == "default" ) { // conventional
                                                  // stochastic decoder
      if(degree==7) {
#ifdef VNDEG7_NB2
        m_implementation = new VariableNodeStoch7_2(index, drelist, pdec);
#else
        m_implementation = new VariableNodeStoch7(index, drelist, pdec);
#endif
      }
      else if(degree==6) {
        m_implementation = new VariableNodeStoch6(index, drelist, pdec);
      }
      else if(degree==3) {
        m_implementation = new VariableNodeStoch3(index, drelist, pdec);
      }
      else { // general implementation
        m_implementation = new VariableNodeStochGen(index,degree,drelist,pdec);
      }
    }
    else if( subtype == "single_tfm" ) {
#ifdef MULTIPLE_DRE  // temp ! until this flag is also runtime
      throw Exception("VariableNode::ctor", 
                          "MULTIPLE_DRE cannot be defined");
#endif
      if( degree == 6) { 
        m_implementation = new VariableNodeStochSmall6(index, drelist, pdec);
      }
//       else if( degree == 4 ) {
//         m_implementation = new VariableNodeStochSmall4(index, drelist);
//       }
      else {
        throw Exception("VariableNode::ctor", 
                            "Invalid node degree for this decoder type");
      }
    }
    else if( subtype == "experimental" ) {
      if( degree != 6) {
        throw Exception("VariableNode::ctor", 
                            "Invalid node degree for this decoder type");
      }
      m_implementation = new VariableNodeStoch6_2(index, drelist, pdec);
    }
    else {
      throw Exception("VariableNode::ctor", "Invalid decoder subtype");
    }

  } // end MSGTYPE_STOCH section
  else {
    throw Exception("VariableNode::ctor", "Invalid decoder type");
  }
}

VariableNode::VariableNode(int index, uint degree, vector<IRandomLLR*> drelist,
                           BlockDecoder* pdec)
  : m_msgtype(MSGTYPE_STOCH)
{
  m_implementation = new VariableNodeStoch_B(index, degree, drelist, pdec);
}

VariableNode::~VariableNode() {
  delete m_implementation;
}

void VariableNode::setStochNDS(double NDS_cst) {
  // this guarantees that the cast below is valid
  if( !ISSTOCHASTICDECODER(m_msgtype) )
    throw Exception("Ay caramba !");

  VariableNodeStoch* vn = static_cast<VariableNodeStoch*>(m_implementation);
  vn->setNDSCst(NDS_cst);
}

/* ----- Class AbstractVariableNode ----- */

AbstractVariableNode::AbstractVariableNode(int index, uint degree, 
                                           int msgtype, BlockDecoder* pdec) 
  : Node(degree, msgtype, pdec),
    m_index(index)
{
  // check node degree
  if(degree==0) throw Exception("Node degree cannot be 0");

}

/* ----- Class VN_int ------ */

VN_int::VN_int(uint degree)
  : m_nextAddIndex(0)
{
  m_pendingMsg = new int[degree];
  m_nodelist = new CN_int*[degree];
  m_myIDs = new uint[degree];
}

VN_int::~VN_int() {
  delete[] m_myIDs;
  delete[] m_pendingMsg;
}

uint VN_int::addNode(CN_int* node, uint myID) {
  int index = m_nextAddIndex;
  m_nextAddIndex++;
  m_nodelist[index] = node;
  // set my ID for sending to that node
  m_myIDs[index] = myID;
  return index;
}

/* ----- Class VN_double ----- */

VN_double::VN_double(uint degree)
  : m_nextAddIndex(0)
{
  m_pendingMsg = new double[degree];
  m_nodelist = new CN_double*[degree];
  m_myIDs = new uint[degree];
}

VN_double::~VN_double() {
  delete[] m_pendingMsg;
  delete[] m_myIDs;
}

uint VN_double::addNode(CN_double* node, uint myID) {
  int index = m_nextAddIndex;
  m_nextAddIndex++;
  m_nodelist[index] = node;
  // set my ID for sending to that node
  m_myIDs[index] = myID;
  return index;
}

/* ----- Class VNSPALLR ----- */

VNSPALLR::VNSPALLR(int index, uint degree, BlockDecoder* pdec)
  : AbstractVariableNode(index, degree, MSGTYPE_LLR, pdec),
    VN_double(degree)
{
  // parameters for dithering algorithms
  Config* config = Config::getInstance();

  // first check whether we're randomizing in CNs or VNs
  bool doRedecodeInVN = false;
  try {
    string tmp = config->getPropertyByName<string>("redecode_nodetype");
    if(tmp == "vn" || tmp == "VN") doRedecodeInVN = true;
  } catch(InvalidKeyException&) {}

  // set "inactive" values for all dithering parameters
  m_redecodeUniRndSignMode = false;
  m_redecodeFlipProb = 0;

  // if we're applying dithering in VN, load parameters
  if(doRedecodeInVN) {

    try {
      m_redecodeUniRndSignMode = config->getPropertyByName<bool>("spa_redecode_unirndsign");
    } catch(InvalidKeyException&) {}
    if(m_redecodeUniRndSignMode) {
      m_redecodeFlipProb = config->getPropertyByName<double>("spa_redecode_flipprob"); // (mandatory if this specific dithering is activated)
    }
  }

  // flag for randomization of the VN input (the prior)
  m_doRandomInitState = false;
  m_randomInitStdDev = 0;
  m_randomInitTotalVar = 0;
  try {
    m_doRandomInitState = config->getPropertyByName<bool>("redecode_random_init");
  } catch(InvalidKeyException&) {
    // m_doRandomInitState remains false
  }

  if(m_doRandomInitState) {
    m_randomInitIsUniform = false;
    try {
      m_randomInitIsUniform = config->getPropertyByName<bool>("redecode_random_init_uniform");
    } catch(InvalidKeyException&) {}

    if(m_randomInitIsUniform) {
      m_randomInitUniformBound = config->getPropertyByName<double>("redecode_random_init_uniform_bound");
      m_randomInitUniformBound*= 2;
    }
    else {
      try {
        m_randomInitTotalVar = config->getPropertyByName<double>("redecode_random_init_constantvar");
      } catch(InvalidKeyException&) {
        // fall back on this key
        m_randomInitStdDev = config->getPropertyByName<float>("redecode_random_init_stddev"); // (not catching the exception here)
      }
    }

    m_quantizeRandomInitState = config->getPropertyByName<bool>("redecode_random_init_quantize");
    if(m_quantizeRandomInitState) {
      
        m_randomInitQuantRange = config->getPropertyByName<double>("demod_llrrange");
        m_randomInitQuantResol= config->getPropertyByName<double>("demod_resol");
    }

    m_dre = new StochDRE();
  }

  m_outputBuffer = new double[getDegree()];

  // VN harmonization algorithm
  try {
    m_useVNHarmonization = config->getPropertyByName<bool>("decoder_vnharmonization");
  } catch(InvalidKeyException&) {
    m_useVNHarmonization = false;
  }
#ifdef VN_HARMONIZE
  if(m_useVNHarmonization) {
    m_harmonizeCst = config->getPropertyByName<double>("decoder_harmonize_cst");
  }
#else
  // make sure that VN Harmonization is not used if support is not compiled
  if(m_useVNHarmonization) {
    throw Exception("VNSPALLR::ctor", "VN Harmonization support not defined at compile-time but \"decoder_vnharmonization\" is true.");
  }
#endif

  m_berkPPUnsatList = 0;
}

VNSPALLR::~VNSPALLR() {
  if(m_doRandomInitState) delete m_dre;
  delete[] m_outputBuffer;
  if(m_berkPPUnsatList != 0) delete[] m_berkPPUnsatList;
}

void VNSPALLR::binaryTokenBcst(bool token) {
  double msg = 1;
  if(!token) msg = 0;
  for(uint i=0; i<getDegree(); i++) {
    m_nodelist[i]->receive(msg, m_myIDs[i]);
  }
}

void VNSPALLR::berkeleyPPInit() {
  m_berkPPInNeighb = false;
  if(m_berkPPUnsatList == 0) m_berkPPUnsatList = new bool[getDegree()];

  for(uint i=0; i<getDegree(); i++) {
    m_berkPPUnsatList[i] = false;
    if( ISTRUETOKEN_DBL(m_pendingMsg[i]) ) {
      m_berkPPInNeighb = true;
      m_berkPPUnsatList[i] = true;
      // load reliability constants
      m_berkPPHighVal = m_config->getPropertyByName<double>("decoder_berkeleyPP_highval");
      m_berkPPLowVal = m_config->getPropertyByName<double>("decoder_berkeleyPP_lowval");
    }
  }
}

void VNSPALLR::uniformSignFlip(double* outputList) {
  double rv = static_cast<double>(random()) / RAND_MAX;
  if( rv < m_redecodeFlipProb) {
    for(uint i=0; i<getDegree(); i++) {
      outputList[i]*= (-1);
    }
  }
}

//note: saturation is only performed in this function if
//      m_quantizeRandomInitState is true
void VNSPALLR::randomizeInput(double& inputLLR) {
  if(!m_doRandomInitState) throw Exception("VNSPALLR::randomizeInput",
                                               "Illegal call");

  if( m_randomInitIsUniform ) {
    double offset = (m_dre->getVal_f() - 0.5) * m_randomInitUniformBound;
    m_dre->nextTick();
    inputLLR+= offset;
  }
  else if( m_randomInitStdDev == 0 ) {
    // using a "total variance" parameter (*known bug*)
    double channelVar = (getDecoder()->getNoisePower()) / 2;
    double llrVar = 2*m_randomInitTotalVar/channelVar - 2;
    if(llrVar<0) {
      throw Exception("VNSPALLR::randomizeInput", 
                          "Negative dithering noise variance");
    }
    inputLLR+= m_dre->getVal_norm(static_cast<float>(sqrt(llrVar)));
    //note: note necessary to call m_dre->nextTick() with this function.
  }
  else {
    inputLLR+= m_dre->getVal_norm(m_randomInitStdDev);
  }

  if(m_quantizeRandomInitState) {
    // (code copied from BPSKDemod::quantize)
    if(inputLLR > m_randomInitQuantRange) inputLLR = m_randomInitQuantRange;
    if(inputLLR < -m_randomInitQuantRange) inputLLR = -m_randomInitQuantRange;
    inputLLR+= m_randomInitQuantRange;
    inputLLR = round( inputLLR*(m_randomInitQuantResol-1) / (2*m_randomInitQuantRange) );
    inputLLR = inputLLR * (2*m_randomInitQuantRange) / (m_randomInitQuantResol-1);
    inputLLR-= m_randomInitQuantRange;
  }
}

/* ----- Class VariableNodeLLR ----- */

VariableNodeLLR::VariableNodeLLR(int index, uint degree, BlockDecoder* pdec) 
  : VNSPALLR(index, degree, pdec),
    m_init(1)
{
  m_outputBuffer = new double[getDegree()];

  m_useProbabilisticUpdate = false;
  try {
    m_updateProb = m_config->getPropertyByName<float>("spa_update_prob");
    m_useProbabilisticUpdate = true;
  } catch(InvalidKeyException&) {}

  m_llrLimit = m_config->getPropertyByName<double>("llr_limit"); // (mandatory)

#ifdef VN_HARMONIZE
  // Set up parameters for Auto VN Harmonization
  m_autoVNHarmonization = false;
  try {
    m_autoVNHarmonization = m_config->getPropertyByName<bool>("decoder_autovnharmonization");
  } catch(InvalidKeyException&) {}

  if(m_autoVNHarmonization) {
    if(!m_useVNHarmonization) {
      throw Exception("VariableNodeLLR::ctor", "VN Harmonization must be activated when Auto VN Harmonization is activated");
    }
    m_autoVnHarmSat = m_config->getPropertyByName<double>("decoder_autovnharmonization_sat");
    double trigFract = m_config->getPropertyByName<double>("decoder_autovnharmonization_trig");
    m_autoVnHarmTrig = ceil(trigFract*degree);
  }
#endif

  m_pDevGen= 0;
  m_pTotDevGen= 0;
  m_devTxSign= 0; // (0 being an invalid value)
}

VariableNodeLLR::~VariableNodeLLR() {
  delete[] m_outputBuffer;
}

void VariableNodeLLR::init(double value) {
  m_init = value;

  // option to add noise to the input prior when redecoding
  if(getDecoder()->getRedecodeMode() && m_doRandomInitState) {
    randomizeInput(m_init);
  }

  // clip the a-priori LLR
  if( m_init > m_llrLimit ) m_init = m_llrLimit;
  else if( m_init < -m_llrLimit ) m_init = -m_llrLimit;
  
  // initialize the pending LLR messages (and the output buffers, just
  // to be safe)
  for(uint i=0; i<getDegree(); i++) {
    m_pendingMsg[i] = 0;
    m_outputBuffer[i] = 0;
  }

  m_firstIt = true;

#ifdef VN_HARMONIZE
  m_vnHarmActive = false;
#endif
}

void VariableNodeLLR::broadcast() {
  // probabilistic update
  if(getDecoder()->getRedecodeMode() && m_useProbabilisticUpdate 
     && !m_firstIt) {
    float rnd_p = static_cast<float>(random()) / 2147483647.0;
    if(rnd_p > m_updateProb) return; // (don't update)
  }

  uint degree = getDegree();

  // combine all the messages
  double total = m_init;
  for(uint i=0; i<degree; i++) {
    total+= m_pendingMsg[i];
  }

#ifdef VN_HARMONIZE
  // Only perform harmonization if node degree is at least 3
  if( degree >= 3 ) {
    // Auto-VN-Harmonization: Determine whether to activate
    if(m_autoVNHarmonization && !m_vnHarmActive) {
      // count number of "saturated" inputs
      uint satcount=0;
      for(uint i=0; i<degree; i++) {
        if(m_pendingMsg[i] >= m_autoVnHarmSat || m_pendingMsg[i] <= -m_autoVnHarmSat) {
          satcount++;
        }
      }
      if(satcount >= m_autoVnHarmTrig) {
        m_vnHarmActive = true;
      }
    } else { // only perform VN Harmonization when in "Redecoding" mode.
      m_vnHarmActive = m_useVNHarmonization && getDecoder()->getRedecodeMode();
    }
  }

  bool negMinoritySet = false; // a minority set exists and has negative sign
  bool posMinoritySet = false; // a minority set exists and has positive sign
  uint lastPosIndex = 0;
  uint lastNegIndex = 0;
  if(m_vnHarmActive) {
    // count total number of inputs that have the minority sign
    // (neglecting channel input)
    uint sum=0;
    for(uint i=0; i<degree; i++) {
      if(m_pendingMsg[i] >= 0) {
        sum++;
        lastPosIndex = i;
      } else {
        lastNegIndex = i;
      }
    }
    posMinoritySet = sum==1;
    negMinoritySet = sum==(degree-1);
    if(posMinoritySet && negMinoritySet) { // (VN of degree 2)
      // use the sign of the prior to define the minority set
      if(m_init >= 0) posMinoritySet = false;
      else negMinoritySet = false;
    }
  }
#endif // VN_HARMONIZE

  // send a message on each edge
  for(uint i=0; i<degree; i++) {

    // all the incomming messages combined, except message i
    double llr = total;
#ifndef VNSUMOUTPUT
    llr-= m_pendingMsg[i];
#endif
    
    // clip the LLR
    if( llr > m_llrLimit ) llr = m_llrLimit;
    else if( llr < -m_llrLimit ) llr = -m_llrLimit;
    // make sure outgoing message is valid
    if(llr!=llr) {
      throw Exception("VariableNodeLLR::broadcast()", 
                          "Invalid outgoing message");
    }

#ifdef VN_HARMONIZE
    if(m_vnHarmActive) {
      if(posMinoritySet && i!=lastPosIndex) {
        llr+= m_harmonizeCst;
      }
      if(negMinoritySet && i!=lastNegIndex) {
        llr-= m_harmonizeCst;
      }
    }
#endif

    if(m_pDevGen!=0 && !m_firstIt) {
	    llr= m_pDevGen->applyDeviation(llr, m_devTxSign);
    }

    // save the message to be sent
    m_outputBuffer[i] = llr;
  }

  // Dithering algorithms
  if(getDecoder()->getRedecodeMode()) {
    if(m_redecodeUniRndSignMode) uniformSignFlip(m_outputBuffer);
  }

  // send all messages
  for(uint i=0; i<degree; i++) {
    m_nodelist[i]->receive(m_outputBuffer[i], m_myIDs[i]);
  }

  m_firstIt = false;
}

#ifdef _NOINLINE
char VariableNodeLLR::currentDecision() {
#else
inline char VariableNodeLLR::currentDecision() {
#endif
  if(currentLLR()>=0.0) return 0;
  else return 1;
}

void VariableNodeLLR::berkeleyPPSend() {
  if(m_berkPPInNeighb) {
    for(uint i=0; i<getDegree(); i++) {
      // keep the sign of incomming messages but change their reliability
      if(m_berkPPUnsatList[i]) {
        m_pendingMsg[i] = ((m_pendingMsg[i]<0.0)*(-2)+1)*m_berkPPHighVal;
      } else {
        m_pendingMsg[i] = ((m_pendingMsg[i]<0.0)*(-2)+1)*m_berkPPLowVal;
      }
    }
  }

  return broadcast();
}

// current LLR at this variable node
double VariableNodeLLR::currentLLR() {
  double llr = m_init; // include a-priori belief
  for(uint i=0; i<getDegree(); i++) {
    llr+= m_pendingMsg[i];
  }
  if(m_pTotDevGen!=0) { // apply deviation on total
	  //Note: applyDeviationSat automatically saturates the LLR value
	  llr= m_pTotDevGen->applyDeviationSat(llr, m_devTxSign);
  }
  return llr;
}

char VariableNodeLLR::sampleExtrinsicDecision() {
	double llr= m_init;
	for(uint i=1; i<getDegree(); i++) {
		llr+= m_pendingMsg[i];
	}
	if(m_pDevGen!=0) {
		llr= m_pDevGen->applyDeviationSat(llr, m_devTxSign);
	}
	if(llr>=0) return 0;
	else return 1;
}

char VariableNodeLLR::samplePrevExtrinsicDecision() {
	if(m_outputBuffer[0] >= 0) return 0;
	else return 1;
}
	
/* ----- Class VNRelaxedLLRSPA ----- */

VNRelaxedLLRSPA::VNRelaxedLLRSPA(int index, uint degree, BlockDecoder* pdec)
  : VNSPALLR(index, degree, pdec),
    m_init(0)
{
  // Retrieve the relaxation factor from the config
  m_beta = m_config->getPropertyByName<double>("spa_beta");
  m_betaComp = 1 - m_beta;

  m_outMsgList = new double[degree];

  // probabilistic update feature
  m_useProbabilisticUpdate = false;
  try {
    m_updateProb = m_config->getPropertyByName<float>("spa_update_prob");
    m_useProbabilisticUpdate = true;
  } catch(InvalidKeyException&) {}

  m_llrLimit = m_config->getPropertyByName<double>("llr_limit"); // (mandatory)
}

VNRelaxedLLRSPA::~VNRelaxedLLRSPA() {
  delete[] m_outMsgList;
}

void VNRelaxedLLRSPA::init(double value) {
  m_init = value;

  // option to add noise to the input prior when redecoding
  if(getDecoder()->getRedecodeMode() && m_doRandomInitState) {
    randomizeInput(m_init);
  }

  // clip the a-priori LLR
  if( m_init > m_llrLimit ) m_init = m_llrLimit;
  else if( m_init < -m_llrLimit ) m_init = -m_llrLimit;

  // Initialize all output memories with the prior LLR
  for(uint i=0; i<getDegree(); i++) {
    m_outMsgList[i] = m_init;
  }

  // reset pending messages
  for(uint i=0; i<getDegree(); i++) {
    m_pendingMsg[i] = 0;
  }

  m_firstIt = true;

#ifdef VN_TOTALRELAX
  m_prevTotal = m_init;
#endif
}

// (code similar to VariableNodeLLR::broadcast())
void VNRelaxedLLRSPA::broadcast() {
  // probabilistic update
  if(getDecoder()->getRedecodeMode() && m_useProbabilisticUpdate 
     && !m_firstIt) {
    float rnd_p = static_cast<float>(random()) / 2147483647.0;
    if(rnd_p > m_updateProb) return; // (don't update)
  }

  // combine all the messages
  double total = m_init;
  for(uint i=0; i<getDegree(); i++) {
    total+= m_pendingMsg[i];
  }

#ifdef VN_TOTALRELAX
  total = m_betaComp * m_prevTotal + m_beta * total;
  m_prevTotal = total;
#endif

  // send a message on each edge
  for(uint i=0; i<getDegree(); i++) {

    // all the incomming messages combined, except message i
    double llr = total;
#ifndef VNSUMOUTPUT
    llr-= m_pendingMsg[i];
#endif
    
    // clip the LLR
    if( llr > m_llrLimit ) llr = m_llrLimit;
    else if( llr < -m_llrLimit ) llr = -m_llrLimit;
    // make sure value is valid
    if(llr!=llr) {
      throw Exception("VNRelaxedLLRSPA::broadcast()", 
                          "Invalid outgoing message");
    }

#ifdef VN_TOTALRELAX
    m_nodelist[i]->receive(llr, m_myIDs[i]);
#else
    // update the output memory using relaxation
    m_outMsgList[i] = m_betaComp * m_outMsgList[i] + m_beta * llr;

    // send the message
    m_nodelist[i]->receive(m_outMsgList[i], m_myIDs[i]);
#endif
  }

  m_firstIt = false;
}

char VNRelaxedLLRSPA::currentDecision() {
  // combine all the messages
  double total = m_init;
  for(uint i=0; i<getDegree(); i++) {
    total+= m_pendingMsg[i];
  }
  if( total >= 0.0 ) return 0;
  return 1;
}

/* ----- Class VNRelaxedInLLRSPA ----- */

VNRelaxedInLLRSPA::VNRelaxedInLLRSPA(int index, uint degree, BlockDecoder* pdec)
  : VNSPALLR(index, degree, pdec)
{
  m_init = 0;
#ifndef ADAPTIVE_BETA
  m_beta = m_config->getPropertyByName<double>("spa_beta");
  m_betaComp = 1 - m_beta;
#endif

  m_inMsgList = new double[degree];

  m_llrLimit = m_config->getPropertyByName<double>("llr_limit"); // (mandatory)

  m_outputBuffer = new double[getDegree()];

  m_useProbabilisticUpdate = false;
  try {
    m_updateProb = m_config->getPropertyByName<float>("spa_update_prob");
    m_useProbabilisticUpdate = true;
  } catch(InvalidKeyException&) {}
}

VNRelaxedInLLRSPA::~VNRelaxedInLLRSPA() {
  delete[] m_inMsgList;
  delete[] m_outputBuffer;
}

void VNRelaxedInLLRSPA::init(double value) {
  m_init = value;

  // option to add noise to the input prior when redecoding
  if(getDecoder()->getRedecodeMode() && m_doRandomInitState) {
    randomizeInput(m_init);
  }

  // clip the a-priori LLR
  if( m_init > m_llrLimit ) m_init = m_llrLimit;
  else if( m_init < -m_llrLimit ) m_init = -m_llrLimit;

  // Initialize input memories to 0
  for(uint i=0; i<getDegree(); i++) {
    m_inMsgList[i] = 0;
  }

  // reset pending messages
  for(uint i=0; i<getDegree(); i++) {
    m_pendingMsg[i] = 0;
  }

  m_firstIt = true;
}

void VNRelaxedInLLRSPA::broadcast() {

  //Note: Input memories are updated in receive()

  // probabilistic update
  if(getDecoder()->getRedecodeMode() && m_useProbabilisticUpdate 
     && !m_firstIt) {
    float rnd_p = static_cast<float>(random()) / 2147483647.0;
    if(rnd_p > m_updateProb) return; // (don't update)
  }

  // compute LLR total
  double total = m_init;
  for(uint i=0; i<getDegree(); i++) {
    total+= m_inMsgList[i];
  }

  // send a message on each edge
  for(uint i=0; i<getDegree(); i++) {
    double msg = total - m_inMsgList[i];

    // clip
    if( msg > m_llrLimit ) msg = m_llrLimit;
    else if( msg < -m_llrLimit ) msg = -m_llrLimit;
    // make sure value is valid
    if(msg != msg) {
      throw Exception("VNRelaxedInLLRSPA::broadcast()",
                          "Invalid outgoing message");
    }

    // save the message in output buffer
    m_outputBuffer[i] = msg;
  }

  // Dithering algorithms
  if(getDecoder()->getRedecodeMode()) {
    if(m_redecodeUniRndSignMode) uniformSignFlip(m_outputBuffer);
  }

  // send all messages
  for(uint i=0; i<getDegree(); i++) {
    m_nodelist[i]->receive(m_outputBuffer[i], m_myIDs[i]);
  }

  m_firstIt = false;
}

char VNRelaxedInLLRSPA::currentDecision() {
  double total = m_init;
  for(uint i=0; i<getDegree(); i++) {
    total+= m_inMsgList[i];
  }
  if( total >= 0.0 ) return 0;
  return 1;
}

/* ----- Class SingleBitVN ----- */

SingleBitVN::SingleBitVN(uint degree) 
  : m_degree(degree),
  m_nextAddIndex(0)
{
  m_myIDs = new uint[degree];
  m_pendingMsg = new char[degree];
  m_nodelist = new SingleBitCN*[degree];

#ifdef RANDOMIZE_CONNECTIONS
  for(int i=0; i<degree; i++) {
    // get a random index that is not already in the list
    bool exists = true;
    int randindex;
    while(exists) {
      randindex = random() % degree;
      exists = false;
      for(int j=0; j<m_neighborOrdering.size(); j++) {
        if( m_neighborOrdering[j] == randindex ) exists = true;
      }
    }
    // add it to the list
    m_neighborOrdering.push_back(randindex);
  }
#endif
}

SingleBitVN::~SingleBitVN() {
  delete[] m_myIDs;
  delete[] m_pendingMsg;
}

// (same code as VN_double::addNode())
uint SingleBitVN::addNode(SingleBitCN* node, uint myID) { 
#ifdef RANDOMIZE_CONNECTIONS
  int index = m_neighborOrdering[m_nextAddIndex];
#else
  int index = m_nextAddIndex;
#endif
  m_nextAddIndex++;
  m_nodelist[index] = node;
  // set my ID for sending to that node
  m_myIDs[index] = myID;
  return index;
}

vector<SingleBitCN*> SingleBitVN::getNeighbors() {
  vector<SingleBitCN*> list;
  for(uint i=0; i<m_degree; i++) {
    list.push_back(m_nodelist[i]);
  }
  return list;
}

/* ----- Class VariableNodeStoch ----- */

VariableNodeStoch::VariableNodeStoch(int index, uint degree, 
                                     vector<IStochDRE*> drelist,
                                     float EMSizeMult,
                                     BlockDecoder* pdec) 
  : IVNStoch(index, degree, MSGTYPE_STOCH, pdec),
//   m_myDRE(dreinst),
  m_training(true),
  m_NDS_cst(0),
  m_alternInput(-1)
{

  m_EMsize = 0; // not always used

  // Create all edge-memories
  m_EMList = new IEdgeMem*[degree];
  for(uint i=0; i<getDegree(); i++) {

    IStochDRE* curDRE;
    if(drelist.size()==1) curDRE = drelist[0];
    // here we make sure we have enough DREs for edge-mems + channel TFM
    else if(drelist.size()>=(degree+1)) curDRE = drelist[i];
    else throw Exception("VariableNodeStoch::ctor", 
                             "Invalid size for DRE list");

    if( m_config->getStochEMType() == EMTYPE_SHIFTREG ) {
      m_EMsize = m_config->getPropertyByName<int>("stoch_EM_size");
      m_EMsize = lround(m_EMsize * EMSizeMult);
      // create shift regs with built-in address offsets
      m_EMList[i] = new ShiftReg(m_EMsize, i, curDRE);
    } 
    else if( m_config->getStochEMType() == EMTYPE_TFM ) {
#ifdef USE_TFMFP
      double beta = m_config->getPropertyByName<double>("stoch_TFMFP_beta");
      m_EMList[i] = new TFMFP(beta, curDRE);
#else
      m_EMsize = m_config->getPropertyByName<int>("stoch_EM_size");
      m_EMsize = lround(m_EMsize * EMSizeMult);
      uint shiftAmnt =m_config->getPropertyByName<uint>("stoch_TFM_shiftAmnt");
      m_EMList[i] = new TFM(m_EMsize, shiftAmnt, curDRE);
#endif
    } 
    else if( m_config->getStochEMType() == EMTYPE_STFM ) {
      m_EMList[i] = new S_TFMFP(STFM_CST, curDRE);
    } 
    else if( m_config->getStochEMType() == EMTYPE_FDBCKTFM ) {
      double beta = m_config->getPropertyByName<double>("stoch_TFMFP_beta");
//       m_EMList[i] = new FeedbackTFMFP(beta, curDRE);  TODO: (debug)
      m_EMList[i] = new TFMFP(beta, curDRE);
    }
    else {
      throw Exception("VariableNodeStoch::ctor", "Invalid EM Type");
    }

  } // end create edge-memories loop

  IStochDRE* curDRE;
  if(drelist.size()==1) curDRE = drelist[0];
  else curDRE = drelist[degree];

  if( m_config->getStochEMType() == EMTYPE_FDBCKTFM ) {
    double beta = m_config->getPropertyByName<double>("stoch_TFMFP_beta");
    m_channelTFM = new FeedbackTFMFP(beta, curDRE);
  }
  else {
#ifdef USE_TFMFP
    m_channelTFM = new TFMFP(0, curDRE);
#else
    // the input TFM can have a different size
    int inputTFMSize;
    try {
      inputTFMSize = m_config->getPropertyByName<int>("stoch_inputtfm_size");
      if( getIndex()==0 ) {
        ofstream* pLog = Logging::getstream();
        *pLog << "Read stoch_inputtfm_size=" << inputTFMSize;
        *pLog <<" from config" << endl;
      }
    } catch(InvalidKeyException&) {
      m_EMsize = m_config->getPropertyByName<int>("stoch_EM_size");
      m_EMsize = lround(m_EMsize * EMSizeMult);
      inputTFMSize = m_EMsize;
    }
    m_channelTFM = new TFM(inputTFMSize, 0, curDRE);
#endif
  }

  // set output counter saturation from the config value (exception is
  // thrown if the key is not found)
  m_counterSat = m_config->getPropertyByName<int>("stoch_countersat");
}

VariableNodeStoch::~VariableNodeStoch() {
  for(uint i=0; i<getDegree(); i++) delete m_EMList[i];
  delete m_channelTFM;
}

// (private)
double VariableNodeStoch::updateInputProb() {
#ifdef STOCH_USELLR
  throw Exception("VariableNodeStoch::updateInputProb", "This function should not be called when STOCH_USELLR is defined");
#endif
  // scaled LLR
  double chLLRNDS = 4*m_curChVal*m_NDS_cst;
  // probability
  double prob = exp(chLLRNDS) / ( exp(chLLRNDS) + 1 ); //note: this is
                                                       // equal to
                                                       // 1/(exp(-chLLRNDS) + 1)
  m_channelTFM->init(prob);
  return prob;
}

// (private)
double VariableNodeStoch::updateInputProb_llr(double llr) {
  llr*= m_NDS_cst; // still allow scaling the LLRs, even if it isn't
                   // "noise dependent" anymore
  double prob = exp(-llr) / (exp(-llr)+1);
  m_channelTFM->init(prob);
  return prob;
}

void VariableNodeStoch::init(double chVal) {
  // calculate the input probability
  double inputProb;
#ifdef STOCH_USELLR
  inputProb = updateInputProb_llr(chVal); // (where chVal is the LLR value)
#else
  m_curChVal = chVal;
  inputProb = updateInputProb();
#endif

  // initialize all the EMs
  if(m_config->getStochEMType() == EMTYPE_SHIFTREG) {
    //TODO: change to number of EM init cycles
    for(int i=0; i < m_EMsize; i++) {
      // bit from the channel
      char initmsg = m_channelTFM->getrnd();

      for(uint j=0; j<getDegree(); j++) {
        IEdgeMem* curEM = m_EMList[j];
        curEM->push(initmsg);
      }

      m_channelTFM->getDRE()->nextTick();
    }
  } else if((m_config->getStochEMType() & 2) != 0) {
    for(uint i=0; i < getDegree(); i++) {
      ITFM* curEM = dynamic_cast<ITFM*>(m_EMList[i]);
      // init the EM to the same prob. as the channel TFM
      curEM->init(inputProb);
    }
  } else {
    throw Exception("VariableNodeStoch::init", "Invalid EM Type");
  }
  // end initialize EMs

#ifdef INIT_OUTPUTCOUNT
  m_outputEdgeCount = lround(m_channelTFM->curVal()*m_counterSat*2-m_counterSat);
#else
  m_outputEdgeCount = 0;
#endif

  m_firstIt = true;
  m_training = true;
  m_noupdateCount = 0;
}

char VariableNodeStoch::currentDecision() {
  if(m_outputEdgeCount >= 0) return 1;
  else return 0;
}

/* ----- Class VariableNodeStochGen ----- */

VariableNodeStochGen::VariableNodeStochGen(int index, uint degree,
                                           vector<IStochDRE*> drelist,
                                           BlockDecoder* pdec)
  : VariableNodeStoch(index, degree, drelist, 1, pdec),
  SingleBitVN(degree)
{}

void VariableNodeStochGen::init(double val) {
  // call parent method
  VariableNodeStoch::init(val);

  // reset pending messages to zero
  for(uint i=0; i<getDegree(); i++) {
    m_pendingMsg[i] = 0;
  }
}

void VariableNodeStochGen::broadcast() {

  // message from the channel edge
  char chanmsg = m_channelTFM->getrnd();

  // compute an outgoing message for each edge
  for(uint i=0; i<getDegree(); i++) {
   
    // update: whether all input messages are equal
    bool update = true;
    for(uint j=0; j<getDegree(); j++) {
      if(i!=j && m_pendingMsg[j] != chanmsg) update = false;
    }

    IEdgeMem* curEM = m_EMList[i];
    
    char msg; // to be sent
    if(update) {
      curEM->push(chanmsg);
      msg = chanmsg;
    }
    else {
      // randomly choose a bit from the EM
      msg = curEM->getrnd();
    }

    // send the message
    if(m_firstIt) {
      char chanHD = 0;
      if(m_channelTFM->curVal() >= 0.5) chanHD = 1;
      m_nodelist[i]->receive(chanHD, m_myIDs[i]);
    } else {
      m_nodelist[i]->receive(msg, m_myIDs[i]);
    }
  }

#ifdef MAJORITY_VN_OUTPUT
  int sum = chanmsg;
  for(uint i=0; i<getDegree(); i++) sum+= m_pendingMsg[i];
  float threshold = static_cast<float>(getDegree()) / 2;
  if( static_cast<float>(sum) > threshold ) m_outputEdgeCount++;
  else if( static_cast<float>(sum) < threshold ) m_outputEdgeCount--;
  // saturation
  if(m_outputEdgeCount > m_counterSat) m_outputEdgeCount = m_counterSat;
  if(m_outputEdgeCount < (0-m_counterSat)) m_outputEdgeCount = 0-m_counterSat;
#else
  // update the outgoing message
  bool update = true;
  for(uint i=0; i<getDegree(); i++) {
    if( m_pendingMsg[i] != chanmsg ) update = false;
  }
  // update the saturating counter with regenerative bits
  if(update) {
    if( chanmsg==1 ) m_outputEdgeCount++;
    else m_outputEdgeCount--;
    if(m_outputEdgeCount > m_counterSat) m_outputEdgeCount = m_counterSat;
    if(m_outputEdgeCount < (0-m_counterSat)) m_outputEdgeCount = 0-m_counterSat;
  }
  // count "no-update" cycles (when not in training)
  if(!m_training && !update) {
    m_noupdateCount++;
    m_noupdateLast = 1;
  } else {
    m_noupdateLast = 0;
  }
#endif

  m_firstIt = false;
}

/* ----- Class VariableNodeStoch3 ----- */

VariableNodeStoch3::VariableNodeStoch3(int index, 
                                       vector<IStochDRE*> drelist,
                                       BlockDecoder* pdec)
  : VariableNodeStoch(index, 3, drelist, 3/2, pdec),
    SingleBitVN(3)
{
  m_FF1List[0] = 0;
  m_FF1List[1] = 0;
  m_FF1List[2] = 0;
}

void VariableNodeStoch3::init(double chVal) {
  // call parent method
  VariableNodeStoch::init(chVal);

  // reset pending messages to zero
  for(uint i=0; i<getDegree(); i++) {
    m_pendingMsg[i] = 0;
  }

  // child initialization (same as constructor)
  m_FF1List[0] = 0;
  m_FF1List[1] = 0;
  m_FF1List[2] = 0;
}

void VariableNodeStoch3::broadcast() {
  // message from the channel edge
  char chanmsg = m_channelTFM->getrnd();

  for(uint i=0; i<3; i++) {
    
    // first "degree-2" node
    bool update;
    char out1;
    if(i==0) {
      update = (m_pendingMsg[1] == chanmsg);
    } else {
      update = (m_pendingMsg[0] == chanmsg);
    }

    if(update) {
      m_FF1List[i] = chanmsg;
      out1 = chanmsg;
    } else {
      out1 = m_FF1List[i];
    }

    // second "degree-2" node
    if(i==2) {
      update = (m_pendingMsg[1] == out1);
    } else {
      update = (m_pendingMsg[2] == out1);
    }

    IEdgeMem* curEM = m_EMList[i];
    char msg; // to be sent
    if(update) {
      curEM->push(out1);
      msg = out1;
    } else {
      msg = curEM->getrnd();
    }

    // send the message
    if(m_firstIt) {
      char chanHD = 0;
      if(m_channelTFM->curVal() >= 0.5) chanHD = 1;
      m_nodelist[i]->receive(chanHD, m_myIDs[i]);
    } else {
      m_nodelist[i]->receive(msg, m_myIDs[i]);
    }

  }
  
#ifdef MAJORITY_VN_OUTPUT
  outputEdge_3(chanmsg); // majority vote
#else
  outputEdge_1(chanmsg); // implementation with subnodes
#endif

  m_firstIt = false;
}

void VariableNodeStoch3::outputEdge_1(char chanmsg) {
  throw Exception("VariableNodeStoch3::outputEdge_1", "Unimplemented!");
}

void VariableNodeStoch3::outputEdge_3(char chanmsg) {
  int sum = chanmsg;
  for(int i=0; i<3; i++) sum+= m_pendingMsg[i];
  if(sum > 2) m_outputEdgeCount++;
  else if(sum < 2) m_outputEdgeCount--;
  // saturate output counter
  if(m_outputEdgeCount > m_counterSat) 
    m_outputEdgeCount = m_counterSat;
  if(m_outputEdgeCount < (0-m_counterSat))
    m_outputEdgeCount = (0-m_counterSat);
}

/* ----- Class VariableNodeStoch6 ----- */

VariableNodeStoch6::VariableNodeStoch6(int index, 
                                       vector<IStochDRE*> drelist,
                                       BlockDecoder* pdec)
  : VariableNodeStoch(index, 6, drelist, 2, pdec),
  SingleBitVN(6)
{
  IStochDRE* dreinst1;
  IStochDRE* dreinst2;
  if(drelist.size()==1) {
    dreinst1 = drelist[0];
    dreinst2 = dreinst1;
  }
  else if(drelist.size()>=9) {
    dreinst1 = drelist[7]; // (the other DREs are used by the parent ctor)
    dreinst2 = drelist[8];
  }
  else {
    throw Exception("VariableNodeStoch6::ctor",
                        "Invalid size for DRE list");
  }

  // get IM size, default to 2
  int sizeIM = 2;
  try {
    sizeIM = m_config->getPropertyByName<int>("stoch_IM_size");
  } catch(InvalidKeyException&) {
    Logging::writeln("Warning: Defaulting to stoch_IM_size = 2");
  }
  if( sizeIM < 1 ) {
    sizeIM = 2;
    Logging::writeln("Warning: Invalid value for key \"stoch_IM_size\". Defaulting to stoch_IM_size = 2");
  }

  // create IMs
  for(uint i=0; i<getDegree(); i++) {
    if(sizeIM == 2) { // bypass the "strategy" parent for speed
      m_IM1List[i] = new ShiftReg2(dreinst1);
      m_IM2List[i] = new ShiftReg2(dreinst2);
    } else {
      m_IM1List[i] = new ShiftRegGeneral(sizeIM, dreinst1);
      m_IM2List[i] = new ShiftRegGeneral(sizeIM, dreinst2);
    }
  }

#ifndef MAJORITY_VN_OUTPUT
  for(uint i=0; i<5; i++) {
    m_IMOut[i] = 0;
  }
#endif
}

void VariableNodeStoch6::init(double chVal) {
  // call parent method
  VariableNodeStoch::init(chVal);

  // reset pending messages to zero
  for(uint i=0; i<6; i++) {
    m_pendingMsg[i] = 0;
  }

#ifdef NO_IM_INIT
  // Zero init
  for(uint i=0; i<6; i++) {
    m_IM1List[i]->reset();
    m_IM2List[i]->reset();
  }
#else
  // Init IMs with stochastic bits
  for(uint i=0; i<6; i++) {
    char bit;
    for(uint j=0; j<m_IM1List[i]->size(); j++) {
      bit = m_channelTFM->getrnd();
      m_IM1List[i]->push(bit);
      m_channelTFM->getDRE()->nextTick();
    }

    for(uint j=0; j<m_IM2List[i]->size(); j++) {
      bit = m_channelTFM->getrnd();
      m_IM2List[i]->push(bit);
      m_channelTFM->getDRE()->nextTick();
    }
  }
#endif

#ifndef MAJORITY_VN_OUTPUT
  for(uint i=0; i<5; i++) {
    m_IMOut[i] = 0;
  }
#endif
}

void VariableNodeStoch6::broadcast() {  

  // message from the channel edge
  char chanmsg;
  //TODO: tentative (unused)
  if( m_alternInput < 0 ) chanmsg = m_channelTFM->getrnd();
  else chanmsg = m_alternInput;

  // compute an outgoing message for each edge
  char out1;
  char out2;
  char inputs[6];
  char outmsg;

  // Generate all the output messages
  for(int i=0; i<6; i++) {

    // build input vector
    inputs[0] = chanmsg;
    int k = 1;
    for(int j=0; j<6; j++) {
      if(i!=j) inputs[k++] = m_pendingMsg[j];
    }

    // first degree-3 subnode
    out1 = deg3subnode(inputs, i, 0);
    // second degree-3 subnode
    out2 = deg3subnode(inputs+3, i, 1);
    // degree-2 subnode using out1, out2
    bool update = (out1 == out2);
    IEdgeMem* curEM = m_EMList[i];
    if(update && !m_firstIt) {
#ifdef USE_UNANIMOUS_BETA
      int sum=0;
      for(int j=0; j<6; j++) sum+= inputs[j];
      if(sum==6 || sum==0) dynamic_cast<TFMFP*>(curEM)->push(out1, 0.25);
      else curEM->push(out1);
#else
      curEM->push(out1);
#endif
      outmsg = out1;
    } 
    else {
      outmsg = curEM->getrnd();
    }
    
    // send the message
    if(m_firstIt) {
      // send the channel hard-decision
      char chanHD = 0;
      if(m_channelTFM->curVal() >= 0.5) chanHD = 1;
      m_nodelist[i]->receive(chanHD, m_myIDs[i]);
    } 
    else {
      m_nodelist[i]->receive(outmsg, m_myIDs[i]);
    }

  }

  // Output Edge
#ifdef LEGACY_VN_OUTPUT
  outputEdge_2(chanmsg); // flat
#else
#ifdef MAJORITY_VN_OUTPUT
  if(!m_firstIt) {
    outputEdge_3(chanmsg); // majority vote
  }
#else
  outputEdge_1(chanmsg); // implementation with subnodes
#endif
#endif

  // clear the first iteration flag
  m_firstIt = false;
}

char VariableNodeStoch6::deg3subnode(char* inputs,
                                     int edgeIndex, int subnodeIndex) {
  char out;
  bool update = (inputs[0] == inputs[1]) &&
    (inputs[0] == inputs[2]);
  if(update && !m_firstIt) {
    out = inputs[0];
    // update IM
    if( subnodeIndex==0 ) {
      m_IM1List[edgeIndex]->push(out);
    } else { // subnodeIndex==1
      m_IM2List[edgeIndex]->push(out);
    }
  } 
  else {
    // take a bit from IM
    if( subnodeIndex==0 ) {
      out = m_IM1List[edgeIndex]->getrnd();
    } else {
      out = m_IM2List[edgeIndex]->getrnd();
    }
  }

  return out;
}

void VariableNodeStoch6::outputEdge_1(char chanmsg) {
  bool update;
  char out1;
  char out2;
  char out3;
  char out4;
  char out5;

  // subnode #1
  update = (m_pendingMsg[0] == m_pendingMsg[1]);
  if(update) {
    out1 = m_pendingMsg[0];
    // update IM #1
    m_IMOut[0] = out1;
  } else {
    out1 = m_IMOut[0];
  }

  // subnode #2
  update = (m_pendingMsg[2] == m_pendingMsg[3]);
  if(update) {
    out2 = m_pendingMsg[2];
    // update IM #2
    m_IMOut[1] = out2;
  } else {
    out2 = m_IMOut[1];
  }

  // subnode #3
  update = (m_pendingMsg[4] == m_pendingMsg[5]);
  if(update) {
    out3 = m_pendingMsg[4];
    // update IM #3
    m_IMOut[2] = out3;
  } else {
    out3 = m_IMOut[2];
  }

  // subnode #4
  update = (out1 == out2);
  if(update) {
    out4 = out1;
    // update IM #4
    m_IMOut[3] = out4;
  } else {
    out4 = m_IMOut[3];
  }

  // subnode #5
  update = (out3 == chanmsg);
  if(update) {
    out5 = out3;
    // update IM #5
    m_IMOut[4] = out5;
  } else {
    out5 = m_IMOut[4];
  }

  // subnode #6
  update = (out4 == out5);
  
  // update the saturating counter with regenerative bits
  if(!m_training && update) {
    if( out4==1 ) m_outputEdgeCount++;
    else m_outputEdgeCount--;
    if(m_outputEdgeCount > m_counterSat) m_outputEdgeCount = m_counterSat;
    if(m_outputEdgeCount < (0-m_counterSat)) m_outputEdgeCount = 0-m_counterSat;
  }

  // count "no-update" cycles (when not in training)
  if(!m_training && !update) {
    m_noupdateCount++;
    m_noupdateLast = 1;
  } else {
    m_noupdateLast = 0;
  }
}

void VariableNodeStoch6::outputEdge_2(char chanmsg) {
  bool update = true;
  for(int i=0; i<6; i++) {
    if( m_pendingMsg[i] != chanmsg ) update = false;
  }

  // update the saturating counter with regenerative bits
  if(!m_training && update) {
    if( chanmsg==1 ) m_outputEdgeCount++;
    else m_outputEdgeCount--;
    if(m_outputEdgeCount > m_counterSat) m_outputEdgeCount = m_counterSat;
    if(m_outputEdgeCount < (0-m_counterSat)) m_outputEdgeCount = 0-m_counterSat;
  }

  // count "no-update" cycles (when not in training)
  if(!m_training && !update) {
    m_noupdateCount++;
    m_noupdateLast = 1;
  } else {
    m_noupdateLast = 0;
  }
}

// update the counter with a majority vote
void VariableNodeStoch6::outputEdge_3(char chanmsg) {
  int sum = chanmsg;
  for(int i=0; i<6; i++) sum+= m_pendingMsg[i];
  if(sum > 3) m_outputEdgeCount++; // (4/7 majority)
  else m_outputEdgeCount--;
//   if(sum > 4) m_outputEdgeCount++; // (5/7 majority)
//   else if(sum < 3) m_outputEdgeCount--;
  // saturate output counter
  if(m_outputEdgeCount > m_counterSat) 
    m_outputEdgeCount = m_counterSat;
  if(m_outputEdgeCount < (0-m_counterSat))
    m_outputEdgeCount = (0-m_counterSat);
}

/* ----- Class VariableNodeStoch6_2 ----- */

VariableNodeStoch6_2::VariableNodeStoch6_2(int index, 
                                           vector<IStochDRE*> drelist,
                                           BlockDecoder* pdec)
  : VariableNodeStoch(index, 6, drelist, 2, pdec),
    SingleBitVN(6)
{
}

void VariableNodeStoch6_2::init(double val) {
  // call parent method
  VariableNodeStoch::init(val);

  // reset pending messages to zero
  for(uint i=0; i<getDegree(); i++) {
    m_pendingMsg[i] = 0;
  }
}

void VariableNodeStoch6_2::broadcast() {

  // generate the message from the channel edge
  char chanmsg = m_channelTFM->getrnd();

  // sum of inputs
  int inputSum = chanmsg;
  for(int i=0; i<6; i++) inputSum+= m_pendingMsg[i];

  // compute and send the outgoing message for each edge
  for(int i=0; i<6; i++) {
    char outmsg;
    int curSum = inputSum - m_pendingMsg[i];
    if(curSum == 6) { // (6/6 majority)
      m_EMList[i]->push(1);
      outmsg = 1;
    }
    else if(curSum == 0) {        
      m_EMList[i]->push(0);
      outmsg = 0;
    } 
    else {
      outmsg = m_EMList[i]->getrnd();
      // (was a temp test for i-saied)
//       int rndindex = m_myDRE->getVal() % 6; //temp
//       if(rndindex==i) outmsg = chanmsg; //temp
//       else outmsg = m_pendingMsg[i]; //temp
    }

    // send the message
    if(m_firstIt) {
      char chanHD = 0;
      if(m_channelTFM->curVal() >= 0.5) chanHD = 1;
      m_nodelist[i]->receive(chanHD, m_myIDs[i]);
    } else {
      m_nodelist[i]->receive(outmsg, m_myIDs[i]);
    }
  }

  // update the output counter
  if(inputSum >= 5) m_outputEdgeCount++; // (5/7 majority)
  else if(inputSum <= 2) m_outputEdgeCount--;
  // saturate output counter
  if(m_outputEdgeCount > m_counterSat) 
    m_outputEdgeCount = m_counterSat;
  if(m_outputEdgeCount < (0-m_counterSat))
    m_outputEdgeCount = (0-m_counterSat);

  m_firstIt = false;
}

/* ----- Class VariableNodeStoch7 ----- */

VariableNodeStoch7::VariableNodeStoch7(int index, vector<IStochDRE*> drelist,
                                       BlockDecoder* pdec)
  : VariableNodeStoch(index, 7, drelist, 2, pdec),
    SingleBitVN(7)
{
  // (same code as VariableNodeStoch6::ctor)

  IStochDRE* dreinst1;
  IStochDRE* dreinst2;
  if(drelist.size()==1) {
    dreinst1 = drelist[0];
    dreinst2 = dreinst1;
  }
  else if(drelist.size()>=9) {
    dreinst1 = drelist[7]; // (the other DREs are used by the parent ctor)
    dreinst2 = drelist[8];
  }
  else {
    throw Exception("VariableNodeStoch7::ctor",
                        "Invalid size for DRE list");
  }

  // get IM size, default to 2
  int sizeIM = 2;
  try {
    sizeIM = m_config->getPropertyByName<int>("stoch_IM_size");
  } catch(InvalidKeyException&) {
    Logging::writeln("Warning: Defaulting to stoch_IM_size = 2");
  }
  if( sizeIM < 1 ) {
    sizeIM = 2;
    Logging::writeln("Warning: Invalid value for key \"stoch_IM_size\". Defaulting to stoch_IM_size = 2");
  }

  // create IMs
  for(uint i=0; i<getDegree(); i++) {
    if(sizeIM == 2) { // bypass the "strategy" parent for speed
      m_IM1List[i] = new ShiftReg2(dreinst1);
      m_IM2List[i] = new ShiftReg2(dreinst2);
    } else {
      m_IM1List[i] = new ShiftRegGeneral(sizeIM, dreinst1);
      m_IM2List[i] = new ShiftRegGeneral(sizeIM, dreinst2);
    }
  }

#ifndef MAJORITY_VN_OUTPUT
  throw Exception("VariableNodeStoch7::ctor", 
                  "MAJORITY_VN_OUTPUT must be defined");
#endif
}

void VariableNodeStoch7::init(double chVal) {
  // (same code as VariableNodeStoch6::init)

  // call parent method
  VariableNodeStoch::init(chVal);

  // reset pending messages to zero
  for(uint i=0; i<getDegree(); i++) {
    m_pendingMsg[i] = 0;
  }

#ifdef NO_IM_INIT
  // Zero init
  for(uint i=0; i<7; i++) {
    m_IM1List[i]->reset();
    m_IM2List[i]->reset();
  }
#else
  // Init IMs with stochastic bits
  for(uint i=0; i<7; i++) {
    char bit;
    for(uint j=0; j<m_IM1List[i]->size(); j++) {
      bit = m_channelTFM->getrnd();
      m_IM1List[i]->push(bit);
      m_channelTFM->getDRE()->nextTick();
    }

    for(uint j=0; j<m_IM2List[i]->size(); j++) {
      bit = m_channelTFM->getrnd();
      m_IM2List[i]->push(bit);
      m_channelTFM->getDRE()->nextTick();
    }
  }
#endif
}

void VariableNodeStoch7::broadcast() {
  // (similar code to VariableNodeStoch6::broadcast)

  // message from the channel edge
  char chanmsg = m_channelTFM->getrnd();

  // compute an outgoing message for each edge
  char out1;
  char out2;
  char inputs[7];
  char outmsg;

  // Generate all the output messages
  for(int i=0; i<7; i++) {

    // build input vector
    inputs[0] = chanmsg;
    int k = 1;
    for(int j=0; j<7; j++) {
      if(i!=j) inputs[k++] = m_pendingMsg[j];
    }

    // first subnode (degree-3)
    out1 = deg3subnode(inputs, i);
    // second subnode (degree-4)
    out2 = deg4subnode(inputs+3, i);
    // degree-2 subnode using out1, out2
    bool update = (out1 == out2);
    IEdgeMem* curEM = m_EMList[i];
    if(update && !m_firstIt) {
      curEM->push(out1);
      outmsg = out1;
    } 
    else {
      outmsg = curEM->getrnd();
    }
    
    // send the message
    if(m_firstIt) {
      // send the channel hard-decision
      char chanHD = 0;
      if(m_channelTFM->curVal() >= 0.5) chanHD = 1;
      m_nodelist[i]->receive(chanHD, m_myIDs[i]);
    } 
    else {
      m_nodelist[i]->receive(outmsg, m_myIDs[i]);
    }
  }

  // Output Edge
  if(!m_firstIt) {
    // update output counter with a majority vote
    int sum = chanmsg;
    for(int i=0; i<7; i++) sum+= m_pendingMsg[i];
    if(sum > 4) m_outputEdgeCount++; // (5/8 majority)
    else if(sum < 4) m_outputEdgeCount--;
    // saturate output counter
    if(m_outputEdgeCount > m_counterSat) 
      m_outputEdgeCount = m_counterSat;
    if(m_outputEdgeCount < (0-m_counterSat))
      m_outputEdgeCount = (0-m_counterSat);
  }

  // clear the first iteration flag
  m_firstIt = false;
}

// (private)
char VariableNodeStoch7::deg3subnode(char* inputs, int edgeIndex) {
  // (similar code to VariableNodeStoch6::deg3subnode)
  char out;
  bool update = (inputs[0] == inputs[1]) &&
    (inputs[0] == inputs[2]);
  if(update && !m_firstIt) {
    out = inputs[0];
    // update IM
    m_IM1List[edgeIndex]->push(out);
  } 
  else {
    // take a bit from IM
    out = m_IM1List[edgeIndex]->getrnd();
  }

  return out;
}

// (private)
char VariableNodeStoch7::deg4subnode(char* inputs, int edgeIndex) {
  // (similar code to VariableNodeStoch6::deg3subnode)
  char out;
  bool update = (inputs[0] == inputs[1]) &&
    (inputs[0] == inputs[2]) &&
    (inputs[0] == inputs[3]);
  if(update && !m_firstIt) {
    out = inputs[0];
    // update IM
    m_IM2List[edgeIndex]->push(out);
  } 
  else {
    // take a bit from IM
    out = m_IM2List[edgeIndex]->getrnd();
  }

  return out;
}

/* ----- Class VariableNodeStoch7_2 ----- */

VariableNodeStoch7_2::VariableNodeStoch7_2(int index, 
                                           vector<IStochDRE*> drelist,
                                           BlockDecoder* pdec)
  : VariableNodeStoch(index, 7, drelist, 2, pdec),
    SingleBitVN(7)
{
  // (code similar to VariableNodeStoch6::ctor)

  IStochDRE* dreinst1;
  IStochDRE* dreinst2;
  IStochDRE* dreinst3;
  if(drelist.size()==1) {
    dreinst1 = drelist[0];
    dreinst2 = dreinst1;
    dreinst3 = dreinst1;
  }
  else if(drelist.size()>=10) {
    dreinst1 = drelist[7]; // (the other DREs are used by the parent ctor)
    dreinst2 = drelist[8];
    dreinst3 = drelist[9];
  }
  else {
    throw Exception("VariableNodeStoch7_2::ctor",
                        "Invalid size for DRE list");
  }

  // get IM size, default to 2
  int sizeIM = 2;
  try {
    sizeIM = m_config->getPropertyByName<int>("stoch_IM_size");
  } catch(InvalidKeyException&) {
    Logging::writeln("Warning: Defaulting to stoch_IM_size = 2");
  }
  if( sizeIM < 1 ) {
    sizeIM = 2;
    Logging::writeln("Warning: Invalid value for key \"stoch_IM_size\". Defaulting to stoch_IM_size = 2");
  }

  // create IMs
  for(uint i=0; i<getDegree(); i++) {
    if(sizeIM == 2) { // bypass the "strategy" parent for speed
      m_IM1List[i] = new ShiftReg2(dreinst1);
      m_IM2List[i] = new ShiftReg2(dreinst2);
      m_IM3List[i] = new ShiftReg2(dreinst3);
    } else {
      m_IM1List[i] = new ShiftRegGeneral(sizeIM, dreinst1);
      m_IM2List[i] = new ShiftRegGeneral(sizeIM, dreinst2);
      m_IM3List[i] = new ShiftRegGeneral(sizeIM, dreinst3);
    }
  }

#ifndef MAJORITY_VN_OUTPUT
  throw Exception("VariableNodeStoch7_2::ctor", 
                  "MAJORITY_VN_OUTPUT must be defined");
#endif
}

void VariableNodeStoch7_2::init(double chVal) {
  // (code similar to VariableNodeStoch6::init)

  // call parent method
  VariableNodeStoch::init(chVal);

  // reset pending messages to zero
  for(uint i=0; i<getDegree(); i++) {
    m_pendingMsg[i] = 0;
  }

#ifdef NO_IM_INIT
  // Zero init
  for(uint i=0; i<7; i++) {
    m_IM1List[i]->reset();
    m_IM2List[i]->reset();
    m_IM3List[i]->reset();
  }
#else
  // Init IMs with stochastic bits
  for(uint i=0; i<7; i++) {
    char bit;
    for(uint j=0; j<m_IM1List[i]->size(); j++) {
      bit = m_channelTFM->getrnd();
      m_IM1List[i]->push(bit);
      m_channelTFM->getDRE()->nextTick();
    }
    for(uint j=0; j<m_IM2List[i]->size(); j++) {
      bit = m_channelTFM->getrnd();
      m_IM2List[i]->push(bit);
      m_channelTFM->getDRE()->nextTick();
    }
    for(uint j=0; j<m_IM3List[i]->size(); j++) {
      bit = m_channelTFM->getrnd();
      m_IM3List[i]->push(bit);
      m_channelTFM->getDRE()->nextTick();
    }
  }
#endif
}

void VariableNodeStoch7_2::broadcast() {
  // (similar code to VariableNodeStoch6::broadcast)

  // message from the channel edge
  char chanmsg = m_channelTFM->getrnd();

  // compute an outgoing message for each edge
  char out1;
  char out2;
  char out3;
  char inputs[7];
  char outmsg;

  // Generate all the output messages
  for(int i=0; i<7; i++) {

    // build input vector
    inputs[0] = chanmsg;
    int k = 1;
    for(int j=0; j<7; j++) {
      if(i!=j) inputs[k++] = m_pendingMsg[j];
    }

    // first subnode (degree-2)
    out1 = deg2subnode_1(inputs, i);
    // second subnode (degree-2)
    out2 = deg2subnode_2(inputs+2, i);
    // thrid subnode (degree-3)
    out3 = deg3subnode(inputs+4, i);
    // degree-3 subnode using out1, out2, out3
    bool update = (out1 == out2) && (out1 == out3);
    IEdgeMem* curEM = m_EMList[i];
    if(update && !m_firstIt) {
      curEM->push(out1);
      outmsg = out1;
    } 
    else {
      outmsg = curEM->getrnd();
    }
    
    // send the message
    if(m_firstIt) {
      // send the channel hard-decision
      char chanHD = 0;
      if(m_channelTFM->curVal() >= 0.5) chanHD = 1;
      m_nodelist[i]->receive(chanHD, m_myIDs[i]);
    } 
    else {
      m_nodelist[i]->receive(outmsg, m_myIDs[i]);
    }
  }

  // Output Edge
  if(!m_firstIt) {
    // update output counter with a majority vote
    int sum = chanmsg;
    for(int i=0; i<7; i++) sum+= m_pendingMsg[i];
    if(sum > 4) m_outputEdgeCount++; // (5/8 majority)
    else if(sum < 4) m_outputEdgeCount--;
    // saturate output counter
    if(m_outputEdgeCount > m_counterSat) 
      m_outputEdgeCount = m_counterSat;
    if(m_outputEdgeCount < (0-m_counterSat))
      m_outputEdgeCount = (0-m_counterSat);
  }

  // clear the first iteration flag
  m_firstIt = false;
}

// (private)
char VariableNodeStoch7_2::deg2subnode_1(char* inputs, int edgeIndex) {
  char out;
  bool update = inputs[0] == inputs[1];

  if(update && !m_firstIt) {
    out = inputs[0];
    // update IM
    m_IM1List[edgeIndex]->push(out);
  } 
  else {
    // take a bit from IM
    out = m_IM1List[edgeIndex]->getrnd();
  }

  return out;
}

// (private)
char VariableNodeStoch7_2::deg2subnode_2(char* inputs, int edgeIndex) {
  char out;
  bool update = inputs[0] == inputs[1];

  if(update && !m_firstIt) {
    out = inputs[0];
    // update IM
    m_IM2List[edgeIndex]->push(out);
  } 
  else {
    // take a bit from IM
    out = m_IM2List[edgeIndex]->getrnd();
  }

  return out;
}

// (private)
char VariableNodeStoch7_2::deg3subnode(char* inputs, int edgeIndex) {
  // (similar code to VariableNodeStoch6::deg3subnode)
  char out;
  bool update = (inputs[0] == inputs[1]) &&
    (inputs[0] == inputs[2]);
  if(update && !m_firstIt) {
    out = inputs[0];
    // update IM
    m_IM3List[edgeIndex]->push(out);
  } 
  else {
    // take a bit from IM
    out = m_IM3List[edgeIndex]->getrnd();
  }

  return out;
}

/* ----- Class VariableNodeStochSmall6 ----- */

VariableNodeStochSmall6::VariableNodeStochSmall6(int index,
                                                 vector<IStochDRE*> drelist,
                                                 BlockDecoder* pdec)
  : VariableNodeStoch(index, 6, drelist, 2, pdec),
    SingleBitVN(6)
{
  // We use a single edge-memory
  m_EM = m_EMList[0];

  IStochDRE* dreinst = drelist[0];

  // get IM size, default to 2
  int sizeIM = 2;
  try {
    sizeIM = m_config->getPropertyByName<int>("stoch_IM_size");
  } catch(InvalidKeyException&) {
    Logging::writeln("Warning: Defaulting to stoch_IM_size = 2");
  }
  if( sizeIM < 1 ) {
    sizeIM = 2;
    Logging::writeln("Warning: Invalid value for key \"stoch_IM_size\". Defaulting to stoch_IM_size = 2");
  }

  // create IMs
  for(uint i=0; i<getDegree(); i++) {
    if(sizeIM == 2) { // bypass the "strategy" parent for speed
      m_IM1List[i] = new ShiftReg2(dreinst);
      m_IM2List[i] = new ShiftReg2(dreinst);
    } else {
      m_IM1List[i] = new ShiftRegGeneral(sizeIM, dreinst);
      m_IM2List[i] = new ShiftRegGeneral(sizeIM, dreinst);
    }
  }
}

// (same code (almost) as VariableNodeStoch6::init(double))
void VariableNodeStochSmall6::init(double chVal) {
    // call parent method
  VariableNodeStoch::init(chVal);

  // reset pending messages to zero
  for(uint i=0; i<6; i++) {
    m_pendingMsg[i] = 0;
  }

#ifdef NO_IM_INIT
  // Zero init
  for(uint i=0; i<6; i++) {
    m_IM1List[i]->reset();
    m_IM2List[i]->reset();
  }
#else
  // Init IMs with stochastic bits
  for(uint i=0; i<6; i++) {
    char bit;
    for(uint j=0; j<m_IM1List[i]->size(); j++) {
      bit = m_channelTFM->getrnd();
      m_IM1List[i]->push(bit);
      m_channelTFM->getDRE()->nextTick();
    }

    for(uint j=0; j<m_IM2List[i]->size(); j++) {
      bit = m_channelTFM->getrnd();
      m_IM2List[i]->push(bit);
      m_channelTFM->getDRE()->nextTick();
    }
  }
#endif
}

void VariableNodeStochSmall6::broadcast() {
  // bit from the channel edge
  char chanmsg = m_channelTFM->getrnd();

  // get a bit from EM to be sent on edges that don't have a regen bit
  char emBit = m_EM->getrnd(); //modified: sample TFM before***

  // count how many regen bits, and keep track of the majority
  int regenCount = 0;
  int regenMaj = 0; // (a positive nb indicates a majority of 1's,
                    // negative number a majority of 0's)

  // Generate all the output messages
  char out1;
  char out2;
  char inputs[6];
  for(uint i=0; i<6; i++) {

    if(m_firstIt) {
      // send the channel hard-decision
      char chanHD = 0;
      if(m_channelTFM->curVal() >= 0.5) chanHD = 1;
      m_nodelist[i]->receive(chanHD, m_myIDs[i]);
    }
//     if(m_training) {
//       // send channel stochastic stream
//       m_nodelist[i]->receive(chanmsg, m_myIDs[i]);
//     }
    else {

      // build input vector
      inputs[0] = chanmsg;
      uint k = 1;
      for(uint j=0; j<6; j++) {
        if(i!=j) inputs[k++] = m_pendingMsg[j];
      }

      // first degree-3 subnode
      out1 = deg3subnode(inputs, i, 0);
      // second degree-3 subnode
      out2 = deg3subnode(inputs+3, i, 1);
      // degree-2 subnode using out1, out2
      char outmsg;
      if(out1 == out2) { // regen bit
        regenCount++;
        if(out1==0) regenMaj--;
        else regenMaj++;
        outmsg = out1;
      } 
      else {
        outmsg = emBit;
        //outmsg = -1; // compatibility with Saeed (sample TFM after update)
      }

      m_nodelist[i]->receive(outmsg, m_myIDs[i]);
      // save the bit to be sent
      //m_outputBuf[i] = outmsg;
    }
  }

  if(!m_firstIt) {

    // update output counter (and saturate)
    uint sum = chanmsg;
    for(uint i=0; i<6; i++) sum+= m_pendingMsg[i];
    if( sum > 3 ) { // (4/7 majority)
      if(m_outputEdgeCount < m_counterSat) {
        m_outputEdgeCount++;
      }
    }
    else {
      if(m_outputEdgeCount > (0-m_counterSat)) {
        m_outputEdgeCount--;
      }
    }
  }

//  if(!m_firstIt && !m_training) {
  if(!m_firstIt) {   // modified***************
    // update the single EM
    if( regenCount >= 6 ) {
      if( regenMaj > 0 )     m_EM->push(1); // removed compatibility
                                            // with Saeed***
      else if( regenMaj < 0 ) m_EM->push(0);
    }

    // send the messages
//     char emBit = m_EM->getrnd(); // sample TFM *after* update
//     for(uint i=0; i<6; i++) {
//       char outmsg = m_outputBuf[i];
//       if(outmsg < 0) outmsg = emBit;
//       m_nodelist[i]->receive(outmsg, m_myIDs[i]);
//     }
  }

  // clear the first iteration flag
  m_firstIt = false;
}

// (private)
char VariableNodeStochSmall6::deg3subnode(char* inputs, int edgeIndex, 
                                          int subnodeIndex) {
  char out;
  bool update = (inputs[0] == inputs[1]) &&
    (inputs[0] == inputs[2]);
  if(update && !m_firstIt) {
    out = inputs[0];
    // update IM
    if( subnodeIndex==0 ) {
      m_IM1List[edgeIndex]->push(out);
    } else { // subnodeIndex==1
      m_IM2List[edgeIndex]->push(out);
    }
  } 
  else {
    // take a bit from IM
    if( subnodeIndex==0 ) {
      out = m_IM1List[edgeIndex]->getrnd();
    } else {
      out = m_IM2List[edgeIndex]->getrnd();
    }
  }

  return out;
}


/* ----- Class VariableNodeStochParallel6 ----- */

VariableNodeStochParallel6::
VariableNodeStochParallel6(int index, vector<IStochDRE*> drelist,
                           int bitWidth, BlockDecoder* pdec)
  : VariableNodeStoch(index, 6, drelist, 2, pdec),
    VN_int(6),
    m_bitWidth(bitWidth)
{
  // check the size of drelist: VariableNodeStoch parent uses 7, we
  // use 2*bitWidth
  if( drelist.size() < (2*bitWidth+7) )
    throw Exception("VariableNodeStochParallel6::ctor",
                        "Not enough elements in drelist");
  // make sure the bitWidth is not more than 31 
  // (bits are packed in an int and need to keep one bit free)
  if( bitWidth > 31 )
    throw Exception("VariableNodeStochParallel6::ctor",
                        "Bit width is too large (>31)");

  for(int i=0; i<6; i++) {
    m_IM1List[i] = new IShiftReg*[bitWidth];
    m_IM2List[i] = new IShiftReg*[bitWidth];
    for(int j=0; j<bitWidth; j++) {
      m_IM1List[i][j] = new ShiftReg2(drelist[7+j]);
      m_IM2List[i][j] = new ShiftReg2(drelist[7+bitWidth+j]);
    }
  }
}

VariableNodeStochParallel6::~VariableNodeStochParallel6() {
  for(int i=0; i<6; i++) {
    delete[] m_IM1List[i];
    delete[] m_IM2List[i];
  }
}

void VariableNodeStochParallel6::init(double chVal) {
  // call parent method
  VariableNodeStoch::init(chVal);

  // reset pending messages to zero
  for(uint i=0; i<6; i++) {
    m_pendingMsg[i] = 0;
  }

#ifdef NO_IM_INIT
  throw Exception("VariableNodeStochParallel6::init", "not supported");
#endif
  // Init IMs with stochastic bits
  for(uint i=0; i<6; i++) {
    for(uint j=0; j<m_bitWidth; j++) {
      for(uint k=0; k<2; k++) { // (assume 2-bit IMs)
        char bit;
        bit = m_channelTFM->getrnd();
        m_channelTFM->getDRE()->nextTick();
        m_IM1List[i][j]->push(bit);
        bit = m_channelTFM->getrnd();
        m_channelTFM->getDRE()->nextTick();
        m_IM2List[i][j]->push(bit);
      }
    }
  }

  // initialize output counters to represent the a-priori hard decision
#ifndef INIT_OUTPUTCOUNT   // otherwise done in VariableNodeStoch::init
  if(chVal >= 0) m_outputEdgeCount = 1;
  else m_outputEdgeCount = -1;
#endif
}

void VariableNodeStochParallel6::broadcast() {
  uint chanmsg = 0;
  // get independent stochastic bits from channel edge
  for(int i=0; i<m_bitWidth; i++) {
    m_channelTFM->getDRE()->nextTick();
    chanmsg+= (m_channelTFM->getrnd() << i);
  }

  uint inputs[6];

  // compute an outgoing message for each edge (each message contains
  // all the parallel bits)
  for(uint i=0; i<6; i++) {

    uint outputMsg;

    // for the first iteration, send channel stochastic bits
    if(m_firstIt) {
      outputMsg = chanmsg;
    }
    else {
      // build input vector
      inputs[0] = chanmsg;
      uint k=1;
      for(uint j=0; j<6; j++) {
        if(i!=j) inputs[k++] = m_pendingMsg[j];
      }
      
      // first degree-3 subnode
      uint out1 = deg3subnode(inputs, i, 0);
      // second degree-3 subnode
      uint out2 = deg3subnode(inputs+3, i, 1);
      // degree-2 subnode
      uint oneVect = out1 & out2;
      uint zeroVect = ~out1 & ~out2;
      // create the output
      outputMsg = oneVect; // the regenerative 1-bits
      uint regenCheck = oneVect | zeroVect;
      for(uint j=0; j<m_bitWidth; j++) {
        if( (regenCheck & (1<<j)) == 0 ) { // non-regen
          uint bit = static_cast<uint>( m_EMList[i]->getrnd() );
          m_EMList[i]->getDRE()->nextTick(); // makes sure all bits from
                                             // EM are generated using a
                                             // different RN
          outputMsg|= (bit << j); // (we know bit j was previously 0)
        }
      }

      // update the edge-memory
      // compute the balance of "one" agreements and "zero" agreements
      int updateOffset=0;
      for(uint j=0; j<m_bitWidth; j++) {
        updateOffset+= (oneVect & 1);
        updateOffset-= (zeroVect & 1);
        oneVect>>= 1;
        zeroVect>>= 1;
      }
      if( updateOffset > 0 ) m_EMList[i]->push(1);
      else if( updateOffset < 0 ) m_EMList[i]->push(0);

      // probabilistic shift of output vector
      IStochDRE* dre = m_channelTFM->getDRE(); // abuse the channel TFM DRE
      // (can re-use the same random number, don't call nextTick)
      // (same shift for all outputs)
      bool doshiftR = dre->getVal_f() < 0.33;
      bool doshiftL = dre->getVal_f() > 0.66;
      if(doshiftL) {
        outputMsg<<= 1;
        // rotate msb back to position 0
        outputMsg|= (outputMsg & (1<<m_bitWidth)) >> m_bitWidth;
      } else if(doshiftR) {
        outputMsg|= (outputMsg&1) << m_bitWidth;
        outputMsg>>= 1;
      }
    }

    // send the message
    m_nodelist[i]->receive(outputMsg, m_myIDs[i]);

  } // END process all output messages

  // Output Edge: get the majority out of all inputs and all instances
  if(!m_firstIt) {
    float sum=0;
    for(uint j=0; j<m_bitWidth; j++) {
      sum+= chanmsg & 1;
      chanmsg>>= 1;
      for(uint i=0; i<6; i++) {
        sum+= m_pendingMsg[i] & 1;
        m_pendingMsg[i]>>= 1; // note: now doesn't matter if we
                              // destroy the inputs
      }
    }
    float threshold = 3.5 * m_bitWidth; // where 3.5 = (d_v+1)/2
    if( sum > threshold ) m_outputEdgeCount++;
    else if( sum < threshold ) m_outputEdgeCount--;
    // saturation
    if(m_outputEdgeCount > m_counterSat) 
      m_outputEdgeCount = m_counterSat;
    if(m_outputEdgeCount < (0-m_counterSat)) 
      m_outputEdgeCount = (0-m_counterSat);
  }

  m_firstIt = false;
}

// (private)
uint VariableNodeStochParallel6::deg3subnode(uint* inputs, uint edgeIndex,
                                             uint subnodeIndex) {
  uint andVect = inputs[0] & inputs[1] & inputs[2];
  uint andNotVect = ~inputs[0] & ~inputs[1] & ~inputs[2];
  uint output = 0;
  uint outputMask = 1;
  
  for(int i=0; i<m_bitWidth; i++) {
    bool update_1 = (andVect & 1) == 1;
    bool update_0 = (andNotVect & 1) == 1;
    bool update = update_1 || update_0;
    if( update && !m_firstIt ) {
      char outBit = 0;
      if(update_1) {
        output|= outputMask;
        outBit = 1;
      }
      // update IM
      if( subnodeIndex==0 ) m_IM1List[edgeIndex][i]->push(outBit);
      else m_IM2List[edgeIndex][i]->push(outBit);
    }
    else {
      // take a bit from the IM
      char outBit;
      if( subnodeIndex==0 ) outBit = m_IM1List[edgeIndex][i]->getrnd();
      else outBit = m_IM2List[edgeIndex][i]->getrnd();
      if( outBit==1 ) output|= outputMask;
    }

    andVect>>= 1;
    andNotVect>>= 1;
    outputMask<<= 1;
  }

  return output;
}


/* ----- Class VariableNodeStoch_B ----- */

VariableNodeStoch_B::VariableNodeStoch_B(int index, uint degree, 
                                         vector<IRandomLLR*> rngList,
                                         BlockDecoder* pdec) 
  : IVNStoch(index, degree, MSGTYPE_STOCH, pdec),
    SingleBitVN(degree)
{
  m_bitsPerMsg = m_config->getPropertyByName<uint>("stoch_parallel_width");
  if( m_bitsPerMsg < 1 ) {
    throw Exception("VariableNodeStoch_B::ctor",
                    "Cannot have \"stoch_parallel_width\" < 1");
  }

#if defined(VNSTOCHB_SIGNEDMSG) && !defined(VNSTOCHB_BETAPROGRESSION)
#error VNSTOCHB_SIGNEDMSG requires VNSTOCHB_BETAPROGRESSION
#endif

  // check size of RNG list
#ifdef VNSTOCHB_SIGNEDMSG
  if( rngList.size() < (m_bitsPerMsg-1) ) {
    throw Exception("VariableNodeStoch_B::ctor", 
                    "rngList.size() < (m_bitsPerMsg-1)");
  }
#else
  if( (rngList.size() != m_bitsPerMsg) ) {
    throw Exception("VariableNodeStoch_B::ctor",
                    "Need one RNG object for each bit per message");
  }
#endif

  // copy pointers in rngList
  for(uint i=0; i<rngList.size(); i++) m_rngList.push_back(rngList[i]);

  float beta = m_config->getPropertyByName<float>("rhs_beta");

#ifdef VNSTOCHB_REFTRACKER
  m_beta = beta;
  m_diffRange = m_config->getPropertyByName<float>("rhs_difftrack_range");
  uint diffTrackBitSize = m_config->getPropertyByName<uint>("rhs_difftrack_bitsize");
  m_diffResol = static_cast<float>(1 << diffTrackBitSize);
  // for(uint i=0; i<degree; i++) {
  //   m_diffTrackerList.push_back(0);
  // }
#endif

  m_llrMsgList = new float[getDegree()];
  m_hasPendingMsg = new bool[getDegree()];

#ifdef VNSTOCHB_BETAPROGRESSION
  m_betaThreshold[0] = m_config->getPropertyByName<int>("rhs_beta_thresh_1");
  m_betaThreshold[1] = m_config->getPropertyByName<int>("rhs_beta_thresh_2");
  // check that the m_betaThreshold[] sequence is increasing
  // (otherwise beta value #3 might be used before value #2)
  if(m_betaThreshold[1] < m_betaThreshold[0]) {
    throw Exception("VariableNodeStoch_B::ctor",
                    "\"rhs_beta_thresh_2\" < \"rhs_beta_thresh_1\" not allowed");
  }
#endif
#ifdef VNSTOCHB_SIGNEDMSG
  // When to switch the message generation rule
  m_msgRuleSwitch = m_config->getPropertyByName<uint>("rhs_ruleSwitch_1");
  // this specifies a 1-based index into m_betaThreshold => make sure it is valid
  if(m_msgRuleSwitch==0 || m_msgRuleSwitch > 2) {
    throw Exception("VariableNodeStoch_B::ctor", "Invalid \"rhs_ruleSwitch_1\"");
  }
  m_msgRuleSwitch--; // make 0-based
#endif

  // get the number of deterministic cycles (mandatory)
  m_determCycles = m_config->getPropertyByName<uint>("rhs_determ_cycles");
  m_determCyclesCopy = m_determCycles;

  m_doPriorScaling = false;
  m_llrScaling = 1;
  try {
    m_doPriorScaling = m_config->getPropertyByName<bool>("redecode_prior_scaling");
    m_llrScaling = m_config->getPropertyByName<float>("redecode_prior_sf");
  } catch(InvalidKeyException&) {  }

  m_llrLimit = m_config->getPropertyByName<float>("llr_limit"); // (mandatory)
  if(m_llrLimit <= 0) {
    throw Exception("VariableNodeStoch_B::ctor", "Invalid value for key \"llr_limit\"");
  }
  m_outputLlrLimit = m_config->getPropertyByName<float>("rhs_outllr_limit");
  if(m_outputLlrLimit <= 0) {
    throw Exception("VariableNodeStoch_B::ctor", "Invalid value for key \"rhs_outllr_limit\"");
  }

#ifdef VNSTOCHB_SIGNEDMSG
  m_binMsgGen = new LLRtoBitsSigned(m_bitsPerMsg, m_rngList);
#else
  m_binMsgGen = new LLRtobits(m_bitsPerMsg, m_determCycles>0, m_rngList);
#endif

  // VN harmonization algorithm (same code as in VNSPALLR::ctor)
  try {
    m_useVNHarmonization = m_config->getPropertyByName<bool>("decoder_vnharmonization");
  } catch(InvalidKeyException&) {
    m_useVNHarmonization = false;
  }
#ifdef VN_HARMONIZE
  if(m_useVNHarmonization) {
    m_harmonizeCst = m_config->getPropertyByName<double>("decoder_harmonize_cst");
    m_harmonizeTrig = m_config->getPropertyByName<double>("decoder_harmonize_trig");
  }
  m_harmonizeFlagList = new bool[getDegree()];
#else
  // make sure that VN Harmonization is not used if support is not compiled
  if(m_useVNHarmonization) {
    throw Exception("VariableNodeStoch_B::ctor", "VN Harmonization support not defined at compile-time but \"decoder_vnharmonization\" is true.");
  }
#endif // VN_HARMONIZE

  // VNSUMOUTPUT is not supported for this class
#ifdef VNSUMOUTPUT
  throw Exception("VariableNodeStoch_B::ctor", 
                  "VNSUMOUTPUT compile-flag not supported");
#endif
} // end constructor

VariableNodeStoch_B::~VariableNodeStoch_B() {
  for(uint i=0; i<getDegree(); i++) {
    delete m_inTrackerList[i];
  }
  delete[] m_llrMsgList;
  delete m_binMsgGen;
  delete[] m_hasPendingMsg;
}

void VariableNodeStoch_B::baseInit() {
  float beta = m_config->getPropertyByName<float>("rhs_beta");

  // In the case of LLR tracking, create the trackers and let them
  // know the degree of the check node that produces the messages they
  // receive.
#ifdef VNSTOCHB_LLRTRACKER
  for(uint i=0; i<getDegree(); i++) {
    m_inTrackerList.push_back(new LLRTracker(beta, m_bitsPerMsg));
  }
#elif defined VNSTOCHB_LLRTRACKER_2
  m_inTrackerList.assign(getDegree(), NULL);
  for(uint i=0; i<getDegree(); i++) {
    uint senderDegree = m_nodelist[i]->getDegree();
    uint senderID = m_nodelist[i]->getID(m_myIDs[i]);
    m_inTrackerList[senderID] = new LLRTracker2(beta, m_bitsPerMsg, senderDegree);
  }
#elif defined VNSTOCHB_LLRTRACKER_FIXED
  uint llrTrackerBitWidth = m_config->getPropertyByName<uint>("llrtrackerfixed_width");
  for(uint i=0; i<getDegree(); i++) {
    m_inTrackerList.push_back(new LLRTrackerFixed2(llrTrackerBitWidth, m_bitsPerMsg));
  }
#elif defined VNSTOCHB_LLRRELAX
  for(uint i=0; i<getDegree(); i++) {
    m_inTrackerList.push_back(new FPLLREMA(m_bitsPerMsg));
  }
#elif defined VNSTOCHB_LLRTRACKER_HW || defined VNSTOCHB_LLRTRACKER_HWOPTIM
  for(uint i=0; i<getDegree(); i++) {
    m_inTrackerList.push_back(new LLRTrackerHWAdaptor());
  }
#elif defined VNSTOCHB_LLRTRACKER_ADAPTIVE
  m_inTrackerList.assign(getDegree(), NULL);
  for(uint i=0; i<getDegree(); i++) {
    uint senderDegree = m_nodelist[i]->getDegree();
    uint senderID = m_nodelist[i]->getID(m_myIDs[i]);
    m_inTrackerList[senderID] = new LLRTrackerAdaptive(beta, m_bitsPerMsg, 
                                                       senderDegree);
  }
#elif defined VNSTOCHB_LLRTRACKER_SIGNED
  for(uint i=0; i<getDegree(); i++) {
    m_inTrackerList.push_back(new LLRTrackerSigned(m_bitsPerMsg));
  }
#else // (same code as VNSTOCHB_LLRTRACKER_2 case above)
  m_inTrackerList.assign(getDegree(), NULL);
  for(uint i=0; i<getDegree(); i++) {
    uint senderDegree = m_nodelist[i]->getDegree();
    uint senderID = m_nodelist[i]->getID(m_myIDs[i]);
    m_inTrackerList[senderID] = new LLRTrackerFP(m_bitsPerMsg, senderDegree);
  }
#endif
}

void VariableNodeStoch_B::init(double llr) {
#ifndef STOCH_USELLR
  throw Exception("VariableNodeStoch_B::init", 
                      "STOCH_USELLR must be defined");
#endif
  m_inputLLR = static_cast<float>(llr);
  m_inputLLR*= (-1); // different parts of the program have different
                     // conventions ;)

#ifndef NO_DECODER
  // special stuff for redecoding
  if(getDecoder()->getRedecodeMode()) {
    // LLR scaling when turned on
    if(m_doPriorScaling) {
      m_inputLLR*= m_llrScaling;
    }

    // no deterministic cycles when redecoding
    m_determCycles = 0;
  }
  else { // else restore stuff that was changed
    // (m_inputLLR gets a new value every time this function is called)
    m_determCycles = m_determCyclesCopy;
  }
#endif // -- not defined NO_DECODER

#ifdef VNSTOCHB_QUANTIZELLR_INPUT
  // shouldn't be used together with demodulation quantization
#ifdef DEMOD_QUANTIZATION
#error VNSTOCHB_QUANTIZELLR_INPUT and DEMOD_QUANTIZATION cannot be defined together
#endif

  m_inputLLR = quantize(m_inputLLR);

#else
  // cap input LLR
  m_inputLLR = std::min(m_inputLLR, m_llrLimit);
  m_inputLLR = std::max(m_inputLLR, -m_llrLimit);
#endif

  // initialize all input trackers (to the zero-knowledge-value)
  // and reset pending-message flags
  for(uint i=0; i<getDegree(); i++) {
    m_inTrackerList[i]->reset();
    m_hasPendingMsg[i] = false;
  }
  m_isDirtyLlrMsgList = true; // invalidates the tracker output cache
  calcInputLLR(); // reloads the cache (necessary with DECODER_COLLAYERSCHED)

  // reset the output VN function compensation (associated with normalizeState())
  m_outputCompensation = 0;

  // reset pending messages to zero
  //note: in most cases this array is never used
  for(uint i=0; i<getDegree(); i++) {
    // if 2 bits/msg are available, use the zero-knowledge message
    if(m_bitsPerMsg==2) m_pendingMsg[i] = 2;
    else m_pendingMsg[i] = 0; 
  }

#ifdef VNSTOCHB_REFTRACKER
  m_refTracker = 0;
  // for(uint i=0; i<getDegree(); i++) {
  //   m_diffTrackerList[i] = 0;
  // }
#endif

  m_outputEdgeCount = 0;
  m_itCount = -1;

#ifdef VN_HARMONIZE
  // Reset the harmonization flags
  for(uint i=0; i<getDegree(); i++) {
    m_harmonizeFlagList[i] = false;
  }
#endif

  // Go back to first message generation rule
  m_binMsgGen->resetRule();

} // --- end VariableNodeStoch_B::init

/// Overrides SingleBitVN::receive
void VariableNodeStoch_B::receive(char msg, uint senderID) {
  m_hasPendingMsg[senderID] = true;
  m_pendingMsg[senderID] = msg;
}

void VariableNodeStoch_B::broadcast() {
  int itCount = getDecoder()->getCurIterCount();
  bool newiter = false;
  if(m_itCount != itCount) newiter = true;
  m_itCount = itCount;

  //note: Trackers and m_llrMsgList are updated in calcInputLLR().
  calcInputLLR();

  // Update the tracking rule for next time.
  // Note: We do this after updating the trackers because if for
  // example the switching iteration is 1, we expect the first rule to
  // be used for 1 iteration, not 0.
#ifdef VNSTOCHB_BETAPROGRESSION
  if((itCount == m_betaThreshold[0] || itCount == m_betaThreshold[1])
     && newiter) {
    for(uint i=0; i<getDegree(); i++) m_inTrackerList[i]->nextParam();
  }
#endif

#ifdef VNSTOCHB_SIGNEDMSG
  // message gen rule switch is aligned with one of the beta switch
  if(itCount == m_betaThreshold[m_msgRuleSwitch] && newiter) {
    m_binMsgGen->nextRule();
  }
#elif defined VNSTOCHB_BETAPROGRESSION
  // switch to next msg gen rule if we're done with deterministic thresholds
  // (does nothing if there is only one rule)
  if(itCount==m_determCycles && newiter) m_binMsgGen->nextRule();
#endif

#ifdef VNSTOCHB_REFTRACKER // (replaces the sum calculation)
  // update the Ref Tracker
  //  prob. value of tracker
  double refProb = 1 / (exp(m_refTracker/getDegree())+1);
  //  compute the 0-msg update
  double zeroUpdate = (1-m_beta)*refProb;
  zeroUpdate = log( (1-zeroUpdate)/zeroUpdate );
  // compute the 1-msg update
  double oneUpdate = (1-m_beta)*refProb + m_beta;
  oneUpdate = log( (1-oneUpdate)/oneUpdate );

  if(m_bitsPerMsg==1) {
    // count the number of each message received
    uint a=0;
    for(uint i=0; i<getDegree(); i++) {
      if(m_pendingMsg[i]==0) a++;
    }
    // compute the new Ref Tracker value
    m_refTracker = a*zeroUpdate + (getDegree()-a)*oneUpdate;
  } else if(m_bitsPerMsg==2) {
    // also compute the 1/2-msg update
    double halfUpdate = (1-m_beta)*refProb + m_beta/2;
    halfUpdate = log( (1-halfUpdate)/halfUpdate );
    // count the number of each message received
    uint a=0; uint b=0; uint c=0;
    for(uint i=0; i<getDegree(); i++) {
      if(m_pendingMsg[i]==0) a++;
      else if(m_pendingMsg[i]==3) c++;
      else b++;
    }
    // compute the new Ref Tracker value
    // for some reason "a" goes with the oneUpdate, and "c" with the zeroUpdate
    m_refTracker = a*oneUpdate + b*halfUpdate + c*zeroUpdate;
  } else {
    throw Exception("VariableNodeStoch_B::broadcast", "m_bitsPerMsg>2 not supported");
  }
  // (diff tracker based computation)
  // compute the "reference total" from which the diff value is subtracted
  float llrTotal = m_inputLLR + 
    static_cast<float>(getDegree()-1)/getDegree() * m_refTracker;
#else
  // compute the LLR sum of all messages
  float llrTotal = llrSum();
#endif

  llrTotal-= m_outputCompensation; // (see normalizeState())

  // compute an outgoing message for each edge
  for(uint i=0; i<getDegree(); i++) {
    // remove the intrinsic message
    float curLLR = llrTotal;
#ifdef VNSTOCHB_REFTRACKER  // (with diff trackers)
    // compute value of ideal diff tracker
    float curDiff = m_llrMsgList[i] - m_refTracker/getDegree();
    curDiff = quantize(curDiff, m_diffRange, m_diffResol);
    //note: (see definition of llrTotal above)
    curLLR-= curDiff;
#else
    curLLR-= m_llrMsgList[i];
#endif

    // cap LLR
    curLLR = std::min(curLLR, m_outputLlrLimit);
    curLLR = std::max(curLLR, -m_outputLlrLimit);

    // send one or many bits
    char msg = m_binMsgGen->convert(curLLR);        
    m_nodelist[i]->receive(msg, m_myIDs[i]);

  } // end: for outgoing edges

  // output edge (posterior)
  // (not using the output counter)
  if(llrTotal >= 0.0) m_outputEdgeCount = 1;
  else m_outputEdgeCount = -1;

#ifdef VN_HARMONIZE 
  // Determine whether to turn on harmonization on some trackers at the next iter.
  if(m_useVNHarmonization && getDecoder()->getRedecodeMode()) {
    harmonizeCheck();
  }
#endif

}

void VariableNodeStoch_B::scaleState(float scaling) {
  // scale the prior
  m_inputLLR*= scaling;
  
  // scale all the trackers (in the LLR domain)
  for(uint i=0; i<getDegree(); i++) {
    // get the LLR value (convert if necessary)
    float curLLR = m_inTrackerList[i]->curVal();

    // scale
    curLLR*= scaling;

    // write back to tracker (same code as in harmonizeTrackers())
    m_inTrackerList[i]->set(curLLR);
  }
  m_isDirtyLlrMsgList = true;
}

void VariableNodeStoch_B::normalizeState() {
  //Note: Code that retrieves and sets the LLR tracker values is also
  //      found in scaleState(float) and harmonizeTrackers(), but here
  //      when converting from prob to LLR the sign of LLRs is
  //      inverted to match the convention used in this class (since
  //      we're updating m_inputLLR).

  // Compute the total of the trackers
  float trackerTotal = 0;
  for(uint i=0; i<getDegree(); i++) {
    // get the LLR value
    float curLLR = m_inTrackerList[i]->curVal();
    trackerTotal+= curLLR;
  }

  float dv = getDegree();
  // update input LLR
  m_inputLLR = m_inputLLR + trackerTotal;
  // update the VN-function compensation
  m_outputCompensation+= trackerTotal/dv;
  // update trackers
  for(uint i=0; i<getDegree(); i++) {
    // get the LLR value
    float curLLR = m_inTrackerList[i]->curVal();
    curLLR = curLLR - trackerTotal/dv;

    // write back to tracker (same code as in harmonizeTrackers())
    m_inTrackerList[i]->set(curLLR);
  }
  m_isDirtyLlrMsgList = true;
}

// Quantization with a zero value.
float VariableNodeStoch_B::quantize(float value, float range, float resol) {
  if(value >= range) return range;
  if(value <= -range) return -range;
  float k = (resol - 2) / 2;
  value*= k / range;
  value = roundf(value);
  value*= range / k;
  return value;
}

void VariableNodeStoch_B::calcInputLLR() {
  // note: This method should not change the state of the node.
  //   ...well we update the trackers, but only if a message is pending

  for(uint i=0; i<getDegree(); i++) {
    // --- 1. Tracker Update ---
    if( m_hasPendingMsg[i] ) {
#ifdef VN_HARMONIZE
      if(m_harmonizeFlagList[i]) {
	m_inTrackerList[i]->harmonize();
      }
#endif //VN_HARMONIZE
      char msg = m_pendingMsg[i];
      //      bool updateDone = false;

      // tracker update done here unless we had a vn harm. update above
      //      if(!updateDone) {
        m_inTrackerList[i]->push(msg);
	//      }
    }

    // --- 2. Update stored LLR values (tracker output) ---
    if( m_hasPendingMsg[i] || m_isDirtyLlrMsgList ) {
      m_llrMsgList[i] = m_inTrackerList[i]->curVal();
    }

    m_hasPendingMsg[i] = false;
  } //end: for each edge

  m_isDirtyLlrMsgList = false;
} //end method

#ifdef VNSTOCHB_REFTRACKER
//With the Ref. Tracker, the sum of incomming msgs is simply the ref tracker value
float VariableNodeStoch_B::llrSum() {
  return m_refTracker + m_inputLLR;
}
#else
float VariableNodeStoch_B::llrSum() {
  //note: Trackers are not updated
  float llrTotal = m_inputLLR;
  for(uint i=0; i<getDegree(); i++) {
    llrTotal+= m_llrMsgList[i];
  }
  return llrTotal;
}
#endif


#ifdef VN_HARMONIZE
// similar code found in VariableNodeLLR::broadcast()
void VariableNodeStoch_B::harmonizeCheck() {
  // skip if degree is less than 3
  if(getDegree() < 3) return;

  bool negMinoritySet = false; // a minority set exists and has negative sign
  bool posMinoritySet = false; // a minority set exists and has positive sign
  uint lastPosIndex = 0;
  uint lastNegIndex = 0;

  // count total number of inputs that have the minority sign
  uint sum=0;
  for(uint i=0; i<getDegree(); i++) {
    if( m_inTrackerList[i]->curVal() >= 0 ) {
      sum++;
      lastPosIndex = i;
    } else {
      lastNegIndex = i;
    }
    posMinoritySet = sum==1;
    negMinoritySet = sum==(getDegree()-1);
  }

  if(posMinoritySet && negMinoritySet) { // (VN of degree 2)
    // use the sign of the prior to define the minority set
    if(m_inputLLR >= 0) posMinoritySet = false;
    else negMinoritySet = false;
  }

  // Reset the flags
  for(uint i=0; i<getDegree(); i++) m_harmonizeFlagList[i] = false;

  uint lastIndex = lastPosIndex;
  if(negMinoritySet) lastIndex = lastNegIndex;

  // Only perform VN harmonization if the minority tracker has a
  // magnitude bigger than some constant
  if( (posMinoritySet && 
       m_inTrackerList[lastIndex]->curVal() > m_harmonizeTrig)
      || (negMinoritySet && 
          m_inTrackerList[lastIndex]->curVal() < -m_harmonizeTrig) ) {
#ifdef VN_HARMONIZE_HW  
    if(lastIndex==0) {
      m_harmonizeFlagList[1] = true;
      m_harmonizeFlagList[2] = true;
    } else if(lastIndex==1) {
      m_harmonizeFlagList[0] = true;
      m_harmonizeFlagList[2] = true;
    } else {
      m_harmonizeFlagList[0] = true;
      m_harmonizeFlagList[1] = true;
    }
#else
    for(uint i=0; i<getDegree(); i++) {
      if(i!=lastIndex) {
        m_harmonizeFlagList[i] = true;
      }
    }
#endif
  }
} //end method harmonizeCheck
#endif //VN_HARMONIZE

// (same code as VNHD::binaryTokenBcst)
void VariableNodeStoch_B::binaryTokenBcst(bool token) {
  char msg = 1;
  if(!token) msg = 0;
  for(uint i=0; i<getDegree(); i++) {
    m_nodelist[i]->receive(msg, m_myIDs[i]);
  }
}


/* ----- Class VNHD ----- */

VNHD::VNHD(int index, uint degree, BlockDecoder* pdec)
  : AbstractVariableNode(index, degree, MSGTYPE_HD, pdec),
    SingleBitVN(degree)
{
	m_majThresh= m_config->getPropertyByName<double>("galB_maj_threshold");
	if(m_majThresh<0.0 || m_majThresh>1.0) {
		throw Exception("VNHD::ctor", "Invalid majority threshold");
	}
}

VNHD::~VNHD() {
}

void VNHD::init(double chLLR) {
  // note the LLR definition (negative => HD=1)
  if( chLLR < 0 ) m_chanVal = 1;
  else m_chanVal = 0;

  m_firstIt = true;
}

void VNHD::broadcast() {

  if(m_firstIt) {
    m_firstIt = false;
    // send the channel bit on all edges
    for(uint i=0; i<getDegree(); i++) {
      m_nodelist[i]->receive(m_chanVal, m_myIDs[i]);
    }

  } else { // normal operation
    
    int totalSum = 0;
    for(uint i=0; i<getDegree(); i++) totalSum+= m_pendingMsg[i];

    // compute an outgoing message for each edge
    for(uint i=0; i<getDegree(); i++) {
      uint curSum = totalSum - m_pendingMsg[i];
      float avg = static_cast<float>(curSum) / 
        (static_cast<float>(getDegree()) - 1);
      
      char outMsg;
      if( avg > m_majThresh ) outMsg = 1;
      else if(avg < (1-m_majThresh)) outMsg = 0;
      else outMsg = m_chanVal; // if there is no majority, send the channel bit
      
      // send the message
      m_nodelist[i]->receive(outMsg, m_myIDs[i]);
    }
  }
}

char VNHD::currentDecision() {
	if(m_firstIt) {
		return m_chanVal;
	} else {
    int totalSum = 0;
    for(uint i=0; i<getDegree(); i++) totalSum+= m_pendingMsg[i];
    
    float avg = static_cast<float>(totalSum) / static_cast<float>(getDegree());
    if( avg > m_majThresh ) return 1;
    else if(avg < (1-m_majThresh)) return 0;
    else return m_chanVal;
	}
}

void VNHD::binaryTokenBcst(bool token) {
  char msg = 1;
  if(!token) msg = 0;
  for(uint i=0; i<getDegree(); i++) {
    m_nodelist[i]->receive(msg, m_myIDs[i]);
  }
}

/* ----- Class VNHDStoch ----- */

VNHDStoch::VNHDStoch(int index, uint degree, IStochDRE* dreinst, 
                     BlockDecoder* pdec) 
  : AbstractVariableNode(index, degree, MSGTYPE_HDSTOCH, pdec),
    SingleBitVN(degree),
    m_myDRE(dreinst)
{
  //m_channelTFM = new TFMFP(0);
  m_channelTFM = new TFMFP(0, m_myDRE);
  // set output counter saturation from the config value (exception is
  // thrown if the key is not found)
  m_counterSat = m_config->getPropertyByName<int>("stoch_countersat");
}

VNHDStoch::~VNHDStoch() {
  delete m_channelTFM;
}

void VNHDStoch::init(double chLLR) {
  double eChLLR = exp(0-chLLR); // BPSKDemod's LLRs are reversed !
  m_channelTFM->init( eChLLR / (eChLLR+1) );

  // reset pending messages to zero
  for(uint i=0; i<getDegree(); i++) {
    m_pendingMsg[i] = 0;
  }

  // reset output counter
  m_outputEdgeCount = 0;

  m_firstIt = true;
}

void VNHDStoch::broadcast() {
  // stochastic bit from the channel input
  char chmsg = m_channelTFM->getrnd();

  if(m_firstIt) {
    m_firstIt = false;
    // send the channel bit on all edges
    for(uint i=0; i<getDegree(); i++) {
      m_nodelist[i]->receive(chmsg, m_myIDs[i]);
    }

    m_outputEdgeCount = chmsg*2 - 1;

  } else {
    // normal operation
    int totalSum = 0;
    for(uint i=0; i<getDegree(); i++) totalSum+= m_pendingMsg[i];

    // compute an outgoing message for each edge
    for(uint i=0; i<getDegree(); i++) {
      uint curSum = totalSum - m_pendingMsg[i];
      float avg = static_cast<float>(curSum) / 
        (static_cast<float>(getDegree()) - 1);
      
      char outMsg;
      if( avg > STOCHHD_MAJVAL ) outMsg = 1;
      else if(avg < (1-STOCHHD_MAJVAL)) outMsg = 0;
      else outMsg = chmsg; // if there is no majority, send the channel bit
      
      // send the message
      m_nodelist[i]->receive(outMsg, m_myIDs[i]);
    }
    
    // udpate the output counter
    float avg = static_cast<float>(totalSum) / static_cast<float>(getDegree());
    if( avg > STOCHHD_MAJVAL ) m_outputEdgeCount++;
    else if(avg < (1-STOCHHD_MAJVAL)) m_outputEdgeCount--;
    // saturate
    if(m_outputEdgeCount > m_counterSat) 
      m_outputEdgeCount = m_counterSat;
    if(m_outputEdgeCount < (0-m_counterSat))
      m_outputEdgeCount = (0-m_counterSat);
  }
}

char VNHDStoch::currentDecision() {
  if(m_outputEdgeCount >= 0) return 1;
  else return 0;
}

// (same code as VNHD::binaryTokenBcst)
void VNHDStoch::binaryTokenBcst(bool token) {
  char msg = 1;
  if(!token) msg = 0;
  for(uint i=0; i<getDegree(); i++) {
    m_nodelist[i]->receive(msg, m_myIDs[i]);
  }
}

/* ----- Class VNHDMC ----- */

VNHDMC::VNHDMC(int index, uint degree, IStochDRE* dreinst, BlockDecoder* pdec)
  : VNHD(index, degree, pdec),
    m_myDRE(dreinst)
{
  //m_channelTFM = new TFMFP(0);
  m_channelTFM = new TFMFP(0, m_myDRE);
}

VNHDMC::~VNHDMC() {
  delete m_channelTFM;
  //VNHD::~VNHD();
}

void VNHDMC::init(double chLLR) {
  // want to initialize the HD decoder with a "stochastic" bit
  double eChLLR = exp(chLLR);
  m_channelTFM->init( eChLLR / (eChLLR+1) );

  // put the stochastic bit in the LLR domain
  double pseudoLLR = 2 * m_channelTFM->getrnd() - 1;

  VNHD::init(pseudoLLR);
}

/* ----- Class VNDDBMP ----- */

VNDDBMP::VNDDBMP(int index, uint degree, BlockDecoder* pdec)
  : AbstractVariableNode(index, degree, MSGTYPE_DDBMP, pdec),
    SingleBitVN(degree)
{
#ifndef DDBMP_RNDTHRESHOLD
  m_counterInitVal = m_counterRange / 2;
#else
  m_dre = new StochDRE();
#endif

#ifndef DDBMP_TFMTRACKERS
  m_outputCounters = new int[degree];
  m_counterRange = m_config->getPropertyByName<int>("ddbmp_counterrange");
#else
  m_outputTrackers = new TFMFP*[degree];
  float beta = m_config->getPropertyByName<float>("ddbmp_beta");
  for(uint i=0; i<getDegree(); i++) m_outputTrackers[i] = new TFMFP(beta, m_dre);
#endif
}

VNDDBMP::~VNDDBMP() {
#ifdef DDBMP_RNDTHRESHOLD
  delete m_dre;
#endif

#ifndef DDBMP_TFMTRACKERS
  delete[] m_outputCounters;
#else
  for(uint i=0; i<getDegree(); i++) {
    delete m_outputTrackers[i];
  }
#endif
}

void VNDDBMP::init(double llr) {
  int curInitVal;

  m_firstIt = true;

#ifdef DDBMP_RNDTHRESHOLD
  if(llr <= 0) m_channelHD = 1; // (used in currentDecision())
  else m_channelHD = -1;
  // convert LLR prior to probability
  double inputProb = 1 / (exp(llr)+1);
  //  m_inputLLR = llr;
#else
  if(llr <= 0) { // that's the convention for LLRs in BPSKDemod
    m_channelHD = 1;
    curInitVal = m_counterInitVal;
  }
  else {
    m_channelHD = -1;
    curInitVal = (0-m_counterInitVal);
  }
#endif

#ifndef DDBMP_TFMTRACKERS
  // express prob on range [-1, 1] and multiply with counter upper bound
  curInitVal = lround((inputProb*2 - 1) * static_cast<double>(m_counterRange));
  for(uint i=0; i<getDegree(); i++) m_outputCounters[i] = curInitVal;
#else
  for(uint i=0; i<getDegree(); i++) m_outputTrackers[i]->init(inputProb);
#endif
}

char VNDDBMP::currentDecision() {
// #ifdef DDBMP_RNDTHRESHOLD
//   double llrSum = m_inputLLR;
//   for(uint i=0; i<getDegree(); i++) {
//     double curProb = static_cast<double>(m_outputCounters[i]) /
//       static_cast<double>(2*m_counterRange) + 0.5;
//     double curLLR = log( (1-curProb) / curProb );
//     llrSum+= curLLR;
//   }
//   if(llrSum >= 0) return 0;
//   else return 1;
// #else
  int sum = m_channelHD;

  for(uint i=0; i<getDegree(); i++) {
#ifndef DDBMP_TFMTRACKERS
    if( m_outputCounters[i] >= 0 ) sum++;
    else sum--;
#else
    if( m_outputTrackers[i]->getHD() == 0 ) sum--;
    else sum++;
#endif
  }

  if( sum >= 0 ) return 1;
  else return 0;
// #endif
}

void VNDDBMP::broadcast() {

  if(!m_firstIt) {
    // majority of all received HD's
    int majority = m_channelHD;  // include prior ***
    for(uint i=0; i<getDegree(); i++) {
      if( m_pendingMsg[i] == 0 ) majority--;
      else majority++;
    }

    // update the output counters
    for(uint i=0; i<getDegree(); i++) {
      int curMajority = majority;
      // remove intrinsic information
      if( m_pendingMsg[i] == 0 ) curMajority++;
      else curMajority--;

#ifndef DDBMP_TFMTRACKERS
    // update counter
    m_outputCounters[i]+= curMajority;
    // saturate
    if( m_outputCounters[i] >= m_counterRange) {
      m_outputCounters[i] = m_counterRange - 1;
    }
    else if( m_outputCounters[i] <= (0-m_counterRange) ) {
      m_outputCounters[i] = 1 - m_counterRange;
    }
#else
    // update TFM trackers
    float p = 0.5; //note: updating the TFM with 0.5 is not the same as not updating it
    if(curMajority > 0) {
      if(curMajority >= 5) p=1;
      else if(curMajority <= 2) p=0.75;
      else p=0.9;
    }
    else if(curMajority < 0) {
      if(curMajority <= -5) p=0;
      else if(curMajority >= -2) p=0.25;
      else p=0.1;
    }
    m_outputTrackers[i]->push(p);
#endif
    }//end: for
  } //end: not first iteration

#ifndef DDBMP_TFMTRACKERS
  // generate a threshold (same for all outputs)
#ifdef DDBMP_RNDTHRESHOLD
  float rndProb = m_dre->getVal_f();
  rndProb = rndProb*2 - 1;
  int threshold = lroundf(rndProb * static_cast<float>(m_counterRange));
#else
  int threshold = 0;
#endif
#endif

  // send all messages
  for(uint i=0; i<getDegree(); i++) {
#ifndef DDBMP_TFMTRACKERS
    if( m_outputCounters[i] >= threshold ) m_nodelist[i]->receive(1, m_myIDs[i]);
    else m_nodelist[i]->receive(0, m_myIDs[i]);
#else // DDBMP_TFMTRACKERS
    m_nodelist[i]->receive(m_outputTrackers[i]->getrnd(), m_myIDs[i]);
#endif
  }

#ifdef DDBMP_RNDTHRESHOLD
  // next random number
  m_dre->nextTick();
#endif
  m_firstIt = false;
} // end VNDDBMP::broadcast()

// (same code as VNHD::binaryTokenBcst)
void VNDDBMP::binaryTokenBcst(bool token) {
  char msg = 1;
  if(!token) msg = 0;
  for(uint i=0; i<getDegree(); i++) {
    m_nodelist[i]->receive(msg, m_myIDs[i]);
  }
}

/* ----- Class CheckNode ----- */

CheckNode::CheckNode(uint degree, int msgtype, 
                     std::vector<VariableNode*> nodelist,
                     BlockDecoder* pdec) {
  if( msgtype == MSGTYPE_LLR ) {
    m_implementation = new CheckNodeLLR(degree, nodelist, pdec);
  }
  else if( msgtype == MSGTYPE_MINSUM ) {
    m_implementation = new CheckNodeMinSum(degree, nodelist, pdec);
  }
  else if( msgtype == MSGTYPE_STOCH || msgtype == MSGTYPE_HDSTOCH ||
           msgtype == MSGTYPE_HD || msgtype == MSGTYPE_HDMC ||
           msgtype == MSGTYPE_DDBMP ) {
    m_implementation = new SingleBitCN(degree, msgtype, nodelist, pdec);
  }
  else if( msgtype == MSGTYPE_PARALSTOCH ) {
    m_implementation = new ParallelCN(degree, nodelist, pdec);
  }
  else {
    throw Exception("CheckNode::ctor", "Invalid message type");
  }
}

CheckNode::~CheckNode() {
  delete m_implementation;
}

/* ----- Class AbstractCheckNode ----- */

AbstractCheckNode::AbstractCheckNode(uint degree, int msgtype, 
                                     BlockDecoder* pdec) 
  : Node(degree, msgtype, pdec) 
{
  // check node degree
  if(degree < 1) 
    throw Exception("AbstractCheckNode::ctor", 
                    "CheckNode degree cannot be <1");

}

/* ----- Class CN_int ----- */

CN_int::CN_int(uint degree, vector<VariableNode*>& nodelist)
{
  m_myIDs = new uint[degree];
  m_pendingMsg = new int[degree];
  m_nodelist = new VN_int*[degree];

  // copy the node pointers
  for(uint i=0; i < degree; i++) {
    AbstractVariableNode* vn = nodelist[i]->getImplementation();
    m_nodelist[i] = dynamic_cast<VN_int*>(vn);
    if(m_nodelist[i] == 0) // (cast fails)
      throw Exception("CN_int::ctor", "Invalid VN node type");
  }

  // go through the nodelist and add this node to their connections
  // the call returns the ID to be used when sending a message to that node
  for(uint i=0; i < degree; i++) {
    m_myIDs[i] = m_nodelist[i]->addNode(this, i);
  }
}

CN_int::~CN_int() {
  delete[] m_myIDs;
  delete[] m_pendingMsg;
}

/* ----- Class CN_double ----- */

CN_double::CN_double(uint degree, vector<VariableNode*>& nodelist)
{
  m_myIDs = new uint[degree];
  m_pendingMsg = new double[degree];
  m_nodelist = new VN_double*[degree];

  // copy the node pointers
  for(uint i=0; i < degree; i++) {
    AbstractVariableNode* vn = nodelist[i]->getImplementation();
    m_nodelist[i] = dynamic_cast<VN_double*>(vn);
    if(m_nodelist[i] == 0) // (cast fails)
      throw Exception("CN_double::ctor", "Invalid VN node type");
  }

  // go through the nodelist and add this node to their connections
  // the call returns the ID to be used when sending a message to that node
  for(uint i=0; i < degree; i++) {
    m_myIDs[i] = m_nodelist[i]->addNode(this, i);
  }
}

CN_double::~CN_double() {
  delete[] m_pendingMsg;
  delete[] m_myIDs;
}

/* ----- Class CNSPALLR ----- */

CNSPALLR::CNSPALLR(uint degree, std::vector<VariableNode*>& nodelist,
                   int msgtype, BlockDecoder* pdec)
  : AbstractCheckNode(degree, msgtype, pdec),
    CN_double(degree, nodelist)
{
  m_outputBuffer = new double[getDegree()];
}

CNSPALLR::~CNSPALLR() {
  delete[] m_outputBuffer;
}

void CNSPALLR::binaryTokenBcst(bool token) {
  double msg = 1;
  if(!token) msg = 0;
  for(uint i=0; i<getDegree(); i++) {
    m_nodelist[i]->receive(msg, m_myIDs[i]);
  }
}

/* ----- Class CheckNodeLLR ----- */

CheckNodeLLR::CheckNodeLLR(uint degree, vector<VariableNode*>& nodelist,
                           BlockDecoder* pdec) 
  : CNSPALLR(degree, nodelist, MSGTYPE_LLR, pdec)
{
  m_buftanh = new double[degree];
}

CheckNodeLLR::~CheckNodeLLR() {
  delete[] m_buftanh;
}

void CheckNodeLLR::broadcast() {

  // compute all tanh
  for(uint i=0; i<getDegree(); i++) {
    m_buftanh[i] = tanh(m_pendingMsg[i]/2);
  }

  // send a message on each edge
  for(uint i=0; i<getDegree(); i++) {
    double llr = 1;
    for(uint j=0; j<getDegree(); j++) {
      if(j!=i) llr*= m_buftanh[j];
    }

    llr = 2*atanh(llr);

    // make sure llr is a valid FP number
    if(llr!=llr) {
      throw Exception("CheckNode::broadcastLLR", "Invalid outgoing message");
    }

    // send the message
    m_nodelist[i]->receive(llr, m_myIDs[i]);

  }
}

char CheckNodeLLR::curParity() {
  int sum=0;
  for(uint i=0; i<getDegree(); i++) {
    if(m_pendingMsg[i] < 0.0) sum++;  // note that LLRs are "reversed"
  }
  return (sum & 1);
}

/* ----- Class CheckNodeMinSum ----- */

CheckNodeMinSum::CheckNodeMinSum(uint degree, vector<VariableNode*>& nodelist,
                                 BlockDecoder* pdec)
  : CNSPALLR(degree, nodelist, MSGTYPE_MINSUM, pdec)
{
  m_signbuf = new int[degree];
#ifdef MINSUM_SINGLEMIN
  m_secondMinOffset = m_config->getPropertyByName<double>("minsum_secondminoffset");
#endif

  // offset for offset min-sum
  m_offset = 0;
  try {
    m_offset = m_config->getPropertyByName<double>("minsum_offset");
  } catch(InvalidKeyException&) {}

  // ----- Redecoding modes -----

  // first check whether we're randomizing in CNs or VNs
  bool doRedecodeInCN = false;
  try {
    string tmp = m_config->getPropertyByName<string>("redecode_nodetype");
    if(tmp == "cn" || tmp == "CN") doRedecodeInCN = true;
  } catch(InvalidKeyException&) {}

  // initialize the flags for the various redecoding algorithms
  m_redecodeRndOffsetMode = false;
  m_redecodeUniRndSignMode = false;
  m_redecodeUniRndSign2Mode = false;
  m_redecodePropRndSignMode = false;
  m_redecodePropRndSign2Mode = false;

  if(doRedecodeInCN) {
    m_redecodeRndOffset = 0;
    try {
      m_redecodeRndOffsetMode = m_config->getPropertyByName<bool>("spa_redecode_rndoffset");
    } catch(InvalidKeyException&) {}
    if(m_redecodeRndOffsetMode) {
      m_redecodeRndOffset = m_config->getPropertyByName<double>("spa_redecode_rndoffset_val"); // (mandatory in this case)
    }
    
    m_redecodeFlipProb = 0;
    try {
      m_redecodeUniRndSignMode = m_config->getPropertyByName<bool>("spa_redecode_unirndsign");
    } catch(InvalidKeyException&) {}
    try {
      m_redecodeUniRndSign2Mode = m_config->getPropertyByName<bool>("spa_redecode_unirndsign2");
    } catch(InvalidKeyException&) {}
    
    if(m_redecodeUniRndSignMode || m_redecodeUniRndSign2Mode) {
      m_redecodeFlipProb = m_config->getPropertyByName<double>("spa_redecode_flipprob");
    }
    
    m_redecodePropRndSignCst = 0;
    try {
      m_redecodePropRndSignMode = m_config->getPropertyByName<bool>("spa_redecode_proprndsign");
    } catch(InvalidKeyException&) {}

    try {
      m_redecodePropRndSign2Mode = m_config->getPropertyByName<bool>("spa_redecode_proprndsign2");
    } catch(InvalidKeyException&) {}

    if(m_redecodePropRndSignMode || m_redecodePropRndSign2Mode) {
      m_redecodePropRndSignCst = m_config->getPropertyByName<double>("spa_redecode_proprndsign_c"); // (mandatory in this case)
      m_redecodePropRndSignCst = 1 / m_redecodePropRndSignCst;
      string rntype = m_config->getPropertyByName<string>("spa_redecode_proprndsign_rntype"); // (mandatory in this case)
      if( rntype == "llr" || rntype == "LLR" ) {
        m_redecodePropRndSignRNTypeLLR = true;
      } else if( rntype == "prob" || rntype == "Prob" ) {
        m_redecodePropRndSignRNTypeLLR = false;
      } else {
        throw Exception("CheckNodeMinSum::ctor", "Invalid value for key \"spa_redecode_proprndsign_rntype\"");
      }
    }

    if(m_redecodeUniRndSign2Mode || m_redecodePropRndSign2Mode) {
      m_redecodeFlipValue = m_config->getPropertyByName<double>("spa_redecode_flipvalue");
    }
    // ----- End redecoding modes -----
  }

  m_llrLimit = m_config->getPropertyByName<double>("llr_limit");

#ifdef MINSUM_CNRELAX
  m_prevMinVal = 0;
#endif
}

CheckNodeMinSum::~CheckNodeMinSum() {
  delete[] m_signbuf;
}

void CheckNodeMinSum::broadcast() {

  int totalSign = 0; // sign of the product of all hard-decisions
  double minVal = fabs(m_pendingMsg[0]); // minimum absolute value
  int minIndex = 0; // msg index corresponding to minVal

  for(uint i=0; i<getDegree(); i++) {
    m_signbuf[i] = (m_pendingMsg[i] < 0.0);
    totalSign^= m_signbuf[i];
    double curAbsVal = fabs(m_pendingMsg[i]);
    if(curAbsVal < minVal) {
      minVal = curAbsVal;
      minIndex = i;
    }
  }

  // find the second smallest value
  double minVal2;
#ifndef MINSUM_SINGLEMIN
  if(minIndex==0) minVal2 = fabs(m_pendingMsg[1]);
  else minVal2 = fabs(m_pendingMsg[0]);
  for(uint i=0; i<getDegree(); i++) {
    double curAbsVal = fabs(m_pendingMsg[i]);
    if( i!=minIndex && curAbsVal < minVal2 ) {
      minVal2 = curAbsVal;
    }
  }
#endif

  // add offsets
#ifdef MINSUM_SCALEMIN
  minVal*= m_offset;
#else
  minVal-= m_offset;
  if(minVal<0) minVal=0;
#endif
#ifdef MINSUM_CNRELAX // perform relaxation on 1st min
  minVal =  minVal/2 + m_prevMinVal/2; // hardcoded beta=0.5
  m_prevMinVal = minVal;
#endif
#ifdef MINSUM_SINGLEMIN
  minVal2 = minVal + m_secondMinOffset;
  // saturate at the LLR limit
  if(minVal2 > m_llrLimit) minVal2 = m_llrLimit;
#else
#ifdef MINSUM_SCALEMIN
  minVal2*= m_offset;
#else
  minVal2-= m_offset;
  if(minVal2<0) minVal2=0;
#endif
#endif

#ifndef NO_DECODER
  if(getDecoder()->getRedecodeMode()) {
    // note1: arguments are passed as references
    // note2: functions might make the minima negative to indicate sign flip
    if(m_redecodeRndOffsetMode) rndAdditiveOffset(minVal, minVal2);
    else if(m_redecodeUniRndSignMode) uniformSignFlip(minVal, minVal2);
    else if(m_redecodeUniRndSign2Mode) uniformSignFlip2(minVal, minVal2);
    else if(m_redecodePropRndSignMode) propSignFlip(minVal, minVal2);
    else if(m_redecodePropRndSign2Mode) propSignFlip2(minVal, minVal2);
  }
#endif // not defined NO_DECODER

  // send output messages
  for(uint i=0; i<getDegree(); i++) {
    double outVal;
    if(i != minIndex) outVal = minVal;
    else outVal = minVal2;
    
    int curSign = totalSign ^ m_signbuf[i];
    if(curSign != 0) outVal*= (-1);
    m_nodelist[i]->receive(outVal, m_myIDs[i]);
  }

}

// same code as CheckNodeLLR::curParity()
char CheckNodeMinSum::curParity() {
  int sum=0;
  for(uint i=0; i<getDegree(); i++) {
    if(m_pendingMsg[i] < 0.0) sum++; // note that LLRs are "reversed"
  }
  return (sum & 1);
}

// ---------- Protected ----------

void CheckNodeMinSum::rndAdditiveOffset(double& min1, double& min2) {
  double rndOffset = static_cast<double>(random()) / RAND_MAX;
  rndOffset = (rndOffset - 0.5) * m_redecodeRndOffset * 2;
  min1+= rndOffset;
  min2+= rndOffset;
  // saturate at the LLR limit
  if(min1 > m_llrLimit) min1 = m_llrLimit;
  if(min2 > m_llrLimit) min2 = m_llrLimit;
}

void CheckNodeMinSum::uniformSignFlip(double& min1, double& min2) {
  double rv = static_cast<double>(random()) / RAND_MAX;
  if( rv < m_redecodeFlipProb) {
    min1*= (-1);
    min2*= (-1);
  }
}

void CheckNodeMinSum::uniformSignFlip2(double& min1, double& min2) {
  double rv = static_cast<double>(random()) / RAND_MAX;
  if( rv < m_redecodeFlipProb) {
    min1 = -m_redecodeFlipValue;
    min2 = -m_redecodeFlipValue;
  }
}

void CheckNodeMinSum::propSignFlip(double& min1, double& min2) {
  double rv = static_cast<double>(random()) / RAND_MAX;
  double rv_llr;
  if(m_redecodePropRndSignRNTypeLLR) {
    rv_llr = rv * m_llrLimit; // ~U(0, llr_limit)
  }
  else {
    rv = rv * 0.5 + 0.5; // ~U(0.5, 1)
    rv_llr = log( rv / (1-rv) );
  }
  rv_llr*= m_redecodePropRndSignCst;

  if(min1 < rv_llr) min1*= (-1);
  if(min2 < rv_llr) min2*= (-1);
}

void CheckNodeMinSum::propSignFlip2(double& min1, double& min2) {
  double rv = static_cast<double>(random()) / RAND_MAX;
  double rv_llr;
  if(m_redecodePropRndSignRNTypeLLR) {
    rv_llr = rv * m_llrLimit; // ~U(0, llr_limit)
  }
  else {
    rv = rv * 0.5 + 0.5; // ~U(0.5, 1)
    rv_llr = log( rv / (1-rv) );
  }
  rv_llr*= m_redecodePropRndSignCst;

  //note: supplied arguments min1 and min2 are absolute values
  if(min1 < rv_llr) min1= -m_redecodeFlipValue;
  if(min2 < rv_llr) min2= -m_redecodeFlipValue;
}

/* ----- Class SingleBitCN ----- */

SingleBitCN::SingleBitCN(uint degree, int msgtype,
                         std::vector<VariableNode*>& nodelist,
                         BlockDecoder* pdec) 
  : AbstractCheckNode(degree, msgtype, pdec)
{
  m_myIDs = new uint[degree];
  m_pendingMsg = new char[degree];
  m_nodelist = new SingleBitVN*[degree];

  // initialize pending messages to zero
  for(uint i=0; i<degree; i++) m_pendingMsg[i] = 0;
  
  // copy the node pointers
  for(uint i=0; i < degree; i++) {
    AbstractVariableNode* vn = nodelist[i]->getImplementation();
    m_nodelist[i] = dynamic_cast<SingleBitVN*>(vn);
    if(m_nodelist[i] == 0)
      throw Exception("SingleBitCN::ctor", "Invalid VN node type");
  }

  // go through the nodelist and add this node to their connections
  // the call returns the ID to be used when sending a message to that node
  for(uint i=0; i < degree; i++) {
    m_myIDs[i] = m_nodelist[i]->addNode(this, i);
  }
}


SingleBitCN::~SingleBitCN() {
  delete[] m_pendingMsg;
  delete[] m_myIDs;
}

void SingleBitCN::broadcast() {
  char totalxor = 0;

  for(uint i=0; i<getDegree(); i++) {
    totalxor^= m_pendingMsg[i];
  }
  for(uint i=0; i<getDegree(); i++) {
    // (remove the message that came from edge i)
    char msg = totalxor ^ m_pendingMsg[i];
    m_nodelist[i]->receive(msg, m_myIDs[i]);
  }
}

char SingleBitCN::curParity() {
  char totalxor=0;
  for(uint i=0; i<getDegree(); i++) {
    totalxor^= m_pendingMsg[i];
  }
  return totalxor;
}

vector<SingleBitVN*> SingleBitCN::getNeighbors() {
  vector<SingleBitVN*> list;
  for(uint i=0; i<getDegree(); i++) {
    list.push_back(m_nodelist[i]);
  }
  return list;
}

// (same code as VNHD::binaryTokenBcst)
void SingleBitCN::binaryTokenBcst(bool token) {
  char msg = 1;
  if(!token) msg = 0;
  for(uint i=0; i<getDegree(); i++) {
    m_nodelist[i]->receive(msg, m_myIDs[i]);
  }
}

/* ----- Class ParallelCN ----- */

ParallelCN::ParallelCN(uint degree, vector<VariableNode*>& nodelist,
                       BlockDecoder* pdec)
  : AbstractCheckNode(degree, MSGTYPE_PARALSTOCH, pdec),
    CN_int(degree, nodelist)
{
}

void ParallelCN::broadcast() {
  int parityVect = m_pendingMsg[0];
  for(uint i=1; i<getDegree(); i++) {
    parityVect^= m_pendingMsg[i];
  }

  // send a message on each edge
  for(uint i=0; i<getDegree(); i++) {
    int msg = parityVect ^ m_pendingMsg[i];
    m_nodelist[i]->receive(msg, m_myIDs[i]);
  }
}

char ParallelCN::curParity() {
  throw Exception("ParallelCN::curParity", "Not supported");
}
