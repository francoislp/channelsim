//Author: Francois Leduc-Primeau

#ifndef _Node_h_
#define _Node_h_

#include "compileflags.h"
#include "GlobalDefs.h"

#include "stoch/IStochDRE.hpp"
#include "stoch/IEdgeMem.hpp"
#include "stoch/ITFM.hpp"
#include "stoch/ShiftReg.hpp"
#include "Config.hpp"
#include "stoch/ILLRTracker.hpp"
#include "stoch/TFMFP.hpp"
#include "stoch/IRandomLLR.hpp"
#include "stoch/ILLRtoBits.hpp"
#include "deviation/LLRDeviationPMF.hpp"

#include <stdexcept>
#include <vector>
#include <string>


#include "DecoderTypes.h"

// Berkeley post-processing algorithm:
#define ISTRUETOKEN_DBL(x) ((x) > 0)
#define ISFALSETOKEN_DBL(x) ((x) < 0)

// forward declarations
class CheckNodeLLR;
class CheckNodeLLRQuant;
class SingleBitCN;
class CN_int;
class CN_double;
class BlockDecoder; // (defined in BlockDecoder.h)

/**
 * (Abstract) Super-class for variable nodes and check nodes of the belief
 * propagation graph.
 */
class Node {
 public:
  /**
   * Instantiates a new Node with an empty connection list.
   *@param degree  The number of connections this Node has.
   *@param msgtype The type of message this node receives and sends.
   *               Must be one of the MSGTYPE_* constants.
   */
  Node( uint degree, int msgtype, BlockDecoder* pdecoder );

  /**
   * Returns the degree of this Node.
   */
  uint getDegree() {return m_degree;}

  int getMsgType() {return m_msgtype;}

  /**
   * Send an updated message to all the neighbors.
   */
  virtual void broadcast() = 0;

  virtual void binaryTokenBcst(bool token) = 0; // (Berkeley post-proc.)

  // virtual destructor
  virtual ~Node() {}

 protected:

  BlockDecoder* getDecoder() { return m_pDecoder; }

  /// Cached handle to the configuration object
  Config* m_config;

 private:

  uint   m_degree;

  /// Type of messages sent and received by this Node.
  int    m_msgtype;

  /// The decoder that uses this node.
  BlockDecoder* m_pDecoder;
};

/**
 * Represents a "variable" node in the belief propagation graph.
 */
class AbstractVariableNode : public Node {
public:
  /**
   * Creates a new variable node of the specified degree.
   */
  AbstractVariableNode( int index, uint degree, int msgtype, 
                        BlockDecoder* pdec );

  virtual ~AbstractVariableNode() {}

  ///@see VariableNode::baseInit
  virtual void baseInit() =0;

  ///@see VariableNode::init
  virtual void init(double) =0;

  virtual char currentDecision() =0;

  int getIndex() {return m_index;}

  virtual void berkeleyPPInit() =0;
                               
  virtual void berkeleyPPSend() =0;

  // void setPunctured(bool val) { m_punctured = val;}

  // bool isPunctured() { return m_punctured; }

	virtual void applyDeviations(LLRDeviationPMF* devgen) =0;
	virtual void applyTotalDeviations(LLRDeviationPMF* devgen) =0;
	virtual void devSetTxBit(int tx) =0;
	virtual char sampleExtrinsicDecision() =0;
	virtual char samplePrevExtrinsicDecision() =0;

private:
  int m_index;

  /// Whether this VN is punctured (receives no input data).
  // bool m_punctured;
};

/// Interface of the strategy pattern for Variable Nodes.
class VariableNode {
public:

  VariableNode( int index, uint degree, int msgtype, BlockDecoder* pdec );

  /// Constructor used for stochastic nodes
  VariableNode( int index, uint degree, int msgtype, 
                std::vector<IStochDRE*> drelist,
                BlockDecoder* pdec);

  /// Constructor for RHS nodes
  VariableNode( int index, uint degree,
                std::vector<IRandomLLR*> drelist,
                BlockDecoder* pdec);

  ~VariableNode();

  /**
   * This method should be called as soon as the graph is set up.
   */
  void baseInit() { return m_implementation->baseInit(); }

  int getMsgType() { return m_msgtype; }

  AbstractVariableNode* getImplementation() { return m_implementation; }

  ///@see Node::broadcast()
  void broadcast() { return m_implementation->broadcast(); }
  
  /**
   * Sets the received channel information for this Variable
   * Node. Most implementations expect that information in the LLR
   * domain, although some expect a BPSK value (stochastic VN's that
   * use NDS). The macro ISSTOCHASTICDECODER() can be used on the
   * return value of getMsgType() to determine whether a BPSK value
   * must be passed instead of a LLR (should double check !). One can
   * also check the implementation's documentation to double check
   * what type of value is expected.
   */
  void init(double value) { return m_implementation->init(value); }

  /**
   * Returns the current hard decision about this bit.
   *@return 0 or 1
   */
  char currentDecision() { return m_implementation->currentDecision(); }

  void setStochNDS(double NDS_cst); //TODO: this should be in VariableNodeStoch

  void binaryTokenBcst(bool token) { // (Berkeley post-proc.)
    return m_implementation->binaryTokenBcst(token);
  }

  void berkeleyPPInit() { return m_implementation->berkeleyPPInit(); }

  void berkeleyPPSend() { return m_implementation->berkeleyPPSend(); }

  // void setPunctured(bool val) { m_implementation->setPunctured(val); }

  // bool isPunctured() { return m_implementation->isPunctured(); }

	/**
	 * Apply deviations on LLR messages sent by this variable node to
	 * neighboring check nodes, using the specified deviation
	 * generator. A zero pointer can be passed to turn off deviations.
	 */
	void applyDeviations(LLRDeviationPMF* devgen) {
		m_implementation->applyDeviations(devgen);
	}

	/**
	 * Applies deviations to the VN total using the specified deviation
	 * generator pointer. This affects the result of currentDecision().
	 */
	void applyTotalDeviations(LLRDeviationPMF* devgen) {
		m_implementation->applyTotalDeviations(devgen);
	}

	void devSetTxBit(int tx) {
		m_implementation->devSetTxBit(tx);
	}

	/**
	 * Similar to currentDecision(), but returns the bit estimate
	 * that corresponds to one of the extrinsic messages.
	 */
	char sampleExtrinsicDecision() {
		return m_implementation->sampleExtrinsicDecision();
	}

	/**
	 * Returns the decision corresponding to one of the extrinsic
	 * messages that was sent after the previous call to broadcast().
	 */	
	char samplePrevExtrinsicDecision() {
		return m_implementation->samplePrevExtrinsicDecision();
	}

 private:

  // --- Data Members ---

  int m_msgtype;

  AbstractVariableNode* m_implementation;

}; // ----- End Class VariableNode ----------

class VN_int {
public:

  VN_int(uint degree);
  virtual ~VN_int();

  uint addNode(CN_int* node, uint myID);

  void receive(int msg, uint senderID) { m_pendingMsg[senderID] = msg; }

protected:

  uint* m_myIDs;

  int* m_pendingMsg;

  CN_int** m_nodelist;

private:

  int m_nextAddIndex;
}; // ----- End Class VN_int -----

class VN_double {
public:

  VN_double(uint degree);

  virtual ~VN_double();

  /**
   * Adds a Check Node to the connection list.
   *@param node Pointer to the node to be added.
   *@param myID The ID for this Node when sending a message to "node" (the 
   *            node being added).
   *@return The ID associated with "node" (to be used when "node" sends 
   *        a message to this Node).
   */
  uint addNode(CN_double* node, uint myID);

  /**
   * Receive a message from sender with ID "senderID". This overwrites
   * the previous message from that sender.
   */
  virtual void receive(double msg, uint senderID) { 
    m_pendingMsg[senderID] = msg;
  }

protected:

  // ----- Data -----

  /**
   * Last message received on each edge.
   */
  double* m_pendingMsg;

  uint* m_myIDs;

  /**
   * List of pointers to neighbor check nodes (of size getDegree()).
   */
  CN_double** m_nodelist;

private:

  int m_nextAddIndex;

}; // ----- End Class VN_double -----

/**
 * Parent class for SPA variable node functions (using LLR
 * messages). Implements functionality common to all variants of the
 * SPA VN function.
 */
class VNSPALLR : public AbstractVariableNode, public VN_double {
public:

  VNSPALLR(int index, uint degree, BlockDecoder* pdec);

  ~VNSPALLR();

  void baseInit() {}

  void binaryTokenBcst(bool token);

  void berkeleyPPInit();

  // "void berkeleyPPSend()" implemented in child class

protected:

  // ----- Methods -----

  /**
   * (Dithering algorithm) Modifies the values in the array
   * <code>outputList</code> (assumed of length <code>m_degree</code>)
   * by randomly flipping the sign.
   */
  void uniformSignFlip(double* outputList);

  /// Adds a random offset to argument <code>inputLLR</code> (by ref).
  void randomizeInput(double& inputLLR);

  // ----- Data -----

  /// Whether to use uniform random sign flip when redecoding.
  bool m_redecodeUniRndSignMode;

  /// Whether to add a random offset to the prior
  bool m_doRandomInitState;
  // (see also related private variables)

  bool m_useVNHarmonization;
#ifdef VN_HARMONIZE
  double m_harmonizeCst;
#endif

  /// (Berkeley PP algorithm) Whether node is in neighborhood of N_o(T)
  bool m_berkPPInNeighb;

  /// (Berkeley PP algorithm) For each neighbor CN, whether was unsatisfied.
  bool* m_berkPPUnsatList;

  /// (Berkeley PP algorithm) High reliability constant.
  double m_berkPPHighVal;

  /// (Berkeley PP algorithm) Low reliability constant.
  double m_berkPPLowVal;

private:

  /// Value of p in dithering algorithm "uniform-random sign flip".
  double m_redecodeFlipProb;

  /**
   * Standard deviation parameter of the Normal distribution used to
   * generate random offsets to be applied to the input prior. A zero
   * value indicates it should not be used.
   */
  float m_randomInitStdDev;

  /// see config key "redecode_random_init_constantvar"
  double m_randomInitTotalVar;

  /// Whether input random offsets should be quantized.
  bool   m_quantizeRandomInitState;
  double m_randomInitQuantRange;
  double m_randomInitQuantResol;
  /// Whether input random offsets should be uniform instead of gaussian
  bool   m_randomInitIsUniform;
  /// Twice the upper bound of the symmetric range for the uniform random nb
  double m_randomInitUniformBound;

  /// Used to generate random prior offsets.
  IStochDRE* m_dre;

  double* m_outputBuffer;

}; // ----- End Class VNSPALLR -----

/**
 * Implements floating-point SPA with LLR messages.
 */
class VariableNodeLLR : public VNSPALLR {
public:
  VariableNodeLLR(int index, uint degree, BlockDecoder* pdec);
  ~VariableNodeLLR();

  /**
   * Sets the received LLR value, and prepares the VN for a new
   * decoding round.
   *@see VariableNode::init()
   */
  void init(double value);

  ///@see Node::broadcast()
  void broadcast();

  /**
   * Returns the current hard decision about this bit.
   *@return 0 or 1
   */
  char currentDecision();

  void berkeleyPPSend();

	/**
	 *@see VariableNode::applyDeviations.
	 */
	void applyDeviations(LLRDeviationPMF* devgen) { m_pDevGen= devgen; }

	/**
	 *@see VariableNode::applyTotalDeviations.
	 */
	void applyTotalDeviations(LLRDeviationPMF* devgen) { m_pTotDevGen= devgen; }

	/**
	 * Inform the node of the actual transmitted bit so it can sample
	 * the proper deviation.
	 *@param tx Either 0 or 1.
	 *@throws std::logic_error  If the supplied tx value is not 0 or 1.
	 */
	void devSetTxBit(int tx) {
		if(tx!=0 && tx!=1) throw std::logic_error("invalid tx bit");
		m_devTxSign= -2*tx+1;
	}

	/**
	 *@see VariableNode::sampleExtrinsicDecision()
	 */
	char sampleExtrinsicDecision();

	/**
	 *@see VariableNode::samplePrevExtrinsicDecision()
	 */
	char samplePrevExtrinsicDecision();

private:

  double currentLLR();

  // --- Data Members ---

  /// A-priori LLR (from the demodulator).
  double m_init;

  /// List of outgoing messages (of size <code>getDegree()</code>)
  double* m_outputBuffer;

  bool m_useProbabilisticUpdate;

  /// Used by the probabilistic update feature
  float m_updateProb;

  /// Flag indicating the first decoding cycle (iteration).
  bool m_firstIt;

  double m_llrLimit;

#ifdef VN_HARMONIZE
  /// Whether the Auto VN Harmonization is turned on.
  bool m_autoVNHarmonization;

  /// Whether VN Harmonization is currently active.
  bool m_vnHarmActive;

  /**
   * In the Auto VN Harmonization alg., the LLR magnitude (positive)
   * at which a VN input is considered to be "saturated".
   */
  double m_autoVnHarmSat;

  /**
   * In the Auto VN Harmonization alg., the number of saturated inputs
   * required to active the VN Harmonization procedure.
   */
  uint m_autoVnHarmTrig;
#endif

	LLRDeviationPMF* m_pDevGen;
	LLRDeviationPMF* m_pTotDevGen;

	int m_devTxSign; // actual tx sign to be used when applying deviations

}; // ----- End Class VariableNodeLLR ----------

/**
 * Relaxed SPA variable node, suitable for SPA and min-sum
 * algorithms. The value of the relaxation factor beta is read from
 * the config file.
 */
class VNRelaxedLLRSPA : public VNSPALLR {
public:

  VNRelaxedLLRSPA(int index, uint degree, BlockDecoder* pdec);

  ~VNRelaxedLLRSPA();

  void init(double value);

  void broadcast();

  char currentDecision();

  void berkeleyPPSend() { 
    throw Exception("VNRelaxedLLRSPA::berkeleyPPSend", "Unimplemented");
  }

	void applyDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyDeviations", "Unimplemented");
	}
	void applyTotalDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyTotalDeviations", "Unimplemented");
	}
	void devSetTxBit(int tx) {
		throw Exception("devSetTxBit", "Unimplemented");
	}
	char sampleExtrinsicDecision() {
		throw Exception("sampleExtrinsicDecision", "Unimplemented");
	}
	char samplePrevExtrinsicDecision() {
		throw Exception("samplePrevExtrinsicDecision", "Unimplemented");
	}

private:

  /// A-priori LLR (from the demodulator).
  double m_init;

  /// Memory for last outgoing LLR message
  double* m_outMsgList;

  /// Relaxation factor
  double m_beta;

  /// 1 - m_beta
  double m_betaComp;

  bool m_useProbabilisticUpdate;

  /// Used by the probabilistic update feature
  float m_updateProb;

  /// Flag indicating the first decoding iteration.
  bool m_firstIt;

  double m_llrLimit;

#ifdef VN_TOTALRELAX
  double m_prevTotal;
#endif
  
};

/**
 * Relaxed SPA/min-sum variable node that performs the relaxation at
 * the input.
 */
class VNRelaxedInLLRSPA : public VNSPALLR {
public:

  VNRelaxedInLLRSPA(int index, uint degree, BlockDecoder* pdec);

  ~VNRelaxedInLLRSPA();

  void init(double value);

  /// Overrides VN_double::receive
  void receive(double msg, uint senderID) {
#ifdef ADAPTIVE_BETA
    // use a beta that is a function of the *new message*
    //note: variable naming abuse (local "m_") for simplicity
    double m_beta = adaptiveBeta(msg);
    double m_betaComp = 1 - m_beta;
#endif
    m_inMsgList[senderID] = m_betaComp*m_inMsgList[senderID] + m_beta*msg;
  }

  void broadcast();

  char currentDecision();

  void berkeleyPPSend() { 
    throw new Exception("VNRelaxedInLLRSPA::berkeleyPPSend", "Unimplemented");
  }

	void applyDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyDeviations", "Unimplemented");
	}
	void applyTotalDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyTotalDeviations", "Unimplemented");
	}
	void devSetTxBit(int tx) {
		throw Exception("devSetTxBit", "Unimplemented");
	}
	char sampleExtrinsicDecision() {
		throw Exception("sampleExtrinsicDecision", "Unimplemented");
	}
	char samplePrevExtrinsicDecision() {
		throw Exception("samplePrevExtrinsicDecision", "Unimplemented");
	}
	
private:

#ifdef ADAPTIVE_BETA
  //  double adaptiveBeta(double llr) { return fabs(llr) * 0.0875; }
  double adaptiveBeta(double llr) { return fabs(llr) * 0.05 + 0.4; }
#endif

  // ---------- Data ----------

  /// A-priori LLR (from the demodulator).
  double m_init;

#ifndef ADAPTIVE_BETA
  /// Relaxation factor.
  double m_beta;
  /// 1 - m_beta
  double m_betaComp;
#endif

  /// Input memories
  double* m_inMsgList;

  double m_llrLimit;

  /// List of outgoing messages (of size <code>getDegree()</code>)
  double* m_outputBuffer;

  bool m_useProbabilisticUpdate;

  /// Used by the probabilistic update feature
  float m_updateProb;

  /// Flag indicating the first decoding iteration.
  bool m_firstIt;
};

/**
 * Parent class of Variable Nodes that send and receive one bit
 * messages. Can be abused to receive up to 8 bits/msg packed in a
 * <code>char</code> type.
 */
class SingleBitVN {
public:

  SingleBitVN(uint degree);

  virtual ~SingleBitVN();

  /**
   * Adds a Check Node to the connection list.
   *@param node Pointer to the node to be added.
   *@param myID The ID for this Node when sending a message to "node" (the 
   *            node being added).
   *@return The ID associated with "node" (to be used when "node" sends 
   *        a message to this Node).
   */
  uint addNode(SingleBitCN* node, uint myID);

  virtual void receive(char bit, uint senderID) { 
    m_pendingMsg[senderID] = bit; 
  }

  std::vector<SingleBitCN*> getNeighbors();

protected:

  char* m_pendingMsg;

  uint* m_myIDs;

  /// Connections to Check Nodes
  SingleBitCN** m_nodelist;

private:

  uint m_degree;

  uint m_nextAddIndex;

  std::vector<int> m_neighborOrdering;

};

/**
 * Interface for stochastic variable nodes.
 */
class IVNStoch : public AbstractVariableNode {
public:

  virtual void init(double) = 0;
  virtual void broadcast() = 0;
  virtual char currentDecision() = 0;
  virtual int  curReliability() = 0;
  virtual void setTrainingMode(bool) = 0;
  virtual void setNDSCst(double) = 0;
  //virtual void setChSat(double) = 0;

  virtual ~IVNStoch() {}

protected:
  IVNStoch(int index, uint degree, int msgtype, BlockDecoder* pdec)
    : AbstractVariableNode(index, degree, msgtype, pdec) {}
};

/**
 * Common structure for stochastic VNs: channel input initialization,
 * edge-memories, etc. Does not implement broadcast().
 */
class VariableNodeStoch : public IVNStoch {

public:
  /**
   * Instantiates a new Stochastic Variable Node.
   *@param degree The node degree (excluding the initialization edge).
   *@param drelist List of DREs. Must contain either 1 or degree+1 elements.
   *               (see also VariableNodeStoch6::ctor).
   *@param EMSizeMult EM size found in config will be multiplied by this value.
   *@param pdec  Pointer to the decoder controlling this node.
   */
  VariableNodeStoch(int index, uint degree, std::vector<IStochDRE*> drelist,
                    float EMSizeMult,
                    BlockDecoder* pdec);

  ~VariableNodeStoch();

  void baseInit() {}

  /**
   * Sets the raw (BPSK) channel output for this Variable Node.
   */
  void init(double chVal);

  /**
   * Returns the current hard decision about this bit.
   *@return 0 or 1
   */
  char currentDecision();

  /**
   * Returns the value of the output counter (which expresses the hard
   * decision (pos or neg) and its reliability (magnitude)).
   */
  int curReliability() { return m_outputEdgeCount; }

  void setTrainingMode(bool val) {m_training = val;} //tentative feature

  void setNDSCst(double val) { m_NDS_cst = val; }

  /**
   * Sets the channel input saturation value for this Node and updates
   * the input probability.
   */
  //void setChSat(double val) { m_chsat = val; updateInputProb(); }

  int getNUCount() { return m_noupdateCount; }

  /**
   * Returns 0 if this Node saw a regenerative bit on its output edge
   * in its last iteration, 1 otherwise.
   */
  char getLastNU() { return m_noupdateLast; }

  void alternativeInput() { // tentative
    // technique #1
    if(m_alternInput<0) m_alternInput = m_channelTFM->getrnd();
    // technique #2
    //m_alternInput = currentDecision();
  }

  void cancelAlternInput() { m_alternInput = -1; } // tentative

  void binaryTokenBcst(bool token) {
    throw new Exception("VariableNodeStoch::binaryTokenBcst", "Unimplemented");
  }

  void berkeleyPPInit() {
    throw new Exception("VariableNodeStoch::berkeleyPPInit", "Unimplemented");
  }
                     
  void berkeleyPPSend() {
    throw new Exception("VariableNodeStoch::berkeleyPPSend", "Unimplemented");
  }

	void applyDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyDeviations", "Unimplemented");
	}
	void applyTotalDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyTotalDeviations", "Unimplemented");
	}
	void devSetTxBit(int tx) {
		throw Exception("devSetTxBit", "Unimplemented");
	}
	char sampleExtrinsicDecision() {
		throw Exception("sampleExtrinsicDecision", "Unimplemented");
	}
	char samplePrevExtrinsicDecision() {
		throw Exception("samplePrevExtrinsicDecision", "Unimplemented");
	}	

protected: // --------------------------------------------------

  int m_noupdateCount; // stats
  char m_noupdateLast; // stats

  /// Input probability
  ITFM* m_channelTFM;

  /// Edge-Memories
  IEdgeMem** m_EMList;
  int m_EMsize;

  int m_outputEdgeCount;

  bool m_firstIt;
  bool m_training; //tentative

  /// Value of alpha/Y for computing scaled LLRs using NDS
  double m_NDS_cst;

  /// Saturation value for output counters
  int m_counterSat;

  /// "Alternative" input value (from the channel)
  char m_alternInput; // tentative

private: // --------------------------------------------------

  /**
   * Updates the input probability by computing it from m_curChVal.
   *@return The probability value that was passed to the channel TFM.
   */
  double updateInputProb();

  /**
   * Updates the input probability by computing it from the supplied LLR.
   *@return The probability value that was passed to the channel TFM.
   */
  double updateInputProb_llr(double llr);

  // ----- Data -----

  /// Current channel value
  double m_curChVal;

};

/**
 * General implementation of a stochastic VN (no subnodes).
 */
class VariableNodeStochGen : public VariableNodeStoch, public SingleBitVN {
public:

  /**
   * ***Note that EM base size is not multiplied by anything.
   */
  VariableNodeStochGen(int index, uint degree, std::vector<IStochDRE*> drelist,
                       BlockDecoder* pdec);

  void init(double);

  void broadcast();
};

/**
 * Specialized implementation of a stochastic VN of degree 3 (which means the
 * VN is connected to 3 check nodes).
 */
class VariableNodeStoch3 : public VariableNodeStoch, public SingleBitVN {
public:
  /**
   * Instantiates a degree-3 Variable Node. Note that EM base size is
   * multiplied by 1.5.
   *@param index      Index of the node in the graph (bit index).
   *@param drelist    List of DREs (see VariableNodeStoch::ctor).
   */
  VariableNodeStoch3(int index, std::vector<IStochDRE*> drelist, 
                     BlockDecoder* pdec);

  void init(double chVal);

  void broadcast();

private:
  
  /// (Currently unimplemented)
  void outputEdge_1(char chanmsg);

  /// Updates the output counter based on a majority vote.
  void outputEdge_3(char chanmsg);

  /// List of 1-bit FF
  char m_FF1List[3];
};

/**
 * Specialized implementation of a stochastic VN of degree 6 (VN is connected
 * to 6 check nodes).
 */
class VariableNodeStoch6 : public VariableNodeStoch, public SingleBitVN {
public:
  /**
   * Instantiates a degree-6 Variable Node. Note that EM base size is
   * multiplied by 2.
   *@param index      Index of the node in the graph (bit index).
   *@param drelist    List of DRE pointers containing either 1 or 9 pointers. If
   *                  the list has 1, the same DRE pointer is used for all random number generation in this node. If the list has 9, a different DRE pointer is assigned to each EM/IM that requires random numbers (1-6 for EMs, 7 for the input and 8-9 for the 2 IMs). Of course some or all of those pointers can in reality point to the same objects (in which case the random numbers are shared).
   */
  VariableNodeStoch6(int index, std::vector<IStochDRE*> drelist, 
                     BlockDecoder* pdec);

  void init(double chVal);
  
  void broadcast();

private:
  /**
   * Implements the degree 3 subnode.
   *@param inputs  Array with the three inputs for the subnode.
   *@param edgeIndex Index of the output edge associated with the subnode 
   *                 that is being simulated.
   *@param subnodeIndex Which of the two subnodes.
   */
  char deg3subnode(char* inputs, int edgeIndex, int subnodeIndex);

  // Various implementations for the output edge logic:
  void outputEdge_1(char chanmsg);
  void outputEdge_2(char chanmsg);
  void outputEdge_3(char chanmsg);

  /// List of IMs #1
  IShiftReg* m_IM1List[6];
  /// List of IMs #2
  IShiftReg* m_IM2List[6];

  /// 5 1-bit IM's for the output sub-node (unused in the case of maj. vote)
  char m_IMOut[5];
};

/**
 * Different implementation for a degree 6 VN. This structure uses no
 * subnodes, and the TFM updates are based on a majority vote.
 */
class VariableNodeStoch6_2 : public VariableNodeStoch, public SingleBitVN {
public:
  VariableNodeStoch6_2(int index, std::vector<IStochDRE*> drelist, 
                       BlockDecoder* pdec);

  void init(double);

  void broadcast();

};

class VNHD : public AbstractVariableNode, public SingleBitVN {
public:
  
  VNHD( int index, uint degree, BlockDecoder* pdec );

  ~VNHD();

  void baseInit() {}

  /// Initializes the input probability from the channel LLR.
  void init(double chLLR);

  void broadcast();

	char currentDecision();

  void binaryTokenBcst(bool token);

  void berkeleyPPInit() {
    throw new Exception("VNHD::berkeleyPPInit", "Unimplemented");
  }
                     
  void berkeleyPPSend() {
    throw new Exception("VNHD::berkeleyPPSend", "Unimplemented");
  }

	void applyDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyDeviations", "Unimplemented");
	}
	void applyTotalDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyTotalDeviations", "Unimplemented");
	}
	void devSetTxBit(int tx) {
		throw Exception("devSetTxBit", "Unimplemented");
	}
	char sampleExtrinsicDecision() {
		throw Exception("sampleExtrinsicDecision", "Unimplemented");
	}
	char samplePrevExtrinsicDecision() {
		throw Exception("samplePrevExtrinsicDecision", "Unimplemented");
	}	

private:

  /**
   * Flag indicating that the next broadcast() will be the first of
   * the decoding cycle.
   */
  bool m_firstIt;

  char m_chanVal;

	// Majority threshold (between 0 and 1)
	double m_majThresh;
};

/**
 * Degree 7 Variable Node made of one degree-4 and one degree-3 subnode.
 */
class VariableNodeStoch7 : public VariableNodeStoch, public SingleBitVN {
public:

  /**
   * Constructor
   *@param index      (see VariableNodeStoch)
   *@param drelist    If using multiple DREs, must supply at least 9.
   */
  VariableNodeStoch7(int index, std::vector<IStochDRE*> drelist,
                     BlockDecoder* pdec);

  void init(double chVal);

  void broadcast();

private:
  
  /// Implements the deg 3 subnode for this type of VN
  char deg3subnode(char* inputs, int edgeIndex);

  /// Implements the deg 4 subnode for this type of VN
  char deg4subnode(char* inputs, int edgeIndex);

  // ----- Data -----
  IShiftReg* m_IM1List[7]; // for deg-3 subnodes
  IShiftReg* m_IM2List[7]; // for deg-4 subnodes
};

/**
 * Degree 7 Variable Node made of two degree-2 and one degree-3 subnode.
 */
class VariableNodeStoch7_2 : public VariableNodeStoch, public SingleBitVN {
public:

  /**
   * Constructor
   *@param index      (see VariableNodeStoch)
   *@param drelist    If using multiple DREs, must supply at least 10.
   */
  VariableNodeStoch7_2(int index, std::vector<IStochDRE*> drelist,
                       BlockDecoder* pdec);

  void init(double chVal);

  void broadcast();

private:

  char deg2subnode_1(char* inputs, int edgeIndex);

  char deg2subnode_2(char* inputs, int edgeIndex);

  char deg3subnode(char* inputs, int edgeIndex);

  // ----- Data -----
  IShiftReg* m_IM1List[7];
  IShiftReg* m_IM2List[7];
  IShiftReg* m_IM3List[7];

}; // ----- End class VariableNodeStoch7_2 -----

/**
 * Stochastic VN of degree 6 with a single output TFM.
 */
class VariableNodeStochSmall6 : public VariableNodeStoch, public SingleBitVN {
public:

  VariableNodeStochSmall6(int index, std::vector<IStochDRE*> drelist,
                          BlockDecoder* pdec);

  void init(double);

  void broadcast();

  //char currentDecision() { return m_EM->getHD(); } // added*******

private:

  /// Same code as VariableNodeStoch6::deg3subnode(...)
  char deg3subnode(char*, int, int);

  // ---------- Data ----------

  IEdgeMem* m_EM;

  /// List of IMs #1
  IShiftReg* m_IM1List[6];
  /// List of IMs #2
  IShiftReg* m_IM2List[6];

  //  char m_outputBuf[6]; *******

}; // ----- End class VariableNodeStochSmall6 -----

/// Unimplemented !
class VariableNodeStochSmall4: public VariableNodeStoch, public SingleBitVN {
public:

  VariableNodeStochSmall4(int index, std::vector<IStochDRE*> drelist,
                          BlockDecoder* pdec);

  void init(double);

  void broadcast();

private:

  char deg2subnode(char*, int, int);

  // ---------- Data ----------

  IEdgeMem* m_EM;

  /// List of IMs #1
  IShiftReg* m_IM1List[6];
  /// List of IMs #2
  IShiftReg* m_IM2List[6];

}; // ----- End class VariableNodeStochSmall4 -----

/**
 * Degree-6 VN that processes multiple sets of incomming messages in
 * parallel. It is like having multiple parallel decoders, except that
 * they all share the same edge-memories.
 */
class VariableNodeStochParallel6 : public VariableNodeStoch, public VN_int {
public:

  VariableNodeStochParallel6(int index, std::vector<IStochDRE*> drelist,
                             int bitWidth,
                             BlockDecoder* pdec);

  ~VariableNodeStochParallel6();

  void init(double chVal);

  void broadcast();

private:

  uint deg3subnode(uint* inputs, uint edgeIndex, uint subnodeIndex);

  // ----- Data -----

  uint m_bitWidth;

  IShiftReg** m_IM1List[6];
  IShiftReg** m_IM2List[6];
};

/**
 * Implementation of the Relaxed Half-Stochastic (RHS) decoder.
 */
class VariableNodeStoch_B : public IVNStoch, public SingleBitVN {
public:
  VariableNodeStoch_B(int index, uint degree, std::vector<IRandomLLR*> dreinst,
                      BlockDecoder* pdec);

  ~VariableNodeStoch_B();

  ///@see VariableNode::baseInit
  void baseInit();

  /// Overrides SingleBitVN::receive
  void receive(char msg, uint senderID);

  // Implements IVNStoch:

  void init(double llr);

  void broadcast();

  char currentDecision() { 
#ifndef DECODER_COLLAYERSCHED  //actually maybe should always be like that...
    calcInputLLR(); // update trackers and LLR input cache
#endif
    if(llrSum() > 0) return 1; return 0; 
  }

  int  curReliability() { return m_outputEdgeCount; }

  void setTrainingMode(bool val) { }

  void setNDSCst(double val) { }

  //void setChSat(double val) {} // (doesn't do anything)

  // -- End IVNStoch

  void resetState() {
    for(uint i=0; i<getDegree(); i++) {
      m_inTrackerList[i]->reset();
    }
  }

  /**
   * This scales the LLR-domain trackers as well as the prior by 
   * <code>scaling</code>.
   *@param scaling Scaling factor.
   */
  void scaleState(float scaling);

  /**
   * Move information common to all input trackers into <code>m_inputLLR</code>.
   */
  void normalizeState();

  void binaryTokenBcst(bool token);

  void berkeleyPPInit() {
    throw new Exception("VariableNodeStoch_B::berkeleyPPInit","Unimplemented");
  }
                     
  void berkeleyPPSend() {
    throw new Exception("VariableNodeStoch_B::berkeleyPPSend","Unimplemented");
  }

	void applyDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyDeviations", "Unimplemented");
	}
	void applyTotalDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyTotalDeviations", "Unimplemented");
	}
	void devSetTxBit(int tx) {
		throw Exception("devSetTxBit", "Unimplemented");
	}
	char sampleExtrinsicDecision() {
		throw Exception("sampleExtrinsicDecision", "Unimplemented");
	}
	char samplePrevExtrinsicDecision() {
		throw Exception("samplePrevExtrinsicDecision", "Unimplemented");
	}	

private:

  // ---------- Methods ----------

  /**
   * Rounds the value to the nearest quantization level corresponding
   * to the specified range and resolution. One value is always used to represent 0.
   *@param value The value to be quantized.
   *@param range Upper bound of the range (symmetric around 0).
   *@param resol The total number of quantization levels (should usually be a power of 2).
   */
  float quantize(float value, float range, float resol);

  /** 
   * Updates the trackers that have a pending message and the
   * m_llrMsgList[i] variables (which represent the output of the
   * message trackers), but only if new messages have been received
   * since the last call. Also clears all pending-message flags.
   */
  void calcInputLLR();

  /// Returns the sum of incomming messages.
  float llrSum();

#ifdef VN_HARMONIZE
  /**
   * Determines on which trackers VN harmonization should be performed
   * (in the next iteration).
   */
  void harmonizeCheck();
#endif

  // ---------- Data ----------

  int m_itCount; // used to detect start of new iteration

  float m_inputLLR;
  float* m_llrMsgList;
  // for each input, indicates whether a new message has been received
  bool* m_hasPendingMsg;
  // can be used to invalidate the whole m_llrMsgList cache
  bool m_isDirtyLlrMsgList;

  std::vector<ILLRTracker*> m_inTrackerList;

#ifdef VNSTOCHB_REFTRACKER
  double m_beta;
  double m_refTracker;
  float m_diffRange; // positive limit of range of diff trackers
  float m_diffResol; // number of quantization levels (2^bit_width)
  //  std::vector<double> m_diffTrackerList;
#endif

  /**
   * Random number generators (that keeps outputting the same number
   * until nextTick() is called).
   */
  std::vector<IRandomLLR*> m_rngList;

  int m_outputEdgeCount; // (not a counter anymore)

#ifdef VNSTOCHB_BETAPROGRESSION
  uint m_betaThreshold[2];  
#endif
#ifdef VNSTOCHB_SIGNEDMSG
  uint m_msgRuleSwitch;
#endif

  /// Generates message bit vectors from LLR values
  ILLRtoBits* m_binMsgGen;

  /// Whether to scale the a-priori LLR when redecoding
  bool m_doPriorScaling;
  /// Value used to scale a-priori LLRs.
  float m_llrScaling;

  /**
   * Number of initialization cycles (can be 0). The a-priori
   * hard-decision is sent on the first init cycle, then random bits
   * (this could be changed...).
   */
  uint m_determCycles;

  uint m_determCyclesCopy;

  /// Internal LLR magnitude limit (doesn't apply to LLR outputs)
  float m_llrLimit;

  /// Magnitude limit for output LLR values (used to generate the binary msgs).
  float m_outputLlrLimit;

  /// Magnitude limit for random thresholds
  float m_threshLimit;

  /// Number of bits per message (sent in a given iteration).
  uint m_bitsPerMsg;

  bool m_useVNHarmonization;
#ifdef VN_HARMONIZE
  double m_harmonizeCst;

  /**
   * VN harmonization is only activated when the minority tracker has
   * magnitude bigger than this value (can be 0 to obtain the old algorithm).
   */
  double m_harmonizeTrig;

  /// Trackers on which to perform VN harmonization at the next iteration
  bool* m_harmonizeFlagList;
#endif

  /**
   * LLR value that is added to every output LLR. Needed for "State
   * Normalization" (see normalizeState()).
   */
  float m_outputCompensation;

}; // end class VariableNodeStoch_B -----


/**
 * Same structure as a hard-decision VN, but with a channel input that
 * is stochastic (changes at every cycle) and an output counter.
 */
class VNHDStoch : public AbstractVariableNode, public SingleBitVN {
public:
  VNHDStoch( int index, uint degree, IStochDRE* dreinst, BlockDecoder* pdec );

  ~VNHDStoch();

  void baseInit() {}

  /// Initializes the input probability from the channel LLR.
  void init(double chLLR);

  void broadcast();

  char currentDecision();

  void binaryTokenBcst(bool token);

  void berkeleyPPInit() {
    throw new Exception("VNHDStoch::berkeleyPPInit", "Unimplemented");
  }
                     
  void berkeleyPPSend() {
    throw new Exception("VNHDStoch::berkeleyPPSend", "Unimplemented");
  }

	void applyDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyDeviations", "Unimplemented");
	}
	void applyTotalDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyTotalDeviations", "Unimplemented");
	}
	void devSetTxBit(int tx) {
		throw Exception("devSetTxBit", "Unimplemented");
	}
	char sampleExtrinsicDecision() {
		throw Exception("sampleExtrinsicDecision", "Unimplemented");
	}
	char samplePrevExtrinsicDecision() {
		throw Exception("samplePrevExtrinsicDecision", "Unimplemented");
	}	

private:

  IStochDRE* m_myDRE;

  /// Input probability
  ITFM* m_channelTFM;

  /// Output edge counter
  int m_outputEdgeCount;

  /**
   * Flag indicating that the next broadcast() will be the first of
   * the decoding cycle.
   */
  bool m_firstIt;

  int m_counterSat;

};

/**
 * Wrapper around VNHD that changes the input to be a stochastic bit
 * (which stays constant for the whole hard-decision decoding
 * operation).
 */
class VNHDMC : public VNHD {
public:
  VNHDMC( int index, uint degree, IStochDRE* dreinst, BlockDecoder* pdec );

  ~VNHDMC();

  /**
   * Initialize the soft channel value. Overrides VNHD::init().
   */
  void init(double chLLR);

private:

  IStochDRE* m_myDRE;

  /// Input probability
  ITFM* m_channelTFM;
};

/**
 * Variable Node for the hard-decision DD-BMP algorithm.
 */
class VNDDBMP : public AbstractVariableNode, public SingleBitVN {
public:

  VNDDBMP(int index, uint degree, BlockDecoder* pdec);

  ~VNDDBMP();

  void baseInit() {}

  void init(double llr);

  char currentDecision();

  void broadcast();

  void binaryTokenBcst(bool token);

  void berkeleyPPInit() {
    throw new Exception("VNDDBMP::berkeleyPPInit", "Unimplemented");
  }
                     
  void berkeleyPPSend() {
    throw new Exception("VNDDBMP::berkeleyPPSend", "Unimplemented");
  }

	void applyDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyDeviations", "Unimplemented");
	}
	void applyTotalDeviations(LLRDeviationPMF* devgen) {
		throw Exception("applyTotalDeviations", "Unimplemented");
	}
	void devSetTxBit(int tx) {
		throw Exception("devSetTxBit", "Unimplemented");
	}
	char sampleExtrinsicDecision() {
		throw Exception("sampleExtrinsicDecision", "Unimplemented");
	}
	char samplePrevExtrinsicDecision() {
		throw Exception("samplePrevExtrinsicDecision", "Unimplemented");
	}
	
private:

  // ----- Data Members -----

  /// Hard-decision received from the channel (+1 or -1)
  int m_channelHD;

  /// Special behavior is required for the first iteration.
  bool m_firstIt;

// #ifdef DDBMP_RNDTHRESHOLD
//   /// A-priori LLR.
//   double m_inputLLR;
// #endif

#ifndef DDBMP_TFMTRACKERS
  /// A counter for each output edge
  int* m_outputCounters;

  /// Positive bound of the output edge counters (exclusive bound).
  int m_counterRange;
#else
  TFMFP** m_outputTrackers;
#endif

#ifndef DDBMP_RNDTHRESHOLD
  /**
   * Initialization value for output edge counters corresponding to a
   * positive channel hard-decision.
   */
  int m_counterInitVal;
#else
  IStochDRE* m_dre;
#endif
}; // End class VNDDBMP

// -------------------- CheckNode Classes --------------------

/**
 * Represents a "check" node in the belief propagation graph.
 */
class AbstractCheckNode : public Node {
 public:

  /**
   * Creates a new check node connected to the specified variable nodes.
   *@param degree  The number of variable nodes this check nodes is 
   *               connected to.
   *@param nodelist A list of pointers to VariableNodes (of size "degree").
   */
  AbstractCheckNode( uint degree, int msgtype, BlockDecoder* pdec );

  virtual ~AbstractCheckNode() {}

  /// Return 0 if the check node has even parity, and non-zero otherwise.
  virtual char curParity() = 0;

}; /* ----- End Class AbstractCheckNode ---------- */

class CN_int {
public:

  CN_int(uint degree, std::vector<VariableNode*>& nodelist);

  ~CN_int();

  void receive(int msg, uint senderID) { m_pendingMsg[senderID] = msg; }

protected:

  uint* m_myIDs;

  int* m_pendingMsg;

  /**
   * List of pointers to neighbor variable nodes (of size getDegree()).
   */
  VN_int** m_nodelist;
};

class CN_double {
public:
  CN_double(uint degree, std::vector<VariableNode*>& nodelist);

  virtual ~CN_double();

  /**
   * Receive a message from sender with ID senderID. This overwrites
   * the previous message from that sender.
   */
  void receive(double msg, uint senderID) { m_pendingMsg[senderID] = msg; }

protected:

  uint* m_myIDs;

  /**
   * Last message received on each edge.
   */
  double* m_pendingMsg;

  /**
   * List of pointers to neighbor variable nodes (of size getDegree()).
   */
  VN_double** m_nodelist;
};

/// Interface of the strategy pattern for Check Nodes.
class CheckNode {
public:
  CheckNode(uint degree, int msgtype, std::vector<VariableNode*> nodelist, 
            BlockDecoder* pdec);
  ~CheckNode();

  ///@see Node::broadcast()
  void broadcast() { return m_implementation->broadcast(); }

  /**
   * Return 0 if the CheckNode has even parity, and non-zero otherwise.
   */
  char curParity() { return m_implementation->curParity(); }

  void binaryTokenBcst(bool token) { 
    return m_implementation->binaryTokenBcst(token);
  }

 private:

  AbstractCheckNode* m_implementation;

};

/// Parent class for check nodes of SPA-family algorithms.
class CNSPALLR : public AbstractCheckNode, public CN_double {
public:

  CNSPALLR(uint degree, std::vector<VariableNode*>& nodelist, int msgtype,
           BlockDecoder* pdec);

  ~CNSPALLR();

  void binaryTokenBcst(bool token);

private:

  double* m_outputBuffer;
};

class CheckNodeLLR : public CNSPALLR {
public:
  CheckNodeLLR(uint degree, std::vector<VariableNode*>& nodelist,
               BlockDecoder* pdec);
  ~CheckNodeLLR();

  ///@see Node::broadcast()
  void broadcast();

  ///@see AbstractCheckNode::curParity()
  char curParity();

private:

  /// temp buffer used by broadcast()
  double* m_buftanh;
};

class CheckNodeMinSum : public CNSPALLR {
public:
  CheckNodeMinSum(uint degree, std::vector<VariableNode*>& nodelist,
                  BlockDecoder* pdec);

  ~CheckNodeMinSum();

  void broadcast();

  char curParity();

protected:

  // The following functions implement different redecoding algorithms.
  // They all have the signature "void f(double&, double&)"

  /**
   * Modifies the two minima according to the "random additive offset"
   * dithering.
   */
  void rndAdditiveOffset(double& min1, double& min2);

  void uniformSignFlip(double& min1, double& min2);

  void uniformSignFlip2(double& min1, double& min2);

  void propSignFlip(double& min1, double& min2);

  void propSignFlip2(double& min1, double& min2);

private:
  int* m_signbuf;

#ifdef MINSUM_SINGLEMIN
  double m_secondMinOffset;
#endif

  /// Offset for offset min-sum (0 if not used)
  double m_offset;

  /// Whether to add a random offset to the check node outputs when redecoding.
  bool m_redecodeRndOffsetMode;

  double m_redecodeRndOffset;

  /// Whether to use uniform random sign flip when redecoding.
  bool m_redecodeUniRndSignMode;

  /// Whether to use uniform random sign flip (version 2) when redecoding.
  bool m_redecodeUniRndSign2Mode;

  /// Value of p in dithering algorithm "uniform-random sign flip".
  double m_redecodeFlipProb;

  /// activates proportional sign flip dithering algorithm
  bool m_redecodePropRndSignMode;

  /// Inverse of constant "c" used by both proportiornal sign flip dith. alg.
  double m_redecodePropRndSignCst;

  /// activates proportional sign flip (version 2) dithering algorithm
  bool m_redecodePropRndSign2Mode;

  /**
   * Option for the two "proportional sign flip" dithering
   * algorithms. True if random numbers are uniform in the LLR domain,
   * false if they are uniform in the probability domain.
   */
  bool m_redecodePropRndSignRNTypeLLR;

  /**
   * LLR magnitude of flipped messages in "uniform random sign flip
   * version 2" or "proportional random sign flip version 2".
   */
  double m_redecodeFlipValue;

  double m_llrLimit;

#ifdef MINSUM_CNRELAX
  double m_prevMinVal;
#endif
}; // ----- End Class CheckNodeMinSum -----

/**
 * Check Node for decoders that exchange one-bit messages. Works both
 * for stochastic and hard-decision decoders. Since there is only one
 * implementation of a "single bit check node", this class is both the
 * implementation and the message handler.
 * The class can be used even if many one-bit messages are packed in 
 * each "char" data type. In this case, the result of curParity() will
 * still be computed based on the value of the lsb's, which means that
 * the actual msb should be at that position.
 */
class SingleBitCN : public AbstractCheckNode {
public:

  SingleBitCN(uint degree, int msgtype, std::vector<VariableNode*>& nodelist,
              BlockDecoder* pdec);

  ~SingleBitCN();

  void receive(char bit, uint senderID) { m_pendingMsg[senderID] = bit; }

  /**
   * Returns the ID of this Node as seen by the neighbor with local ID
   * <code>neighborID</code>.
   */
  uint getID(uint neighborID) { return m_myIDs[neighborID]; }

  ///@see Node::broadcast()
  void broadcast();

  /**
   * Returns the XOR of all inputs. If there are multiple bits per
   * message, the parity result will also have multiple bits, one for
   * each message bit (so the parity is "even" if it is even for all
   * message bits).
   *@see AbstractCheckNode::curParity()
   */
  char curParity();

  std::vector<SingleBitVN*> getNeighbors();

  void binaryTokenBcst(bool token);

protected:

  uint* m_myIDs;

  char* m_pendingMsg;

  /**
   * List of pointers to neighbor variable nodes (of size getDegree()).
   */
  SingleBitVN** m_nodelist;
};

class ParallelCN : public AbstractCheckNode, public CN_int {
public:

  ParallelCN(uint degree, std::vector<VariableNode*>& nodelist,
             BlockDecoder* pdec);

  void broadcast();

  char curParity();

  void binaryTokenBcst(bool token) {
    throw new Exception("ParallelCN::binaryTokenBcst", "Unimplemented");
  }
};

#endif
