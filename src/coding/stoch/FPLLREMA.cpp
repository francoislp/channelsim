#include "FPLLREMA.hpp"
#include "util/Exception.hpp"
#include "Config.hpp"

FPLLREMA::FPLLREMA(uint msgSize)
  : m_llrVal(0)
{
  if(msgSize==0 || msgSize>2) {
    throw Exception("FPLLREMA::ctor", "Message size not supported");
  }

  m_twoBitMsg = (msgSize==2);
  m_paramIndex = 0;

  Config* conf = Config::getInstance();

  m_rangeBound = conf->getPropertyByName<float>("llrrelax_range");

  // Load up to 3 beta values (and at least 1)
  float beta = conf->getPropertyByName<float>("llrrelax_beta");
  m_a.push_back(1-beta);
  m_b.push_back(beta * m_rangeBound);
  try {
    beta = conf->getPropertyByName<float>("llrrelax_beta_2");
    m_a.push_back(1-beta);
    m_b.push_back(beta * m_rangeBound);
    beta = conf->getPropertyByName<float>("llrrelax_beta_3");
    m_a.push_back(1-beta);
    m_b.push_back(beta * m_rangeBound);
  } catch(InvalidKeyException&) {}

}

void FPLLREMA::push(char msg) {
  m_llrVal*= m_a[m_paramIndex];

  // non-zero messages:
  if(m_twoBitMsg) {
    msg&= 3;
    if( msg==3 ) m_llrVal+= m_b[m_paramIndex];
  } else {
    msg&= 1;
    if( msg!=0 ) m_llrVal+= m_b[m_paramIndex];
  }

  if( msg==0 ) {
    m_llrVal-= m_b[m_paramIndex];
  }

}

void FPLLREMA::nextParam() {
  if( m_paramIndex >= (m_a.size()-1) ) {
    throw new Exception("FPLLREMA::nextParam", "There is no next parameter");
  }
  m_paramIndex++;
}
