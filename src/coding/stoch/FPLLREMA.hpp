//Author: Francois Leduc-Primeau

#ifndef _FPLLREMA_h_
#define _FPLLREMA_h_

#include "GlobalDefs.h"
#include "ILLRTracker.hpp"
#include <vector>

/**
 * An LLR exponential moving average for a probability-domain
 * stochastic stream. Received messages are converted from their
 * probability value to the LLR domain, and this LLR value is used to
 * update the EMA.
 */
class FPLLREMA : public ILLRTracker {
public:

  /**
   * Constructor
   *@param msgSize Number of bits/msg (packed in a 'char' type).
   */
  FPLLREMA(uint msgSize);

  void init(float llrVal) { m_llrVal = llrVal; m_paramIndex = 0; }

  void set(float llrVal) { m_llrVal = llrVal; }

  void reset() { m_llrVal = 0; m_paramIndex = 0; }

  void push(char msg);

  float curVal() { return m_llrVal; }

  void nextParam();

private:

  /// Whether messages have 2 bits (true) or 1 bit (false).
  bool m_twoBitMsg;

  /// Current LLR value
  float m_llrVal;

  /// Upper bound of the LLR range
  float m_rangeBound;

  /// Relaxation parameter: (1-beta).
  std::vector<float> m_a;

  /// Relaxation parameter: beta * m_rangeBound.
  std::vector<float> m_b;

  /// Index for m_betaList.
  uint m_paramIndex;
};

#endif
