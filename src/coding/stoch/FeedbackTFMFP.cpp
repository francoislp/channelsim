#include "FeedbackTFMFP.hpp"
#include "util/Exception.hpp"
#include "compileflags.h"

FeedbackTFMFP::FeedbackTFMFP(float beta, IStochDRE* dre) {
  m_beta = beta;
  m_reg = 0;
  m_regOut = 0;
  m_dre = dre;
  m_rvMask = 1;
  m_rndval = dre->getVal();

  if(m_beta > 1 || m_beta < 0)
    throw Exception("FeedbackTFMFP::ctor", "Invalid beta value");

  if( sizeof(uint) != 4 )
    throw Exception("FeedbackTFMFP::ctor", "This class assumes type uint has 32 bits");
}

void FeedbackTFMFP::init(double prob) {
  if(prob<0 || prob>1) throw Exception("FeedbackTFMFP::init", 
                                           "Invalid prob value");
  m_reg = static_cast<float>(prob);
  m_regOut = m_reg;
}

void FeedbackTFMFP::push(char bit) {
  m_reg = m_reg + m_beta * (static_cast<float>(bit) - m_reg);
  // also update the output TFM (the extracted probability)
  updateOutputTFM(bit);
}

char FeedbackTFMFP::getrnd() {
  char output;
  if( m_reg >= m_regOut ) output = 1;
  else output = 0;

  updateOutputTFM(output);

  return output;
}

// (private)
void FeedbackTFMFP::updateOutputTFM(char output) {
  // get a random bit (as +1 or -1)
  float rndBit;
  if( (m_rndval & m_rvMask) == 0 ) rndBit = -1;
  else rndBit = 1;
  if(m_rvMask == 0x80000000) {
    m_rvMask = 1;
    m_rndval = m_dre->getVal();
  }
  else m_rvMask<<= 1;

  // update the "extracted" probability
  float x = static_cast<float>(output) - m_regOut;
  x+= rndBit * FDBCKTFM_NOISEFACTOR;
  m_regOut = m_regOut + FDBCKTFM_BETAOUT * x;
}
