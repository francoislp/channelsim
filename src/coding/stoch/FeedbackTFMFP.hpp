#ifndef _FeedbackTFM_h_
#define _FeedbackTFM_h_

#include "GlobalDefs.h"
#include "ITFM.hpp"

/**
 * This class is like TFMFP with the difference that stochastic
 * streams are generated using a second TFM module and a feedback
 * path. Only uses one random bit per clock.
 */
class FeedbackTFMFP : public ITFM {
public:

  FeedbackTFMFP(float beta, IStochDRE* dre);

  void init(double prob);

  void reset() { init(0.5); }

  void push(char bit);

  char getrnd();

  char getHD() {
    if(m_reg >= 0.5) return 1;
    else return 0;
  }

  IStochDRE* getDRE() { return m_dre; }

  double curVal() { return static_cast<double>(m_reg); }

private:

  /**
   * Updates the output TFM, which keeps tracks of a probability value
   * extracted from the stochastic stream generated.
   */
  void updateOutputTFM(char output);

  // ----- Data -----

  float m_reg;
  float m_regOut; // stores the extracted probability

  float m_beta;

  IStochDRE* m_dre;
  uint m_rndval;
  /// Only need a 1-bit RV => use all bits of the integer RV
  uint m_rvMask;
};

#endif
