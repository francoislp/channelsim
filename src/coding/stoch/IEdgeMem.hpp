//Author: Francois Leduc-Primeau

#ifndef _IEdgeMem_h_
#define _IEdgeMem_h_

#include "IStochDRE.hpp"

/// Identifies an Edge-Memory implemented by class ShiftReg
#define EMTYPE_SHIFTREG 1
/// Identifies an Edge-Memory that is a child of ITFM
#define EMTYPE_TFM 2
/// Identifies an Edge-Memory implemented by S_TFMFP (also child of ITFM)
#define EMTYPE_STFM 3
/// FeedbackTFMFP, child of ITFM
#define EMTYPE_FDBCKTFM 6
// Notes:
// - Bit #1 indicates ITFM compatibility (THIS MUST BE PRESERVED)
// - Negative values are reserved.

class IEdgeMem {
public:
  virtual ~IEdgeMem() {}

  /// Update the Edge-Memory with a new bit.
  virtual void push(char bit) = 0;

  /// Retrieve a random bit from the Edge-Memory.
  virtual char getrnd() = 0;

  /**
   * Get the hard-decision for the current state of the EM. The return
   * value can be -1 to indicate "unknown".
   */
  virtual char getHD() = 0;

  /// Returns the DRE pointer used by this Edge-Memory.
  virtual IStochDRE* getDRE() = 0;

  virtual void reset() = 0;

protected:
  IEdgeMem() {} // prevent direct instantiation
};

#endif
