//Author: Francois Leduc-Primeau

#ifndef ILLRTracker_h_
#define ILLRTracker_h_

class ILLRTracker {
public:

  virtual ~ILLRTracker() {}

  /**
   * Initialize the tracker with a specific LLR value and reset the
   * parameter-set index.
   */
  virtual void init(float llrVal) = 0;

  /**
   * Set the tracker to a specific LLR value, but do not change the
   * parameter-set index. (If only one parameter set is used, this is
   * the same as calling <code>init(float)</code>).
   */
  virtual void set(float llrVal) = 0;

  /**
   * Reset the tracker to a "no-knowledge" value, and also reset the
   * parameter-set index if we are using many parameter sets (i.e. if
   * using a beta sequence). The advantage of using this method
   * instead of <code>init(float)</code> is that the caller does not
   * need to know what the "no-knowledge" value is.
   */
  virtual void reset() = 0;

  /**
   * Updates the tracker with a new stochastic message. If the
   * messages have more than 1 bit, msg&1 is understood to be the
   * first bit received, msg&2 the second, and so on (in case the
   * order makes a difference).
   */
  virtual void push(char msg) = 0;

  /**
   * Returns the current value of the tracker.
   */
  virtual float curVal() = 0;

  /**
   * Move to the next tracking parameter or parameter set in a
   * pre-determined list.
   */
  virtual void nextParam() = 0;

  /**
   * Perform an additive correction on the tracker, by some constant
   * that is either fixed or determined by a configuration key. Used
   * for VN Harmonization.
   */
  virtual void harmonize() = 0;

protected:
  ILLRTracker() {} // prevent direct instantiation
};

#endif
