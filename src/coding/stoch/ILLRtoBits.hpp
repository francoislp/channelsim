//Author: Francois Leduc-Primeau

#ifndef ILLRtoBits_hpp_
#define ILLRtoBits_hpp_

#include "GlobalDefs.h"

/**
 * Interface for classes that convert LLR values to bit vectors.
 */
class ILLRtoBits {
public:

  virtual ~ILLRtoBits() {}

  /**
   * Convert an LLR value to a bit vector.
   *@return The bit vector, of length at most 8 since it is packed in a 
   *        'char' type.
   */
  virtual char convert(float llrval) = 0;

  /**
   * Selects the next message generation rule. The implementation is
   * free to choose what happens when the last rule has been reached
   * and this method is called (the only choices that make sense are
   * probably stay at the last rule, or go back to the
   * beginning). However, it must be safe to call this when there is
   * only one rule (nothing should happen).
   */
  virtual void nextRule() = 0;

  /**
   * Selects the first message generation rule.
   */
  virtual void resetRule() = 0;

};

#endif
