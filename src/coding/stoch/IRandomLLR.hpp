//Author: Francois Leduc-Primeau

#ifndef IRandomLLR_h_
#define IRandomLLR_h_

#include "base/ISequential.hpp"

/**
 * Interface for Classes that generate random LLR values.
 */
class IRandomLLR : public ISequential {
public:

  virtual ~IRandomLLR() {}

  /**
   * Get the current random LLR value. Successive calls to this
   * function return the same value unless nextTick() or reset() is
   * called in between.
   */
  virtual float getCurVal() = 0;

  /**
   * Get the current random fair bit. Successive calls to this
   * function return the same bit unless nextTick() or reset() is
   * called in between.
   */
  virtual char getCurBit() = 0;

  //void nextTick() defined by ISequential

  //void reset() defined by ISequential 
  //Note: nextTick() does not need to be called before calling
  //      getCurVal() or getCurBit()

protected:
  IRandomLLR() {} // prevent direct instantiation
};

#endif
