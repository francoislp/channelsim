//Author: Francois Leduc-Primeau

#ifndef _IStochDRE_h_
#define _IStochDRE_h_

#include "GlobalDefs.h"
#include "base/ISequential.hpp"

/**
 * Interface for the Distributed Random Generators used in the
 * Stochastic Decoder. The random value does not change unless
 * nextTick() is called.
 */
class IStochDRE : public ISequential {
public:

  virtual ~IStochDRE() {}

  /**
   * Returns the current random value. Successive calls to this
   * function should return the same number, unless <code>nextTick()</code> is
   * called in between.
   */
  virtual uint getVal() = 0;

  /**
   * Returns a uniform random number in the range [0, 1[. Successive
   * calls to this function should return the same number, unless
   * <code>nextTick()</code> is called in between.
   */
  virtual float getVal_f() = 0;

  /// Returns a normal random number with zero mean and std dev sigma.
  virtual float getVal_norm(float sigma) = 0;

  // void nextTick() provided by ISequential

  // void reset() provided by ISequential

};

#endif
