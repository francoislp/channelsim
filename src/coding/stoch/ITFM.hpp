//Author: Francois Leduc-Primeau

#ifndef _ITFM_h_
#define _ITFM_h_

#include "IEdgeMem.hpp"

class ITFM : public IEdgeMem {
public:

  virtual void init(double prob) = 0;

  virtual double curVal() = 0;

  virtual ~ITFM() {}

protected:
  ITFM() {} // prevent direct instantiation

};

#endif
