//Author: Francois Leduc-Primeau

#include "LLRTracker.hpp"
#include "Config.hpp"

LLRTracker::LLRTracker(float beta, uint msgSize)
  : m_llrVal(0)
{
  if(msgSize==0 || msgSize>2) {
    throw Exception("LLRTracker::ctor", "Message size not supported");
  }
  m_twoBitMsg = (msgSize==2);

  // beta is usually ignored except when the exact tracking function is needed
#ifdef VNSTOCHB_LLRTRACKER_EXACTRANGE
  m_beta = beta;
#endif

  // get the linear fit parameters from the config
  Config* conf = Config::getInstance();
  m_a = conf->getPropertyByName<float>("llrtracker_a");
  m_b = conf->getPropertyByName<float>("llrtracker_b");
  m_c = conf->getPropertyByName<float>("llrtracker_c");
  m_d = conf->getPropertyByName<float>("llrtracker_d");
  m_fitThreshold = conf->getPropertyByName<float>("llrtracker_fitthreshold");
  if(m_twoBitMsg) {
    m_oneHalfCap = conf->getPropertyByName<float>("llrtracker_onehalfcap");
    m_oneHalf_a = conf->getPropertyByName<float>("llrtracker_onehalf_a");
  }

#ifdef VNSTOCHB_LLRTRACKER_EXACTRANGE
  // retrieve the exact range
  m_exactUpperOne = conf->getPropertyByName<float>("llrtracker_exactupper_one");
  m_exactLowerOne = conf->getPropertyByName<float>("llrtracker_exactlower_one");
  m_exactUpperHalf = conf->getPropertyByName<float>("llrtracker_exactupper_half");
  m_exactLowerHalf = conf->getPropertyByName<float>("llrtracker_exactlower_half");
#endif
}

void LLRTracker::push(char msg) {
  if(m_twoBitMsg && (msg==1 || msg==2)) {
#ifdef VNSTOCHB_LLRTRACKER_EXACTRANGE
    if(m_llrVal > m_exactLowerHalf && m_llrVal < m_exactUpperHalf) {
      m_llrVal = exactTrack(m_llrVal, 0.5, m_beta);
      return;
    }
#endif
    if(m_llrVal > m_oneHalfCap) {
      m_llrVal = m_oneHalfCap;
      return;
    }
    if(m_llrVal < -m_oneHalfCap) {
      m_llrVal = -m_oneHalfCap;
      return;
    }
    m_llrVal*= m_oneHalf_a;
    return;
  }

  if( (msg&1)==0 ) {
#ifdef VNSTOCHB_LLRTRACKER_EXACTRANGE
    if(m_llrVal < -m_exactLowerOne && m_llrVal > -m_exactUpperOne) {
      m_llrVal = exactTrack(m_llrVal, 0, m_beta);
      return;
    }
#endif
    if( m_llrVal <= m_fitThreshold ) {
      m_llrVal = m_a*m_llrVal - m_b;
    }
    else m_llrVal = m_c*m_llrVal - m_d;
  }
  else { // bit==1
#ifdef VNSTOCHB_LLRTRACKER_EXACTRANGE
    if(m_llrVal > m_exactLowerOne && m_llrVal < m_exactUpperOne) {
      m_llrVal = exactTrack(m_llrVal, 1, m_beta);
      return;
    }
#endif
    if( m_llrVal >= (-m_fitThreshold) ) {
      m_llrVal = m_a*m_llrVal + m_b;
    }
    else m_llrVal = m_c*m_llrVal + m_d;
  }
}

#ifdef VNSTOCHB_LLRTRACKER_EXACTRANGE
float LLRTracker::exactTrack(float curLLR, float probMsg, float beta) {
  float prob = expf(m_llrVal) / (expf(m_llrVal)+1); // current prob
  prob = (1-beta) * prob + beta * probMsg; // next prob
  return logf( prob / (1-prob) ); // next LLR
}
#endif
