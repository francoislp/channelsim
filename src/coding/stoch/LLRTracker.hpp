//Author: Francois Leduc-Primeau

#ifndef LLRTracker_h_
#define LLRTracker_h_

#include "compileflags.h"
#include "ILLRTracker.hpp"
#include "util/Exception.hpp"

class LLRTracker : public ILLRTracker {
public:

  /**
   * Constructor.
   *@param beta (Unused)
   *@param msgSize Number of bits/msg (packed in a 'char' type).
   */
  LLRTracker(float beta, uint msgSize);

  /**
   * This function is currently the same as set() because parameter
   * lists are not supported.
   */
  void init(float llrVal) { m_llrVal = llrVal; }

  void set(float llrVal) { m_llrVal = llrVal; }

  void reset() { m_llrVal = 0; }

  /**
   * Updates the tracker with a new message. If the messages have more
   * than 1 bit, the msb should be <code>(msg & 1)</code>.
   */
  void push(char msg);
 
  float curVal() { return m_llrVal; }

  void nextParam() { throw Exception("LLRTracker::nextParam", "not supported"); }

private:

  // ---- Methods ----

#ifdef VNSTOCHB_LLRTRACKER_EXACTRANGE
  /**
   * Returns the exact tracker update for the specified probability message.
   */
  float exactTrack(float curLLR, float probMsg, float beta);
#endif

  // ---- Data ----

  /// Whether messages have two bits.
  bool m_twoBitMsg;

  // Linear fit constants
  float m_a;
  float m_b;
  float m_c;
  float m_d;
  float m_fitThreshold;
  float m_oneHalfCap;
  float m_oneHalf_a;

  /// Current LLR value
  float m_llrVal;

#ifdef VNSTOCHB_LLRTRACKER_EXACTRANGE
  float m_beta;
  float m_exactUpperOne;
  float m_exactLowerOne;
  float m_exactUpperHalf;
  float m_exactLowerHalf;
#endif

};

#endif
