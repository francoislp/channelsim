//Author: Francois Leduc-Primeau

#include "LLRTracker2.hpp"
#include "Config.hpp"
#include <bitset>
#include <sstream>
#include <algorithm>

using std::bitset;
using std::stringstream;

LLRTracker2::LLRTracker2(float beta, uint msgSize, uint cnDeg)
  : m_llrVal(0)
{
  if(msgSize==0 || msgSize==3 || msgSize>4) {
    throw Exception("LLRTracker2::ctor", "Message size not supported");
  }

  m_msgSize = msgSize;
  m_paramSetIndex = 0;

  Config* conf = Config::getInstance();

  //Note: Some parameters remain the same in all "parameter sets". For
  //      others, we try loading up to 3 values (for beta sequences).

  if(m_msgSize >= 2) {
    m_oneHalfCap = conf->getPropertyByName<float>("llrtracker_onehalfcap");
  }
  m_limit = conf->getPropertyByName<float>("llrtracker_limit");

#ifdef VNSTOCHB_MSGCOMPENSATION
  // get the saturation bound constant corresponding with the CN degree
  stringstream ssKey1;
  ssKey1 << "llrtracker_satbound_deg" << cnDeg;
  m_satBound = conf->getPropertyByName<float>(ssKey1.str());
  // get the "b" constants corresponding with the CN degree
  stringstream ssKey2;
  ssKey2 << "llrtracker_b_deg" << cnDeg;
  m_b.push_back(conf->getPropertyByName<float>(ssKey2.str()));
  try {
    m_b.push_back(conf->getPropertyByName<float>(ssKey2.str() + "_2"));
    m_b.push_back(conf->getPropertyByName<float>(ssKey2.str() + "_3"));
  } catch(InvalidKeyException&) {}
  stringstream ssKey3;
  ssKey3 << "llrtracker_d_deg" << cnDeg;
  m_d = conf->getPropertyByName<float>(ssKey3.str());
#else
  m_d = conf->getPropertyByName<float>("llrtracker_d");
  m_b.push_back(conf->getPropertyByName<float>("llrtracker_b")); // first is mandatory
  try {
    m_b.push_back(conf->getPropertyByName<float>("llrtracker_b_2"));
    m_b.push_back(conf->getPropertyByName<float>("llrtracker_b_3"));
  } catch(InvalidKeyException&) {}
#endif

  if(m_msgSize>=2) {
    m_oneHalf_a.push_back(conf->getPropertyByName<float>("llrtracker_onehalf_a"));
    try {
      m_oneHalf_a.push_back(conf->getPropertyByName<float>("llrtracker_onehalf_a_2"));
      m_oneHalf_a.push_back(conf->getPropertyByName<float>("llrtracker_onehalf_a_3"));
    } catch(InvalidKeyException&) {}
    // make sure the size agrees with m_b
    if(m_oneHalf_a.size() != m_b.size()) {
      throw Exception("LLRTracker2::ctor", 
                      "Error reading config key llrtracker_onehalf_a*");
    }
  } //end: if m_msgSize>=2

  if(m_msgSize==4) {
    m_3of4_a = conf->getPropertyByName<float>("llrtracker_3of4_a");
    m_3of4_b = conf->getPropertyByName<float>("llrtracker_3of4_b");
    m_3of4_cap = conf->getPropertyByName<float>("llrtracker_3of4_cap");
  }

#ifdef VN_HARMONIZE
  m_harmCst = conf->getPropertyByName<float>("decoder_harmonize_cst");
#else
  m_harmCst = 0;
#endif
}

void LLRTracker2::push(char msg) {
  bitset<8> msgBits(msg); // unused bits in 'msg' are required to be 0
  uint count = msgBits.count();

  if(count==0) {
    if(m_llrVal > -m_d) m_llrVal = -m_d;
#ifdef VNSTOCHB_MSGCOMPENSATION
    else if(m_llrVal < -m_satBound) m_llrVal = -m_limit;
#endif
    else {
      m_llrVal-= m_b[m_paramSetIndex];
      m_llrVal = std::max(m_llrVal, -m_limit);
    }
  }
  else if(count==m_msgSize) {
    if(m_llrVal < m_d) m_llrVal = m_d;
#ifdef VNSTOCHB_MSGCOMPENSATION
    else if(m_llrVal > m_satBound) m_llrVal = m_limit;
#endif
    else {
      m_llrVal+= m_b[m_paramSetIndex];
      m_llrVal = std::min(m_llrVal, m_limit);
    }
  }
  else if(count==(m_msgSize/2)) {
    if(m_llrVal > m_oneHalfCap) m_llrVal = m_oneHalfCap;
    else if(m_llrVal < -m_oneHalfCap) m_llrVal = -m_oneHalfCap;
    else m_llrVal*= m_oneHalf_a[m_paramSetIndex];
  }
  else if(count==3) { //note: max allowed message size is 4
    if(m_llrVal > m_3of4_cap) m_llrVal = m_3of4_cap;
    else if(m_llrVal < -m_oneHalfCap) m_llrVal = -m_oneHalfCap; // using 1/2 cap
    else m_llrVal = m_3of4_a * m_llrVal + m_3of4_b;
  }
  else if(count==1) {
    if(m_llrVal < -m_3of4_cap) m_llrVal = -m_3of4_cap;
    else if(m_llrVal > m_oneHalfCap) m_llrVal = m_oneHalfCap; //using 1/2 cap
    else m_llrVal = m_3of4_a * m_llrVal - m_3of4_b;
  }
  else {
    throw Exception("LLRTracker2::push", "Invalid message");
  }
}

void LLRTracker2::nextParam() {
  if( m_paramSetIndex >= (m_b.size()-1) ) {
    throw Exception("LLRTracker2::nextParam", "There is no next parameter set");
  }
  m_paramSetIndex++;
}

void LLRTracker2::harmonize() {
  if(m_llrVal >= 0) m_llrVal-= m_harmCst;
  else m_llrVal+= m_harmCst;
}
