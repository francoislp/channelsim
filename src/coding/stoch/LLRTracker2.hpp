//Author: Francois Leduc-Primeau

#ifndef LLRTracker2_h_
#define LLRTracker2_h_

#include "compileflags.h"
#include "ILLRTracker.hpp"
#include "GlobalDefs.h"
#include <vector>

/**
 * Similar to LLRTracker but some parameters are hardcoded (a=1 and c=0).
 */
class LLRTracker2 : public ILLRTracker {
public:

  /**
   * Constructor.
   *@param beta    (Unused)
   *@param msgSize Number of bits/msg (packed in a 'char' type).
   *@param cnDeg   Degree of the CN that generates the messages received by this tracker.
   */
  LLRTracker2(float beta, uint msgSize, uint cnDeg);

  void init(float llrVal) { m_llrVal = llrVal; m_paramSetIndex = 0; }

  void set(float llrVal) { m_llrVal = llrVal; }

  void reset() { m_llrVal = 0; m_paramSetIndex = 0; }

  /**
   * Updates the tracker with a new message. All unused bits must be 0.
   */
  void push(char msg);

#ifdef VNSTOCHB_QUANTIZELLR
#ifdef VNSTOCHB_QUANTIZELLR_RND
  float curVal() {
    return roundf(m_llrVal*VNSTOCHB_QUANTIZELLR_RNDSF) / VNSTOCHB_QUANTIZELLR_RNDSF;
  }
#else
  //TODO: Currently always quantize to integer. This should be configurable.
  float curVal() { 
    float x = floorf(fabs(m_llrVal));
    if(m_llrVal>=0) return x;
    else return -x;
  }
#endif
#else 
  float curVal() { return m_llrVal; }
#endif

  /**
   * Go to the next set of update parameters.
   *@throws Exception If no next parameter set exists.
   */
  void nextParam();

  void harmonize();

private:

  /// Number of bits per message.
  uint m_msgSize;

  // Linear fit constants
  std::vector<float> m_b;
  float m_d;
  float m_oneHalfCap;
  std::vector<float> m_oneHalf_a;
  float m_3of4_a; // for a p=0.75 or p=0.25 message
  float m_3of4_b;
  float m_3of4_cap;

  /// Current LLR value
  float m_llrVal;

  /// The current parameter set index (index for m_b and m_oneHalf_a)
  uint m_paramSetIndex;

  /// Max value that positive LLRs can take.
  float m_limit;

#ifdef VNSTOCHB_MSGCOMPENSATION
  /**
   * If the current value is more reliable than this bound, a strong
   * message causes the next value to be equal to the saturation
   * value.
   */
  float m_satBound;
#endif

  /// VN Harmonization constant
  float m_harmCst;
};

#endif
