//Author: Francois Leduc-Primeau

#include "LLRTrackerAdaptive.hpp"
#include "util/Exception.hpp"
#include "Config.hpp"
#include <bitset>
#include <sstream>
#include <algorithm>

using std::bitset;
using std::stringstream;

LLRTrackerAdaptive::LLRTrackerAdaptive(float beta, uint msgSize, uint cnDeg)
  : m_llrVal(0)
{
  if(msgSize==0 || msgSize==3 || msgSize>4) {
    throw Exception("LLRTrackerAdaptive::ctor", "Message size not supported");
  }

  m_msgSize = msgSize;
  m_ruleIndex = 0;

  Config* conf = Config::getInstance();

  if(m_msgSize >= 2) {
    m_oneHalfCap = conf->getPropertyByName<float>("llrtracker_onehalfcap");
  }
  m_limit = conf->getPropertyByName<float>("llrtracker_limit");

#ifdef VNSTOCHB_MSGCOMPENSATION
  // get the saturation bound constant corresponding with the CN degree
  stringstream ssKey1;
  ssKey1 << "llrtracker_satbound_deg" << cnDeg;
  m_satBound = conf->getPropertyByName<float>(ssKey1.str());
  // get the "b" constants corresponding with the CN degree
  stringstream ssKey2;
  ssKey2 << "llrtracker_b_deg" << cnDeg;
  m_b = conf->getPropertyByName<float>(ssKey2.str());
  stringstream ssKey3;
  ssKey3 << "llrtracker_d_deg" << cnDeg;
  m_d = conf->getPropertyByName<float>(ssKey3.str());
#else
  m_b = conf->getPropertyByName<float>("llrtracker_b");
  m_d = conf->getPropertyByName<float>("llrtracker_d");
#endif

  if(m_msgSize>=2) {
    m_oneHalf_a = conf->getPropertyByName<float>("llrtracker_onehalf_a");
  }

  if(m_msgSize==4) {
    m_3of4_a = conf->getPropertyByName<float>("llrtracker_3of4_a");
    m_3of4_b = conf->getPropertyByName<float>("llrtracker_3of4_b");
    m_3of4_cap = conf->getPropertyByName<float>("llrtracker_3of4_cap");
  }

#ifdef VN_HARMONIZE
  m_harmCst = conf->getPropertyByName<float>("decoder_harmonize_cst");
#else
  m_harmCst = 0;
#endif
}

void LLRTrackerAdaptive::push(char msg) {
  // (Only support m_msgSize==4 for now)
  if(m_ruleIndex==0) {
    if(msg==15) {
      m_llrVal = 1.0;
    } else if(msg==0) {
      m_llrVal = -1.0;
    } else {
      m_llrVal = 0;
    }
  }
  else if(m_ruleIndex==1) {
    bitset<8> msgBits(msg); // unused bits in 'msg' are required to be 0
    uint count = msgBits.count();

    if(count==0) {
      if(m_llrVal > -m_d) m_llrVal = -m_d;
#ifdef VNSTOCHB_MSGCOMPENSATION
      else if(m_llrVal < -m_satBound) m_llrVal = -m_limit;
#endif
      else {
        m_llrVal-= m_b;
        m_llrVal = std::max(m_llrVal, -m_limit);
      }
    }
    else if(count==m_msgSize) {
      if(m_llrVal < m_d) m_llrVal = m_d;
#ifdef VNSTOCHB_MSGCOMPENSATION
      else if(m_llrVal > m_satBound) m_llrVal = m_limit;
#endif
      else {
        m_llrVal+= m_b;
        m_llrVal = std::min(m_llrVal, m_limit);
      }
    }
    else {
      if(m_llrVal > m_oneHalfCap) m_llrVal = m_oneHalfCap;
      else if(m_llrVal < -m_oneHalfCap) m_llrVal = -m_oneHalfCap;
      else m_llrVal*= m_oneHalf_a;
    }
  }
  else {
    bitset<8> msgBits(msg); // unused bits in 'msg' are required to be 0
    uint count = msgBits.count();

    if(count==0) {
      if(m_llrVal > -m_d) m_llrVal = -m_d;
#ifdef VNSTOCHB_MSGCOMPENSATION
      else if(m_llrVal < -m_satBound) m_llrVal = -m_limit;
#endif
      else {
        m_llrVal-= m_b;
        m_llrVal = std::max(m_llrVal, -m_limit);
      }
    }
    else if(count==m_msgSize) {
      if(m_llrVal < m_d) m_llrVal = m_d;
#ifdef VNSTOCHB_MSGCOMPENSATION
      else if(m_llrVal > m_satBound) m_llrVal = m_limit;
#endif
      else {
        m_llrVal+= m_b;
        m_llrVal = std::min(m_llrVal, m_limit);
      }
    }
    else if(count==(m_msgSize/2)) {
      if(m_llrVal > m_oneHalfCap) m_llrVal = m_oneHalfCap;
      else if(m_llrVal < -m_oneHalfCap) m_llrVal = -m_oneHalfCap;
      else m_llrVal*= m_oneHalf_a;
    }
    else if(count==3) { //note: max allowed message size is 4
      if(m_llrVal > m_3of4_cap) m_llrVal = m_3of4_cap;
      else if(m_llrVal < -m_oneHalfCap) m_llrVal = -m_oneHalfCap; // using 1/2 cap
      else m_llrVal = m_3of4_a * m_llrVal + m_3of4_b;
    }
    else if(count==1) {
      if(m_llrVal < -m_3of4_cap) m_llrVal = -m_3of4_cap;
      else if(m_llrVal > m_oneHalfCap) m_llrVal = m_oneHalfCap; //using 1/2 cap
      else m_llrVal = m_3of4_a * m_llrVal - m_3of4_b;
    }
    else {
      throw Exception("LLRTrackerAdaptive::push", "Invalid message");
    }
  }
}

void LLRTrackerAdaptive::nextParam() {
  // if( m_ruleIndex >= (m_b.size()-1) ) { //TODO: what determines max nb of rules?
  //   throw Exception("LLRTrackerAdaptive::nextParam", "There is no next tracker rule");
  // }
  m_ruleIndex++;
}

void LLRTrackerAdaptive::harmonize() {
  if(m_llrVal >= 0) m_llrVal-= m_harmCst;
  else m_llrVal+= m_harmCst;
}
