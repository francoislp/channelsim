//Author: Francois Leduc-Primeau

#include "LLRTrackerFP.hpp"
#include "util/Exception.hpp"
#include "Config.hpp"
#include "math.h"
#include <bitset>
#include <algorithm>

LLRTrackerFP::LLRTrackerFP(uint msgSize, uint cnDeg) {
  Config* conf = Config::getInstance();

  //Note: size of m_betaSeq is hardcoded in other methods
  m_betaSeq[0] = conf->getPropertyByName<double>("rhs_beta");
  // try loading two other values
  try {
    m_betaSeq[1] = conf->getPropertyByName<double>("rhs_beta_2");
  } catch(InvalidKeyException&) {
    m_betaSeq[1] = 0;
  }
  try {
    m_betaSeq[2] = conf->getPropertyByName<double>("rhs_beta_3");
  } catch(InvalidKeyException&) {
    m_betaSeq[2] = 0;
  }
#ifdef VNSTOCHB_SIGNEDMSG
  m_msgRuleSwitch = conf->getPropertyByName<uint>("rhs_ruleSwitch_1");
#endif

  m_llrLimit = conf->getPropertyByName<float>("llr_limit");

#ifdef VNSTOCHB_SIGNEDMSG
  if(msgSize!=4) throw Exception("LLRTrackerFP::ctor", 
                                 "only supports 4-bit messages");
#else
#ifdef VNSTOCHB_MSGCOMPENSATION
  double outputLlrLimit = conf->getPropertyByName<double>("rhs_outllr_limit");
  // compute estimation constant "a" for this edge
  double a = 1 - 2 / (exp(outputLlrLimit) + 1);
  a = pow(a, cnDeg - 1);
  if(a==0) throw Exception("LLRTrackerFP::ctor", "Invalid 'a' value");
#endif

  // generate the list of possible messages
  for(uint bitcount=0; bitcount<(msgSize+1); bitcount++) {
    double msg = static_cast<double>(bitcount) / msgSize;
#ifdef VNSTOCHB_MSGCOMPENSATION
    msg/= a;
    msg+= 0.5f*(1 - 1/a);
#endif
    m_probMsgLookUp[bitcount] = msg;
  }
#endif

#ifdef VN_HARMONIZE
  m_harmCst = conf->getPropertyByName<float>("decoder_harmonize_cst");
#else
  m_harmCst = 0;
#endif
}

LLRTrackerFP::~LLRTrackerFP() {
#ifndef VNSTOCHB_SIGNEDMSG
  delete[] m_probMsgLookUp;
#endif
}

void LLRTrackerFP::init(float llrVal) {
  m_prob = 1 / (1 + exp( static_cast<double>(-llrVal) )); // minus to follow
                                                          // convention in
                                                          // VariableNodeStoch_B
  m_ruleIndex = 0;
}

void LLRTrackerFP::set(float llrVal) {
  m_prob = 1 / (1 + exp( static_cast<double>(-llrVal) ));
}

void LLRTrackerFP::push(char msg) {
#ifdef VNSTOCHB_SIGNEDMSG
  double beta;
  if(m_ruleIndex < 3) beta = m_betaSeq[m_ruleIndex];
  else beta = m_betaSeq[2];

  double pmsg;
  //TODO: TEMP code (note: assuming 4 bits/msg)
  if(m_ruleIndex < m_msgRuleSwitch) {
    std::bitset<8> msgBits(msg&254); // count bits, excluding sign bit (lsb)
    uint count = msgBits.count();
    if((msg&1)==0) { // sign bit is 0
      if(count==0)      pmsg = 0.0;
      else if(count==1) pmsg = 0.18;
      else if(count==2) pmsg = 0.41;
      else              pmsg = 0.45;
    } else { // sign bit is 1
      if(count==0)      pmsg = 0.55;
      else if(count==1) pmsg = 0.59;
      else if(count==2) pmsg = 0.82;
      else              pmsg = 1.0;
    }
  }
  else {
    if((msg&1)==0) { // sign bit is 0
      if((msg&2)!=0)      pmsg = 0.348; // [dc,dc,1]
      else if((msg&4)!=0) pmsg = 2.3e-04;
      else if((msg&8)!=0) pmsg = 2.6e-05;
      else if(msg==0)     pmsg = 0.0;
      else throw Exception("LLRTrackerFP::push");
    } else { // sign bit is 1
      if((msg&2)==0)      pmsg = 0.652; // [dc,dc,0]
      else if((msg&4)==0) pmsg = 0.99977; // [dc,0,1]
      else if((msg&8)==0) pmsg = 0.999974; // [0,1,1]
      else if(msg==15)    pmsg = 1.0; // [1,1,1]
      else throw Exception("LLRTrackerFP::push");
    }
  }

  // update tracker
  m_prob = (1-beta)*m_prob + beta*pmsg;

#else // Tracking for the standard message generation rule

#ifdef VNSTOCHB_BETAPROGRESSION
  if(m_ruleIndex>2) {
    throw Exception("LLRTrackerFP::push", 
                    "Rule index exceeds beta sequence length");
  }
  double beta = m_betaSeq[m_ruleIndex];
#else
  double beta = m_betaSeq[0];
#endif //VNSTOCHB_BETAPROGRESSION

  // count bits
  std::bitset<8> msgBits(msg);
  uint count = msgBits.count();
  // look-up message and feed it to the probability tracker
  m_prob = (1-beta)*m_prob + beta*m_probMsgLookUp[count];

#endif //VNSTOCHB_SIGNEDMSG
}

float LLRTrackerFP::curVal() {
  // (LLR value is "flipped" to follow the convention in VariableNodeStoch_B)
  if(m_prob >= 1) {
    return m_llrLimit;
  }
  else if(m_prob <= 0) {
    return -m_llrLimit;
  }
  else {
    float llrVal = log( m_prob/(1-m_prob) );
#ifdef VNSTOCHB_QUANTIZELLR
    throw Exception("LLRTrackerFP::curVal", "Prob tracking currently unsupported when VNSTOCHB_QUANTIZELLR is defined"); //TODO: now easy to add, just copy code from LLRTracker2
#else
    // cap each LLR value
    llrVal = std::min(llrVal, m_llrLimit);
    llrVal = std::max(llrVal, -m_llrLimit);
#endif // end: #ifdef VNSTOCHB_QUANTIZELLR

    return llrVal;
  }
}

void LLRTrackerFP::harmonize() {
  float llrVal = curVal();
  if(llrVal >= 0) llrVal-= m_harmCst;
  else            llrVal+= m_harmCst;
  set(llrVal);
}
