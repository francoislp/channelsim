//Author: Francois Leduc-Primeau

#ifndef LLRTrackerFP_hpp_
#define LLRTrackerFP_hpp_

#include "ILLRTracker.hpp"
#include "GlobalDefs.h"

class LLRTrackerFP : public ILLRTracker {
public:
  LLRTrackerFP(uint msgSize, uint cnDeg);

  ~LLRTrackerFP();

  void init(float llrVal);

  void set(float llrVal);

  void reset() { m_prob = 0.5; m_ruleIndex=0; }

  void push(char msg);

  float curVal();

  void nextParam() { m_ruleIndex++; }

  void harmonize();

private:

  /// Current probability value of the tracker.
  double m_prob;

  double m_betaSeq[3];

  /// Index of the current estimation rule
  uint m_ruleIndex;

#ifdef VNSTOCHB_SIGNEDMSG
  uint m_msgRuleSwitch;
#endif

  /// Max value that positive LLRs can take.
  float m_llrLimit;

#ifndef VNSTOCHB_SIGNEDMSG
  /**
   * Lookup table for converting check node output to probability message.
   */
  double* m_probMsgLookUp;
#endif

  /// Additive constant used for VN harmonization.
  float m_harmCst;
};

#endif
