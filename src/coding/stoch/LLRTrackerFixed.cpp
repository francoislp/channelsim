//Author: Francois Leduc-Primeau

#include "LLRTrackerFixed.hpp"

#define THRESH 1.0
#define BCST 0.125
//#define DCST -0.6091
//#define DCST -0.609375  // (fits on 6 bits)
#define DCST -0.625 // (fits on 3 bits)
//#define BCST 0.1252
//#define DCST -0.5955

LLRTrackerFixed::LLRTrackerFixed(int bitWidth)
{
  m_maxVal_int = (1 << (bitWidth-1)) - 1;
  m_maxVal = static_cast<float>( m_maxVal_int );
  m_fitThreshold = lroundf(THRESH / RANGE * m_maxVal);
  m_b = lroundf(BCST / RANGE * m_maxVal);
  m_d = lroundf(DCST / RANGE * m_maxVal);
  m_curVal = 0;
}

void LLRTrackerFixed::push(char bit) {

  if( bit==0 ) {
    if( m_curVal <= m_fitThreshold ) {
      // note: x/64 is equivalent to right-shift-arithmetic by 6
      m_curVal = m_curVal - (m_curVal / 64) - m_b;
      if(m_curVal < (-m_maxVal_int)) m_curVal = -m_maxVal_int;
    }
    else {
      m_curVal = (m_curVal / 4) - m_d;
    }
  }
  else { // bit==1
    if( m_curVal >= (-m_fitThreshold) ) {
      m_curVal = m_curVal - (m_curVal / 64) + m_b;
      if(m_curVal > m_maxVal_int) m_curVal = m_maxVal_int;
    }
    else {
      m_curVal = (m_curVal / 4) + m_d;
    }
  }

}
