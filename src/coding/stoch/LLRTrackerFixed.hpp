//Author: Francois Leduc-Primeau

#ifndef _LLRTrackerFixed_h_
#define _LLRTrackerFixed_h_

#include <math.h>

#define RANGE 8

class LLRTrackerFixed {
public:

  LLRTrackerFixed(int bitWidth);

  void init(float llrVal) { 
    m_curVal = lroundf(llrVal / RANGE * m_maxVal);
  }

  void reset() { m_curVal = 0; }

  void push(char bit);

  float curVal() { 
    return static_cast<float>(m_curVal) / m_maxVal * RANGE;
  }

private:

  int m_curVal;

  float m_maxVal;
  int m_maxVal_int;

  int m_fitThreshold;

  int m_b;
  int m_d;
};

#endif
