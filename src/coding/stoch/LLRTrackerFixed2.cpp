//Author: Francois Leduc-Primeau

#include "LLRTrackerFixed2.hpp"
#include "Config.hpp"

LLRTrackerFixed2::LLRTrackerFixed2(uint bitWidth, uint msgSize)
  : m_llrVal(0)
{
  if(msgSize==0 || msgSize>2) {
    throw Exception("LLRTrackerFixed2::ctor", "Message size not supported");
  }

  Config* conf = Config::getInstance();

  m_maxVal_int = (1 << (bitWidth-1)) - 1;
  m_maxVal = static_cast<float>( m_maxVal_int );
  m_range = conf->getPropertyByName<float>("llrtrackerfixed_range");
#ifdef VNSTOCHB_QUANTIZELLR
  m_bitWidth = bitWidth;
  m_quantWidth = conf->getPropertyByName<uint>("llrtrackerfixed_quantOutputWidth");
  // output quantization cannot be bigger than internal quantization
  if(m_quantWidth > m_bitWidth) {
    throw Exception("LLRTrackerFixed2::ctor", "bitWidth ctor argument and config \"llrtrackerfixed_quantOutputWidth\" are not compatible.");
  }
  m_outputRange = m_range / m_maxVal;
  m_outputRange*= ((1<<(m_bitWidth-m_quantWidth))-1);
  m_outputRange = m_range - m_outputRange;
#else
  m_outputRange = m_range;
#endif

  m_twoBitMsg = (msgSize==2);
  m_paramSetIndex = 0;

  // some parameters remain the same in all "parameter sets"
  float d = conf->getPropertyByName<float>("llrtracker_d");
  m_d =  lroundf(d / m_range * m_maxVal);
  if(m_twoBitMsg) {
    float oneHalfCap = conf->getPropertyByName<float>("llrtracker_onehalfcap");
    m_oneHalfCap = lroundf(oneHalfCap / m_range * m_maxVal);
  }

  // for the other parameters, try loading up to 3 values
  float b = conf->getPropertyByName<float>("llrtracker_b"); // first is mandatory
  m_b.push_back(lroundf(b / m_range * m_maxVal));
  try {
    b = conf->getPropertyByName<float>("llrtracker_b_2");
    m_b.push_back(lroundf(b / m_range * m_maxVal));
    b = conf->getPropertyByName<float>("llrtracker_b_3");
    m_b.push_back(lroundf(b / m_range * m_maxVal));
  } catch(InvalidKeyException&) {}
  // if(m_twoBitMsg) {
  //   m_oneHalf_a.push_back(conf->getPropertyByName<float>("llrtracker_onehalf_a"));
  //   try {
  //     m_oneHalf_a.push_back(conf->getPropertyByName<float>("llrtracker_onehalf_a_2"));
  //     m_oneHalf_a.push_back(conf->getPropertyByName<float>("llrtracker_onehalf_a_3"));
  //   } catch(InvalidKeyException&) {}
  //   // make sure the size agrees with m_b
  //   if(m_oneHalf_a.size() != m_b.size()) {
  //     throw Exception("LLRTrackerFixed2::ctor", 
  //                     "Error reading config key llrtracker_onehalf_a*");
  //   }
  // } //end: if m_twoBitMsg

#ifdef VN_HARMONIZE
  float harmCst_f = conf->getPropertyByName<float>("decoder_harmonize_cst");
  m_harmCst = lroundf(harmCst_f / m_range * m_maxVal);
#else
  m_harmCst = 0;
#endif
}

void LLRTrackerFixed2::push(char msg) {
  if(m_twoBitMsg && ((msg&0x3)==1 || (msg&0x3)==2)) {
    //FP calculation: m_llrVal*= m_oneHalf_a[m_paramSetIndex];
    //m_llrVal = m_llrVal - m_llrVal/4; (Only perform saturation)

    // This alternative expression provides better rounding, but the BER
    // is worse.
    //int newllrVal = (2*m_llrVal - (m_llrVal/2))/2;

    //Note: Doesn't seem to make a difference on BER whether the block
    //      below is included or not.
    // if( (newllrVal == m_llrVal) and (newllrVal<0) ) {
    //   m_llrVal = newllrVal + 1;
    // } else if( (newllrVal == m_llrVal) and
    //            (newllrVal > 0) ) {
    //   m_llrVal = newllrVal - 1;
    // } else {
    //   m_llrVal = newllrVal;
    // }

    if(m_llrVal > m_oneHalfCap) {
      m_llrVal = m_oneHalfCap;
    }
    if(m_llrVal < -m_oneHalfCap) {
      m_llrVal = -m_oneHalfCap;
    }
    return;
  }

  if( (msg&1)==0 ) {
    if( m_llrVal <= -m_d ) {
      m_llrVal-= m_b[m_paramSetIndex];
      if(m_llrVal < -m_maxVal_int) m_llrVal = -m_maxVal_int; // saturate
    }
    else {
      m_llrVal = -m_d;
    }
  }
  else { // bit==1
    if( m_llrVal >= m_d ) {
      m_llrVal+= m_b[m_paramSetIndex];
      if(m_llrVal > m_maxVal_int) m_llrVal = m_maxVal_int; // saturate
    }
    else {
      m_llrVal = m_d;
    }
  }
}

#ifdef VNSTOCHB_QUANTIZELLR
float LLRTrackerFixed2::curVal() {
  int divider = 1 << (m_bitWidth - m_quantWidth);
  int llr = m_llrVal / divider;
  int max = m_maxVal_int / divider;
  return static_cast<float>(llr) / static_cast<float>(max) 
    * m_outputRange;
}
#endif

void LLRTrackerFixed2::nextParam() {
  if( m_paramSetIndex >= (m_b.size()-1) ) {
    throw Exception("LLRTrackerFixed2::nextParam", "There is no next parameter set");
  }
  m_paramSetIndex++;
}

void LLRTrackerFixed2::harmonize() {
  if(m_llrVal >= 0) m_llrVal-= m_harmCst;
  else m_llrVal+= m_harmCst;
}
