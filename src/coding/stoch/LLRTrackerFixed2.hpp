//Author: Francois Leduc-Primeau

#ifndef LLRTrackerFixed2_h_
#define LLRTrackerFixed2_h_

#include "GlobalDefs.h"
#include "compileflags.h"
#include "ILLRTracker.hpp"
#include "util/Exception.hpp"
#include <vector>
#include <math.h>


/**
 * Similar to LLRTrackerFixed but some parameters are hardcoded (a=1 and c=0).
 */
class LLRTrackerFixed2 : public ILLRTracker {
public:

  /**
   * Constructor.
   *@param bitWidth The number of quantization bits used to represent the tracker
   *                value.
   *@param msgSize Number of bits/msg (packed in a 'char' type).
   */
  LLRTrackerFixed2(uint bitWidth, uint msgSize);

  void init(float llrVal) { 
    m_llrVal = lroundf(llrVal / m_range * m_maxVal); 
    m_paramSetIndex = 0;
  }

  void set(float llrVal) { throw Exception("LLRTrackerFixed2", 
                                           "Method set(float) unimplemented."); }

  void reset() { m_llrVal = 0; m_paramSetIndex = 0;}

  /**
   * Updates the tracker with a new message. If the messages have more
   * than 1 bit, the m.s.b. should be <code>(msg & 1)</code>.
   */
  void push(char msg);
 
#ifndef VNSTOCHB_QUANTIZELLR
  float curVal() { return static_cast<float>(m_llrVal) / m_maxVal * m_range; }
#else
  /**
   * Returns the current value of the tracker, only retaining the
   * precision corresponding to a number of msb's. That number is given
   * by config key "llrtrackerfixed_quantOutputWidth".
   */
  float curVal();
#endif

  /**
   * Go to the next set of update parameters.
   *@throws Exception If no next parameter set exists.
   */
  void nextParam();

  void harmonize();

private:

  float m_maxVal;
  int   m_maxVal_int;

  /// Range when considering all the bits (in terms of m_bitWidth)
  float m_range;

  /// Range when dropping some lsb's (in terms of m_quantWidth)
  float m_outputRange;

#ifdef VNSTOCHB_QUANTIZELLR
  uint  m_bitWidth;
  /// The output can have a coarser quantization.
  uint  m_quantWidth;
#endif

  /// Whether messages have two bits.
  bool m_twoBitMsg;

  // Linear fit constants
  std::vector<int> m_b;
  int m_d;
  int m_oneHalfCap;
  //std::vector<int> m_oneHalf_a;  (currently hard-coded to 0.75)

  /// Current LLR value
  int m_llrVal;

  /// The current parameter set index (index for m_b and m_oneHalf_a)
  uint m_paramSetIndex;

  /// VN Harmonization constant.
  int m_harmCst;
};

#endif
