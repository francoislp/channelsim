//Author: Francois Leduc-Primeau

#ifndef LLRTRACKERHWADAPTOR_HPP
#define LLRTRACKERHWADAPTOR_HPP

#include "ILLRTracker.h"
#include "RHS_models/LLRTrackerHW.hpp"

/**
 * Adapts LLRTrackerHW to the convention used in the channelsim
 * simulator by changing 0-bits to 1-bits at the input side.
 */
class LLRTrackerHWAdaptor : public ILLRTracker {
private:
  LLRTrackerHW m_delegate;

public:
  LLRTrackerHWAdaptor() {}
  void init(float llrVal) { m_delegate.init(llrVal); }
  void set(float llrVal) { m_delegate.set(llrVal); }
  void reset() { m_delegate.reset(); }
  void push(char msg) { m_delegate.push(~msg); }
  float curVal() { return m_delegate.curVal(); }
  void nextParam() { m_delegate.nextParam(); }
  void harmonize() { m_delegate.harmonize(); }
};

#endif
