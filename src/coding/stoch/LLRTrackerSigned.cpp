//Author: Francois Leduc-Primeau

#include "LLRTrackerSigned.hpp"
#include "util/Exception.hpp"
#include "Config.hpp"
#include <bitset>
#include <algorithm>
#include <cmath>

using std::min;
using std::max;

LLRTrackerSigned::LLRTrackerSigned(uint msgSize) {
#ifdef VNSTOCHB_MSGCOMPENSATION
  throw Exception("LLRTrackerSigned::ctor", "VNSTOCHB_MSGCOMPENSATION not supported");
#endif

  // (temp) currently only support 4-bit messages
  if(msgSize!=4) throw Exception("LLRTrackerSigned::ctor", 
                                 "Only support 4-bit messages");

  // Retrieve the tracker limit
  Config* conf = Config::getInstance();
  m_limit = conf->getPropertyByName<float>("llrtracker_limit");

#ifdef VN_HARMONIZE
  m_harmCst = conf->getPropertyByName<float>("decoder_harmonize_cst");
#else
  m_harmCst = 0;
#endif

  // Get parameter "b"
  m_b[0] = conf->getPropertyByName<float>("llrtracker_b");
  // try loading two other values
  try {
    m_b[1] = conf->getPropertyByName<float>("llrtracker_b_2");
  } catch(InvalidKeyException&) {
    m_b[1] = 0;
  }
  try {
    m_b[2] = conf->getPropertyByName<float>("llrtracker_b_3");
  } catch(InvalidKeyException&) {
    m_b[2] = 0;
  }

  m_msgRuleSwitch = conf->getPropertyByName<uint>("rhs_ruleSwitch_1");

  reset();
}

void LLRTrackerSigned::push(char msg) {
  float cur_b;
  if(m_ruleIndex < 3) cur_b = m_b[m_ruleIndex];
  else cur_b = m_b[2];

  //Note: LLR convention follows VariableNodeStoch_B.
  if(m_ruleIndex < m_msgRuleSwitch) {
    std::bitset<8> msgBits(msg&254); // count bits, excluding sign bit (lsb)
    uint count = msgBits.count();
    if((msg&1)==0) { // sign bit is 0
      if(count==0) { // pmsg = 1
        m_llrVal = min(0.5f, m_llrVal-cur_b);
        m_llrVal = max(-m_limit, m_llrVal);
      } else if(count==1) { // pmsg = 0.82;
        m_llrVal = 0.0f - p18Update(-m_llrVal);
      } else if(count==2) { // pmsg = 0.59
        m_llrVal = 0.0f - p41LUT(-m_llrVal);
      } else { // pmsg = 0.55
        m_llrVal = 0.0f - p45LUT(-m_llrVal);
      }
    } else { // sign bit is 1
      if(count==2) { // pmsg = 0.18
        m_llrVal = p18Update(m_llrVal);
      } else if(count==3) {  // pmsg = 0
        m_llrVal = max(-0.5f, m_llrVal+cur_b);
        m_llrVal = min(m_limit, m_llrVal);
      } else if(count==1) { // pmsg = 0.41
        m_llrVal = p41LUT(m_llrVal);
      } else {             // pmsg = 0.45;
        m_llrVal = p45LUT(m_llrVal);
      }
    }
  }
  else { // tracking for message generation rule #2
    if((msg&1)==0) { // sign bit is 0
      if((msg&2)!=0)      m_llrVal = 0.0f - p348LUT(-m_llrVal); //p=0.652
      else if((msg&4)!=0) m_llrVal = max(-9.5f, min(0.5f, m_llrVal-cur_b)); //p=0.99977
      else if((msg&8)!=0) m_llrVal= max(-12.0f, min(0.5f, m_llrVal-cur_b));//p=0.999974
      else if(msg==0) { //p=1
        m_llrVal = min(0.5f, m_llrVal-cur_b);
        m_llrVal = max(-m_limit, m_llrVal);
      }
      else throw Exception("LLRTrackerFP::push");
    } else { // sign bit is 1
      if((msg&2)==0)      m_llrVal = p348LUT(m_llrVal); //p=0.348
      else if((msg&4)==0) m_llrVal = min(9.5f, max(-0.5f, m_llrVal+cur_b)); //p=2.3e-04
      else if((msg&8)==0) m_llrVal= min(12.0f, max(-0.5f, m_llrVal+cur_b)); //p=2.6e-05
      else if(msg==15) { //p=0
        m_llrVal = max(-0.5f, m_llrVal+cur_b);
        m_llrVal = min(m_limit, m_llrVal);
      }
      else throw Exception("LLRTrackerFP::push");
    }
  }
}

void LLRTrackerSigned::harmonize() {
  if(m_llrVal >= 0) m_llrVal-= m_harmCst;
  else m_llrVal+= m_harmCst;
}

#define FPTOL 0.001f
// (private)
float LLRTrackerSigned::p41LUT(float curLLR) {
  curLLR = roundf(curLLR*2.0f) * 0.5f;
  if(curLLR+FPTOL < -3.5f) return -1.5f;
  else if(curLLR+FPTOL < -1.5f) return -1.0f;
  else if(curLLR+FPTOL < -0.5f) return -0.5f;
  else if(curLLR+FPTOL < 0.5f) return 0.0f;
  else if(curLLR+FPTOL < 1.0f) return 0.5f;
  else if(curLLR+FPTOL < 2.0f) return 1.0f;
  else if(curLLR+FPTOL < 5.0f) return 1.5f;
  else return 2.0f;

  // (temp) Ideal update, rounded to 0.5
  // double p = 1.0 / (1.0 + exp(static_cast<double>(curLLR)));
  // p = (1-0.35) * p + 0.35 * 0.41;
  // return round(log( (1-p)/p ) * 2.0) * 0.5;
}

// (private)
float LLRTrackerSigned::p45LUT(float curLLR) {
  curLLR = roundf(curLLR*2.0f) * 0.5f;
  if(curLLR+FPTOL < -3.0f) return -1.5f;
  else if(curLLR+FPTOL < -1.0f) return -1.0f;
  else if(curLLR+FPTOL < -0.5f) return -0.5f;
  else if(curLLR+FPTOL < 0.5f) return 0.0f;
  else if(curLLR+FPTOL < 1.5f) return 0.5f;
  else if(curLLR+FPTOL < 2.5f) return 1.0f;
  else return 1.5f;

  // (temp) Ideal update, rounded to 0.5
  // double p = 1.0 / (1.0 + exp(static_cast<double>(curLLR)));
  // p = (1-0.35) * p + 0.35 * 0.45;
  // return round(log( (1-p)/p ) * 2.0) * 0.5;
}

// (private)
float LLRTrackerSigned::p348LUT(float curLLR) {
  curLLR = roundf(curLLR*2.0f) *0.5f;
  if(curLLR+FPTOL < -1.5f) return -1.0f;
  else if(curLLR+FPTOL < -0.5f) return -0.5f;
  else if(curLLR+FPTOL < 0.5f) return 0.0f;
  else if(curLLR+FPTOL < 1.0f) return 0.5f;
  else if(curLLR+FPTOL < 2.0f) return 1.0f;
  else if(curLLR+FPTOL < 3.5f) return 1.5f;
  else return 2.0f;

  // (temp) Ideal update, rounded to 0.5
  // double p = 1.0 / (1.0 + exp(static_cast<double>(curLLR)));
  // p = (1-0.35) * p + 0.35 * 0.348;
  // return round(log( (1-p)/p ) * 2.0) * 0.5;
}

float LLRTrackerSigned::p18Update(float curLLR) {
  //        return max(-1.0f, min(2.0f, curLLR+0.5f));

  //TEMP: ideal update, rounded to 0.5
  // double p = 1.0 / (1.0 + exp(static_cast<double>(curLLR)));
  // p = (1-0.35) * p + 0.35 * 0.18;
  // return round(log( (1-p)/p ) * 2.0) * 0.5;

  //Note: not an exact LUT
  curLLR = roundf(curLLR*2.0f) *0.5f;
  if(curLLR+FPTOL < -2.5f) return -1.0f;
  else if(curLLR+FPTOL < -1.0f) return -0.5f;
  else if(curLLR+FPTOL < 1.0f) return curLLR+0.5f;
  else if(curLLR+FPTOL < 2.5f) return curLLR;
  else return 2.5f;
}
