//Author: Francois Leduc-Primeau

#ifndef LLRTrackerSigned_hpp_
#define LLRTrackerSigned_hpp_

#include "compileflags.h"
#include "GlobalDefs.h"
#include "ILLRTracker.hpp"
#include <vector>

/**
 * Similar to LLRTracker but some parameters are hardcoded (a=1 and c=0).
 */
class LLRTrackerSigned : public ILLRTracker {
public:

  /**
   * Constructor.
   *@param msgSize Number of bits/msg (packed in a 'char' type).
   */
  LLRTrackerSigned(uint msgSize);

  void init(float llrVal) { m_llrVal = llrVal; m_ruleIndex = 0; }

  void set(float llrVal) { m_llrVal = llrVal; }

  void reset() { m_llrVal = 0; m_ruleIndex = 0; }

  /**
   * Updates the tracker with a new message. All unused bits must be 0.
   */
  void push(char msg);

#ifdef VNSTOCHB_QUANTIZELLR
#ifdef VNSTOCHB_QUANTIZELLR_RND
  float curVal() {
    return roundf(m_llrVal*VNSTOCHB_QUANTIZELLR_RNDSF) / VNSTOCHB_QUANTIZELLR_RNDSF;
  }
#else
  //TODO: Currently always quantize to integer. This should be configurable.
  float curVal() { 
    float x = floorf(fabs(m_llrVal));
    if(m_llrVal>=0) return x;
    else return -x;
  }
#endif
#else 
  float curVal() { return m_llrVal; }
#endif

  /**
   * Go to the next set of update parameters.
   */
  void nextParam() { m_ruleIndex++; }

  void harmonize();

private:

  /// Number of bits per message.
  uint m_msgSize;

  /// Current LLR value
  float m_llrVal;

  /// Index of the current tracking rule
  uint m_ruleIndex;

  /// Max value that positive LLRs can take.
  float m_limit;

  /// Additive parameter for "strong" messages
  float m_b[3];

  uint m_msgRuleSwitch;

  /// VN Harmonization constant
  float m_harmCst;

  // ----- Methods -----
  float p41LUT(float);
  float p45LUT(float);
  float p348LUT(float);
  float p18Update(float);
};

#endif
