//Author: Francois Leduc-Primeau

#include "LLRtoBitsSigned.hpp"
#include "Config.hpp"
#include <algorithm>

LLRtoBitsSigned::LLRtoBitsSigned(uint bitsize, std::vector<IRandomLLR*>& rngList)
  : m_bitsize(bitsize),
    m_rngList(rngList)
{
  if(m_bitsize==0 || m_bitsize>8) {
    throw Exception("LLRtoBitsSigned::ctor", "bitsize value not supported");
  }

  Config* conf = Config::getInstance();
  m_threshLimit = conf->getPropertyByName<float>("rhs_threshold_limit");
  if(m_threshLimit <= 0) {
    throw Exception("LLRtobits::ctor", "Invalid value for key \"rhs_threshold_limit\"");
  }

  resetRule();
}

char LLRtoBitsSigned::convert(float llrval) {
  char msg;
  char setmask = 2;

  if(m_ruleIndex==0) {
    if(llrval>=0) {
      msg = 1;
      for(uint i=0; i<m_bitsize-1; i++) {
        float curThresh = m_rngList[i]->getCurVal(); // assume positive
        curThresh = std::min(curThresh, m_threshLimit); // cap
        if(llrval >= curThresh) msg|= setmask;
        //else if(llrval == curThresh && m_rngList[i]->getCurBit()==1) msg|= setmask;
        setmask<<= 1;
      }
    } 
    else {
      msg = 0;
      for(uint i=0; i<m_bitsize-1; i++) {
        float curThresh = 0 - m_rngList[i]->getCurVal();
        curThresh = std::max(curThresh, -m_threshLimit); // cap
        if(llrval >= curThresh) msg|= setmask;
        //else if(llrval == curThresh && m_rngList[i]->getCurBit()==0) msg|= setmask;
        setmask<<= 1;
      }
    }
  }
  else {
    float threshList[3];
    threshList[0] = m_rngList[0]->getCurVal();
    threshList[0] = std::min(threshList[0], m_threshLimit); // cap
//     threshList[1] = 6.9;
//     threshList[2] = 9.2;
    threshList[1] = m_rngList[3]->getCurVal();
    threshList[1] = std::min(threshList[1], m_threshLimit);
    threshList[2] = m_rngList[4]->getCurVal();
    threshList[2] = std::min(threshList[2], m_threshLimit);

    if(llrval>=0) {
      msg = 1;
      for(uint i=0; i<m_bitsize-1; i++) {
        if(llrval >= threshList[i]) msg|= setmask;
        //else if(llrval==threshList[i] && m_rngList[i]->getCurBit()==1) msg|= setmask;
        setmask<<= 1;
      }
    }
    else {
      msg = 0;
      for(uint i=0; i<m_bitsize-1; i++) {
        if(llrval >= -threshList[i]) msg|= setmask;
        //else if(llrval==threshList[i] && m_rngList[i]->getCurBit()==0) msg|= setmask;
        setmask<<= 1;
      }
    }
  }

  return msg;
}
