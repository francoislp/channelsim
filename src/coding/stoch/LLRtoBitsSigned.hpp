//Author: Francois Leduc-Primeau

#ifndef LLRtoBitsSigned_hpp_
#define LLRtoBitsSigned_hpp_

#include "GlobalDefs.h"
#include "ILLRtoBits.hpp"
#include "IRandomLLR.hpp"
#include "util/Exception.hpp"
#include <vector>

/**
 * Converts an LLR value to a "signed" binary-message representation.
 */
class LLRtoBitsSigned : public ILLRtoBits {
public:

  /**
   * Create a new message converter.
   *@param bitsize  The number of bits/message, including the sign bit.
   *@param rngList  List of random LLR threshold generators. Must be of size
   *                (bitsize-1).
   */
  LLRtoBitsSigned(uint bitsize, std::vector<IRandomLLR*>& rngList);

  char convert(float llrval);

  void nextRule() { 
    m_ruleIndex++;
    // make sure we have access to the thresholds with the other distributions
    if(m_rngList.size() <= m_bitsize) throw Exception("LLRtoBitsSigned::nextRule",
                                                      "Invalid size for rngList");
  }

  void resetRule() { m_ruleIndex = 0; }

private:

  // ----- Data -----

  uint m_bitsize;

  std::vector<IRandomLLR*>& m_rngList;

  float m_threshLimit;

  uint m_ruleIndex;
};

#endif
