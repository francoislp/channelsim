#include "LLRtobits.hpp"
#include "util/Exception.hpp"
#include "Config.hpp"
#include <algorithm>

LLRtobits::LLRtobits(uint bitsize, bool deterministicStart, 
                     std::vector<IRandomLLR*>& rngList)
  : m_rngList(rngList),
    m_deterministicStart(deterministicStart)
{
  m_bitsize = bitsize;
  if(m_bitsize==0 || m_bitsize>8) {
    throw Exception("LLRtobits::ctor", "bitsize not supported");
  }

  if( m_deterministicStart ) {
    if(m_bitsize==1) {
      m_threshList = new float[1];
      m_threshList[0] = 0;
    } else if(m_bitsize==2) {
      m_threshList = new float[2];
      m_threshList[0] = 0;
      m_threshList[1] = 1.0986f;
    // } else if(m_bitsize==4) {
    //   m_threshList = new float[4];
    } else {
      throw Exception("LLRtobits::ctor", 
                      "bitsize not supported with deterministic start");
    }
  }
  else {
    m_threshList = 0;
  }

  Config* conf = Config::getInstance();
  m_threshLimit = conf->getPropertyByName<float>("rhs_threshold_limit");
  if(m_threshLimit <= 0) {
    throw Exception("LLRtobits::ctor", "Invalid value for key \"rhs_threshold_limit\"");
  }

  resetRule();
}

LLRtobits::~LLRtobits() {
  if(m_threshList!=0) delete[] m_threshList;
}

char LLRtobits::convert(float llrval) {
  char msg;
  if( m_useDeterministic ) {
    msg = llrval > m_threshList[0];
    // Loop currently hardcoded because only up to 2 bits are supported
    if(m_bitsize>1) {
      msg|= (msg==1 && llrval > m_threshList[1]) << 1;
      msg|= (msg==0 && llrval > (0-m_threshList[1])) << 1;
    }
  } else { // use random thresholds
    msg = 0;
    char setmask = 1;
    for(uint i=0; i<m_bitsize; i++) {
      float curThresh = m_rngList[i]->getCurVal();
      // cap the random number so that it is always smaller in
      // magnitude than a saturated output value
      curThresh = std::min(curThresh, m_threshLimit);
      curThresh = std::max(curThresh, -m_threshLimit);

      if(llrval > curThresh) msg|= setmask;
      else if(llrval == curThresh && m_rngList[i]->getCurBit()==1) msg|= setmask;
      setmask<<= 1;
    }
  }
  return msg;
}
