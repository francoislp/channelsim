//Author: Francois Leduc-Primeau

#ifndef LLRtobits_h_
#define LLRtobits_h_

#include "GlobalDefs.h"
#include "ILLRtoBits.hpp"
#include "IRandomLLR.hpp"
#include <vector>

/**
 * Converts a LLR value to binary-message representation. There is an
 * option to have a first rule that uses deterministic thresholds. The
 * other rule uses k uniform i.i.d. random thresholds.
 */
class LLRtobits : public ILLRtoBits {
public:

  /**
   * Create a new converter object.
   *@param bitsize             The number of bits / message.
   *@param deterministicStart  Whether the first rule should use deterministic 
   *                           thresholds.
   *@param rngList             One LLR threshold generator for each message bit.
   */
  LLRtobits(uint bitsize, bool deterministicStart, 
            std::vector<IRandomLLR*>& rngList);

  ~LLRtobits();

  char convert(float llrval);

  void nextRule() { m_useDeterministic = false; }

  void resetRule() { m_useDeterministic = m_deterministicStart; }

private:

  // ----- Data Members -----

  std::vector<IRandomLLR*>& m_rngList;

  /// Whether the first rule is deterministic
  bool m_deterministicStart;

  /// Whether we are currently using the deterministic thresholds.
  bool m_useDeterministic;

  /// The list of k deterministic thresholds (can be NULL).
  float* m_threshList;

  /// Number of bits in the binary-message representation.
  uint m_bitsize;

  float m_threshLimit;
};

#endif
