//Author: Francois Leduc-Primeau

#include "RandomLLRFloat.hpp"
#include <math.h>

RandomLLRFloat::RandomLLRFloat() {
  m_dre = new StochDRE();
}

RandomLLRFloat::RandomLLRFloat(float pMean) {
  m_dre = new StochDRE(pMean);
}

RandomLLRFloat::~RandomLLRFloat() {
  delete m_dre;
}

void RandomLLRFloat::nextTick() {
  m_dre->nextTick();
  m_curBit = m_dre->getVal() & 1;
  float p = m_dre->getVal_f();
  m_curLLR = logf( (1-p) / p );
}
