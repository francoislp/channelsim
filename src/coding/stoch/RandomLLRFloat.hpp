//Author: Francois Leduc-Primeau

#ifndef RandomLLRFloat_h_
#define RandomLLRFloat_h_

#include "IRandomLLR.hpp"
#include "StochDRE.hpp"

/**
 * Generates random LLR values having a uniform distribution in the
 * probability domain. The random number generation is performed in
 * floating point by an internal StochDRE object.
 */
class RandomLLRFloat : public IRandomLLR {
public:

  RandomLLRFloat();

  /**
   * Creates a new random LLR generator with a distribution that
   * corresponds to random probability values uniformly distributed
   * over (0, 2*pMean).
   */
  RandomLLRFloat(float pMean);

  ~RandomLLRFloat();

  /**
   * Returns the current random LLR value. Note: Must call nextTick()
   * to update this value.
   */
  float getCurVal() {return m_curLLR;}

  /**
   * Returns a fair random bit based on the current random
   * value. Note: Must call nextTick() to update this bit.
   */
  char getCurBit() {return m_curBit;}

  /// Updates the current random LLR value.
  void nextTick();

  void reset() { m_dre->reset(); nextTick(); }

private:

  StochDRE* m_dre;

  float m_curLLR;

  char m_curBit;
};
#endif
