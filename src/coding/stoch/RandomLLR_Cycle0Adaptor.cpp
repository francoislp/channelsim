#include "RandomLLR_Cycle0Adaptor.hpp"

RandomLLR_Cycle0Adaptor::RandomLLR_Cycle0Adaptor(IRandomLLR* baseDRE)
: m_baseDRE(baseDRE),
  m_outdated(true)
{
}

float RandomLLR_Cycle0Adaptor::getCurVal() {
  if(m_outdated) {
    cacheValues();
    m_outdated = false;
  }
  return m_curVal;
}

char RandomLLR_Cycle0Adaptor::getCurBit() {
  if(m_outdated) {
    cacheValues();
    m_outdated = false;
  }
  return m_curBit;
}

// (private)
void RandomLLR_Cycle0Adaptor::cacheValues() {
  m_curVal = m_baseDRE->getCurVal();
  m_curBit = m_baseDRE->getCurBit();
}
