#ifndef RandomLLR_Cycle0Adaptor_hpp_
#define RandomLLR_Cycle0Adaptor_hpp_

#include "IRandomLLR.hpp"

/**
 * Adaptor that caches the return values of an underlying IRandomLLR
 * object (the "base DRE"). The values are refreshed only if reset()
 * or nextTick() are called. Calls to reset() and nextTick() are also
 * passed on to the base DRE, and the base DRE is destroyed when this
 * object is destroyed.
 */
class RandomLLR_Cycle0Adaptor : public IRandomLLR {
public:

  RandomLLR_Cycle0Adaptor(IRandomLLR* baseDRE);

  ~RandomLLR_Cycle0Adaptor() { delete m_baseDRE; }

  void reset() { 
    m_outdated = true;
    m_baseDRE->reset();
  }

  void nextTick() { 
    m_outdated = true; 
    m_baseDRE->nextTick();
  }

  float getCurVal();

  char getCurBit();

private:

  // Retrieve all values from the underlying DRE
  void cacheValues();

  IRandomLLR* m_baseDRE;

  bool m_outdated;

  float m_curVal;
  char  m_curBit;
};

#endif
