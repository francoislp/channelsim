#include "RandomLLR_CycleAdaptor.hpp"

RandomLLR_CycleAdaptor::RandomLLR_CycleAdaptor(IRandomLLR* baseDRE)
  : m_baseDRE(baseDRE),
    m_outdated(true)
{
}

float RandomLLR_CycleAdaptor::getCurVal() {
  if(m_outdated) {
    m_outdated = false;
    m_baseDRE->nextTick();
    cacheValues();
  }
  return m_curVal;
}

char RandomLLR_CycleAdaptor::getCurBit() {
  if(m_outdated) {
    m_outdated = false;
    m_baseDRE->nextTick();
    cacheValues();
  }
  return m_curBit;
}

// (private)
void RandomLLR_CycleAdaptor::cacheValues() {
  m_curVal = m_baseDRE->getCurVal();
  m_curBit = m_baseDRE->getCurBit();
}
