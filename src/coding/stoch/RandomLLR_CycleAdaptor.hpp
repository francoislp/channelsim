#ifndef RandomLLR_CycleAdaptor_h_
#define RandomLLR_CycleAdaptor_h_

#include "IRandomLLR.hpp"

/**
 * This class generates random LLR values by referring to another
 * IRandomLLR object and advancing that object by one clock cycle.
 */
class RandomLLR_CycleAdaptor : public IRandomLLR {
public:

  RandomLLR_CycleAdaptor(IRandomLLR* baseDRE);

  void reset() { 
    m_outdated = true; 
  }

  void nextTick() {
    m_outdated = true; 
  }

  float getCurVal();

  char getCurBit();

private:

  // Retrieve all values from the underlying DRE
  void cacheValues();

  IRandomLLR* m_baseDRE;

  bool m_outdated;

  float m_curVal;
  char m_curBit;
};

#endif
