/* RandomLLR_LFSR.cpp
 *
 * Copyright (C) 2011 Pascal Giard, Francois Leduc-Primeau
 *
 * Author: Pascal Giard <evilynux@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "RandomLLR_LFSR.hpp"
#include "util/Exception.hpp"
#include <math.h>
#ifdef DEBUG
#include <stdio.h>
#endif

#ifndef RANDOMLLR_LFSR_STARTLLR
#define RANDOMLLR_LFSR_STARTLLR 1
#endif

// Reset value of the threshold output register
#ifndef RANDOMLLR_LFSR_REGRESETVAL
#define RANDOMLLR_LFSR_REGRESETVAL 2
#endif

// Reset value of the random bit output register
#ifndef RANDOMLLR_LFSR_REGRESETBIT
#define RANDOMLLR_LFSR_REGRESETBIT 0
#endif

RandomLLR_LFSR::RandomLLR_LFSR(uint8_t lfsr_type, uint8_t lfsr_wl,
                               uint16_t lfsr_seed_idx) {
  init(lfsr_type, lfsr_wl, lfsr_seed_idx, false, false, 0);
}

RandomLLR_LFSR::RandomLLR_LFSR(uint8_t lfsr_type, uint8_t lfsr_wl,
                               uint16_t lfsr_seed_idx, bool floorDist) {
  init(lfsr_type, lfsr_wl, lfsr_seed_idx, floorDist, false, 0);
}

RandomLLR_LFSR::RandomLLR_LFSR(uint8_t lfsr_type, uint8_t lfsr_wl,
                               uint16_t lfsr_seed_idx, bool floorDist,
                               bool onlyPos) {
  init(lfsr_type, lfsr_wl, lfsr_seed_idx, floorDist, onlyPos, 0);
}

RandomLLR_LFSR::RandomLLR_LFSR(uint8_t lfsr_type, uint8_t lfsr_wl,
                               uint16_t lfsr_seed_idx, bool floorDist,
                               bool onlyPos, uint8_t msgRuleIndex) {
  init(lfsr_type, lfsr_wl, lfsr_seed_idx, floorDist, onlyPos, msgRuleIndex);
}

void RandomLLR_LFSR::init(uint8_t  lfsr_type,
                          uint8_t  lfsr_wl,
                          uint16_t lfsr_seed_idx,
                          bool     floorDist,
                          bool     onlyPos,
                          uint8_t  msgRuleIndex) {
  m_floorDist = floorDist;
  m_onlyPos = onlyPos;
  m_msgRuleIndex = msgRuleIndex;

  if( m_floorDist and lfsr_wl != 10 ) {
    throw Exception("RandomLLR_LFSR::ctor",
                    "floorDist only supported with wordlength of 10bits.");
  }

  if(m_msgRuleIndex>2) {
    throw Exception("RandomLLR_LFSR::ctor", "Max supported rule index is 2");
  }
  if(m_msgRuleIndex>0 && (m_floorDist || !m_onlyPos || lfsr_wl<9)) {
    throw Exception("RandomLLR_LFSR::ctor",
                    "Unsupported argument combination");
  }

  if( lfsr_seed_idx > (pow(2.0, lfsr_wl)-2) ) {
    throw Exception("RandomLLR_LFSR::ctor",
                    "Invalid seed idx.");
  }

  else if( lfsr_wl > 8 ) {
    m_seed_16 = getSeed16(lfsr_seed_idx, lfsr_wl);
    prng = new LfsrPrng(lfsr_type, lfsr_wl, m_seed_16);
  }
  else {
    m_seed = getSeed(lfsr_seed_idx, lfsr_wl);
    prng = new LfsrPrng(lfsr_type, lfsr_wl, m_seed);
  }

  m_firstTick = true;

  m_wl = lfsr_wl;
}

RandomLLR_LFSR::~RandomLLR_LFSR() {
  delete prng;
}

// (private)
uint8_t RandomLLR_LFSR::getSeed(uint8_t seed_idx, uint8_t wl) {
  uint8_t idx = 1;
  
  switch(wl)
    {
    case(7):
      idx = seed_idx+1;
      return idx;
      // return (((idx & 0x10) << 2) | (idx & 0xF)) & 0x4F;
      break;
    default:
      throw Exception("RandomLLR_LFSR::getSeed", "Unsupported wordlength.");
#ifdef DEBUG
      printf("RandomLLR_LFSR::getSeed: Unsupported wordlength %d.\n", wl);
#endif
      return idx+1;
      break;
    }
}

uint16_t RandomLLR_LFSR::getSeed16(uint16_t seed_idx, uint8_t wl) {
  // pre-defined "good" seeds
  uint16_t seeds9[32] = {
    95, 351, 162, 356, 99, 417, 253, 420,
    240, 442, 190, 434, 118, 352, 159, 380,
    228, 429, 242, 432, 187, 480, 161, 444,
    106, 489, 163, 490, 164, 506, 228, 500,
  };

#if RANDOMLLR_LFSR_STARTLLR == 1
  // +1 and -1 alternatively
  uint16_t seeds10[32] = {
    501, 920, 437, 970, 484, 934, 476, 996, 
    496, 941, 473, 947, 489, 1015, 487, 936, 
    386, 1017, 434, 983, 477, 932, 260, 928, 
    410, 945, 448, 905, 460, 963, 265, 942
  };
#else
  // +2 and -2 alternatively
  uint16_t seeds10[32] = {
    372, 891, 337, 864, 370, 840, 323, 880, 
    361, 839, 361, 858, 349, 871, 352, 892, 
    345, 892, 358, 875, 376, 832, 335, 891, 
    361, 872, 343, 874, 345, 883, 326, 856
  };
#endif

  switch(wl)
    {
    case(10):
      if(seed_idx > 31) {
        throw Exception("RandomLLR_LFSR::getSeed16", "Seed index > 31 unsupported");
      } else {
        return seeds10[seed_idx]+1;
      }
      break;
    case(9):
      if(seed_idx > 31) {
        throw Exception("RandomLLR_LFSR::getSeed16", "Seed index > 31 unsupported");
      } else {
        return seeds9[seed_idx]+1;
      }
      break;
    default:
      throw Exception("RandomLLR_LFSR::getSeed16", "Unsupported wordlength.");
#ifdef DEBUG
      printf("RandomLLR_LFSR::getSeed16: Unsupported wordlength %d.\n", wl);
#endif
      break;
    }
  // suppress compiler warning:
  throw Exception("RandomLLR_LFSR::getSeed16", "Should never come here");
}

void RandomLLR_LFSR::reset() {
  m_firstTick = true;
}

void RandomLLR_LFSR::nextTick() {
  if( m_firstTick ) {
    // reset the LFSR
    if( m_wl > 8 ) {
      prng->reset(m_seed_16);
      m_rndSample_16 = m_seed_16;
    } else {
      prng->reset(m_seed);
      m_rndSample = m_seed;
    }
  }
  else {
    // normal operation
    if( m_wl > 8 ) {
      m_rndSample_16 = prng->gen16();
    } else {
      m_rndSample = prng->gen();
    }
  }

  m_firstTick = false;
}

float RandomLLR_LFSR::getCurVal() {
  if(m_firstTick) return RANDOMLLR_LFSR_REGRESETVAL; // output reg reset val

  float thresh;
  if( m_floorDist ) {
    thresh = lfsr2LLR_floor(m_rndSample_16);
  } else if( m_wl > 8 ) {
    thresh = lfsr2LLR(m_rndSample_16);
  } else {
    thresh = lfsr2LLR(m_rndSample);
  }

  if(m_onlyPos) return thresh;
  // switch the sign for compatibility with the channelsim LLR
  // convention (matters for correspondence with HW models, even
  // though the distribution should be symmetric)
  else return 0-thresh;
}

int8_t RandomLLR_LFSR::getCurValInt() {
  if(m_firstTick) return RANDOMLLR_LFSR_REGRESETVAL; // output reg reset val
  if( m_floorDist ) throw Exception("RandomLLR_LFSR::getCurValInt", "Aaaah!");
  return lfsr2LLRInt(m_rndSample);
}

int16_t RandomLLR_LFSR::getCurValInt16() {
  if(m_firstTick) return RANDOMLLR_LFSR_REGRESETVAL; // output reg reset val
  if( m_floorDist ) return lfsr2LLRInt_floor(m_rndSample_16);
  return lfsr2LLRInt(m_rndSample_16);
}

char RandomLLR_LFSR::getCurBit() {
  if(m_firstTick) return RANDOMLLR_LFSR_REGRESETBIT; // output reg reset val
  if( m_wl > 8 ) {
    return lfsr2RndBit(m_rndSample_16);
  } else {
    return lfsr2RndBit(m_rndSample);
  }
}

// (private)
float RandomLLR_LFSR::lfsr2LLR(uint8_t rn) {
  return static_cast<float>(lfsr2LLRInt(rn));
}

// (private)
float RandomLLR_LFSR::lfsr2LLR(uint16_t rn) {
  return static_cast<float>(lfsr2LLRInt(rn));
}

// (private)
int8_t RandomLLR_LFSR::lfsr2LLRInt(uint8_t rn) {
  int8_t sign;

  if( ((rn>>4) & 0x3) == 0 ) return 0;

  sign = ( ((rn>>6)&1) == 0 ) ? 1 : -1;

  if( ((rn>>3)&1) == 1 ) return sign*1;
  if( ((rn>>2)&1) == 1 ) return sign*2;
  if( ((rn>>1)&1) == 1 ) return sign*3;
  if( (rn&1) == 1 ) return sign*4;
  return sign*2;
}

// (private)
int16_t RandomLLR_LFSR::lfsr2LLRInt(uint16_t rn) {

  if( m_onlyPos && m_wl>=8) {
    if(m_msgRuleIndex==0) {
      if( ((rn>>6) & 0x3) == 0 ) return 0;
      if( ((rn>>5)&1) == 1 )     return 1;
      if( ((rn>>4)&1) == 1 )     return 2;
      if( ((rn>>3)&1) == 1 )     return 3;
      if( ((rn>>2)&1) == 1 )     return 4;
      if( ((rn>>1)&1) == 1 )     return 5;
      if( (rn&1) == 1 )          return 2;
      return 6;
    } else if(m_msgRuleIndex==1) {
      if( ((rn>>6) & 0x3) == 0 ) return 9;
      if( ((rn>>5)&1) == 1 )     return 10;
      if( ((rn>>4)&1) == 1 )     return 11;
      if( ((rn>>3)&1) == 1 )     return 9;
      if( ((rn>>2)&1) == 1 )     return 12;
      if( ((rn>>1)&1) == 1 )     return 13;
      if( (rn&1) == 1 )          return 11;
      return 12;
    } else {
      if( ((rn>>6) & 0x3) == 0 ) return 13;
      if( ((rn>>5)&1) == 1 )     return 12;
      if( ((rn>>4)&1) == 1 )     return 12;
      if( ((rn>>3)&1) == 1 )     return 14;
      if( ((rn>>2)&1) == 1 )     return 12;
      if( ((rn>>1)&1) == 1 )     return 15;
      if( (rn&1) == 1 )          return 12;
      return 15;
    }
  }
  else if( !m_onlyPos && m_wl>=9 ) {
    int16_t sign;
    if( ((rn>>6) & 0x3) == 0 ) return 0;

    sign = ( ((rn>>8)&1) == 0 ) ? 1 : -1;
    if( ((rn>>5)&1) == 1 ) return sign*1;
    if( ((rn>>4)&1) == 1 ) return sign*2;
    if( ((rn>>3)&1) == 1 ) return sign*3;
    if( ((rn>>2)&1) == 1 ) return sign*4;
    if( ((rn>>1)&1) == 1 ) return sign*5;
    if( (rn&1) == 1 ) return sign*2;
    return sign*6;
  }
  else {
    throw Exception("RandomLLR_LFSR::lfsr2LLR", "Unsupported wordlength.");
  }
}

// (private)
float RandomLLR_LFSR::lfsr2LLR_floor(uint16_t rn) {
  return static_cast<float>(lfsr2LLRInt_floor(rn));
}

int16_t RandomLLR_LFSR::lfsr2LLRInt_floor(uint16_t rn) {

  if( ((rn>>8)&1) == 0 ) return 0;

  if( m_onlyPos ) {
    if( ((rn>>7)&1) == 0 ) return 1;
    if( ((rn>>6)&1) == 0 ) return 2;
    if( ((rn>>5)&1) == 0 ) return 3;
    if( ((rn>>4)&1) == 0 ) return 4;
    if( ((rn>>2)&3) == 3 ) return 5;
    if( (rn&3) == 3 )      return 6;
    return 1;    
  } 
  else {
    int16_t sign;
    sign = ( ((rn>>9)&1) == 0 ) ? 1 : -1;
    if( ((rn>>7)&1) == 0 ) return sign*1;
    if( ((rn>>6)&1) == 0 ) return sign*2;
    if( ((rn>>5)&1) == 0 ) return sign*3;
    if( ((rn>>4)&1) == 0 ) return sign*4;
    if( ((rn>>2)&3) == 3 ) return sign*5;
    if( (rn&3) == 3 )      return sign*6;
    return sign*1;
  }
}

// (private)
char RandomLLR_LFSR::lfsr2RndBit(uint8_t rn) {
  return rn&1;
}

// (private)
char RandomLLR_LFSR::lfsr2RndBit(uint16_t rn) {
  return rn&1;
}
