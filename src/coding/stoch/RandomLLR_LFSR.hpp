/* RandomLLR_LFSR.h
 *
 * Copyright (C) 2011 Pascal Giard, Francois Leduc-Primeau
 *
 * Author: Pascal Giard <evilynux@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef RANDOMLLR_LFSR_H
#define RANDOMLLR_LFSR_H

#include <stdint.h>
#include "lfsr_prng.h"
#include "IRandomLLR.hpp"

class RandomLLR_LFSR : public IRandomLLR {
 private:

  // ---------- Data ----------
  LfsrPrng *prng;
  uint8_t    m_rndSample; // current output of the LFSR
  uint16_t   m_rndSample_16; // current output of the LFSR
  uint8_t   m_seed;
  uint16_t  m_seed_16;
  uint8_t   m_wl;
  bool      m_firstTick; // Whether we are in the first clock cycle
  bool      m_floorDist; // True if we generate the "floor" distribution
  bool      m_onlyPos; // True if we only generate positive values
  uint8_t   m_msgRuleIndex; // first rule has index 0

  // ---------- Private Methods ----------
  void   init(uint8_t, uint8_t, uint16_t, bool, bool, uint8_t); // called by constructors
  uint8_t  getSeed(uint8_t seed_idx, uint8_t wl);
  uint16_t getSeed16(uint16_t seed_idx, uint8_t wl);
  float   lfsr2LLR(uint8_t rn);
  float   lfsr2LLR(uint16_t rn);
  float   lfsr2LLR_floor(uint16_t rn);
  int8_t  lfsr2LLRInt(uint8_t rn);
  int16_t lfsr2LLRInt(uint16_t rn);
  int16_t lfsr2LLRInt_floor(uint16_t rn);
  char    lfsr2RndBit(uint8_t rn);
  char    lfsr2RndBit(uint16_t rn);

 public:
  /**
   * Constructor.
   *@param lfsr_type Type of LFSR PRNG generator algorithm: either Fibonacci
   * (LfsrPrng::FIBO) or Galois (LfsrPrng::GALOIS).
   *@param lfsr_wl Wordlength for the LFSR PRNG, where \f${lfsr\_wl} \in [4,7]\f$.
   *@param lfsr_seed_idx LFSR PRNG seed index that generates a LLR of 0, where
   *\f${lfsr\_seed_idx}\in[0,2^{lfsr\_wl-2}-2]\f$.
   *@throws Exception if \c lfsr_wl value support is not implemented yet.
   *@throws Exception if \c lfsr_seed_idx is out of range.
   */
  RandomLLR_LFSR(uint8_t lfsr_type,
                 uint8_t lfsr_wl,
                 uint16_t lfsr_seed_idx);

  /**
   * Constructor, with possibility of choosing the type of distribution.
   *@param lfsr_type Type of LFSR PRNG generator algorithm: either Fibonacci
   * (LfsrPrng::FIBO) or Galois (LfsrPrng::GALOIS).
   *@param lfsr_wl Wordlength for the LFSR PRNG, where \f${lfsr\_wl} \in [4,7]\f$.
   *@param lfsr_seed_idx LFSR PRNG seed index that generates a LLR of 0, where
   *\f${lfsr\_seed_idx}\in[0,2^{lfsr\_wl-2}-2]\f$.
   *@param floorDist True to use the "floor" distribution.
   *@throws Exception if \c lfsr_wl value support is not implemented yet.
   *@throws Exception if \c lfsr_seed_idx is out of range.
   */
  RandomLLR_LFSR(uint8_t  lfsr_type,
                 uint8_t  lfsr_wl, 
                 uint16_t lfsr_seed_idx,
                 bool     floorDist);

  RandomLLR_LFSR(uint8_t  lfsr_type,
                 uint8_t  lfsr_wl, 
                 uint16_t lfsr_seed_idx,
                 bool     floorDist,
                 bool     onlyPos);

  RandomLLR_LFSR(uint8_t  lfsr_type,
                 uint8_t  lfsr_wl, 
                 uint16_t lfsr_seed_idx,
                 bool     floorDist,
                 bool     onlyPos,
                 uint8_t  msgRuleIndex);

  ~RandomLLR_LFSR();

  ///@see ISequential::reset()
  void reset();

  ///@see ISequential::nextTick()
  void nextTick();

  ///@see IRandomLLR::getCurVal()
  float getCurVal();
  int8_t getCurValInt();
  int16_t getCurValInt16();

  ///@see IRandomLLR::getCurBit()
  char getCurBit();  
};

#endif /* RANDOMLLR_LFSR_H */
