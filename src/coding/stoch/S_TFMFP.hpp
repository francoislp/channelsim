// Author: Francois Leduc-Primeau

#ifndef _S_TFMFP_h_
#define _S_TFMFP_h_

#include "ITFM.hpp"
#include "IStochDRE.hpp"
#include "util/Exception.hpp"

class S_TFMFP : public ITFM {
public:

  S_TFMFP(float addcst, IStochDRE* dre);

  void init(double prob) { m_val = static_cast<float>(prob); }

  void reset() { init(0.5); }

  void push(char bit) {
    if(bit==0) m_val-= m_addcst;
    else m_val+= m_addcst;
    // saturate at 0 and 1
    if(m_val > 1.0) m_val = 1.0;
    if(m_val < 0.0) m_val = 0.0;
  }

  char getrnd() {
    float rndvalf = m_dre->getVal_f();
    if( m_val > rndvalf ) return 1;
    return 0;
  }

  char getHD() { throw new Exception("S_TFMFP::getHD", "Unimplemented"); }

  IStochDRE* getDRE() { return m_dre; }

  double curVal() { return static_cast<double>(m_val); }

private:

  IStochDRE* m_dre;

  float m_addcst;

  float m_val;
};

#endif
