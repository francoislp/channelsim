//Author: Francois Leduc-Primeau

#include "ShiftReg.hpp"

ShiftReg::ShiftReg(unsigned int size, IStochDRE* dre) {
  if(size==2) m_implementation = new ShiftReg2(dre);
  else m_implementation = new ShiftRegGeneral(size, dre);
}

ShiftReg::ShiftReg(unsigned int size, unsigned int offset, IStochDRE* dre) {
  m_implementation = new ShiftRegGeneral(size, offset, dre);
}

ShiftReg::~ShiftReg() {
  delete m_implementation;
}

/* ---------- Class ShiftRegGeneral ---------- */

ShiftRegGeneral::ShiftRegGeneral(unsigned int size, IStochDRE* dre)
  : m_size(size),
    m_zeroIndex(0),
    m_offset(0),
    m_dre(dre)
{
  m_bitarray = new char[size];
}

ShiftRegGeneral::ShiftRegGeneral(unsigned int size, unsigned int offset,
                                 IStochDRE* dre)
  : m_size(size),
    m_zeroIndex(0),
    m_offset(offset),
    m_dre(dre)
{
  m_bitarray = new char[size];
}

ShiftRegGeneral::~ShiftRegGeneral() {
  delete m_bitarray;
}

#ifdef _NOINLINE
void ShiftRegGeneral::push(char bit) {
#else
inline void ShiftRegGeneral::push(char bit) {
#endif

  if(m_zeroIndex==0) m_zeroIndex = m_size-1;
  else m_zeroIndex--;
  m_bitarray[m_zeroIndex] = bit;
}

char ShiftRegGeneral::getHD() {
  // count number of '1' bits
  int count = 0;
  for(uint i=0; i<m_size; i++) count+= m_bitarray[i];
  if(count > m_size/2) return 1;
  else if(count < m_size/2) return 0;
  else return -1;
}

/* ---------- Class ShiftReg2 ---------- */

ShiftReg2::ShiftReg2(IStochDRE* dre)
  : m_dre(dre)
{}
