//Author: Francois Leduc-Primeau

#ifndef _ShiftReg_h_
#define _ShiftReg_h_

#include "IEdgeMem.hpp"
#include "IStochDRE.hpp"
#include "GlobalDefs.h"
#include "util/Exception.hpp"

class IShiftReg : public IEdgeMem {
public:
  virtual void push(char) =0;
  virtual char get(uint) =0;
  virtual void reset() =0;
  virtual uint size() =0;
  virtual uint sum() =0;

protected:
  IShiftReg() {}
};

class ShiftReg : public IShiftReg {
public:
  ShiftReg(uint size, IStochDRE* dre);
  ShiftReg(uint size, uint offset, IStochDRE* dre);
  ~ShiftReg();

  void push(char val) { m_implementation->push(val); }
  char get(uint val) { return m_implementation->get(val); }
  char getrnd() { return m_implementation->getrnd(); }
  char getHD() { return m_implementation->getHD(); }
  IStochDRE* getDRE() { return m_implementation->getDRE(); }
  void reset() { m_implementation->reset(); }
  uint size() { return m_implementation->size(); }
  uint sum() { return m_implementation->sum(); }

private:
  IShiftReg* m_implementation;
};

class ShiftRegGeneral : public IShiftReg {
public:

  /**
   * Create a new shift register that can hold "size" bits.
   */
  ShiftRegGeneral(uint size, IStochDRE* dre);

  /**
   * Create a new shift register that can hold "size" bits, and
   * applies the specified offset to addresses.
   */
  ShiftRegGeneral(uint size, uint offset, IStochDRE* dre);

  ~ShiftRegGeneral();

  /**
   * Inserts a new bit at the beginning of the Shift Register, and
   * discards the oldest bit.
   */
  void push(char bit);

  /**
   * Returns the bit at the specified index. Note that if the index is
   * bigger than the register's size it will still be mapped on the
   * valid address space.
   */
  char get(uint index) {
    return m_bitarray[ (m_zeroIndex + index + m_offset) % m_size ];
  }

  char getrnd() { return get(m_dre->getVal()); }

  /// Implements IEdgeMem::getHD()
  char getHD();

  IStochDRE* getDRE() { return m_dre; }

  /**
   * Resets the register to zero.
   */
  void reset() { for(uint i=0; i<m_size; i++) m_bitarray[i]=0; }

  uint size() { return m_size; }

  uint sum() { 
    uint sum=0; 
    for(uint i=0; i<m_size; i++) sum+= m_bitarray[i];
    return sum;
  }

private:
  /// Number of bits in the Shift Register.
  uint m_size;

  char* m_bitarray;

  /// Index of bit 0.
  uint m_zeroIndex;

  /// Offset to be applied to addresses.
  uint m_offset;

  IStochDRE* m_dre;
};

/**
 * A size 2 shift register. Can be instantiated directly or through
 * the class ShiftReg(size) with size=2.
 */
class ShiftReg2 : public IShiftReg {
public:
  ShiftReg2(IStochDRE* dre);

  void push(char bit) {
    m_bittwo = m_bitone;
    m_bitone = bit;
  }

  char get(uint index) {
    if((index&1)==0) return m_bitone;
    else return m_bittwo;
  }

  char getrnd() { return get(m_dre->getVal()); }

  char getHD() { throw new Exception("ShiftReg2::getHD", "Unimplemented"); }

  IStochDRE* getDRE() { return m_dre; }

  void reset() { m_bitone = 0; m_bittwo = 0; }

  uint size() { return 2; }

  uint sum() { return m_bitone + m_bittwo; }

private:
  char m_bitone;
  char m_bittwo;

  IStochDRE* m_dre;
};

#endif
