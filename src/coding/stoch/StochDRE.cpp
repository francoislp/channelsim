//Author: Francois Leduc-Primeau

#include "StochDRE.hpp"
#ifndef NO_CONFIG
#include "Config.hpp"
#endif
#include "util/Exception.hpp"
#include <unistd.h>
#include <sys/time.h>
//#include <math.h>

// Initialize static data
StochDRE* StochDRE::s_staticOwner = 0;
gsl_rng* StochDRE::s_prndgen = 0;
unsigned long StochDRE::s_seed = 0;
//vector<StochDRE*> StochDRE::s_instanceList;

StochDRE::StochDRE() {
  m_meanCorr = 1;
  init();
}

StochDRE::StochDRE(float mean) {
  if(mean<0 || mean > 0.5) throw Exception("StochDRE::ctor", 
                                           "Invalid mean parameter");
  m_meanCorr = 2*mean;
  init();
}

// (private)
void StochDRE::init() {
  // Init the static RNG only once
  if( s_prndgen == 0 ) {
#ifndef NO_CONFIG
    // retrieve the seed from the config file
    Config* c = Config::getInstance();
    s_seed = c->getPropertyByName<unsigned long>("stoch_rng_seed");
#else
    s_seed = 1;
#endif

    // note: if exception is thrown above, s_prndgen will remain 0

    //gsl_rng_taus2: fastest "simulation quality" generator
    s_prndgen = gsl_rng_alloc(gsl_rng_taus2);
  }

  m_maxRN = gsl_rng_max(s_prndgen) + 1.0;

  m_dirty = true;
  nextTick(); // go to first clock tick (load a random number)

  // register instance
  //s_instanceList.push_back(this);
}

StochDRE::~StochDRE() {
  // don't do gsl_rng_free(s_prndgen) because might still be used by
  // other instances
  // we could keep track of the number of active instances, but we
  // don't really care, the program is ending anyways.
}

// (static, private)
void StochDRE::resetRNG_static(StochDRE* caller) {
  // Only the "owner" of the static data is allowed to perform the reset
  if(s_staticOwner==0) {
    s_staticOwner = caller;
  } else if(s_staticOwner != caller) {
    return;
  }
  // reset the global RNG
  if(s_prndgen!=0) gsl_rng_set(s_prndgen, s_seed);
}

