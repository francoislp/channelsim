//Author: Francois Leduc-Primeau

#ifndef _StochDRE_h_
#define _StochDRE_h_

#include "IStochDRE.hpp"
#include "GlobalDefs.h"
#include <gsl/gsl_rng.h> // GNU Scientific Library Random Number Generation
#include <gsl/gsl_randist.h>
#include <vector>

/**
 * Ideal random number generator for the Stochastic Decoder (which has
 * also grown uses beyond stochastic decoders). Allows control over
 * which object share random numbers (by supplying them the same
 * StochDRE object), while ensuring that all distinct objects have
 * independent random numbers.
 *
 * The floating-point RNs are single-precision.
 */
class StochDRE : public IStochDRE {
public:

  /**
   * Create a new DRE object that generates uniform probabilities when
   * getVal_f() is called.
   */
  StochDRE();

  /**
   * Create a new DRE object that generates uniform random numbers
   * over (0, 2*mean). The mean correction only applies to calls to
   * getVal_f().
   *@param mean  Desired mean value, which must be in [0, 1/2].
   *@throws Exception  If mean value is outside permitted range.
   */
  StochDRE(float mean);

  ~StochDRE();

  /// Returns the current random value.
  uint getVal() { 
    m_dirty = true;
    return m_curVal_i;
  }

  float getVal_f() { 
    m_dirty = true; 
    return static_cast<float>(m_curVal_i) / m_maxRN * m_meanCorr;
  }

  /** 
   * Returns a normal random number with mean zero and standard deviation
   * sigma. IMPORTANT: This function always gets a new random number
   * (independently of calls to nextTick()).
   *@param sigma Standard deviation of the Normal distribution (not the 
   *             variance!).
   */
  float getVal_norm(float sigma) {
    return static_cast<float>(gsl_ran_gaussian(s_prndgen, static_cast<double>(sigma)));
  }


  /**
   * Updates the current random value (as on a clock tick). Note that
   * this has no effect on the value returned by getVal_norm().
   */
  void nextTick() { 
    if(m_dirty) {
      m_dirty = false;
      m_curVal_i = gsl_rng_get(s_prndgen);
    }
  }

  /**
   * Resets the state of this DRE.
   *@see ISequential::reset()
   *@see resetRNG_static()
   */
  void reset() { 
    StochDRE::resetRNG_static(this); // ignored if "this" is not the owner
    m_dirty = true; 
    nextTick();
  }

protected:

  void setDirty(bool val) { m_dirty = val; }

private:

  // ----- Methods -----

  /// Initialization common to all constructors.
  void init();

  // ----- Data -----

  /** 
   * Scaling factor to generate uniform distributions over U(0,
   * m_meanCorr), which has a mean of m_meanCorr/2.
   */
  float m_meanCorr;

  /// Current output of the random number engine
  uint m_curVal_i;

  /// Whether the random number has been used.
  bool m_dirty;

  /// Maximum integer random number + 1
  float m_maxRN;

  // ---------- STATIC ----------

  /**
   * Resets the state of the static RNG used by all instances. The
   * first StochDRE object to call this function becomes the "owner"
   * of the static store, and the only one that is allowed to reset
   * the state.
   *@param caller Pointer to calling object used as a unique ID.
   */
  static void resetRNG_static(StochDRE* caller);

  // ----- Data -----

  static StochDRE* s_staticOwner;

  /**
   * The (static) random number generator that provides random values
   * to all DRE instances.
   */
  static gsl_rng* s_prndgen;

  static unsigned long s_seed;

  //static std::vector<StochDRE*> s_instanceList;

};

#endif
