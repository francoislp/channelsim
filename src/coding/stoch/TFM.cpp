//Author: Francois Leduc-Primeau

#include "TFM.hpp"
#include "util/Exception.hpp"
#include <cmath>

TFM::TFM(uint size, uint shiftAmnt, IStochDRE* dre)
  : m_reg(0),
    m_size(size),
    m_srlAmnt(shiftAmnt),
    m_dre(dre)
{
  if(size > 31) throw Exception("TFM::ctor", "size>31 not supported");
  if(m_srlAmnt > m_size) throw Exception("TFM::ctor",
                                             "right shift amout > size");

  m_sllAmnt = m_size - m_srlAmnt;
  m_maxVal = 1 << m_size;

#ifdef TFMSTATS
  m_saturationCount = 0;
  m_cycleCount = 0;
#endif
}

TFM::~TFM() {
#ifdef TFMSTATS
  cout << "TFM stat = ("<<m_saturationCount<<", "<<m_cycleCount<<")"<<endl;
#endif
}

void TFM::init(double prob) { 
  if(prob<0 || prob>1) throw Exception("TFM::init", "Invalid prob value");
  m_reg = lroundl(prob * (m_maxVal-1));
}

void TFM::push(char bit) {
  uint subVal = m_reg >> m_srlAmnt;
  uint addVal = bit << m_sllAmnt;
  m_reg = m_reg - subVal + addVal;
  // saturation
  if(m_reg < 0) m_reg = 0;
  else if(m_reg >= m_maxVal) m_reg = m_maxVal-1;

#ifdef TFMSTATS
  // statistics
  // note: clients call either push or get at each cycle (not both)
  m_cycleCount++;
  if(m_reg==0 || m_reg==(m_maxVal-1)) m_saturationCount++;
#endif
}

char TFM::getrnd() {
#ifdef TFMSTATS
  // statistics
  // note: clients call either push or get at each cycle (not both)
  m_cycleCount++;
  if(m_reg==0 || m_reg==(m_maxVal-1)) m_saturationCount++;
#endif

  uint compVal = m_dre->getVal() % m_maxVal;
  if( m_reg > compVal ) return 1;
  return 0;
}
