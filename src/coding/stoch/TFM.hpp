//Author: Francois Leduc-Primeau

#ifndef _TFM_h_
#define _TFM_h_

#include "ITFM.hpp"
#include "GlobalDefs.h"
#include "IStochDRE.hpp"

/**
 * A fixed-point TFM register.
 */
class TFM : public ITFM {
public:

  /**
   * Create a new TFM register.
   *@param size The number of bits in the register.
   *@param shiftAmnt The amount by which to right shift the values when
   *                 updating the register. For example if the weight of
   *                 a new bit is 1/16, the shift amount is 4.
   */
  TFM(uint size, uint shiftAmnt, IStochDRE* dre);

  ~TFM();

  /**
   * Inits the register by converting the floating-point probability
   * to a fixed precision value.
   */
  void init(double prob);

  void reset() { init(0.5); }

  /// Implements IEdgeMem::push()
  void push(char bit);

  /// Implements IEdgeMem::getrnd()
  char getrnd();

  /// Implements IEdgeMem::getHD()
  char getHD() {
    if(m_reg >= (m_maxVal/2)) return 1;
    else return 0;
  }

  IStochDRE* getDRE() { return m_dre; }

  /**
   * Returns the probability currently represented in this TFM.
   */
  double curVal() { 
    return static_cast<double>(m_reg) / static_cast<double>(m_maxVal-1); 
  }

private:

  /// The random number generator assigned to this TFM
  IStochDRE* m_dre;

  /// The register
  int m_reg;

  /// Number of bits in the register.
  uint m_size;

  /// The amount by which to right shift the previous reg content.
  uint m_srlAmnt;

  /// m_size - m_srlAmnt
  uint m_sllAmnt;

  /// 2 ^ m_size
  uint m_maxVal;

#ifdef TFMSTATS
  /// statistics: number of cycles where the TFM is saturated
  unsigned long long m_saturationCount;

  /// statistics: total number of cycles in the life of this TFM
  unsigned long long m_cycleCount;
#endif
};

#endif
