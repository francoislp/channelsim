//Author: Francois Leduc-Primeau

#include "TFMDP.hpp"
#include "util/Exception.hpp"

TFMDP::TFMDP(double beta, IStochDRE* dre) {
  m_beta = beta;
  m_betacomp = 1-beta;
  m_dre = dre;
  m_reg = 0;
}
