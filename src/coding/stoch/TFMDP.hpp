//Author: Francois Leduc-Primeau

#ifndef _TFMDP_h_
#define _TFMDP_h_

#include "IEdgeMem.hpp"
#include "IStochDRE.hpp"
#include "ITFM.hpp"
#include "util/Exception.hpp"

/**
 * Double-precision floating-point implementation of a TFM register.
 */
class TFMDP : public ITFM {
public:
  
  TFMDP(double beta, IStochDRE* dre);

  void init(double prob) { m_reg = prob; }

  void push(char bit) {
      m_reg = m_betacomp*m_reg + m_beta*static_cast<double>(bit); 
  }

  char getrnd() {
    double rndvalf = static_cast<double>(m_dre->getVal_f()); //TODO: should get a dbl rnd number
#ifdef TFM_NONUNIFORM_RV
//     rndvalf = rndvalf*0.8 + 0.1; // ~U[0.1,0.9]
    throw new Exception("TFMDP::getrnd", "-DTFM_NONUNIFORM_RV not supported");
#endif
    if( m_reg > rndvalf ) return 1;
    return 0;
  }

  IStochDRE* getDRE() { return m_dre; }

  void reset() { m_reg = 0.5; }

  /// Returns the current probability value of this TFMDP.
  double curVal() { return m_reg; }

private:
  
  IStochDRE* m_dre;

  /// The floating-point register
  double m_reg;

  double m_beta;

  /// 1-beta
  double m_betacomp;

};

#endif
