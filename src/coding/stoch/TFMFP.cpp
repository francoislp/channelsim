//Author: Francois Leduc-Primeau

#include "TFMFP.hpp"
#include "util/Exception.hpp"

TFMFP::TFMFP(float beta, IStochDRE* dre) {
  m_beta = beta;
  m_reg = 0;
  m_dre = dre;

  if(m_beta > 1 || m_beta < 0)
    throw Exception("TFMFP::ctor", "Invalid beta value");
}
