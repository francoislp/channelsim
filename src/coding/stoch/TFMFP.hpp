//Author: Francois Leduc-Primeau

#ifndef _TFMFP_h_
#define _TFMFP_h_

#include "ITFM.hpp"
#include "IStochDRE.hpp"
#include "util/Exception.hpp"

// Define TFM_NONUNIFORM_RV globally to use a different distribution
// than ~U[0,1] to generate stoch bits.

/**
 * Floating-point implementation of a TFM register.
 */
class TFMFP : public ITFM {
public:
  
  TFMFP(float beta, IStochDRE* dre);

  void setBeta(float beta) { m_beta = beta; }

  /// Implements ITFM::init(double).
  void init(double prob) {
    if(prob<0 || prob>1) 
      throw new Exception("TFMFP::init", "Invalid prob value");
    m_reg = static_cast<float>(prob);
  }

  void init(float prob) {
    if(prob<0 || prob>1) 
      throw new Exception("TFMFP::init", "Invalid prob value");
    m_reg = prob;
  }

  void push(char bit) {
    m_reg = m_reg + m_beta * (static_cast<float>(bit) - m_reg);
  }

  /**
   * Update the TFM with a new bit while specifying the beta that
   * should be used.
   */
  void push(char bit, float beta) {
    m_reg = m_reg + beta * (static_cast<float>(bit) - m_reg);
  }

  /**
   * Update the TFM with a soft value and the default beta.
   */
  void push(float update) {
    m_reg = m_reg + m_beta * (update - m_reg);
  }

  char getrnd() {
#if defined TFM_NONUNIFORM_RV
    float rndvalf = m_dre->getVal_norm(0.25);
    rndvalf+= 0.5;
    if(rndvalf<0.5) rndvalf = 0.5 - rndvalf;
    else rndvalf = 1.5 - rndvalf;
#elif defined TFM_RESTRICTED_RV
    float rndvalf = m_dre->getVal_f();
    rndvalf = rndvalf * 0.9 + 0.05;
#else
    float rndvalf = m_dre->getVal_f();
#endif
    if( m_reg > rndvalf ) return 1;
    return 0;
  }

  char getHD() {
    if(m_reg >= 0.5) return 1;
    else return 0;
  }

  IStochDRE* getDRE() { return m_dre; }

  void reset() { m_reg = 0.5; }

  /// Returns the current probability value of this TFMFP.
  double curVal() { return static_cast<double>(m_reg); }

private:
  
  /// The floating-point register
  float m_reg;

  float m_beta;

  IStochDRE* m_dre;

};

#endif
