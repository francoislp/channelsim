//Author: Francois Leduc-Primeau

#include "TFMFPBuffered.hpp"

TFMFPBuffered::TFMFPBuffered(float beta, int bufferSize, int updateShift,
                             IStochDRE* dre)
{
  m_p = 0.5;
  m_beta = beta;
  m_betacomp = 1-beta;
  m_bufferSize = static_cast<float>(bufferSize);
  m_curSum = 0;
  m_curCount = m_bufferSize - updateShift;
  m_dre = dre;
}

TFMFPBuffered::~TFMFPBuffered() {
}

void TFMFPBuffered::push(char bit) {
  m_curSum+= bit;
  m_curCount--;

  if(m_curCount == 0) {
    // update the probability
    m_p = m_betacomp * m_p + m_beta * (m_curSum/m_bufferSize);

    // reset counters
    m_curCount = m_bufferSize;
    m_curSum = 0;
  }
}
