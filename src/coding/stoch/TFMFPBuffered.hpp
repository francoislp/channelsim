//Author: Francois Leduc-Primeau

#ifndef _TFMFPBuffered_h_
#define _TFMFPBuffered_h_

#include "ITFM.hpp"
#include "TFMFP.hpp"

class TFMFPBuffered : public ITFM {
public:

  /**
   * Constructor
   *@param beta The beta of the exponential moving average function.
   *@param bufferSize How many incomming bits are buffered before updating 
   *       the TFM.
   *@param updateShift Allows shifting the TFM-update cycle, so that not 
   *       all objects in a group are updated at the same time.
   *@param dre DRE object that is used to sample random bits from the TFM.
   */
  TFMFPBuffered(float beta, int bufferSize, int updateShift, IStochDRE* dre);

  ~TFMFPBuffered();

  void setBeta(float value) { m_beta = value; m_betacomp = 1-value; }

  // IEdgeMem functions:

  void push(char bit);

  char getrnd() {
    float rndvalf = m_dre->getVal_f();
    if( m_p > rndvalf ) return 1;
    return 0;
  }

  char getHD() { 
    if(m_p >= 0.5) return 1;
    return 0;
  }

  IStochDRE* getDRE() { return m_dre; }

  // ITFM functions:

  void init(double prob) { m_p = static_cast<float>(prob); }
  void init(float prob) { m_p = prob; }

  void reset() { m_p = 0.5; }

  double curVal() { return m_p; }

private:

  float m_p; // probability value
  float m_beta;
  float m_betacomp;

  float m_bufferSize;

  float m_curSum;
  float m_curCount;

  IStochDRE* m_dre;

};

#endif
