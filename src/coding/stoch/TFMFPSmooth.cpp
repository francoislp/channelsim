//Author: Francois Leduc-Primeau

#include "TFMFPSmooth.hpp"
#include "ShiftReg.hpp"

TFMFPSmooth::TFMFPSmooth(float beta, int windowSize, IStochDRE* dre) {
  m_p = 0.5;
  m_betacomp = 1 - beta;
  m_beta = beta / windowSize;
  m_window = new ShiftReg(windowSize, NULL);
  m_windowSize = windowSize;
  m_dre = dre;

  m_window->reset();
}

TFMFPSmooth::~TFMFPSmooth() {
  delete m_window;
}

void TFMFPSmooth::push(char bit) {
  m_window->push(bit);
  // m_beta includes the averaging constant
  m_p = m_betacomp * m_p + m_beta * m_window->sum();
}
