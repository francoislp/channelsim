//Author: Francois Leduc-Primeau

#ifndef _TFMFPSmooth_h_
#define _TFMFPSmooth_h_

#include "GlobalDefs.h"
#include "ITFM.hpp"
#include "IStochDRE.hpp"
#include "ShiftReg.hpp"

/**
 * Exponential moving average that is updated with the value of a
 * sliding window linear average.
 */
class TFMFPSmooth : public ITFM {
public:

  TFMFPSmooth(float beta, int windowSize, IStochDRE* dre);

  ~TFMFPSmooth();

  void setBeta(float value) { m_beta = value; }

  // IEdgeMem functions:

  void push(char bit);

  char getrnd() {
    float rndvalf = m_dre->getVal_f();
    if( m_p > rndvalf ) return 1;
    return 0;
  }

  char getHD() { 
    if(m_p >= 0.5) return 1;
    return 0;
  }

  IStochDRE* getDRE() { return m_dre; }

  // ITFM functions:

  void init(double prob) { m_p = static_cast<float>(prob); }
  void init(float prob) { m_p = prob; }

  void reset() { m_p = 0.5; }

  double curVal() { return m_p; }

private:

  /// Current probability value
  float m_p;

  float m_beta; // actually: beta / windowSize
  float m_betacomp; // 1-beta

  float m_windowSize;

  ShiftReg* m_window;

  IStochDRE* m_dre;

};

#endif
