//Author: Francois Leduc-Primeau

#include "ThreshGen.hpp"
#include "Config.hpp"
#include "util/Exception.hpp"

ThreshGen::ThreshGen() {
  Config* conf = Config::getInstance();

  m_listSize = conf->getPropertyByName<uint>("rhs_threshold_list_size");
  if(m_listSize < 1) {
    throw Exception("ThreshGen::ctor", "Invalid threshold list size");
  }
  m_curIndex = 0;
  m_threshList = new float[4];
  m_threshList[0] = 0;
  m_threshList[1] = 2.2;
  m_threshList[2] = 0;
  m_threshList[3] = -2.2;
}

ThreshGen::~ThreshGen() {
  delete[] m_threshList;
}
