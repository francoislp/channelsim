//Author: Francois Leduc-Primeau

#ifndef ThreshGen_h_
#define ThreshGen_h_

#include "GlobalDefs.h"

/**
 * Generates LLR-domain compare thresholds used to generate 1-bit messages.
 */
class ThreshGen {
public:

  ThreshGen();

  ~ThreshGen();

  float getVal() { return m_threshList[m_curIndex]; }

  void next() { m_curIndex = (m_curIndex + 1) % m_listSize; }

private:

  // ----- Data Members -----

  float* m_threshList;

  uint m_listSize;

  uint m_curIndex;
};

#endif
