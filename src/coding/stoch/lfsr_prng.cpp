/* lfsr_prng.cpp - this file is part of $PROJECT_NAME_HERE$
 *
 * Copyright (C) 2011 Pascal Giard
 *
 * Author: Pascal Giard <evilynux@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef DEBUG
#include <stdio.h>
#endif

#include "lfsr_prng.h"

LfsrPrng::LfsrPrng(const uint8_t lfsr_type,
                   const uint8_t wordlength,
                   const uint8_t seed) {

  poly[0] = 0xC;  // "0001100"
  poly[1] = 0x1D; // "0011101",
  poly[2] = 0x36; // "0110110",
  poly[3] = 0x5C; // "1011100"

  type = lfsr_type;
  if(wordlength > 8) {
    wl = 8;
  }
  else if(wordlength > 7 or wordlength < 4) { 
#ifdef DEBUG
    printf("LfsrPrng: Invalid wordlength of %d.", wordlength);
#endif
    throw Exception("LFSR_PRNG", "Unsupported wordlength");
  } else {
    wl = wordlength;
  }
  reset(seed);
}

LfsrPrng::LfsrPrng(const uint8_t lfsr_type,
                   const uint8_t wordlength,
                   const uint16_t seed) {

  type = lfsr_type;
  if(wordlength > 8) {
    wl = wordlength;
  }
  else{ 
    throw Exception("LFSR_PRNG", "You should use the constructor with 8bit seed");
  }
  reset(seed);
}

void LfsrPrng::reset(const uint8_t seed) {
  cur_val = seed;
#ifdef DEBUG
      printf("LfsrPrng: seed %X\n", seed);
      printf("LfsrPrng: poly %X\n", poly[wl-4]);
#endif
}

void LfsrPrng::reset(const uint16_t seed) {
  cur_val_16 = seed;
#ifdef DEBUG
      printf("LfsrPrng: seed %X\n", seed);
#endif
}

uint8_t LfsrPrng::gen() {
  uint8_t feedback;
  static const uint8_t mask[4] = {0x7, 0xF, 0x1F, 0x3F};

  // Special support for > 8bit cases
  if( wl > 8 ) {
    throw Exception("LFSR_PRNG", "Call gen16(), seems you're expecting more than 8bits!");
  }

#ifdef DEBUG
  printf("LfsrPrng: cur_val %X\n", cur_val);
#endif
  switch(type)
    {
    case FIBO:
      // Same behavior as below, just closer in formulation to actual HW impl.
      if( wl == 7 ) {
        feedback = ((cur_val&1)^((cur_val>>2)&1)^((cur_val>>3)&1)^((cur_val>>4)&1));
        cur_val = ((cur_val>>1)&0x3F) | (feedback<<6);
      }
      else {
        feedback = (cur_val&1) & ((poly[wl-4]>>(wl-1))&1);
        for( uint8_t i = 1; i < wl; i++ ) {
          feedback = feedback ^ (((cur_val>>i)&1) & ((poly[wl-4]>>(wl-1-i))&1));
        }
#ifdef DEBUG
        printf("LfsrPrng: feedback %X\n", feedback);
#endif
        cur_val = (feedback<<(wl-1)) | ((cur_val>>1)&mask[wl-4]);
      }
      break;
    case GALOIS:
      if( (cur_val&1) != 0 ) {
        for( uint8_t i = 0; i < wl-1; i++ ) {
          cur_val &= ~(1<<i);
          cur_val |= (((cur_val>>(i+1))&1) ^ ((poly[wl-4]>>i)&1))<<i;
        }        
        cur_val &= ~(1<<(wl-1));
        cur_val |= 1<<(wl-1);
      } else {
        cur_val = (cur_val>>1) & mask[wl-4];
      }
      break;
    }
  return cur_val;
}

uint16_t LfsrPrng::gen16() {
  uint16_t feedback;
  if( wl != 9 and wl != 10) {
    throw Exception("LFSR_PRNG", "Only wordlengths of 9 or 10bits are supported.");
  }
#ifdef DEBUG
  printf("LfsrPrng: cur_val %X\n", cur_val);
#endif
  switch(type)
    {
    case FIBO:
      switch(wl) {
      case 10:
        // Polynome 0b1011010000
        feedback = ((cur_val_16&1)^((cur_val_16>>2)&1)^((cur_val_16>>3)&1)^((cur_val_16>>5)&1));
        cur_val_16 = ((cur_val_16>>1)&0x1FF) | (feedback<<9);
        break;
      default: // case 9:
        // Polynome 0b100110100
        feedback = ((cur_val_16&1)^((cur_val_16>>3)&1)^((cur_val_16>>4)&1)^((cur_val_16>>6)&1));
        cur_val_16 = ((cur_val_16>>1)&0xFF) | (feedback<<8);
        break;
      }
      break;
    default: // GALOIS
      throw Exception("LFSR_PRNG", "Galois not supported for 9bits wordlength");
      break;
    }

  return cur_val_16;
}
