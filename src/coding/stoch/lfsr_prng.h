/* lfsr_prng.h - this file is part of $PROJECT_NAME_HERE$
 *
 * Copyright (C) 2011 Pascal Giard
 *
 * Author: Pascal Giard <evilynux@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef LFSR_PRNG_H
#define LFSR_PRNG_H

#include <stdint.h>
#include "util/Exception.hpp"

class LfsrPrng {
 private:
  uint8_t poly[4];
  uint8_t type;
  uint8_t wl;
  uint8_t cur_val;
  uint16_t cur_val_16;
 public:
  enum LfsrType { FIBO, GALOIS };
  LfsrPrng(const uint8_t lfsr_type,
           const uint8_t wordlength,
           const uint8_t seed);
  LfsrPrng(const uint8_t lfsr_type,
           const uint8_t wordlength,
           const uint16_t seed);
  void reset(const uint8_t seed);
  void reset(const uint16_t seed);
  uint8_t gen();
  uint16_t gen16();
};

#endif /* LFSR_PRNG_H */
