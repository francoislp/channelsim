// Note: When adding a flag, the code of
// TaskList::writeHeader(ofstream&) needs to be updated so that the
// value of the flag is shown in the output file.

// ---------- Used by multiple files ----------

/* 
 * Define STOCH_USELLR to use LLRs computed from No to init the
 * stochastic VNs. In the case of the "all-stochastic" decoder, 
 * the scaling parameter in the config file can still be used,
 * even if it is not "NDS" anymore.
 * Used by: BlockDecoder.cpp, Node.cpp
 */
#define STOCH_USELLR


// ---------- Used by BlockDecoder.cpp ----------

/**
 * Number of cycles to hold the random values in the stoch decoder
 * (should normally be 1).
 */
#define RNDVAL_HOLD 1

// Define MULTIPLE_DRE to use multiple DREs per VN (i.e. never re-use a RN)
//#define MULTIPLE_DRE

// Activates new randomized algorithm which is a "generalization" of redecoding
//#define DECODER_RANDOMRESET

// Activates the collection of data for a BER vs iteration histogram
//#define DECODER_BERHIST

// Use a model of the HW scheme to decide when to stop iterating,
// instead of checking the decision outputs.
//#define DECODER_HWEARLYTERM

// Use a "layered" schedule *by column* for message-passing
//#define DECODER_COLLAYERSCHED
// Use a layered schedule by row
//#define DECODER_ROWLAYERSCHED

// Output a hard decision corresponding to one of the extrinsic
// messages, instead of the VN total. Used to compare the performance
// of the decoder with DE results.
//#define DECODER_EXTRINSIC_OUTPUT


// ---------- Used by Node.cpp ----------

// Send total LLR value on all outputs of the VN
//#define VNSUMOUTPUT

// All constants that apply to the stochastic decoders
#include "stochdecflags.h"

// compute a single minimum in Min-Sum SPA
//#define MINSUM_SINGLEMIN

// perform relaxation on the min in the check node
//#define MINSUM_CNRELAX

// use multiplicative scaling instead of additive offset in Min-Sum
//#define MINSUM_SCALEMIN

// perform relaxation on the LLR total in the VN
//#define VN_TOTALRELAX

// different relaxation function: beta is a function of the message
//#define ADAPTIVE_BETA

// activate TS breaking mechanism in SPA VN
//#define VN_HARMONIZE

// Special implementation of VN Harmonization for RHS HW implementation
// (VN_HARMONIZE must *also* be defined)
//#define VN_HARMONIZE_HW

// Use random thresholds in the DDBMP algorithm, instead of the hard-decision.
#define DDBMP_RNDTHRESHOLD

// Use TFM output trackers in the DDBMP decoder
#define DDBMP_TFMTRACKERS

// ---------- Used by BPSKDemod.cpp ----------

// Blind channel estimation: do not use No when computing the prior "LLR".
// Note: Config key "demod_llrscaling" becomes mandatory.
//#define DEMOD_BLINDESTIM

// ---------- Used by BlockCodeExperiment.cpp ----------

// Size limit for arrays (effectively for the nb of samples generated at once)
#define BLOCKSIZELIMIT 131072 // number of simulation bits

// Define USEIS to use importance sampling
//#define USEIS

// Ratio of the variances, simulation / nominal
#define ISRATIO 1.064

// ---------- Used by TFMFP ----------

// Define TFM_RESTRICTED_RV to use uniform random values distributed
// over a restricted range (for example [0.1, 0.9])
//#define TFM_RESTRICTED_RV

// ---------- Used by FeedbackTFMFP.cpp ----------

// Beta of the TFM that extracts the prob of the output stream
#define FDBCKTFM_BETAOUT 0.125

// This constant multiplies the noise bit that is added to the output
// TFM of a FeedbackTFMFP
#define FDBCKTFM_NOISEFACTOR 0.125

// ---------- Used by DecodingStats.{h,cpp} ----------

/*
 * The channel values and transmitted symbols for all successfully
 * decoded codewords are recorded in two specific files.
 */
#define LOG_ALL_CW

/*
 * Define to reverse the order in which symbols or channel values are
 * logged. Applies to all codeword logs that are written by
 * DecodingStats, but not to e.g. "decoder_infofile".
 */
#define LOG_CW_REVERSE

// ---------- Used by FileInputCoder.cpp ----------

// Define to reverse the bit ordering of the codewords read from the file.
//#define FILEINPUTCODER_REVERSE_BITORDER

// ---------- Used by FileChannel.cpp ----------

/// Character indicating comments in the channel file.
#define COMMENTCHAR '#'

// Define to reverse the ordering of the channel values in each
// codeword (the bit ordering).
//#define FILECHANNEL_REVERSE_BITORDER
