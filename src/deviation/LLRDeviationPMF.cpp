//Author: Francois Leduc-Primeau

#include "LLRDeviationPMF.hpp"
#include "DevPMFFileParser.hpp"
#include <stdexcept>
#include <math.h>
#include <boost/random/discrete_distribution.hpp>

//#define LLRDeviationPMF_CLOSESTMATCH

using std::string;
using std::vector;

LLRDeviationPMF::LLRDeviationPMF(config& conf) {
	m_pInKey= -1; // (negative values are invalid)
	init(conf);
}

void LLRDeviationPMF::init(config& conf) {
	m_nbErrPtsPerDec= conf.parseParamUInt("LLRDeviation::nbErrPtsPerDec");
	m_fractMult= conf.parseParamDouble("LLRDeviation::fract_mult");

	// checks
	if(m_nbErrPtsPerDec < 1) {
		throw std::invalid_argument("Invalid value for key \"LLRDeviation::nbErrPtsPerDec\"");
	}
	if(m_fractMult <= 0) {
		throw std::invalid_argument("Invalid value for key \"LLRDeviation::fract_mult\"");
	}

	// initialize the tables from the deviation file (requires the
	// values initialized above)
	setDeviationFile(conf.getParamString("LLRDeviation::dev_pmf_file"));

	// initialize the random number generator used to sample from the deviation PMF
	unsigned long seed = 0;
	try {
		seed= conf.parseParamUInt("LLRDeviation::seed");
	} catch(key_not_found&) {}
	m_pRng = new boost::mt19937(seed);
}

LLRDeviationPMF::~LLRDeviationPMF() {
	delete m_pRng;
}

void LLRDeviationPMF::setDeviationFile(string filepath) {
	DevPMFFileParser parser;
	parser.parseFile(filepath);
	vector<double>& pInList= parser.getPInList();
	if(pInList.size()==0) {
		throw std::runtime_error("empty file");
	}
#ifndef LLRDeviationPMF_CLOSESTMATCH
	if(pInList.size()<2) {
		throw std::runtime_error("two few p_in points to interpolate");
	}
#endif

	for(int i=0; i<pInList.size(); i++) {
		double key= errRateKey(pInList[i]);
		m_originalPInKeySet.insert( key );

		m_pmfMap[key]= parser.getPmfs(pInList[i]);
	}

	auto it= m_pmfMap.begin();
	m_Q= (it->second.size()-1)/2;
	// check that all PMFs have the same size
	for(it=it; it!=m_pmfMap.end(); it++) {
		if(it->second.size() != 2*m_Q+1) throw std::runtime_error("Malformed PMF data structures.");
		for(auto it2=it->second.begin(); it2!=it->second.end(); it2++) {
			if(it2->size() != 2*m_Q+1) throw std::runtime_error("Malformed PMF data structures.");
		}
	}
}

void LLRDeviationPMF::setInputErrorRate(double errRate) {
	if(errRate<0 || errRate>1) {
		throw std::invalid_argument("error rate must be in [0,1]");
	}

	if(errRate>0) {
		// turn error rate into a table-lookup key
		m_pInKey= errRateKey(errRate);
	} else { // errRate==0
		// since we do not extrapolate use the smallest error rate in the
		// characterized range
		setdblIt_t it= m_originalPInKeySet.end();
		m_pInKey= *(--it);
		return;
	}

	// find the closest existing key
	setdblIt_t it= m_originalPInKeySet.begin();
	double minDiff= fabs(m_pInKey - *it);
	double closestKey= *it;
	for(it= ++it; it!= m_originalPInKeySet.end(); it++) {
		double curDiff= fabs(m_pInKey - *it);
		if(curDiff < minDiff) {
			minDiff= curDiff;
			closestKey= *it;
		}
	}
		
#ifdef LLRDeviationPMF_CLOSESTMATCH
	m_pInKey= closestKey;
	// sanity check: make sure key is in the original set
	it= m_originalPInKeySet.find(m_pInKey);
	if(it==m_originalPInKeySet.end()) throw std::logic_error("bug!");
#else // interpolate
	// check if the PMFs have been generated for the new error rate
	if(m_pmfMap.find(m_pInKey) == m_pmfMap.end() ) { // not found
		// low and high x-axis values (error rate key) used for interpolation
		double keyL,keyH;
		
		setdblIt_t itOrig= m_originalPInKeySet.begin();
		setdblIt_t itOrigEnd= m_originalPInKeySet.end();
		if(itOrig==itOrigEnd) throw std::logic_error("bug!");

		if(*itOrig > m_pInKey) {
			// point requested is smaller than lower bound of characterized range
			// => extrapolate using the two smallest values from the original data
			// keyL= *itOrig;
			// itOrig++;
			// keyH= *itOrig;
			
			// We don't know how to extrapolate, simply return the closest key
			m_pInKey= closestKey;
			return;
		}
		else if(*(--itOrigEnd) < m_pInKey) {
			// point requested is larger than upper bound of characterized range
			// => extrapolate using the two largest values from the original data
			// keyH= *itOrigEnd;
			// itOrigEnd--;
			// keyL= *itOrigEnd;
			
			// We don't know how to extrapolate, simply return the closest key
			m_pInKey= closestKey;
			return;
		}
		else {
			// point requested is within the characterized range
			while(*itOrig < m_pInKey && itOrig!=m_originalPInKeySet.end()) {
				itOrig++;
			}
			if(itOrig==m_originalPInKeySet.begin()) throw std::logic_error("bug!");
			if(itOrig==m_originalPInKeySet.end()) throw std::logic_error("bug!");
			// now itOrig points at the first available point larger than
			// what we want
			keyH= *itOrig;
			itOrig--;
			keyL= *itOrig;
		}

		// setup the data structure for the new PMF matrix
		matrix_t matNew;
		matNew.resize(2*m_Q+1);
		for(int i=0; i<matNew.size(); i++) matNew[i].resize(2*m_Q+1);
		
		// compute the extrapolation / interpolation (linear with saturation)
		matrix_t& matL= m_pmfMap[keyL];
		matrix_t& matH= m_pmfMap[keyH];
		if(matL.size() != matH.size()) throw std::logic_error("bug!");
		if(matL.size() != matNew.size()) throw std::logic_error("bug!");
		// convert error keys to error probability
		double xH= errFromKey(keyH);
		double xL= errFromKey(keyL);
		double xNew= errFromKey(m_pInKey);

		for(int i=0; i<matL.size(); i++) {
			vector<double>& pmfL= matL[i];
			vector<double>& pmfH= matH[i];
			vector<double>& pmfNew= matNew[i];
			if(pmfL.size() != pmfH.size()) throw std::logic_error("bug!");
			if(pmfL.size() != pmfNew.size()) throw std::logic_error("bug!");
			for(int j=0; j<pmfL.size(); j++) {
				double yH= pmfH[j];
				double yL= pmfL[j];
				double yNew= (yH-yL)*(xNew-xL)/(xH-xL) + yL;
				if(yNew<0) pmfNew[j]= 0;
				else if(yNew>1) pmfNew[j]= 1;
				else pmfNew[j]= yNew;
			}
		}

		m_pmfMap[m_pInKey]= matNew;
	} //end: not found
#endif
}

double LLRDeviationPMF::applyDeviation(double msg, int txsign) {
	if(txsign != 1 && txsign != -1) throw std::runtime_error("invalid txsign");

	// retrieve the appropriate PMF
	vector<double> const& curPMF= getPmf(txsign*msg);

	// Note: The PMF returned by getPmf is conditional on tx_sign=1. The
	// PMF conditional on tx_sign=-1 is simply a flipped version of the
	// tx_sign=1 one.
	
	// convert the vector to a discrete-distribution object
	//TODO: There must be a way to use a single type for both iterator classes?
	boost::random::discrete_distribution<>* pDist;
	if(txsign==1) {
		pDist= new boost::random::discrete_distribution<>(curPMF.cbegin(),
		                                                 curPMF.cend());
	} else {
		pDist= new boost::random::discrete_distribution<>(curPMF.crbegin(),
		                                                 curPMF.crend());
	}

	// sample and convert to appropriate range
	int msgInt= (*pDist)(*m_pRng);
	delete pDist;
	msgInt-= m_Q; // convert to range [-Q, Q]
	double nu= static_cast<double>(msgInt) / m_fractMult;
	return nu;
}

double LLRDeviationPMF::applyDeviationSat(double msg, int txsign) {
	double maxMsg= m_Q / m_fractMult;
	double minMsg= -m_Q / m_fractMult;
	if(msg>maxMsg) msg= maxMsg;
	if(msg<minMsg) msg= minMsg;
	return applyDeviation(msg, txsign);
}

std::vector<double> const& LLRDeviationPMF::getPmf(double msg) {
	// convert LLR msg to integer domain
	int msgInt= lround(msg * m_fractMult);
	if(msgInt<-m_Q || msgInt>m_Q) throw std::invalid_argument("msg is outside range");

	//TODO: the lookup in m_pmfMap could be performed in setInputErrorRate(...)
	if(m_pmfMap.find(m_pInKey) == m_pmfMap.end()) throw std::runtime_error("Error rate key not found! (was probably not set)");
	return m_pmfMap[m_pInKey].at(msgInt+m_Q);
}

// (private)
double LLRDeviationPMF::errRateKey(double errRate) {
	/* Note: The std::map "begin" iterator returns elements in
	 * increasing key order.  by negating the exponent, the largest
	 * error rate will correspond to begin()
	 */
	/* Note: The key is rounded to avoid performing too many interpolations.
	 */
	return 0-round(log10(errRate)*m_nbErrPtsPerDec);
}

// (private)
// "Inverse" function of errRateKey
double LLRDeviationPMF::errFromKey(double errKey) {
	return pow(10, -errKey/m_nbErrPtsPerDec);
}
