//Author: Francois Leduc-Primeau

#ifndef _NOMPI
#include <mpi.h>
#include "MPITags.h"
#endif

#include "TaskList.hpp"
#include "util/Exception.hpp"
#include "Logging.hpp"
#include "source/ISource.hpp" // (for the constants)
#include "coding/DecoderTypes.h"
#include "COMMIT_ID.h"
#include "compileflags.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <utility> // pair
#include <sstream>
#include <string>
#include <time.h>

using std::cerr;
using std::endl;
using std::ofstream;
using std::vector;
using std::pair;
using std::ostringstream;
using std::string;

// ---------- class Task -------------------

u64_t Task::s_curID = 1;

Task::Task()
  : m_completed(false),
    m_bitErrorCnt(0),
    m_bitTrialCnt(0),
    m_wordTrialCnt(0),
    m_MLErrorCnt(0),
    m_itercnt(0),
    m_failedCnt(0),
    m_iterHist(0),
    m_ISErrorWeight(0),
    m_berrHist(0)
{
  m_myID = s_curID++;
}

Task::Task(TaskList* mylist, 
           double No, int minBErrors,
           int minWErrors, int minTrials, u64_t maxTrials) 
  : m_mylist(mylist),
    m_No(No),
    m_minBErrors(minBErrors),
    m_minWErrors(minWErrors),
    m_minTrials(minTrials),
    m_maxTrials(maxTrials),
    m_isStoch(false),
    m_completed(false),
    m_bitErrorCnt(0),
    m_bitTrialCnt(0),
    m_wordTrialCnt(0),
    m_MLErrorCnt(0),
    m_itercnt(0),
    m_failedCnt(0),
    m_iterHist(0),
    m_ISErrorWeight(0),
    m_berrHist(0)
{
  m_myID = s_curID++;
}

Task::Task(TaskList* mylist,
           double No, int minBErrors, int minWErrors,
           int minTrials, u64_t maxTrials,
           double ndsScaling)
  : m_mylist(mylist),
    m_No(No),
    m_minBErrors(minBErrors),
    m_minWErrors(minWErrors),
    m_minTrials(minTrials),
    m_maxTrials(maxTrials),
    m_isStoch(true),
    m_ndsScaling(ndsScaling),
    m_completed(false),
    m_bitErrorCnt(0),
    m_bitTrialCnt(0),
    m_wordTrialCnt(0),
    m_MLErrorCnt(0),
    m_itercnt(0),
    m_failedCnt(0),
    m_iterHist(0),
    m_ISErrorWeight(0),
    m_berrHist(0)
{
  m_myID = s_curID++;
}

Task::Task(Task* t) {
  m_myID = s_curID++;

  if(t->isStochastic()) {
    m_isStoch    = true;
    m_ndsScaling = t->NDSScaling();
  }
  else {
    m_isStoch    = false;
  }
  
  m_mylist       = t->m_mylist;
  m_No           = t->No();
  m_minBErrors   = t->minBErrors();
  m_minWErrors   = t->minWErrors();
  m_minTrials    = t->minTrials();
  m_maxTrials    = t->maxTrials();
  m_completed    = false;
  m_bitErrorCnt  = 0;
  m_bitTrialCnt  = 0;
  m_wordTrialCnt = 0;
  m_MLErrorCnt   = 0;
  m_itercnt      = 0;
  m_iterHist     = 0;
  m_ISErrorWeight = 0;
  m_berrHist     = 0;
  
}

Task::~Task() {
  if(m_iterHist != 0) delete[] m_iterHist;
  if(m_berrHist != 0) delete[] m_berrHist;
}

#ifndef _NOMPI
#define TASKBUFSIZE 6
#define TASKBUFDBLSIZE 2
void Task::dispatch(int machineIndex) {

  // send simulation parameters
  u64_t buf[TASKBUFSIZE];
  buf[0] = m_minWErrors;
  buf[1] = m_minBErrors;
  buf[2] = m_minTrials;
  buf[3] = m_maxTrials;
  if(m_isStoch) buf[4] = 1;
  else buf[4] = 0;
  buf[5] = m_myID;

  int code = MPI_Send(buf, TASKBUFSIZE, MPI_LONG_LONG_INT, machineIndex,
                      TAG_SIMPARAM, MPI_COMM_WORLD);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Send failed in TaskList.cpp:"<<(__LINE__ - 4);
    *pLog << endl;
    throw Exception("Task::dispatch", "MPI_Send failed");
  }

  // send noise power
  double bufdbl[TASKBUFDBLSIZE];
  bufdbl[0] = m_No;
  bufdbl[1] = m_ndsScaling;
  code = MPI_Send(bufdbl, TASKBUFDBLSIZE, MPI_DOUBLE, machineIndex,
                  TAG_SIMNOISE, MPI_COMM_WORLD);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Send failed in TaskList.cpp:"<<(__LINE__ - 4);
    *pLog << endl;
    throw Exception("Task::dispatch", "MPI_Send failed");
  }

}

void Task::receive(int senderID) {
  // Receive integers
  u64_t buf[TASKBUFSIZE];
  int code = MPI_Recv(buf, TASKBUFSIZE, MPI_LONG_LONG_INT, senderID,
                      TAG_SIMPARAM, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Recv failed in TaskList.cpp:"<<(__LINE__ - 3);
    *pLog << endl;
    throw Exception("Task::receive", "MPI_Recv failed");
  }

  m_minWErrors = static_cast<int>(buf[0]);
  m_minBErrors = static_cast<int>(buf[1]);
  m_minTrials  = static_cast<int>(buf[2]);
  m_maxTrials  = buf[3];
  if( buf[4]==1 ) m_isStoch = true;
  else m_isStoch = false;
  m_myID = buf[5];

  // Receive doubles
  double bufdbl[TASKBUFDBLSIZE];
  code = MPI_Recv(bufdbl, TASKBUFDBLSIZE, MPI_DOUBLE, senderID,
                  TAG_SIMNOISE, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Recv failed in TaskList.cpp:"<<(__LINE__ - 3);
    *pLog << endl;
    throw Exception("Task::receive", "MPI_Recv failed");
  }
  m_No = bufdbl[0];
  m_ndsScaling = bufdbl[1];
}

#define RESULTBUFSIZE 8
void Task::sendResult(int destID) {
  u64_t buf[RESULTBUFSIZE];
  buf[0] = m_bitErrorCnt;
  buf[1] = m_bitTrialCnt;
  buf[2] = m_wordErrorCnt;
  buf[3] = m_wordTrialCnt;
  buf[4] = m_MLErrorCnt;
  buf[5] = m_itercnt;
  buf[6] = m_failedCnt;
  buf[7] = m_myID; // Task ID for validation

  int code = MPI_Send(&buf, RESULTBUFSIZE, MPI_LONG_LONG_INT, destID,
                      TAG_SIMRESULT, MPI_COMM_WORLD);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Send failed in TaskList.cpp:"<<(__LINE__ - 4);
    *pLog << endl;
    throw Exception("Task::sendResult", "MPI_Send failed");
  }

  // send iteration histogram
  if(m_iterHist==0) {
    m_iterHist = new int[10];
    for(int i=0; i<10; i++) m_iterHist[i] = 0;
  }
  code = MPI_Send(m_iterHist, 10, MPI_INT, destID, TAG_SIMRESULT,
                  MPI_COMM_WORLD);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Send failed in TaskList.cpp:"<<(__LINE__ - 4);
    *pLog << endl;
    throw Exception("Task::sendResult", "MPI_Send failed");
  }
  // send bit error count histogram
  if(m_berrHist==0) {
    m_berrHist = new u64_t[21];
    for(uint i=0; i<21; i++) m_berrHist[i] = 0;
  }
  code = MPI_Send(m_berrHist, 21, MPI_UNSIGNED_LONG_LONG, destID, TAG_SIMRESULT,
                  MPI_COMM_WORLD);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Send failed in TaskList.cpp:"<<(__LINE__ - 4);
    *pLog << endl;
    throw Exception("Task::sendResult", "MPI_Send failed");
  }

  // send the doubles
  code = MPI_Send(&m_ISErrorWeight, 1, MPI_DOUBLE, destID, TAG_SIMRESULT,
                  MPI_COMM_WORLD);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Send failed in TaskList.cpp:"<<(__LINE__ - 4);
    *pLog << endl;
    throw Exception("Task::sendResult", "MPI_Send failed");
  }
}

void Task::receiveResult(int senderID) {
  u64_t buf[RESULTBUFSIZE];

  int code = MPI_Recv(&buf, RESULTBUFSIZE, MPI_LONG_LONG_INT, senderID,
                      TAG_SIMRESULT, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Recv failed in TaskList.cpp:"<<(__LINE__ - 4);
    *pLog << endl;
    throw Exception("Task::receiveResult", "MPI_Recv failed");
  }

  m_bitErrorCnt  = buf[0];
  m_bitTrialCnt  = buf[1];
  m_wordErrorCnt = buf[2];
  m_wordTrialCnt = buf[3];
  m_MLErrorCnt   = buf[4];
  m_itercnt      = buf[5];
  m_failedCnt    = buf[6];
  // check ID
  if( m_myID != buf[7] ) {
    ofstream* pLog = Logging::getstream();
    *pLog << "Task ID validation failed for task #"<<m_myID;
    *pLog << " (received "<<buf[6]<<")"<< endl;
    throw Exception("Task::receiveResult","ID check failed");
  }

  // receive iteration histogram
  if(m_iterHist != 0) delete[] m_iterHist;
  m_iterHist = new int[10];
  code = MPI_Recv(m_iterHist, 10, MPI_INT, senderID, TAG_SIMRESULT,
                  MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Recv failed in TaskList.cpp:"<<(__LINE__ - 4);
    *pLog << endl;
    throw Exception("Task", "MPI_Recv failed");
  }
  // receive bit error count histogram
  if(m_berrHist != 0) delete[] m_berrHist;
  m_berrHist = new u64_t[21];
  code = MPI_Recv(m_berrHist, 21, MPI_UNSIGNED_LONG_LONG, senderID, TAG_SIMRESULT,
                  MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Recv failed in TaskList.cpp:"<<(__LINE__ - 4);
    *pLog << endl;
    throw Exception("Task", "MPI_Recv failed");
  }

  // receive the doubles
  code = MPI_Recv(&m_ISErrorWeight, 1, MPI_DOUBLE, senderID, TAG_SIMRESULT,
                  MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  if(code != MPI_SUCCESS) {
    ofstream* pLog = Logging::getstream();
    *pLog << "MPI_Recv failed in TaskList.cpp:"<<(__LINE__ - 3);
    *pLog << endl;
    throw Exception("Task", "MPI_Recv failed");
  }

  m_completed = true;
}
#endif

void Task::setResult(u64_t biterrorcnt, u64_t bittrialcnt,
                     u64_t worderrorcnt, u64_t wordtrialcnt,
                     u64_t MLerrorcnt) {
  
  m_completed = true;
  m_bitErrorCnt = biterrorcnt;
  m_bitTrialCnt = bittrialcnt;
  m_wordErrorCnt = worderrorcnt;
  m_wordTrialCnt = wordtrialcnt;
  m_MLErrorCnt = MLerrorcnt;

  // make sure the histograms are set
  if(m_iterHist==0) {
	  m_iterHist = new int[10];
	  for(int i=0; i<10; i++) m_iterHist[i] = 0;
  }
  if(m_berrHist==0) {
	  m_berrHist = new u64_t[21];
    for(uint i=0; i<21; i++) m_berrHist[i] = 0;
  }
}

void Task::setIterStats(u64_t itercnt, int* iterHist, int failedCnt, u64_t* berrHist) {
    m_itercnt = itercnt;
    if(m_iterHist != 0) delete[] m_iterHist;
    m_iterHist = iterHist;
    m_failedCnt = failedCnt;
    if(m_berrHist != 0) delete[] m_berrHist;
    m_berrHist = berrHist;
}

string Task::toString() const {
  ostringstream ss;
  ss << "No="<< m_No << ", min w.err=" << m_minWErrors;
  ss << ", min trials=" << m_minTrials;
  if( isStochastic() ) {
    ss << ", scaling=" << m_ndsScaling;
  }

  return ss.str();
}

std::ostream& operator<<(std::ostream& stream, Task const & t) {
  return stream << t.toString();
}

// ---------- class GroupObjectiveStd ---------------

GroupObjectiveStd::GroupObjectiveStd(int berrcnt, int werrcnt, int minTrials)
  : m_minberrcnt(berrcnt),
    m_minwerrcnt(werrcnt),
    m_minTrials(minTrials)
{}

bool GroupObjectiveStd::evalObjective(vector<Task*> const& tasklist) {
  int berrcnt = 0;
  int werrcnt = 0;
  int trialcnt = 0;
  for(uint i=0; i<tasklist.size(); i++) {
    if( tasklist[i]->completed() ) {
      berrcnt+= tasklist[i]->errorCnt();
      werrcnt+= tasklist[i]->wordErrorCnt();
      trialcnt+= tasklist[i]->wordTrialCnt();
    }
  }

  // return whether or not the 3 criteria were satisfied
  return (berrcnt >= m_minberrcnt)
    && (werrcnt >= m_minwerrcnt)
    && (trialcnt >= m_minTrials);
}

// ---------- class TaskGroup ---------------

TaskGroup::TaskGroup(vector<Task*> const& tasklist) 
  : m_curIndex(0),
    m_pobj(0)
{
  if( tasklist.size() == 0 ) throw Exception("TaskGroup::ctor", "Cannot create empty static TaskGroup");

  // copy the list
  for(uint i=0; i<tasklist.size(); i++) m_tasklist.push_back(tasklist[i]);

  // take the first task in the list as the base task
  m_basetask = m_tasklist[0];

  m_static = true;
}

TaskGroup::TaskGroup(GroupObjective* obj, Task* baseTask) 
  : m_curIndex(0),
    m_pobj(obj),
    m_basetask(baseTask)
{
    m_static = false;
}

TaskGroup::~TaskGroup() {
    for(uint i=0; i<m_tasklist.size(); i++) {
	delete m_tasklist[i];
    }
    delete m_pobj;
}

bool TaskGroup::hasNext() {
    if(m_static) return (m_curIndex < m_tasklist.size());

    // else (dynamic): must run more Tasks if the objective is NOT satisfied
    return !m_pobj->evalObjective(m_tasklist);

}

bool TaskGroup::completed() {
  if( hasNext() ) return false;
  // check for Tasks that have been issued but are not completed
  bool completed = true;
  for(uint i=0; i < m_tasklist.size(); i++) {
    if( !m_tasklist[i]->completed() ) completed = false;
  }
  return completed;
}

Task* TaskGroup::next() {
    if(!hasNext()) throw OutOfBoundException();

    if(m_static) return m_tasklist[m_curIndex++];

    // -dynamic-
    // create a new Task by duplicating the base task
    Task* t = new Task( m_basetask );
    // add to the list
    m_tasklist.push_back(t);
    
    return t;
}

// note: param finalWrite currently unused
void TaskGroup::outputText(int formatCode, bool finalWrite,
                           pair<double, string> &returnVal) {
  u64_t  bitErrorCnt = 0;
  u64_t  wordErrorCnt = 0;
  u64_t  MLErrorCnt = 0;
  u64_t  bTrialCnt = 0;
  u64_t  iterCnt = 0;
  u64_t  wTrialCnt = 0;
  u64_t  iterHist[10];
  u64_t  bErrHist[21];
  int    failedCnt = 0;
  double ISErrorW = 0;

  for(uint j=0; j<m_tasklist.size(); j++) {
    if( m_tasklist[j]->completed() ) {
      bitErrorCnt+=  m_tasklist[j]->errorCnt();
      wordErrorCnt+= m_tasklist[j]->wordErrorCnt();
      MLErrorCnt+=   m_tasklist[j]->MLErrorCnt();
      bTrialCnt+=    m_tasklist[j]->trialCnt();
      iterCnt+=      m_tasklist[j]->iterCnt();
      wTrialCnt+=    m_tasklist[j]->wordTrialCnt();
      failedCnt+=    m_tasklist[j]->failedCnt();
      ISErrorW+=     m_tasklist[j]->ISErrorWeight();
    }
  }
  combineHistograms(iterHist, bErrHist);
    
  double No = m_basetask->No();
  double ebOverNo_dB = 10*log10(1/No);
  double errrate = static_cast<double>(bitErrorCnt) / 
    static_cast<double>(bTrialCnt);
  double wErrRate;
  if( ISErrorW > 0 ) { // a non-zero IS weight means IS is being used
    wErrRate = ISErrorW / static_cast<double>(wTrialCnt);
  } else {
    wErrRate = static_cast<double>(wordErrorCnt) / 
      static_cast<double>(wTrialCnt);
  }
  double avgiter = static_cast<double>(iterCnt) / 
    static_cast<double>(wTrialCnt);
    
  ostringstream ss;
  if(formatCode == OUTPUT_NDSSWEEP) {  
    ss << m_basetask->NDSScaling() << "   ";
  }
  ss << errrate << "   " << wErrRate << "   " << avgiter;
  ss << "   " << wTrialCnt << "   " << failedCnt;
  // number of undetected errors
  ss << " (" << (wordErrorCnt - failedCnt) << ")";
  // among those undetected errors, number of ML errors
  ss << "(" << MLErrorCnt << ")";
  // write the iteration histogram
  ss << "  ";
  for(uint j=0; j<10; j++) ss << " " << iterHist[j];
  if(!completed()) ss << " # (partial)";
  if(ISErrorW>0) ss << " # (IS)"; //TODO: temp, this info should be in
                                  //      the header

#ifdef DECODER_BERHIST
  // On a new commented line, write the BER array
  ss << endl;
  ss << "#BERLIST: ";
  for(uint j=0; j<21; j++) {
    double curBER =static_cast<double>(bErrHist[j]) / static_cast<double>(bTrialCnt);
    ss << " " << curBER;
  }
#endif

  // return the data
  returnVal.first = ebOverNo_dB;
  returnVal.second = ss.str();

}

// (assumes arrays are pre-allocated)
void TaskGroup::combineHistograms(u64_t* iterHistTotal, u64_t* berrHistTotal) {
  for(uint i=0; i<10; i++) iterHistTotal[i] = 0;
  for(uint i=0; i<21; i++) berrHistTotal[i] = 0;

  for(uint i=0; i<m_tasklist.size(); i++) {
    if( m_tasklist[i]->completed() ) { // this is required by iterHist()
      int* curHist = m_tasklist[i]->iterHist();
      for(uint j=0; j<10; j++) iterHistTotal[j]+= curHist[j];
#ifdef DECODER_BERHIST
      u64_t* curBerrHist = m_tasklist[i]->berrHist();
      for(uint j=0; j<21; j++) berrHistTotal[j]+= curBerrHist[j];
#endif
    }
  }
}

// ---------- class TaskList ---------------

TaskList::TaskList(ofstream& outstr, Config& conf)
  : m_curIndex(0),
    m_outstr(outstr),
    m_config(conf),
    m_outFormat(OUTPUT_NORMAL),
    m_headerText("")
{}

TaskList::~TaskList() {
  for(uint i=0; i<m_grouplist.size(); i++) {
    delete m_grouplist[i];
  }
}

void TaskList::addExperiment(double No, int minBErrors, int minWErrors,
                        int minTrials, u64_t maxTrials, int split) {
  if(split<1) throw Exception("TaskList::addExperiment",
                                  "\"split\" must be >=1");

  minBErrors = ceilf(static_cast<float>(minBErrors) / split);
  minWErrors = ceilf(static_cast<float>(minWErrors) / split);
  minTrials = ceilf(static_cast<float>(minTrials) / split);
  vector<Task*> groupTasks;
  for(int i=0; i<split; i++) {
    Task* ptask = new Task(this, No, minBErrors, minWErrors,
                           minTrials, maxTrials);
    groupTasks.push_back(ptask);
  }

  // create a new (static) group from the above task list
  TaskGroup* group = new TaskGroup( groupTasks );
  // add to group list
  m_grouplist.push_back(group);
}

// similar code as addExperiment(...)
void TaskList::addExperimentStoch(double No, int minBErrors, int minWErrors,
                             int minTrials, u64_t maxTrials, int split,
                             double ndsScaling) {
  if(split<1) throw Exception("TaskList::addExperiment",
                                  "\"split\" must be >=1");

  minBErrors = ceilf(static_cast<float>(minBErrors) / split);
  minWErrors = ceilf(static_cast<float>(minWErrors) / split);
  minTrials = ceilf(static_cast<float>(minTrials) / split);
  vector<Task*> groupTasks;
  for(int i=0; i<split; i++) {
    Task* ptask = new Task(this, No, minBErrors, minWErrors,
                           minTrials, maxTrials, ndsScaling);
    groupTasks.push_back(ptask);
  }

  // create a new (static) group from the above task list
  TaskGroup* group = new TaskGroup( groupTasks );
  // add to group list
  m_grouplist.push_back(group);
}

void TaskList::addExperiment_dyn(double No, int minBErrors, int minWErrors,
                                 int minTrials, int taskSize) {
  // experiment objective
  GroupObjectiveStd* obj = new GroupObjectiveStd(minBErrors, minWErrors,
                                                 minTrials);
  // base task (only specify a number of trials)
  Task* baseTask = new Task(this, No, 0, 0, taskSize, 0);
  // dynamic task group
  TaskGroup* group = new TaskGroup(obj, baseTask);
  // add to group list
  m_grouplist.push_back(group);
}

// similar code as addExperiment_dyn(...)
void TaskList::addExperimentStoch_dyn(double No, int minBErrors, int minWErrors,
                                      int minTrials, double ndsScaling,
                                      int taskSize) {
  // experiment objective
  GroupObjectiveStd* obj = new GroupObjectiveStd(minBErrors, minWErrors,
                                                 minTrials);
  // base task (only specify a number of trials)
  Task* baseTask = new Task(this, No, 0, 0, taskSize, 0, ndsScaling);
  // dynamic task group
  TaskGroup* group = new TaskGroup(obj, baseTask);
  // add to group list
  m_grouplist.push_back(group);
}

Task* TaskList::next() {
  if( !hasNext() ) throw OutOfBoundException();

  if( m_grouplist[m_curIndex]->hasNext() )
    return m_grouplist[m_curIndex]->next();
  // else:
  return m_grouplist[++m_curIndex]->next(); // (assume no empty groups)
}

bool TaskList::hasNext() {
  // (assume there are no empty groups)
  if( (m_curIndex+1) < m_grouplist.size() ) return true;
  // else:
  return (m_curIndex < m_grouplist.size())
    && (m_grouplist[m_curIndex]->hasNext());
}

// note: this method assumes that m_tasklist is sorted by group ID
void TaskList::writeResults(bool finalWrite) {
  if( !m_outstr.good() )
    throw Exception("TaskList::writeResults", "Invalid ofstream.");

  m_outstr.seekp(std::ios_base::beg);
  
  writeDynamicHeader(m_outstr); // prints the current time
  // the rest of the header is static
  if( m_headerText == "" ) m_headerText = getStaticHeader();
  m_outstr << m_headerText;

  // list used to sort the results (key: noise power, value: result text)
  vector< pair<double, string> > resultmap;

  // get the output from each TaskGroup
  for(uint i=0; i<m_grouplist.size(); i++) {
    pair<double, string> p; // output param
    m_grouplist[i]->outputText(m_outFormat, finalWrite, p);
    resultmap.push_back(p);
  }

  // if this is the final write, make sure all groups have finished
  if( finalWrite ) {
    int notcompleted = 0;
    for(uint i=0; i<m_grouplist.size(); i++) {
      if( !m_grouplist[i]->completed() ) notcompleted++;
    }
    if(notcompleted>0) {
      ofstream* pLog = Logging::getstream();
      *pLog << "***Warning: "<<notcompleted<<" groups have not completed"<<endl;
    }
  }

  //TODO: sort the results by increasing Eb/No

  // write the results to the output file
  for(uint i=0; i<resultmap.size(); i++) {
    m_outstr << resultmap[i].first << "    " << resultmap[i].second << endl;
  }
}


void TaskList::writeDynamicHeader(ofstream& ofs) {
  time_t rawtime;
  time( &rawtime );
  ofs << "# Created on            : " << ctime(&rawtime);
  // (it seems that ctime introduces a new line)
}

string TaskList::getStaticHeader() {

  ostringstream ss;

  ss << "# Matrix base filename  : " << m_config.getMatrixFileName() << endl;
  ss << "# Min word errors       : " << m_config.getMinWordErrors() << endl;
  ss << "# Min bit errors        : " << m_config.getMinBitErrors() << endl;
  ss << "# Min trials            : " << m_config.getMinTrials() << endl;

  ss << "# Source type           : ";
  int srcType = m_config.getSrcType();
  if( srcType == SRCTYPE_EXTENDED ) {
    ss << "Extended (real encoder)" << endl;
  } else if( srcType == SRCTYPE_ZERO ) {
    ss << "Zero source" << endl;
  } else {
    ss << "Unknown (# " << srcType << ")" << endl;
  }

  try {
	  int msgType = m_config.getMsgType();
	  ss << "# Decoder type          : ";
	  if( msgType == MSGTYPE_LLR ) {
		  ss << "LLR SPA" << endl;
	  } else if( msgType == MSGTYPE_LR ) {
		  ss << "LR SPA" << endl;
	  } else if( msgType == MSGTYPE_STOCH ) {
		  ss << "Stochastic" << endl;
	  }
	  else if( msgType == MSGTYPE_MINSUM ) {
		  ss << "Min-Sum SPA" << endl;
	  }
	  else if( msgType == MSGTYPE_PARALSTOCH ) {
		  ss << "Parallel Stochastic" << endl;
	  }
	  else if( msgType == MSGTYPE_HD ) {
		  ss << "Gallager-A/B" << endl;
	  }
	  else {
		  ss << "Unknown (# " << msgType << ")" << endl;
	  }
  } catch(...) {}
  // decoder subtype
  try {
    string val = m_config.getPropertyByName<string>("decoder_subtype");
    ss << "#  subtype              : " << val << endl;
  } 
  catch(InvalidKeyException&) {} 
  catch(...) {
    ss << "#  subtype              : ???" << endl;
  }

  ss << "# Error count type      : ";
  try {
	  ss << m_config.getPropertyByName<string>("error_type");
  } catch(...) {
	  ss << "(could not be retrieved!)";
  }
  ss << endl;

  try {
	  int maxIter= m_config.getMaxDecodeIter();
	  ss << "# Max decoder iterations: "<<maxIter << endl;
  } catch(...) {}

  try {
    float beta = m_config.getPropertyByName<float>("rhs_beta");
    ss << "# rhs_beta = " << beta << endl;
  } catch(...) {}
  try {
    string quantType = m_config.getPropertyByName<string>("demod_quant_type");
    uint quantResol = m_config.getPropertyByName<uint>("demod_resol");
    if(quantType == "BPSK" || quantType == "bpsk") {
      double quantRange = m_config.getPropertyByName<double>("demod_bpskrange");
      ss << "# Demod BPSK quant: (range="<<quantRange<<", resol="<<quantResol;
      ss << ")" << endl;
    }
    else if(quantType == "LLR" || quantType == "llr") {
      double quantRange = m_config.getPropertyByName<double>("demod_llrrange");
      ss << "# Demod LLR quant: (range="<<quantRange<<", resol="<<quantResol;
      ss << ")" << endl;
    }
  } catch(...) {}
  try {
    double llrLimit = m_config.getPropertyByName<double>("llr_limit");
    ss << "# LLR limit: " << llrLimit << endl;
  } catch(...) {}
  try {
    uint msg_bitwidth = m_config.getPropertyByName<uint>("stoch_parallel_width");
    ss << "# stoch_parallel_width = "<<msg_bitwidth<< endl;
  } catch(...) {}

  // "define" (compile-time) flags in use (from "compileflags.h")
  ss << "# Compile-time flags: ";
#ifdef DEMOD_BLINDESTIM
  ss << "DEMOD_BLINDESTIM, ";
#endif
#ifdef RNDVAL_HOLD
  ss << "RNDVAL_HOLD=" << RNDVAL_HOLD << ", ";
#endif
#ifdef LOG_ALL_CW
  ss << "LOG_ALL_CW, ";
#endif
#ifdef LOG_CW_REVERSE
  ss << "LOG_CW_REVERSE, ";
#endif
#ifdef MULTIPLE_DRE
  ss << "MULTIPLE_DRE, ";
#endif
#ifdef LEGACY_VN_OUTPUT
  ss << "LEGACY_VN_OUTPUT, ";
#endif
#ifdef MAJORITY_VN_OUTPUT
  ss << "MAJORITY_VN_OUTPUT, ";
#endif
#ifdef USE_TFMFP
  ss << "USE_TFMFP, ";
#endif
#ifdef STFM_CST
  ss << "STFM_CST=" << STFM_CST << ", ";
#endif
#ifdef HD_MAJVAL
  ss << "HD_MAJVAL=" << HD_MAJVAL << ", ";
#endif
#ifdef STOCHHD_MAJVAL
  ss << "STOCHHD_MAJVAL=" << STOCHHD_MAJVAL << ", ";
#endif
#ifdef RANDOMIZE_CONNECTIONS
  ss << "RANDOMIZE_CONNECTIONS, ";
#endif
#ifdef EXPERIMENTAL_STOCHVN
  ss << "EXPERIMENTAL_STOCHVN, ";
#endif
#ifdef INIT_OUTPUTCOUNT
  ss << "INIT_OUTPUTCOUNT, ";
#endif
#ifdef NO_IM_INIT
  ss << "***NO_IM_INIT***, ";
#endif
#ifdef BLOCKSIZELIMIT
  ss << "BLOCKSIZELIMIT=" << BLOCKSIZELIMIT << ", ";
#endif
#ifdef USEIS
  ss << "USEIS, ";
  ss << "ISRATIO=" << ISRATIO << ", ";
#endif
#ifdef TFM_NONUNIFORM_RV
  ss << "TFM_NONUNIFORM_RV, ";
#endif
#ifdef STOCH_USELLR
  ss << "STOCH_USELLR, ";
#endif
#ifdef USE_UNANIMOUS_BETA
  ss << "USE_UNANIMOUS_BETA, ";
#endif
#ifdef VNDEG7_NB2
  ss << "VNDEG7_NB2, ";
#endif
#ifdef TFM_RESTRICTED_RV
  ss << "TFM_RESTRICTED_RV, ";
#endif
#ifdef VNSUMOUTPUT
  ss << "VNSUMOUTPUT, ";
#endif
#ifdef VNSTOCHB_LLRTRACKER
  ss << "VNSTOCHB_LLRTRACKER, ";
#endif
#ifdef VNSTOCHB_LLRTRACKER_2
  ss << " VNSTOCHB_LLRTRACKER_2, ";
#endif
#ifdef VNSTOCHB_LLRTRACKER_FIXED
  ss << "VNSTOCHB_LLRTRACKER_FIXED, ";
#endif
#ifdef VNSTOCHB_LLRTRACKER_HW
  ss << "VNSTOCHB_LLRTRACKER_HW, ";
#endif
#ifdef VNSTOCHB_LLRTRACKER_HWOPTIM
  ss << "VNSTOCHB_LLRTRACKER_HWOPTIM, ";
#endif
#ifdef VNSTOCHB_LLRTRACKER_EXACTRANGE
  ss << "VNSTOCHB_LLRTRACKER_EXACTRANGE, ";
#endif
#ifdef VNSTOCHB_LLRTRACKER_ADAPTIVE
  ss << "VNSTOCHB_LLRTRACKER_ADAPTIVE, ";
#endif
#ifdef VNSTOCHB_LLRTRACKER_SIGNED
  ss << "VNSTOCHB_LLRTRACKER_SIGNED, ";
#endif
#ifdef VNSTOCHB_LLRRELAX
  ss << "VNSTOCHB_LLRRELAX, ";
#endif
#ifdef VNSTOCHB_REFTRACKER
  ss << "VNSTOCHB_REFTRACKER, ";
#endif
#ifdef VNSTOCHB_BUFFEREDTFM
  ss << "VNSTOCHB_BUFFEREDTFM, ";
#endif
#ifdef VNSTOCHB_QUANTIZELLR
  ss << "VNSTOCHB_QUANTIZELLR, ";
#ifdef VNSTOCHB_QUANTIZELLR_RND // (always used with VNSTOCHB_QUANTIZELLR)
  ss << "VNSTOCHB_QUANTIZELLR_RND, ";
  ss << "sf=" << VNSTOCHB_QUANTIZELLR_RNDSF << ", ";
#endif
#endif
#ifdef VNSTOCHB_QUANTIZELLR_INPUT
  ss << "VNSTOCHB_QUANTIZELLR_INPUT, ";
#endif
#ifdef VNSTOCHB_QUANTIZE_RNDLLR
  ss << "VNSTOCHB_QUANTIZE_RNDLLR, ";
#endif
#ifdef VNSTOCHB_BETAPROGRESSION
  ss << "VNSTOCHB_BETAPROGRESSION, ";
#endif
#ifdef VNSTOCHB_MSGCOMPENSATION
  ss << "VNSTOCHB_MSGCOMPENSATION, ";
#endif
#ifdef VNSTOCHB_SIGNEDMSG
  ss << "VNSTOCHB_SIGNEDMSG, ";
#endif
#ifdef RANDOMLLR_LFSR_STARTLLR
  ss << "RANDOMLLR_LFSR_STARTLLR=" << RANDOMLLR_LFSR_STARTLLR << ", ";
#endif
#ifdef DDBMP_RNDTHRESHOLD
  ss << "DDBMP_RNDTHRESHOLD, ";
#endif
#ifdef DDBMP_TFMTRACKERS
  ss << "DDBMP_TFMTRACKERS, ";
#endif
#ifdef DECODER_RANDOMRESET
  ss << "DECODER_RANDOMRESET, ";
#endif
#ifdef MINSUM_SINGLEMIN
  ss << "MINSUM_SINGLEMIN, ";
#endif
#ifdef ONE_RN_PER_CYCLE
  ss << "ONE_RN_PER_CYCLE, ";
#endif
#ifdef ADAPTIVE_BETA
  ss << "ADAPTIVE_BETA, ";
#endif
#ifdef MINSUM_CNRELAX
  ss << "MINSUM_CNRELAX, ";
#endif
#ifdef VN_TOTALRELAX
  ss << "VN_TOTALRELAX, ";
#endif
#ifdef MINSUM_SCALEMIN
  ss << "MINSUM_SCALEMIN, ";
#endif
#ifdef VN_HARMONIZE
  ss << "VN_HARMONIZE, ";
#endif
#ifdef VN_HARMONIZE_HW
  ss << "VN_HARMONIZE_HW, ";
#endif
#ifdef DECODER_BERHIST
  ss << "DECODER_BERHIST, ";
#endif
#ifdef DECODER_HWEARLYTERM
  ss << "DECODER_HWEARLYTERM, ";
#endif
#ifdef DECODER_COLLAYERSCHED
  ss << "DECODER_COLLAYERSCHED, ";
#endif
#ifdef DECODER_ROWLAYERSCHED
  ss << "DECODER_ROWLAYERSCHED, ";
#endif
#ifdef DECODER_EXTRINSIC_OUTPUT
  ss << "DECODER_EXTRINSIC_OUTPUT, ";
#endif
  ss << endl;
  // ID of last commit
  ss << "# Git SHA1: ";
#ifdef GIT_COMMIT_SHA1
  ss << GIT_COMMIT_SHA1;
#else
  ss << "???";
#endif
  ss << endl;

  ss << "#" << endl;

  // Column description
  if(m_outFormat == OUTPUT_NORMAL) {
    ss << "# Eb/No - BER - FER - avg iter - trials - known failed cw (+undetected)(ML) - iter histogram (10% bins)" << endl;
  } else {
    ss << "# Eb/No - NDS - BER - FER - avg iter - trials - known failed cw - iter histogram (10% bins)" << endl;
  }
  ss << "#------------------------------------------------" << endl;

  return ss.str();
}



