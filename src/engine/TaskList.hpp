//Author: Francois Leduc-Primeau

#ifndef _TaskList_h_
#define _TaskList_h_

#include "GlobalDefs.h"
#include "Config.hpp"
#include <vector>

class TaskList;

/**
 * Represents a part of an Experiment to be computed by a single CPU.
 */
class Task {
public:
  Task();

  /**
   * Instantiates a new Task for a discrete-message decoder.
   *@param maxTrials Use 0 to not specify a limit.
   */
  Task(TaskList* mylist,
       double No, int minBErrors, int minWErrors,
       int minTrials, u64_t maxTrials);

  /**
   * Instantiates a new Task for a stochastic decoder
   *@param maxTrials Use 0 to not specify a limit.
   */
  Task(TaskList* mylist,
       double No, int minBErrors, int minWErrors,
       int minTrials, u64_t maxTrials,
       double ndsScaling);

  /**
   * Copy constructor: creates a new Task that has the same parameters as
   * another one. Note that the result is not copied.
   */
  Task(Task* p);

  ~Task();

  u64_t ID() { return m_myID; }

#ifndef _NOMPI
  /**
   * Dispatch the task to a machine using MPI.
   */
  void dispatch(int machineIndex);

  /**
   * Receive a task using MPI.
   *@param senderID The ID of the MPI node that is sending the Task.
   *@throws Exception* If an MPI error occurs.
   */
  void receive(int senderID);

  /**
   * Send the result using MPI.
   *@param destID The MPI node that is receiving the result (typically node 0).
   */
  void sendResult(int destID);

  /**
   * Receive the result using MPI.
   *@param senderID The ID of the MPI node that is sending the result.
   *@throws Exception* If an MPI error occurs.
   */
  void receiveResult(int senderID);
#endif

  /**
   * Set the error rate results.
   */
  void setResult(u64_t biterrorcnt, u64_t bittrialcnt, 
                 u64_t worderrorcnt, u64_t wordtrialcnt,
                 u64_t MLerrorcnt);
  /**
   * Set decoder iteration results. Note that it is a bit more
   * efficient to call this method before setResults(...).
   *
   *@param itercnt The total number of iterations corresponding to the
   *               word trial count specified in setResult().
   *@param iterHist The 10 bins iteration histogram.
   *@param failedCnt
   *@param berrHist The 20+1 bins bit error count histogram.
   */
	void setIterStats(u64_t itercnt, int* iterHist, int failedCnt, u64_t* berrHist);

  void setISResult(double weight) { m_ISErrorWeight = weight; }


  bool completed() { return m_completed; }

  // Retrieving Task parameters:
  double No() { return m_No; }
  //bool   useBER() { return m_useBER; }
  int    minBErrors() { return m_minBErrors; }
  int    minWErrors() { return m_minWErrors; }
  int    minTrials()  { return m_minTrials; }

  /**
   * Maximum number of trials before failing, with zero meaning no limit.
   */
  u64_t maxTrials() { return m_maxTrials; }

  // Optional parameters

  /**
   * Whether this Task is for the stochastic decoder. This determines
   * whether NDSScaling() can be called.
   */
  bool   isStochastic() const { return m_isStoch; }

  double NDSScaling() { return m_ndsScaling; }

  // Get Task results
  /// The number of bit errors
  u64_t errorCnt() { return m_bitErrorCnt; }
  /// The number of "bit trials".
  u64_t trialCnt() { return m_bitTrialCnt; }
  /// The number of frame errors.
  u64_t wordErrorCnt() { return m_wordErrorCnt; }
  /// The number of word trials.
  u64_t wordTrialCnt() { return m_wordTrialCnt; }
  /// The number of ML decoding errors
  u64_t MLErrorCnt() { return m_MLErrorCnt; }
  /// Total number of iterations.
  u64_t iterCnt() { return m_itercnt; }
  /// Number of failed codewords
  int       failedCnt() { return m_failedCnt; }
  /// Iteration histogram (*only valid if completed() is true*)
  int*      iterHist() { return m_iterHist; }
  /// Importance Sampling error weight
  double    ISErrorWeight() { return m_ISErrorWeight; }
  /// Bit error count histogram
  u64_t*    berrHist() { return m_berrHist; }

  /**
   * A string representation of this Task.
   */
  std::string toString() const;

private:
  /// Used to assign unique IDs to each object
  static u64_t s_curID;

  /// Unique identifier for this Task.
  u64_t m_myID;

  TaskList* m_mylist;

  // Simulation parameters
  double    m_No;
  //bool      m_useBER;
  int       m_minBErrors;
  int       m_minWErrors;
  int       m_minTrials;
  u64_t     m_maxTrials;

  // Optional parameters
  bool   m_isStoch; // Whether this is a stochastic decoder task
  double m_ndsScaling;

  // Results
  bool      m_completed;
  u64_t     m_bitErrorCnt;
  u64_t     m_bitTrialCnt;
  u64_t     m_wordErrorCnt;
  u64_t     m_wordTrialCnt;
  u64_t     m_MLErrorCnt;
  u64_t     m_itercnt;
  int       m_failedCnt;
  int*      m_iterHist;
  double    m_ISErrorWeight;
  u64_t*    m_berrHist;

}; // end class Task

/**
 * Outputs a string representation of a Task to an output stream.
 */
std::ostream& operator<<(std::ostream&, Task const &);

/**
 * Classes that implement this interface are used to define an
 * objective for a group of Tasks.
 */
class GroupObjective {
public:
  virtual ~GroupObjective() {}

  virtual bool evalObjective(std::vector<Task*> const& tasklist) =0;
};

/**
 * Experiment objective that is defined in terms of all the following: a
 * minimum bit error count, a minimum frame error count and a minimum number
 * of trials.
 */
class GroupObjectiveStd : public GroupObjective {
public:
    GroupObjectiveStd(int berrcnt, int werrcnt, int minTrials);
    
    bool evalObjective(std::vector<Task*> const& tasklist);

private:
  int m_minberrcnt;
  int m_minwerrcnt;
  int m_minTrials;
};

#define OUTPUT_NORMAL 1
#define OUTPUT_NDSSWEEP 2

/**
 * Hold Tasks that belong to the same Experiment (the same SNR point), and
 * takes care of merging tasks together. 
 */
class TaskGroup {
public:
  
  /**
   * Creates a static TaskGroup from the specified list.
   *@param tasklist Task pointers are copied from that list.
   *@throws Exception If tasklist is empty.
   */
  TaskGroup(std::vector<Task*> const& tasklist);

  /**
   * Creates a dynamic TaskGroup. New Tasks are created by duplicating
   * the base Task.
   *@param obj      Object used to evaluate whether the objective has been
   *                reached by the completed Tasks.
   *@param baseTask The base Task that gets duplicated.
   */
  TaskGroup(GroupObjective* obj, Task* baseTask);
  
  ~TaskGroup();

  /**
   * Retrieve the next Task in the group.
   *@throws OutOfBoundException If hasNext() returns false.
   */
  Task* next();

  /**
   * Whether this Group still contains Task to be executed.
   */
  bool hasNext();

  /**
   * Whether all Tasks in the Group have completed, and, in the case
   * of a dynamic group, the objective has been reached.
   */
  bool completed();

  /**
   * Returns one line of text that represents the experiment's result. This
   * method can be called at any time and will output the partial results if
   * the experiment's objective is not yet completed.
   *@param formatCode A code that describes the output text format (one of
   *                  OUTPUT_*).
   *@param finalWrite Whether this is the final output.
   *@param returnVal A pair object that is used to return the Eb/No ratio 
   *                 and the corresponding text line.
   */
  void outputText(int formatCode, bool finalWrite,
                  std::pair<double, std::string> &returnVal);

private:

  void combineHistograms(u64_t*, u64_t*);
  
  // ----- Data Members -----
  
  /// Whether the list is static (true) or dynamic (false).
  bool m_static;
  
  std::vector<Task*> m_tasklist;
  
  /// Current index for reading from m_tasklist (static list).
  uint m_curIndex;
  
  /// For dynamic lists, a description of the objective.
  GroupObjective* m_pobj;
  
  /** 
   * For dynamic lists, the base task. This object is also used for
   * static lists to provide general information on the TaskGroup (=>
   * must always be initialized).
   */
  Task* m_basetask;

}; // end class TaskGroup


/**
 * This class manages a list of Tasks to be executed, and
 * writes the results to an output file.
 */
class TaskList {
public:
  /**
   * Constructor.
   *@param outstr The output stream where results will be written. The 
   *              stream must be opened by the caller.
   *@param conf   Configuration object
   */
  TaskList(std::ofstream& outstr, Config& conf);

  ~TaskList();

  void setOutputFormat(int cst) { m_outFormat = cst; }

  /**
   * Create tasks (possibly many) that will perform an experiment.
   *@param split How many tasks to split the experiment into.
   */
  void addExperiment(double No, int minBErrors, int minWErrors,
                     int minTrials, u64_t maxTrials, int split);

  /**
   * Same as addExperiment() for stochastic decoding.
   */
  void addExperimentStoch(double No, int minBErrors, int minWErrors,
                          int minTrials, u64_t maxTrials, int split,
                          double ndsScaling);

  /**
   * Create a new experiment that will use dynamic Tasks.
   *@param taskSize The number of frame trials per Task.
   */
  void addExperiment_dyn(double No, int minBErrors, int minWErrors,
                         int minTrials, int taskSize);

  /**
   * Same as addExperiment_dyn() for stochastic decoding.
   */
  void addExperimentStoch_dyn(double No, int minBErrors, int minWErrors,
                              int minTrials, double ndsScaling,
                              int taskSize);

  /**
   * Retrieves the next Task in the list.
   *@throws OutOfBoundException If hasNext() returns false.
   */
  Task* next();

  /// Whether there are still Tasks to be fetched from the list.
  bool hasNext();

  /**
   * Write the results related to all the tasks in the list that have
   * been executed.
   */
  void writeResults() {
    writeResults(true);
  }

  /**
   * Write the result file.
   *@param finalWrite True if this is the last time we update the result file.
   */
  void writeResults(bool finalWrite);

private:

  void writeDynamicHeader(std::ofstream& ofs);
  std::string getStaticHeader();

  // ----- Data Members -----

  /// Current index for reading from the TaskGroup list.
  int m_curIndex;

  std::vector<TaskGroup*> m_grouplist;

  /// File stream where results are written
  std::ofstream& m_outstr;

  /// Provides info on parameters
  Config& m_config;

  /// Format of the output file (one of the OUTPUT_* constants).
  int m_outFormat;

  /// Cached header text for the result file
  std::string m_headerText;

};

#endif
