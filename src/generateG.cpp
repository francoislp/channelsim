/**
 * Author: Francois Leduc-Primeau 
 * This is a program that generates a G matrix from a parity-check
 * matrix ("H") specified in a-list format. The G matrix is written to
 * a file in a dense binary format (see "doc/plan.rtf"), or in text
 * format if the "-f2" option is specified.
 */

#include "util/BitBlock.hpp"
#include "parsers/AListParser.hpp"
#include "parsers/AListElem.hpp"
#include "parsers/MaskParser.hpp"
#include "util/Exception.hpp"
#include "config/config.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <boost/ptr_container/ptr_list.hpp>

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::vector;

typedef boost::ptr_list<BitBlock>::iterator BitBlock_iterator;

void setAuthorizedKeys(config& conf) {
	// key-value pairs
	conf.addValidKey("H");
	conf.addValidKey("G");
	conf.addValidKey("format");
	conf.addValidKey("punct_file");
}

// Print usage info and exit.
void usageExit(int exitCode) {
	cout << "Usage: generateG H=<H_file> G=<output_file> [format={1|2}] [punct_file=<file>]" << endl;
	exit(exitCode);
}

int main(int argc, char** argv) {

  if(argc==1) {
	  usageExit(0);
  }

  config cliConf;
  setAuthorizedKeys(cliConf);

  try {
	  cliConf.initCL(argc, argv);
  } catch(syntax_exception& e) {
    cerr << "Invalid syntax in cmd-line arguments: "<<e.what()<<endl;
    usageExit(1);
  } catch(invalidkey_exception& e) {
    cerr << "Invalid key or option: "<<e.what()<<endl;
    usageExit(1);
  }

  int formatCode= 1;
  try {
	  formatCode= cliConf.parseParamUInt("format");
  } catch(key_not_found&) {}
  if(formatCode<1 || formatCode>2) {
	  cerr << "Invalid format code (must be 1 or 2)" << endl;
	  return 1;
  }

  // matrix file names
  string filenameH= cliConf.getParamString("H");
  string filenameG= cliConf.getParamString("G");

  // load the puncture file if one is specified
  MaskParser punctMask;
  try {
	  punctMask.parseFile(cliConf.getParamString("punct_file"));
  }
  catch(key_not_found&) {}
  catch(Exception& e) {
    cerr << e << endl;
    return 1;
  }

  // Load the H matrix
  cout << "Reading H matrix (A-List) from " << filenameH << endl;
  AListParser parserH;
  try {
    parserH.ParseFile(filenameH);
  } catch(Exception& e) {
    cerr << e << endl;
    return 1;
  }
  vector<AListElem> hElemList;
  parserH.GetElements(hElemList);
  int Hm = parserH.GetRowCount();
  int Hn = parserH.GetColCount();

  // create an array of (empty) BitBlocks to store the matrix
  BitBlock** H_tmp = new BitBlock*[Hm];
  for(int i=0; i<Hm; i++) H_tmp[i] = new BitBlock(Hn);
  // set some elements to 1
  for(uint i=0; i<hElemList.size(); i++) {
    int row = hElemList[i].GetRow();
    int col = hElemList[i].GetCol();
    H_tmp[row]->setbit(col, 1);
  }

  // transfer the pointers to a boost::ptr_list
  boost::ptr_list<BitBlock> H;
  for(int i=0; i<Hm; i++) H.push_back( H_tmp[i] );

  // put H in RREF
  cout << "Finding the RREF of H...";
  cout.flush();
  H.sort( std::greater<BitBlock>() ); // sort in decreasing order
  for(BitBlock_iterator it_i = H.begin(); it_i!=H.end(); it_i++) {
    int msb = it_i->getMsbIndex(); // pivot column
    if( msb>=0 ) {
      // zero the pivot column for all other rows
      for(BitBlock_iterator it_j = H.begin(); it_j!=H.end(); it_j++) {
        if(it_j != it_i && it_j->getbit(msb)==1) { 
          (*it_j)+= (*it_i);
        }
      }
    }
    // re-sort before the iterator is incremented
    H.sort( std::greater<BitBlock>() );
  }
  cout << "   Done!" << endl;

  // debug: check that H is in RREF (partial check)
  int zeroRowCount = 0;
  for(BitBlock_iterator it_i = H.begin(); it_i!=H.end(); it_i++) {
    int msbIndex = it_i->getMsbIndex();
    if(msbIndex < 0) {
      zeroRowCount++;
    }
    else {
      BitBlock_iterator it_j = it_i;
      it_j++;
      for(; it_j!=H.end(); it_j++) {
        if( it_j->getbit(msbIndex) != 0 ) {
          cout << "Error !" << endl;
          return 1;
        }
      }
    }

    //    it_i->printbits(); // print the matrix
  }
  cout << "Number of zero rows: " << zeroRowCount << endl;
  // end debug------

  // identify pivot columns and free columns of H
  vector<int> freecols;
  // (the "stupid" way)
  for(int i=0; i<Hn; i++) {
    int sum = 0;
    for(BitBlock_iterator it_i = H.begin(); it_i!=H.end(); it_i++) {
      char bit = it_i->getbit(i);
      sum+= bit;
      if( sum>1 ) {
        freecols.push_back(i);
        break;
      }
    }
  }
  //TODO: much better way: we can build a list of pivot columns while
  //computing the RREF, the free cols are all the other columns.

  // debug
  cout << "H has " << freecols.size() << " free columns" << endl;

  // allocate G
  int k = freecols.size();
  // store G by column, row
  vector<vector<char>> G;
  G.resize(Hn);

  // build rows of free elements
  for(BitBlock_iterator it_i = H.begin(); it_i!=H.end(); it_i++) {
    int msbIndex = it_i->getMsbIndex();
    if(msbIndex >= 0) { // row is non-zero
	    G.at(msbIndex).resize(k);
      for(int j=0; j<k; j++) {
        G[msbIndex][j] = it_i->getbit(freecols[j]);
      }
    }
  }

  for(int i=0; i < freecols.size(); i++) {
    int colindex = freecols[i];
    G.at(colindex).resize(k);
    // zero the whole column
    for(int l=0; l<k; l++) G[colindex][l] = 0;
    // set one element to 1
    G[colindex][i] = 1;
  }

  cout << "Done!" << endl;

  // remove columns of G corresponding to punctured bits
  vector<bool> const& punctPattern= punctMask.getMask();
  if(punctPattern.size()>0) {
	  if(punctPattern.size() != Hn) {
		  cerr<< "Length of puncture pattern ("<<punctPattern.size()<<")";
		  cerr<< " does not match Hn="<<Hn << endl;
		  return 1;
	  }
	  
	  int j=0;
	  //Note: Puncturing pattern must be read in reversed order.
	  auto patIt= punctPattern.crbegin();
	  for(int i=0; i<Hn && j<Hn; i++) {
		  if(patIt==punctPattern.crend()) {
			  cerr<< "Ay caramba!" << endl;
			  return 1;
		  }
		  while(*patIt) {
			  j++; patIt++;
		  }
		  if(j<Hn && j>i) G.at(i)= G.at(j);
		  j++; patIt++;
	  }
	  G.resize(punctMask.getWeightComp());
  }
  cout << "Column-size of G after puncturing is "<<G.size() << endl;

  // debug: compute G * H'
  // pack G as BitBlocks
//   boost::ptr_list<BitBlock> G_bb;
//   char* row = new char[Hn];
//   for(int i=0; i<k; i++) {
//     for(int j=0; j<Hn; j++) {
//       row[j] = G[j][i];
//     }
//     G_bb.push_back(new BitBlock(Hn, row));
//   }
//   delete[] row;
//   // check each element of the matrix product
//   for(BitBlock_iterator it_i = G_bb.begin(); it_i!=G_bb.end(); it_i++) {
//     for(BitBlock_iterator it_j = H.begin(); it_j!=H.end(); it_j++) {
//       char bit = it_i->dotprod(*it_j);
//       if(bit != 0) {
//         cout << "Non-zero bit !" << endl;
//         return 1;
//       }
//     }
//   }
  // end debug

  // (debug) check if G has zero columns
  for(int i=0; i<G.size(); i++) {
    int sum=0;
    for(int j=0; j<k; j++) sum+= G.at(i).at(j);
    if(sum==0) cout << "Zero column at index "<< i << endl;
  }

  // write the matrix to the output file
  std::ofstream ofs(filenameG.c_str());
  if( !ofs.good() ) {
    cout << "Cannot open output file" << endl;
    return 1;
  }

  if( formatCode == 1 ) { // binary output
    // write format code
    int format = 1;
    ofs.write(reinterpret_cast<char*>(&format), 4);
    
    // write number of rows
    ofs.write(reinterpret_cast<char*>(&k), 4);
    // write number of columns
    int nbCol= G.size();
    ofs.write(reinterpret_cast<char*>(&nbCol), 4);

    // write matrix data by column
    for(int i=0; i<G.size(); i++) {
	    char* curCol= G.at(i).data();
      ofs.write(curCol, k);
    }
  }
  else if( formatCode == 2 ) { // text output
	  for(int i=0; i<G.size(); i++) {
      for(int j=0; j<k; j++) {
	      if( G[i].at(j) == 0 ) ofs << "0 ";
        else ofs << "1 ";
      }
      ofs << endl;
    }
  }

  ofs.close();
}
