//Author: Francois Leduc-Primeau

#include "compileflags.h"
#include "BPSKDemod.hpp"
#include "util/Exception.hpp"
#include "Config.hpp"

#include <cmath>
#include <string>

using std::string;
using std::vector;

BPSKDemod::BPSKDemod(BPSKMod* mod, IChannel* channel, double No) 
  : m_pchannel(channel),
    m_No(No)
{
  m_amp = mod->getAmp();
  init();
}

BPSKDemod::BPSKDemod(IChannel* channel, double No)
  : m_pchannel(channel),
    m_No(No)
{
  m_amp = 1;
  init();
}

// (private)
void BPSKDemod::init() {
  Config* conf = Config::getInstance();
  m_doQuantizeBPSK = false;
  m_doQuantizeLLR = false;
  m_quantWithZero = false;
  string quantType = "";
  try {
    quantType = conf->getPropertyByName<string>("demod_quant_type");
  } catch(InvalidKeyException&) {}
  if(quantType != "") {
    if( quantType == "BPSK" || quantType == "bpsk" ) {
      m_quantBpskRange = conf->getPropertyByName<double>("demod_bpskrange");
      m_doQuantizeBPSK = true;
    }
    else if( quantType == "LLR" || quantType == "llr" ) {
      m_quantLLRRange = conf->getPropertyByName<double>("demod_llrrange");
      m_doQuantizeLLR = true;
    }
    else {
      throw Exception("BPSKDemod", "Invalid value for key \"demod_quant_type\"");
    }
    m_quantResol = conf->getPropertyByName<uint>("demod_resol");
    m_quantWithZero = conf->getPropertyByName<bool>("demod_zeroval");

    // if we include a zero value, make sure that the number of
    // quantization levels is odd (so the quantization is symmetric).
    if(m_quantWithZero) {
      if(m_quantResol < 3) {
        throw Exception("BPSKDemod", "Number of quantization levels cannot be <3 when including a zero value");
      }
      if((m_quantResol & 1) == 0) m_quantResol--;
    }
  } //end: if quantization activated

#ifdef DEMOD_BLINDESTIM
  // (mandatory key)
  m_llrScaling = conf->getPropertyByName<double>("demod_llrscaling");
  m_llrScaling*= -1;
#endif

  m_frameRepeatCount = 1;
  m_frameRepeatCountdown = 0;
  m_savedFrame = 0; // (NULL pointer)
}

BPSKDemod::~BPSKDemod() {
  if(m_savedFrame!=0) delete[] m_savedFrame;
}

int BPSKDemod::nextLR(int n, double* array) {
#ifdef DEMOD_BLINDESTIM
  throw Exception("BPSKDemod::nextLR", "DEMOD_BLINDESTIM not supported");
#endif

  if(m_amp!=1) 
    throw Exception("BPSKDemod::nextLR", "BPSK amplitude must be 1");

  int size = n; // (size is not assigned if using a saved frame)
  if(m_frameRepeatCount==1 || m_frameRepeatCountdown==0) {
    // get the n channel values
    size = m_pchannel->nextValues(n, array);
    if(m_frameRepeatCount>1) {
      // save the frame and reset the counter
      for(int i=0; i<n; i++) m_savedFrame[i] = array[i];
      m_frameRepeatCountdown = m_frameRepeatCount;
    }
  }
  else {
    // re-use a saved frame and decrement the counter
    for(int i=0; i<n; i++) array[i] = m_savedFrame[i];
    m_frameRepeatCountdown--;
  }

  for(int i=0; i<size; i++) {
    array[i] = exp(-4 * array[i] / m_No);
  }

  return size;
}

int BPSKDemod::nextLLR(int n, double* array) {

  if(m_amp!=1) 
    throw Exception("BPSKDemod::nextLLR(int,double*)", 
                        "BPSK amplitude must be 1");

  int size = n; // (size is not assigned if using a saved frame)
  if(m_frameRepeatCount==1 || m_frameRepeatCountdown==0) {
    // get the n channel values
    size = m_pchannel->nextValues(n, array);
    if(m_frameRepeatCount>1) {
      // save the frame and reset the counter
      for(int i=0; i<n; i++) m_savedFrame[i] = array[i];
      m_frameRepeatCountdown = m_frameRepeatCount;
    }
  }
  else {
    // re-use a saved frame and decrement the counter
    for(int i=0; i<n; i++) array[i] = m_savedFrame[i];
    m_frameRepeatCountdown--;
  }

  m_lastLLRValues.clear();
  if(m_lastChValues.size() < size) m_lastChValues.resize(size);

#ifndef DEMOD_BLINDESTIM
  double const* fading= m_pchannel->lastFading();
#endif
  
  for(int i=0; i<size; i++) {
    double chval = array[i];
	  m_lastChValues[i]= chval;

    // quantization
    if(m_doQuantizeBPSK) {
      chval = quantize(chval, m_quantBpskRange);
    }

#ifdef DEMOD_BLINDESTIM
    array[i] = chval * m_llrScaling;
#else
    array[i] = -4 * chval / m_No * fading[i];  // (No has been scaled already)
#endif

    if(m_doQuantizeLLR) {
      array[i] = quantize(array[i], m_quantLLRRange);
    }

    // save the last LLR values
    m_lastLLRValues.push_back(array[i]);
  }

  return size;
}

int BPSKDemod::nextValues(int n, double* array) {

  int size = n; // (size is not assigned if using a saved frame)
  if(m_frameRepeatCount==1 || m_frameRepeatCountdown==0) {
    // get the n channel values
    size = m_pchannel->nextValues(n, array);
    if(m_frameRepeatCount>1) {
      // save the frame and reset the counter
      for(int i=0; i<n; i++) m_savedFrame[i] = array[i];
      m_frameRepeatCountdown = m_frameRepeatCount - 1;
    }
  }
  else {
    // re-use a saved frame and decrement the counter
    for(int i=0; i<n; i++) array[i] = m_savedFrame[i];
    m_frameRepeatCountdown--;
  }

  if(m_lastChValues.size() < size) m_lastChValues.resize(size);
  for(int i=0; i<size; i++) m_lastChValues[i]= array[i];

  if(m_doQuantizeBPSK) {
    for(int i=0; i<size; i++) {
      array[i] = quantize(array[i], m_quantBpskRange);
    }
  }

  return size;
}

// same as nextValues but the raw channel values are placed in "rawvalues"
int BPSKDemod::nextValues(int n, double* chvalues, double* rawvalues) {

  int size = n; // (size is not assigned if using a saved frame)
  if(m_frameRepeatCount==1 || m_frameRepeatCountdown==0) {
    // get the n channel values
    size = m_pchannel->nextValues(n, rawvalues);
    if(m_frameRepeatCount>1) {
      // save the frame and reset the counter
      for(int i=0; i<n; i++) m_savedFrame[i] = rawvalues[i];
      m_frameRepeatCountdown = m_frameRepeatCount - 1;
    }
  }
  else {
    // re-use a saved frame and decrement the counter
    for(int i=0; i<n; i++) rawvalues[i] = m_savedFrame[i];
    m_frameRepeatCountdown--;
  }

  if(m_lastChValues.size() < size) m_lastChValues.resize(size);
  for(int i=0; i<size; i++) m_lastChValues[i]= rawvalues[i];
  
  if(!m_doQuantizeBPSK) {
    for(int i=0; i<size; i++) chvalues[i] = rawvalues[i];
  }
  else {
    for(int i=0; i<size; i++) chvalues[i] = quantize(rawvalues[i],
                                                     m_quantBpskRange);
  }

  return size;
}

bool BPSKDemod::hasNext() {
  if(m_frameRepeatCount>1 && m_frameRepeatCountdown>0) return true;
  return m_pchannel->hasNext();
}

void BPSKDemod::duplicateFrames(uint repeatCount, uint frameSize) {
  if(repeatCount==0) {
    throw Exception("BPSKDemod::duplicateFrames","Repeat cannot be 0");
  }

  // allocate memory for the saved frame if needed
  if(m_savedFrame==0 && repeatCount>1) {
    m_savedFrame = new double[frameSize];
  }

  m_frameRepeatCount = repeatCount;
  m_frameRepeatCountdown = 0; // 0 signals that a new frame must be loaded
}

vector<char> const& BPSKDemod::lastHD() {
	m_lastHD.resize(m_lastChValues.size());
	for(int i=0; i<m_lastHD.size(); i++) {
		if(m_lastChValues[i] >= 0) m_lastHD[i]= 1;
		else m_lastHD[i]= 0;
	}
	return m_lastHD;
}

//(private)
double BPSKDemod::quantize(double val, double range) {
  if(val > range) val = range;
  if(val < -range) val = -range;
  if(m_quantWithZero) {
    //note: m_quantResol is adjusted when retrieved
    double d = 2*range/(m_quantResol-1);
    val = d * round(val/d);
  } else {
    val+= range;
    val = round( val*(m_quantResol-1) / (2*range) );
    val = val * (2*range) / (m_quantResol-1);
    val-= range;
  }
  return val;
}
