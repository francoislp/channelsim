//Author: Francois Leduc-Primeau

#ifndef _BPSKDemod_h_
#define _BPSKDemod_h_

#include "GlobalDefs.h"
#include "IDemodulator.hpp"
#include "BPSKMod.hpp"
#include "channel/IChannel.hpp"

#include <queue>

/**
 * Demodulates a BPSK signal using a fixed 0 threshold.
 */
class BPSKDemod : public IDemodulator {
 public:

  /**
   * Creates a new BPSK demodulator.
   *@param mod The modulator that this demodulator is meant to demodulate.
   *@param channel The channel that provides the received values.
   *@param No  The noise power at which we are operating.
   */
  BPSKDemod(BPSKMod* mod, IChannel* channel, double No);

  /**
   * Creates a new BPSK demodulator, assuming the BPSK amplitude is 1.
   *@param channel The channel that provides the received values.
   *@param No      The noise power at which we are operating.
   */
  BPSKDemod(IChannel* channel, double No);

  ~BPSKDemod();

  /**
   * Sets the noise power that is used to estimate the likelihood of
   * the received values. Note that the demodulator does not know/care
   * about the code rate, therefore this function expects the scaled
   * noise.
   */
  void setNo(double No) {m_No = No;}

  double getNo() { return m_No; }

  /**
   * Writes the likelihood ratio of the next "n" bits into "array". A 
   * LR > 1 indicates a hard decision of "0", while LR < 1 indicates a
   * hard decision of "1". *This function assumes a Gaussian channel*.
   *@param n     Number of bits to return.
   *@param array An array of size > n where the values will be written. The
   *             array index corresponds to the index of each bit.
   *@return The number of values written in the array
   */
  int nextLR(int n, double* array);

  /**
   * Writes the log-likelihood ratio of the next "n" bits into "array". A 
   * LLR > 0 indicates a hard decision of "0", while LLR < 0 indicates a
   * hard decision of "1". *This function assumes a Gaussian channel*.
   *@param n     Number of bits to return.
   *@param array An array of size > n where the values will be written. The
   *             array index corresponds to the index of each bit.
   *@return The number of values written in the array
   */
  int nextLLR(int n, double* array);

  /**
   * Writes the demodulated BPSK values of the next "n" bits into "array".
   *@return The number of values written in the array.
   */
  int nextValues(int n, double* array);

  /**
   * Same as nextValues(int,double*), but also writes the ideal channel
   * values in "rawvalues".
   */
  int nextValues(int n, double* chvalues, double* rawvalues);

  double nextISCoeff() { return m_pchannel->nextISCoeff(); }

  /// Whether there is still some data to be retrieved.
  bool hasNext();

  ///@see IDemodulator::getDim()
  int getDim() {return 1;}

  /**
   * Activates a mode where frames obtained from the channels are
   * re-used a number of times before being discarded. By default,
   * each frame is used once (which corresponds to
   * <code>repeatCount</code>=1). The repeat mode remains in effect 
   * until the repeat value is modified by another call to this method.
   *@param repeatCount
   *@param frameSize Number of bits in the frame that is to be repeated. This has to be the same as the parameter <code>n</code> supplied to <code>nextXXX</code> method calls, and there is no protection against a mismatch.
   *@throws Exception If repeatCount==0.
   */
  void duplicateFrames(uint repeatCount, uint frameSize);

  /**
   * Returns the values that were last returned through nextLLR(*).
   */
  std::vector<double> const& lastLLRValues() { return m_lastLLRValues; }

	std::vector<double> const& lastChValues() { return m_lastChValues; }

	std::vector<char> const& lastHD();

  // End IDemodulator

 private:

  /// Called by the constructors
  void init();

  /**
   * Quantizes parameter <code>chval</code> on a range
   * <code>range</code> and resolution given by
   * <code>m_quantResol</code>.
   */
  double quantize(double chval, double range);

  // ---------- Data ----------

  /// The amplitude of the BPSK modulation
  double m_amp;

  IChannel* m_pchannel;

  /// Noise power
  double   m_No;

  // Demodulation and scaling parameters

  /// Whether to perform quantization on a BPSK range
  bool m_doQuantizeBPSK;

  /// Whether to perform quantization on a LLR range
  bool m_doQuantizeLLR;

  /// Whether to include a zero value in the quantized values.
  bool m_quantWithZero;

  /// Range for the quantization of the received BPSK values.
  double m_quantBpskRange;

  /// Range for the quantization of LLR values.
  double m_quantLLRRange;

  /// Number of values in the quantization of BPSK or LLR values.
  uint m_quantResol;

#ifdef DEMOD_BLINDESTIM
  double m_llrScaling;
#endif

  uint m_frameRepeatCount;
  uint m_frameRepeatCountdown;

  double* m_savedFrame;

  std::vector<double> m_lastLLRValues;
	std::vector<double> m_lastChValues;
	std::vector<char>   m_lastHD;
};

#endif
