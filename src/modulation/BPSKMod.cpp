//Author: Francois Leduc-Primeau

#include "GlobalDefs.h"
#include "BPSKMod.hpp"
#include "util/Exception.hpp"

BPSKMod::BPSKMod(ISymbolProvider* src) 
  : m_psource(src),
    m_amp(1)
{
}

BPSKMod::BPSKMod(ISymbolProvider* src, double amp) 
  : m_psource(src),
    m_amp(amp)
{
}

BPSKMod::~BPSKMod() {}

void BPSKMod::nextElem(std::vector<double>& a) {
	a.at(0) = nextElem1D();
}

double BPSKMod::nextElem1D() {

  int bit=-1;
  if(m_bitqueue.empty()) {
    BitBlock const& cw = m_psource->nextSymbol();
    // add each bit to the send queue
    for(uint i=0; i < cw.size(); i++) m_bitqueue.push(cw[i]);
  }

  bit = m_bitqueue.front();
  m_bitqueue.pop();

  if(bit==1) return m_amp;
  else if(bit==0) return -m_amp;
  else {
    throw Exception("BPSKMod::nextElem1D()", "Invalid bit value");
  }
}

/*
 * Whether it's possible to call nextElem() or nextElem1D().
 */
inline bool BPSKMod::hasNext() {
  // check if source was set
  if(m_psource == NULL) 
    throw EndOfFeedException("Source has not been set");

  return !m_bitqueue.empty() || m_psource->hasNext();
}
