//Author: Francois Leduc-Primeau

#ifndef _BPSKMod_h_
#define _BPSKMod_h_

#include "source/ISource.hpp"
#include "IModulator.hpp"

#include <queue>

/**
 * This class modulates a source of binary or higher order symbols. In
 * the case of higher order symbols, the least significant bits are
 * sent first. This modulator sends two symbols: +A and -A. +A
 * corresponds to a 1 and -A to a 0.
 */
class BPSKMod : public IModulator {
 public:

  /**
   * Creates a new modulator object.
   *@param src The initial symbol source for this modulator.
   */
  BPSKMod(ISymbolProvider* src);

  /**
   * Creates a new modulator object.
   *@param src The initial symbol source for this modulator.
   *@param amp The amplitude of the modulated signal.
   */
  BPSKMod(ISymbolProvider* src, double amp);

  ~BPSKMod();

  /**
   * Returns the amplitude used by this modulator.
   */
  double getAmp() {return m_amp;}

  /**
   * Sets the amplitude of the modulated signal.
   */
  void setAmp(double amp) {m_amp = amp;}

  //implements IModulator
  
  ///@see IModulator::setSource()
  void setSource(ISymbolProvider* src) {m_psource = src;}
//TODO: throw an exception if m_psource->getAlphaSize() is not a power of 2

  /**
   * Calls nextElem1D() (this function should be called directly).
   *@see IModulator::nextElem()
   */
	void nextElem(std::vector<double>&);

  /**
   * Returns the coordinate of the next modulated bit. In the case the
   * symbol source for this modulator is multi-bit, the least significant
   * bits are sent first.
   *@see IModulator::nextElem1D()
   */
  double nextElem1D();

  ///@see IModulator::hasNext()
  bool hasNext();

  ///@see IModulator::getDim()
  int getDim() {return 1;}

	int getPolarity() { return -1; }

	int getNbBitsPerSymbol() { return 1; }

 private:
  ISymbolProvider* m_psource;
  double           m_amp; //amplitude
  std::queue<char> m_bitqueue;
};

#endif
