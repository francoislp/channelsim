//Author: Francois Leduc-Primeau

#ifndef _IDemodulator_h_
#define _IDemodulator_h_

#include "GlobalDefs.h"
#include <vector>

class IDemodulator {
 public:

  /**
   * Returns the number of dimensions of the modulation scheme.
   */
  virtual int getDim() = 0;

	virtual void setNo(double) = 0;
	
	virtual double getNo() = 0;

  /// Whether there is still some data to be retrieved.
  virtual bool hasNext() = 0;

	/**
	 * Writes the likelihood ratio of the next "n" bits into "array". A 
   * LR > 1 indicates a hard decision of "0", while LR < 1 indicates a
   * hard decision of "1".
   *@param n     Number of bits to return.
   *@param array An array of size > n where the values will be written. The
   *             array index corresponds to the index of each bit.
   *@return The number of values written in the array
   */
	virtual int nextLR(int n, double* array) = 0;

  /**
   * Writes the log-likelihood ratio of the next "n" bits into "array". A 
   * LLR > 0 indicates a hard decision of "0", while LLR < 0 indicates a
   * hard decision of "1".
   *@param n     Number of bits to return.
   *@param array An array of size > n where the values will be written. The
   *             array index corresponds to the index of each bit.
   *@return The number of values written in the array
   */
	virtual int nextLLR(int n, double* array) = 0;

 /**
   * Writes the demodulated values of the next "n" bits into "array".
   *@return The number of values written in the array.
   */
	virtual int nextValues(int n, double* array) = 0;

	/**
   * This activates a mode where a given channel frame of size
   * <code>frameSize</code> is re-used through
   * <code>repeatCount</code> calls to <code>nextSymbol()</code>.
   */
  virtual void duplicateFrames(uint repeatCount, uint frameSize) = 0;

  /**
   * Returns the last LLR values computed by the demodulator.
   */
  virtual std::vector<double> const& lastLLRValues() = 0;

	/**
	 * Returns the raw bit-channel outputs associated with the last call
	 * to next<...>(...).
	 */
	virtual std::vector<double> const& lastChValues() = 0;

	/**
	 * Returns the bit hard-decisions associated with the last call to
	 * next<...>(...).
	 */
	virtual std::vector<char> const& lastHD() = 0;

  virtual ~IDemodulator() {}

 protected:
  IDemodulator() {}
};

#endif
