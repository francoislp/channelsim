//Author: Francois Leduc-Primeau

#ifndef _IModulator_h_
#define _IModulator_h_

#include "base/ISymbolProvider.hpp"
#include <vector>

class IModulator {
public:

  /**
   * Set the source for the modulation.
   */
  virtual void setSource(ISymbolProvider* src) = 0;

  /**
   * Returns an array of Hilbert space coordinates for the next element of
   * the modulated block. The length of the array is given by getDim().
   */
	virtual void nextElem(std::vector<double>&) = 0;

  /**
   * Same as nextElem() but returns only the first coordinate.
   */
  virtual double nextElem1D() = 0;

  /**
   * Whether it's possible to call nextElem() or nextElem1D().
   */
  virtual bool hasNext() = 0;

  /**
   * Returns the number of dimensions of the modulated signal.
   */
  virtual int getDim() = 0;

	/**
	 * Whether a positive hard decision on a bit channel maps to a zero
	 * bit or to a one bit.
	 *
	 *@return +1 if a positive hard decision is associated with a zero
	 *        bit, -1 otherwise.
	 */
	virtual int getPolarity() = 0;

	/**
	 * Number of data bits in a modulated symbol.
	 */
	virtual int getNbBitsPerSymbol() = 0;


  virtual ~IModulator() {}

protected:
  IModulator() {}
};

#endif
