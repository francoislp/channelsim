//Author: Francois Leduc-Primeau

#include "QPSKDemod.hpp"
#include "Config.hpp"
#include <string>

using std::string;
using std::vector;

QPSKDemod::QPSKDemod(IChannelCplx* channel)
	: m_pchannel(channel)
{
	init(1.0);
}

QPSKDemod::QPSKDemod(IChannelCplx* channel, double No)
	: m_pchannel(channel)
{
	init(No);
}

void QPSKDemod::init(double No) {
	setNo(No);
	
  Config* conf = Config::getInstance();
  m_doQuantizeLLR = false;
  m_quantWithZero = false;
  string quantType = "";
  try {
    quantType = conf->getPropertyByName<string>("demod_quant_type");
  } catch(InvalidKeyException&) {}
  if(quantType != "") {
	  if( quantType == "LLR" || quantType == "llr" ) {
		  m_quantLLRRange = conf->getPropertyByName<double>("demod_llrrange");
      m_doQuantizeLLR = true;
    }
    else {
      throw Exception("QPSKDemod", "Invalid value for key \"demod_quant_type\"");
    }
    m_quantResol = conf->getPropertyByName<uint>("demod_resol");
    m_quantWithZero = conf->getPropertyByName<bool>("demod_zeroval");

    // if we include a zero value, make sure that the number of
    // quantization levels is odd (so the quantization is symmetric).
    if(m_quantWithZero) {
      if(m_quantResol < 3) {
        throw Exception("QPSKDemod", "Number of quantization levels cannot be <3 when including a zero value");
      }
      if((m_quantResol & 1) == 0) m_quantResol--;
    }
  } //end: if quantization activated
}

int QPSKDemod::nextLLR(int n, double* array) {
	// retrieve n/2 2-bit symbols from the channel
	if(n/2*2 != n) {
		throw Exception("QPSKDemod::nextLLR", "number of bits must be even");
	}
	if(m_chBuffer.size() < n/2) m_chBuffer.resize(n/2);
	int size= m_pchannel->nextValues(n/2, m_chBuffer);
	double const* fading= m_pchannel->lastFading();

	if(m_lastLLRValues.size() < 2*size) m_lastLLRValues.resize(2*size);
	if(m_lastChValues.size() < 2*size) m_lastChValues.resize(2*size);

	// process channel symbols
	for(int i=0; i<size; i++) {
		// save a copy of the bit-channel outputs
		m_lastChValues[2*i]= std::real(m_chBuffer[i]);
		m_lastChValues[2*i+1]= std::imag(m_chBuffer[i]);

		//TODO: check fading correction
		array[2*i]= m_lastChValues[2*i] * m_llrScaling * fading[i];
		array[2*i+1]= m_lastChValues[2*i+1] * m_llrScaling * fading[i];
		if(m_doQuantizeLLR) {
			array[2*i]= quantize(array[2*i], m_quantLLRRange);
			array[2*i+1]= quantize(array[2*i+1], m_quantLLRRange);
		}
		m_lastLLRValues[2*i]= array[2*i];
		m_lastLLRValues[2*i+1]= array[2*i+1];
	}

	return 2*size;
}

int QPSKDemod::nextLR(int n, double* array) {
	throw Exception("QPSKDemod::nextLR", "not implemented");
}

int QPSKDemod::nextValues(int n, double* array) {
	throw Exception("QPSKDemod::nextValues", "not implemented");
}

//(private)
// Note: Same code as BPSKDemod::quantize
double QPSKDemod::quantize(double val, double range) {
  if(val > range) val = range;
  if(val < -range) val = -range;
  if(m_quantWithZero) {
    //note: m_quantResol is adjusted when retrieved
    double d = 2*range/(m_quantResol-1);
    val = d * round(val/d);
  } else {
    val+= range;
    val = round( val*(m_quantResol-1) / (2*range) );
    val = val * (2*range) / (m_quantResol-1);
    val-= range;
  }
  return val;
}

vector<char> const& QPSKDemod::lastHD() {
	m_lastHD.resize(m_lastChValues.size());
	for(int i=0; i<m_lastHD.size(); i++) {
		if(m_lastChValues[i] < 0) m_lastHD[i]= 1;
		else m_lastHD[i]= 0;
	}
	return m_lastHD;
}
