//Author: Francois Leduc-Primeau

#ifndef QPSKDemod_hpp_
#define QPSKDemod_hpp_

#include "IDemodulator.hpp"
#include "util/Exception.hpp"
#include "channel/IChannelCplx.hpp"

#include <vector>
#include <complex>
#include <cmath>


class QPSKDemod : public IDemodulator {
public:

	/**
	 * Creates a new QPSK demodulator, assuming the energy per symbol is
	 * 1. The noise power must be set by calling setNo(...).
	 *
	 *@param channel The complex-valued channel that provides the
	 *received values.
	 */	
	QPSKDemod(IChannelCplx* channel);
	
	/**
	 * Creates a new QPSK demodulator, assuming the energy per symbol is 1.
	 *
	 *@param channel  The complex-valued channel that provides the 
	 *received values.
	 *@param No  The noise power at which we are operating.
	 */
	QPSKDemod(IChannelCplx* channel, double No);

	~QPSKDemod() {}

  /**
   * Sets the noise power that is used to estimate the likelihood of
   * the received values. Note that the demodulator does not know the
   * code rate, therefore this function expects the scaled noise.
   */
	void setNo(double No) {
		double A= 1.0/ std::sqrt(2.0);
		m_llrScaling= 8.0 * A / No;
		m_No= No;
  }

  double getNo() { return m_No; }

	int getDim() { return 2; }

	void duplicateFrames(uint repeatCount, uint frameSize) {
		throw Exception("QPSKDemod::duplicateFrames", "not implemented");
	}

	bool hasNext() { return m_pchannel->hasNext(); }

	int nextLR(int n, double* array);

	int nextLLR(int n, double* array);

	int nextValues(int n, double* array);

  /**
   * Returns the values that were last returned through nextLLR(*).
   */
  std::vector<double> const& lastLLRValues() { return m_lastLLRValues; }

	std::vector<double> const& lastChValues() { return m_lastChValues; }

	std::vector<char> const& lastHD();

private:

	void init(double No);
	
	double quantize(double, double);

	// ---------- Data Members ----------
	
	IChannelCplx* m_pchannel;

	double m_No;
	double m_llrScaling;

	std::vector<std::complex<double>> m_chBuffer;

  std::vector<double> m_lastLLRValues;
	std::vector<double> m_lastChValues;
	std::vector<char>   m_lastHD;

	// quantization parameters:

  /// Whether to perform quantization on a LLR range
  bool m_doQuantizeLLR;

  /// Whether to include a zero value in the quantized values.
  bool m_quantWithZero;

  /// Range for the quantization of LLR values.
  double m_quantLLRRange;

  /// Number of values in the quantization of BPSK or LLR values.
  uint m_quantResol;

};
	
#endif
