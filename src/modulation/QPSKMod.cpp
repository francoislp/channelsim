//Author: Francois Leduc-Primeau

#include "QPSKMod.hpp"
#include "util/BitBlock.hpp"
#include <cmath>

QPSKMod::QPSKMod(ISymbolProvider* src)
	: m_psource(src)
{}

QPSKMod::~QPSKMod() {}

void QPSKMod::nextElem(std::vector<double>& a) {
	if(m_bitQueue.empty()) {
		BitBlock const& cw= m_psource->nextSymbol();
    // add each bit to the send queue
    for(uint i=0; i < cw.size(); i++) m_bitQueue.push(cw[i]);
  }

	int_fast8_t bit0= m_bitQueue.front();
	m_bitQueue.pop();
	if(m_bitQueue.empty()) {
		throw Exception("QPSKMod::nextElem", "source symbol has odd number of bits");
	}
	int_fast8_t bit1= m_bitQueue.front();
	m_bitQueue.pop();

	/* Gray mapping used in LTE (3GPP 36.211 v10.4.0)
	 * Modulation
	 * (A, A) (A, -A) (-A, -A) (-A, A)
	 *   00      01      11      10
	 */
	// Note: assuming Es=1 => Eb=1/2, A=sqrt(Eb)
	double A= 1.0/std::sqrt(2.0);
	a.at(0)= A * static_cast<double>(-2*bit0 + 1);
	a.at(1)= A * static_cast<double>(-2*bit1 + 1);
}

bool QPSKMod::hasNext() {
	return !m_bitQueue.empty() || m_psource->hasNext();
}
