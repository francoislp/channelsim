//Author: Francois Leduc-Primeau

#ifndef QPSKMod_hpp_
#define QPSKMod_hpp_

#include "source/ISource.hpp"
#include "IModulator.hpp"
#include "util/Exception.hpp"
#include <cstdint>
#include <queue>

/**
 * QPSK modulation with symbol energy of 1.
 */
class QPSKMod : public IModulator {
public:

	QPSKMod(ISymbolProvider* src);

	~QPSKMod();

	void setSource(ISymbolProvider* src) { m_psource= src; }

	void nextElem(std::vector<double>&);

	double nextElem1D() {
		throw Exception("QPSKMod::nextElem1D", "not implemented");
	}

	bool hasNext();

	int getDim() { return 2; }

	int getPolarity() { return 1; }

	int getNbBitsPerSymbol() { return 2; }

private:
	ISymbolProvider* m_psource;
	std::queue<int_fast8_t> m_bitQueue;
};

#endif
