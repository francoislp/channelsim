#include "AListElem.hpp"

#include <iostream>

using std::cout;
using std::endl;

AListElem::AListElem(int row, int col, int value) {

  this->row = row;
  this->col = col;
  this->value = value;

}

void AListElem::Print() {

  cout << "(" << row << ", " << col << ") = " << value << endl;

}

int AListElem::GetRow() {

  return(row-1);

}

int AListElem::GetCol() {
  
  return(col-1);

}

int AListElem::GetValue() {
  
  return(value);

}
