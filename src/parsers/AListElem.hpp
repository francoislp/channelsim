#ifndef _ALIST_ELEM_HPP
#define _ALIST_ELEM_HPP

/**
 *A class that represents a non-zero element in a matrix.
 */

class AListElem {

  //> coordinates
  int row;
  //> coordinates
  int col;

  int value;

public:

  AListElem(int row, int col, int value);
  void Print();
  int GetRow();
  int GetCol();
  int GetValue();

};

#endif
