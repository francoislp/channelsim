//Author: Gabi Sarkis
//Modified by: Francois Leduc-Primeau

#ifdef FRANCOIS
 #include "../GlobalDefs.h"  // sets the value of DEBUG
 #include "../Exception.hpp"
#else
 #define DEBUG 0
#endif

#include "AListParser.hpp"

#include <stdio.h>

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

using std::vector;
using std::string;
using std::ifstream;
using std::ws;
using std::cout;
using std::cerr;
using std::endl;
using std::getline;
using std::stringstream;

// Character indicating a line comment
#define COMMENTCHAR '#'

void AListParser::ParseFile(string fileName) {

  stringstream sstream;
  //int row, col, value;
  int temp;
  string inLine;
  ifstream inFile;
  vector<int> colWeight;
  bool binaryformat; // whether we are dealing with binary a-list
                     // format or qa-list format

  inFile.open(fileName.c_str());

  if (!inFile.good()) {
#ifdef FRANCOIS
    throw Exception("AListParser::ParseFile", "Cannot open input file");
#else
    cerr << "Error! Cannot open " << fileName << endl;
    return;
#endif
  }
  
  //get row and column count
  MyGetline(inFile, inLine);
  sstream << inLine;
  m_colCnt = 0;
  m_rowCnt = 0;
  sstream >> m_colCnt >> m_rowCnt >> ws; // need to get rid of
                                         // trailing whitespace before
                                         // calling sstream.eof()
  if( m_colCnt <= 0 || m_rowCnt <= 0 ) {
#ifdef FRANCOIS
    throw Exception("AListParser::ParseFile", "Invalid column or row size specification");
#else
    cerr << "Error: Invalid column or row size specification" << endl;
    return;
#endif
  }
  if(sstream.eof()) {
    binaryformat = true;
    m_fieldOrder = 2;
  }
  else {
    binaryformat = false;
    sstream >> m_fieldOrder;
  }
//   if(DEBUG) {
//     cout << "(AListParser) rows: " << m_rowCnt << ", cols: " << m_colCnt
//          <<", field order:  " << m_fieldOrder << endl;
//   }

  //ignoring information about maximum row and col weights
  if (inFile.eof()) {
    cerr << "Invalid input file." << endl;
    cerr << "Missing maximum row and column weight information" << endl;
    return;
  }
  MyGetline(inFile, inLine);

  //create a vector with column weights
  if (inFile.eof()) {
    cerr << "Invalid input file." << endl;
    cerr << "Missing column weight vector" << endl;
    return;
  }
  MyGetline(inFile, inLine);
  colWeight.reserve(m_colCnt);
  sstream.clear();
  sstream << inLine;
  for (int i = 0; i < m_colCnt; i++) {
    sstream >> ws; // ignore any whitespace
    if (sstream.eof()) {
      cerr << "Invalid input file." << endl;
      cerr << "Missing col weight vector element " << i + 1 << endl;
      return;
    }
    //cout << "Weight(row " << i + 1 << ") = " << temp << endl;
    sstream >> temp;
    colWeight.push_back(temp);
  }
  m_elems.reserve(m_colCnt);

  //ignoring number of elements in each row
  if (inFile.eof()) {
    cerr << "Invalid input file." << endl;
    cerr << "Missing row weight vector" << endl;
    return;
  }
  MyGetline(inFile, inLine);

  // call the appropriate parser for the body
  if(binaryformat) ParseBinAList(inFile, colWeight);
  else ParseQAList(inFile, colWeight);

  inFile.close();

}

void AListParser::GetElements(vector<AListElem> &ret) {

  ret.clear();
  ret = m_elems;

}

int AListParser::GetFieldOrder() {

  return(m_fieldOrder);

}

int AListParser::GetRowCount() {
  
  return(m_rowCnt);

}

int AListParser::GetColCount() {

  return(m_colCnt);

}

// ---------- Private --------------------

// Calls getline but skips comment lines
void AListParser::MyGetline(ifstream& inFile, string& inLine) {
  char firstchar = COMMENTCHAR;

  while(firstchar == COMMENTCHAR) {
    if(!inFile.eof()) getline(inFile, inLine);
    else inLine = "";
    stringstream sstream;
    sstream << inLine;
    sstream >> std::ws; // extract whitespace
    firstchar = sstream.peek();
  }
}

// parses the QA-List format, minus the header
void AListParser::ParseQAList(ifstream& inFile, vector<int> colWeight) {
  string inLine;
  int row, value;

  for (int i = 0; i < m_colCnt; i++) {

    stringstream colstream;

    if (inFile.eof()) {
      cerr << "Invalid input file." << endl;
      cerr << "Error in matrix row " << i + 1 << endl;
      cerr << "Missing row" << endl;
      return;
    }
    MyGetline(inFile, inLine);

    colstream << inLine;

    for (int j = 0; j < colWeight[i]; j++) {
      colstream >> ws;
      if (colstream.eof()) {
	cerr << "Invalid input file." << endl;
	cerr << "Error in matrix col " << i + 1;
	cerr << ". Element " << j + 1 << endl;
	return;
      }

      colstream >> row >> value;
      if (value >= m_fieldOrder) {
	cerr << "Invalid input file." << endl;
	cerr << "Element at (" << row << ", " << i+1 << ") = a^" << value;
	cerr << " does not belong to GF(" << m_fieldOrder << ")." << endl;
	return;
      }
      AListElem elem = AListElem(row, i+1, value);
      m_elems.push_back(elem);
    }
    
  }
}

// parses the (binary) A-List format, minus the header
void AListParser::ParseBinAList(ifstream& inFile, vector<int> colWeight) {
  string inLine;
  int row;

  for (int i = 0; i < m_colCnt; i++) {

    stringstream colstream;

    if (inFile.eof()) {
      cerr << "Invalid input file." << endl;
      cerr << "Error in matrix row " << i + 1 << endl;
      cerr << "Missing row" << endl;
      return;
    }
    MyGetline(inFile, inLine);

    colstream << inLine;

    for (int j = 0; j < colWeight[i]; j++) {
      colstream >> ws;
      if (colstream.eof()) {
	cerr << "Invalid input file." << endl;
	cerr << "Error in matrix col " << i + 1;
	cerr << ". Element " << j + 1 << endl;
	return;
      }

      colstream >> row;
      AListElem elem = AListElem(row, i+1, 1); //binary => value always 1
      m_elems.push_back(elem);
    }
    
  }
}
