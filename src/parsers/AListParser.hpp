//Author: Gabi Sarkis
//Modified by: Francois Leduc-Primeau

#ifndef _ALIST_PARSER_HPP
#define _ALIST_PARSER_HPP

#include <vector>
#include <string>
#include <fstream>

#include "AListElem.hpp"

/**
 *A class to parse a-list files and generate a list of coordinates with
 * non-zero elements and their values.
 */
class AListParser {

  int m_rowCnt;
  int m_colCnt;

  int m_fieldOrder;

  std::vector<AListElem> m_elems;

public:
  void ParseFile(std::string fileName);
  void GetElements(std::vector<AListElem> &ret);
  int GetFieldOrder();
  int GetRowCount();
  int GetColCount();

private:

  /**
   * This function calls getline, but skips over comment lines. Note
   * that inline comments are not supported.
   */
  void MyGetline(std::ifstream& inFile, std::string& inLine);

  // The ParseFile function calls one of those depending on the file header:
  void ParseQAList(std::ifstream& inFile, std::vector<int> colWeight);
  void ParseBinAList(std::ifstream& inFile, std::vector<int> colWeight);

};

#endif
