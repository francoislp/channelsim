//Author: Francois Leduc-Primeau

#include "GParser.hpp"
#include "GlobalDefs.h"
#include "util/Exception.hpp"

#include <sstream>
#include <iostream>

using std::string;
using std::ifstream;
using std::stringstream;
using std::cout;
using std::cerr;
using std::endl;
using std::ws;

GParser::GParser()
  : m_n(0),
    m_k(0)
{}

char** GParser::parseFile(const char* filepath) {
  ifstream s(filepath);

  if( !s.good() ) throw Exception("GParser: Cannot open file");
  if( sizeof(int) != 4 ) throw Exception("GParser: We assume int's are 4 bytes but this is not the case.");

  // Read file format
  int fileformat = -1;
  s.read( reinterpret_cast<char*>(&fileformat), 4);

  if( fileformat!=1 ) 
    throw Exception("GParser::parseFile", "Invalid file format code");

  // parse the format #1
  // get matrix size
  s.read( reinterpret_cast<char*>(&m_k), 4);
  s.read( reinterpret_cast<char*>(&m_n), 4);

  char** G;
  try {
    G = new char*[m_n];
  } catch(std::bad_alloc) {
    cerr << "Mem alloc fails in GParser:" << (__LINE__ - 2) << endl;
    s.close();
    throw Exception("GParser::parseFile", "Memory allocation failed");
  }

  // read each column
  for(int i=0; i<m_n; i++) {
    try {
      G[i] = new char[m_k];
      // (one byte per element => m bytes for the whole column)
      s.read( G[i], m_k );
    } catch(std::bad_alloc) {
      cerr << "Mem alloc fails in GParser:" << (__LINE__ - 4);
      cerr << " (i="<<i<<")" << endl;
      s.close();
      throw Exception("GParser::parseFile", "Memory allocation failed");
    }
  }

  s.close();

  return G;
}

// Same as above but matrix is stored as an array of BitBlocks
BitBlock* GParser::parseFile_block(const char* filepath) {
  ifstream s(filepath);

  if( !s.good() ) throw Exception("GParser::parseFile_block", "Cannot open file");
  if( sizeof(int) != 4 ) throw Exception("GParser: We assume int's are 4 bytes but this is not the case.");

  // Read file format
  int fileformat = -1;
  s.read( reinterpret_cast<char*>(&fileformat), 4);

  if( fileformat == 1 ) {
    // parse the format #1
    return parse1_block(s);
  } 
  else if( fileformat == 842080547 ) { // "#!12"
    return parse2_block(s);
  }
  else if( fileformat == 825303331 ) { // "#!11"
    return parse3_block(s);
  }
  else { 
    stringstream ss;
    ss << "Invalid file format code (read "<<fileformat<<")";
    throw Exception("GParser::parseFile_block", ss.str());
  }


}

// ---------- Private ----------

BitBlock* GParser::parse1_block(ifstream& s) {
  //  cout << "Reading G matrix in binary format" << endl; //debug

  // get matrix size
  s.read( reinterpret_cast<char*>(&m_k), 4); // size "m"
  s.read( reinterpret_cast<char*>(&m_n), 4); // size "n"

  BitBlock* G;
  try {
    G = new BitBlock[m_n];
  } catch(std::bad_alloc) {
    cerr << "Mem alloc fails in GParser:" << (__LINE__ - 2) << endl;
    s.close();
    throw Exception("GParser::parse1_block", "Memory allocation failed");
  }

  // read each column
  char* colbuf = new char[m_k];
  for(int i=0; i<m_n; i++) {
    // (one byte per element => m_k bytes for the whole column)
    s.read( colbuf, m_k );
    G[i].init_rev(m_k, colbuf);
  }
  delete[] colbuf;

  s.close();

  return G;
}

// For the format of the G matrix supplied with the 802.3an standard
BitBlock* GParser::parse2_block(ifstream& s) {
  m_n = 2048;
  m_k = 1723;

  char** Gtmp = new char*[m_k];

  s >> ws; // required because the EOL char of the format code line wasn't read

  for(int i=0; i<m_k; i++) {
    string line;

    if( s.good() && !s.eof() ) {
      getline(s, line);
    } else {
      throw Exception("GParser::parse2_block", "Premature EOF");
    }

    stringstream linestr;
    linestr << line;
    
    Gtmp[i] = new char[m_n];
    // zero all the elements
    for(int j=0; j<m_n; j++) Gtmp[i][j] = 0;

    linestr >> ws;
    if( linestr.eof() ) {
      stringstream ss;
      ss << "Invalid empty line at line #" << i;
      throw Exception("GParser::parse2_block", ss.str());
    }
    while( !linestr.eof() ) {
      int colindex;
      linestr >> colindex >> ws;
      if((colindex < 0) || (colindex >= m_n)) {
        throw Exception("GParser::parse2_block", "Invalid column index");
      }
      Gtmp[i][colindex] = 1;
    }
  }

  // transpose Gtmp
  char** Gtmp2 = new char*[m_n];
  for(int i=0; i<m_n; i++) {
    Gtmp2[i] = new char[m_k];
    for(int j=0; j<m_k; j++) {
      Gtmp2[i][j] = Gtmp[j][i];
    }
  }

  // convert to BitBlocks
  BitBlock* G;
  try {
    G = new BitBlock[m_n];
  } catch(std::bad_alloc) {
    cerr << "Mem alloc fails in GParser:" << (__LINE__ - 2) << endl;
    s.close();
    throw Exception("GParser::parse1_block", "Memory allocation failed");
  }

  // each column is a BitBlock
  for(int i=0; i<m_n; i++) {
    G[i].init_rev(m_k, Gtmp2[i]);
    //G[i].init(m_k, Gtmp2[i]); // this is equivalent to using init_rev (?)
    //G[i].init_rev(m_k, Gtmp2[m_n-1-i]);
    //G[i].init(m_k, Gtmp2[m_n-1-i]);
  }

  // cleanup
  for(int i=0; i<m_k; i++) {
    delete[] Gtmp[i];
  }
  delete[] Gtmp;
  for(int i=0; i<m_n; i++) {
    delete[] Gtmp2[i];
  }
  delete[] Gtmp2;
  s.close();

  return G;
}

// Format used by prof. Gross and Saeed S. T.
//TODO: doesn't seem to work
BitBlock* GParser::parse3_block(ifstream& s) {
  m_n = 2048;
  m_k = 1723;

  char** Gtmp = new char*[m_n];

  s >> ws; // required because the EOL char of the format code line wasn't read

  // ignore the first line ("column order")
  string line;
  getline(s, line);

  // The G matrix is in systematic form and the identity matrix is not
  // included in the file.
//  for(int i=0; i< (m_n-m_k); i++) {
  for(int i=m_k; i<m_n; i++) {
    Gtmp[i] = new char[m_k];
    for(int j=0; j<m_k; j++) {
      char tmp;
      s >> tmp;
      if(tmp == '0') Gtmp[i][j] = 0;
      else if(tmp == '1') Gtmp[i][j] = 1;
      else throw Exception("GParser::parse3_block", "Invalid matrix element");
    }
  }

  // add the identity matrix (and initialize the zero elements)
//  for(int i=(m_n-m_k); i<m_n; i++) {
  for(int i=0; i<m_k; i++) {
    Gtmp[i] = new char[m_k];
    for(int j=0; j<m_k; j++) {
      if(j==i) Gtmp[i][i] = 1;
      else Gtmp[i][j] = 0;
    }
  }

  // convert to BitBlocks
  BitBlock* G;
  try {
    G = new BitBlock[m_n];
  } catch(std::bad_alloc) {
    cerr << "Mem alloc fails in GParser:" << (__LINE__ - 2) << endl;
    s.close();
    throw Exception("GParser::parse3_block", "Memory allocation failed");
  }

  // each column is a BitBlock
  for(int i=0; i<m_n; i++) {
    G[i].init_rev(m_k, Gtmp[i]);
    //G[i].init_rev(m_k, Gtmp[m_n-1-i]);
    //G[i].init(m_k, Gtmp[i]);
    //G[i].init(m_k, Gtmp[m_n-1-i]);
  }

  // cleanup
  for(int i=0; i<m_n; i++) {
    delete[] Gtmp[i];
  }
  delete[] Gtmp;

  return G;
}
