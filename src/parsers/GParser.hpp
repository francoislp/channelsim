//Author: Francois Leduc-Primeau

#ifndef _GParser_h_
#define _GParser_h_

#include "util/BitBlock.hpp"

#include <fstream>

class GParser {
 public:
  GParser();

  /**
   * Parses the file and returns the G matrix, stored such that G[i]
   * is the i-th column vector. The matrix has dimension kxn, but is
   * stored transposed, which means that the bottom right element has
   * index (getN(), getK()). It is the responsability of the caller to
   * free the memory allocated for the matrix.
   */
  char** parseFile(const char* filepath);

  /**
   * Same as parseFile, but each column is stored as a BitBlock. The
   * bit ordering in each BitBlock is as expected, meaning that col[0]
   * returns the lsb for that column. However, just like for
   * parseFile, the ordering of columns is reversed.
   */
  BitBlock* parseFile_block(const char* filepath);

  /// Returns the value of n (number of columns) contained in the file.
  int getN() {return m_n;}

  /// Returns the value of k (number of rows) contained in the file.
  int getK() {return m_k;}

 private:

  /// Parser for format code 1
  BitBlock* parse1_block(std::ifstream& s);

  /// Parser for format code 842080547 ("#!12"), the 802.3an format.
  BitBlock* parse2_block(std::ifstream& s);

  /// Parser for format code "#!11" (Saeed's format).
  BitBlock* parse3_block(std::ifstream& s);

  // ---------- Data Members ----------
  int m_n;
  int m_k;
};

#endif 
