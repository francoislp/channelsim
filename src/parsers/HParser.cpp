//Author: Francois Leduc-Primeau

#include "HParser.hpp"
#include "util/Exception.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using std::ifstream;
using std::stringstream;
using std::ws;
using std::string;
using std::cerr;
using std::endl;

HParser::HParser()
  : m_rowcnt(0),
    m_colcnt(0)
{}

// similar code to GParser::parse2_block
BitBlock* HParser::parseFile_block(const char* filepath) {
  ifstream s(filepath);

  if( !s.good() ) throw Exception("HParser::parseFile_block", 
                                      "Cannot open file");

  m_colcnt = 2048;
  m_rowcnt = 384;

  char** Htmp = new char*[m_rowcnt];

  s >> ws; // required because the EOL char of the format code line wasn't read

  for(int i=0; i<m_rowcnt; i++) {
    string line;

    if( s.good() && !s.eof() ) {
      getline(s, line);
    } else {
      throw Exception("HParser::parseFile_block", "Premature EOF");
    }

    stringstream linestr;
    linestr << line;
    
    Htmp[i] = new char[m_colcnt];
    // zero all the elements
    for(int j=0; j<m_colcnt; j++) Htmp[i][j] = 0;

    linestr >> ws;
    if( linestr.eof() ) {
      stringstream ss;
      ss << "Invalid empty line at line #" << i;
      throw Exception("HParser::parseFile_block", ss.str());
    }
    while( !linestr.eof() ) {
      int colindex;
      // the values are comma-separated, extract a single token
      string token;
      getline(linestr, token, ',');
      linestr >> ws; // prepare for next eof() call
      stringstream tokenstr;
      tokenstr << token;
      tokenstr >> ws;
      if(tokenstr.eof()) {
        throw Exception("HParser::parseFile_block", "Invalid token");
      }
      tokenstr >> colindex;
      if((colindex < 0) || (colindex >= m_colcnt)) {
        throw Exception("HParser::parseFile_block", "Invalid column index");
      }
      Htmp[i][colindex] = 1;
    }
  }

  // convert to BitBlocks
  BitBlock* H;
  try {
    H = new BitBlock[m_rowcnt];
  } catch(std::bad_alloc) {
    cerr << "Mem alloc fails in HParser:" << (__LINE__ - 2) << endl;
    s.close();
    throw Exception("HParser::parseFile_block", "Memory allocation failed");
  }

  // each row is a BitBlock
  for(int i=0; i<m_rowcnt; i++) {
    H[i].init_rev(m_colcnt, Htmp[i]);
    //G[i].init_rev(m_k, Gtmp2[m_n-1-i]);
    //G[i].init(m_k, Gtmp2[i]);
    //G[i].init(m_k, Gtmp2[m_n-1-i]);
  }

  // cleanup
  for(int i=0; i<m_rowcnt; i++) {
    delete[] Htmp[i];
  }
  delete[] Htmp;
  s.close();

  return H;
}
