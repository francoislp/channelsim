//Author: Francois Leduc-Primeau

#ifndef _HParser_h_
#define _HParser_h_

#include "util/BitBlock.hpp"

class HParser {
public:

  HParser();

  /**
   * Parses the file format that is used in the 802.3an standard.
   */
  BitBlock* parseFile_block(const char* filepath);

  int getRowCount() {return m_rowcnt;}

  int getColCount() {return m_colcnt;}

private:

  int m_rowcnt;
  int m_colcnt;
};

#endif
