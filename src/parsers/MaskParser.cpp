#include "MaskParser.hpp"
#include "util/Exception.hpp"

#include <fstream>
#include <sstream>

MaskParser::MaskParser()
	: m_falseCount(0)
{}

void MaskParser::parseFile(std::string filepath) {
	m_falseCount= 0;
	m_mask.clear();
	
  std::ifstream ifs(filepath.c_str());
  if(!ifs.good()) {
    throw Exception("MaskParser::parseFile", "Error opening file");
  }
  
  //note: almost same code as FileInputCoder::readNext
  std::string line;
  ifs >> std::ws;
  if(ifs.eof()) {
	  ifs.close();
	  throw Exception("MaskParser::parseFile", "Premature end of input file");
  }
  getline(ifs, line);
  std::stringstream linestr;
  linestr << line;
  linestr >> std::ws;
  while(!linestr.eof()) {
	  int tmp;
	  linestr >> tmp;
	  if(tmp==0) m_mask.push_back(false);
	  else if(tmp==1) m_mask.push_back(true);
	  else {
		  ifs.close();
		  throw Exception("MaskParser::parseFile", "Invalid value for bit");
	  }
	  linestr >> std::ws;
  }

  // second line gives the number of zero elements
  getline(ifs, line);
  std::stringstream linestr2;
  linestr2 << line;
  linestr2 >> std::ws;
  if(linestr2.eof()) {
	  ifs.close();
	  throw Exception("MaskParser::parseFile", "Premature end of input file");
  }
  linestr2 >> m_falseCount;

  // sanity check: number of true elements should match weight
  int trueSum=0;
  for(int i=0; i<m_mask.size(); i++) {
	  if(m_mask[i]) trueSum++;
  }
  int vectorLength= m_mask.size();
  if(trueSum != (vectorLength - m_falseCount)) {
	  ifs.close();
	  throw Exception("MaskParser::parseFile", "weight does not match mask vector");
  }

  ifs.close();	
}
