//Author: Francois Leduc-Primeau

#ifndef MaskParser_hpp_
#define MaskParser_hpp_

#include <string>
#include <vector>

/**
 * Parses a simple file describing a mask, i.e. a sequence of
 * true/false flags.
 */
class MaskParser {
public:
	MaskParser();

	/**
	 * Parse the specified file.
	 *
	 *@throws Exception  If the file cannot be opened or if the file
	 *        format is incorrect.
	 */
	void parseFile(std::string filePath);

	std::vector<bool> const& getMask() { return m_mask; }

	/**
	 * Returns number of true elements.
	 */
	int getWeight() { return m_mask.size() - m_falseCount; }

	/**
	 * Returns number of false elements.
	 */
	int getWeightComp() { return m_falseCount; }

private:
	std::vector<bool> m_mask;
	int m_falseCount;
};

#endif
