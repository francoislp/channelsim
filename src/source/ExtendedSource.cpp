//Author: Francois Leduc-Primeau

#include "GlobalDefs.h"
#include "ExtendedSource.hpp"
#include "util/Exception.hpp"

#include <math.h>
#include <unistd.h>
#include <sys/time.h>

ExtendedSource::ExtendedSource(int bitsize) 
  : m_alphabitsize(bitsize),
    m_curIndex(0)
{
  mp_rndgen = gsl_rng_alloc(gsl_rng_ranlxd1);
  // seed the random number generator
  pid_t mypid = getpid();
  struct timeval tp;
  gettimeofday(&tp, NULL);
  unsigned long seed = mypid * tp.tv_usec;
  gsl_rng_set(mp_rndgen, seed);

  //temp: can only do equiprobable symbols
  // TODO: use a probability array
//   m_probList = new double[nbsymbols];
//   for(int i=0; i<nbsymbols; i++) {
//     m_probList[i] = 1/static_cast<double>(nbsymbols);
//   }
}

ExtendedSource::~ExtendedSource() {
  delete[] m_block;
  gsl_rng_free(mp_rndgen);
}

/* 
 * Generates a random sequence (a block) of source symbols. The
 * block size is determined by "size". The block can then be
 * accessed using getElem() or nextBits().
 */
int ExtendedSource::genBlock(int size) {
  if(m_block != NULL) delete[] m_block;
  try {
    m_block = new BitBlock[size];
  } catch(std::bad_alloc e) {
    return 0;
  }
  m_size = size;

  // number of integers per block
  int nbint = BitBlock::getintsize(m_alphabitsize);
  uint* intarray = new uint[nbint];
  for(int i=0; i<size; i++) {
    for(int j=0; j<nbint; j++) intarray[j] = gsl_rng_get(mp_rndgen);
    m_block[i].init(m_alphabitsize, intarray);
  }
  delete[] intarray;

  m_curIndex = 0;
  return 1; //success
}

inline BitBlock const& ExtendedSource::nextSymbol() {
  if(!hasNext()) throw EndOfFeedException("No more symbols");
  return getSym(m_curIndex++);
}

inline bool ExtendedSource::hasNext() {
  if(m_curIndex < getSize()) return true;
  return false;
}

