//Author: Francois Leduc-Primeau

#ifndef _ExtendedSource_h_
#define _ExtendedSource_h_

#include "ISource.hpp"
#include <gsl/gsl_rng.h> // GNU Scientific Library Random Number Generation
#include <gsl/gsl_randist.h>

class ExtendedSource : public ISource {
 public:

  /**
   * Creates a source that outputs higher order symbols. The alphabet
   * size is always a power of 2.
   *@param bitsize   The number of bits required to code the symbols.
   */
  ExtendedSource(int bitsize);

  ~ExtendedSource();

  //Implements ISource

  ///@see ISource::getType()
  int getType() { return SRCTYPE_EXTENDED; }
  
  ///@see ISource::genBlock(int)
  int genBlock(int size);

  ///@see ISource::getAlphaBitsize()
  int getAlphaBitsize() {return m_alphabitsize;}

  void resetIter() {m_curIndex = 0;}

  ///@see ISource::nextSymbol()
  BitBlock const& nextSymbol();

  ///@see ISource::hasNext()
  bool hasNext();
  

  //void setProb(double* probList);

 protected:
  int      m_alphabitsize; // number of bits/block
  //double*  m_probList; // probability of each source symbol
  gsl_rng* mp_rndgen;
  int      m_curIndex; // current index for accessing the symbol array
};

#endif
