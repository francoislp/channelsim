//Author: Francois Leduc-Primeau

#include "ISource.hpp"
#include "util/Exception.hpp"
#include "Logging.hpp"
#include <iostream>

using std::cout;
using std::endl;

ISource::ISource()
  : m_block(0),
    m_size(0)
{}

int ISource::errors(ISymbolProvider* provider) {
  if( getSize()==0 )
    throw Exception("Source is empty");

  int errcnt=0;
  int i;
  for(i=0; provider->hasNext() && i < getSize(); i++) {
    if(provider->nextSymbol() != getSym(i)) errcnt++;
  }

  // size mismatch is not allowed
  if(provider->hasNext() || i < getSize())
    throw Exception("ISource::biterrors", "Size mismatch");

  return errcnt;
}

int ISource::errors(BitBlock* symbols, int size) {
  if( getSize()==0 )
    throw Exception("Source is empty");
  // size mismatch is not allowed
  if(size != getSize())
    throw Exception("ISource::errors", "Size mismatch");

  int errcnt=0;
  for(int i=0; i < size && i < getSize(); i++) {
    if(symbols[i] != getSym(i)) errcnt++;
  }

  return errcnt;
}

int ISource::biterrors(ISymbolProvider* provider, int& symErrors) {
  if( getSize()==0 )
    throw Exception("Source is empty");

  int errcnt=0;
  symErrors = 0;
  int i;
  for(i=0; provider->hasNext() && i < getSize(); i++) {
    BitBlock const& b1 = provider->nextSymbol();
    BitBlock const& b2 = getSym(i);
    int tmp = b1.biterrorcnt(b2);
    if(tmp>0) symErrors++;
    errcnt+= tmp;
  }

  // size mismatch is not allowed
  if(provider->hasNext() || i < getSize()) {
    throw Exception("ISource::biterrors", "Size mismatch");
  }

  return errcnt;
}

// Same as biterrors, but for each codeword adds the number of bit
// errors to errorLog
int ISource::biterrors_dbg(ISymbolProvider* provider, int& symErrors,
                           std::vector<int>& errorLog) {
  if( getSize()==0 )
    throw Exception("Source is empty");

  int errcnt=0;
  symErrors = 0;
  int i;
  for(i=0; provider->hasNext() && i < getSize(); i++) {
    BitBlock const& b1 = provider->nextSymbol();
    BitBlock const& b2 = getSym(i);
    int tmp = b1.biterrorcnt(b2);
    if(tmp>0) symErrors++;
    errcnt+= tmp;
    errorLog.push_back(tmp);
  }

  // size mismatch is not allowed
  if(provider->hasNext() || i < getSize())
    throw Exception("ISource::biterrors", "Size mismatch");

  return errcnt;
}
