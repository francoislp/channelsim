//Author: Francois Leduc-Primeau

#ifndef _ISource_h_
#define _ISource_h_

#include "../GlobalDefs.h"
#include "base/ISymbolProvider.hpp"
#include <vector>

// Types of symbol sources
#define SRCTYPE_EXTENDED 1
#define SRCTYPE_ZERO     2

/*
 * A source of random symbols.
 * Note that this class is not a pure interface.
 */
class ISource : public ISymbolProvider {
 public:

  /**
   * Returns the type of Source (one of the SRCTYPE_* constants).
   */
  virtual int getType() = 0;

  /** 
   * Generates a random sequence of source symbols (codewords). The
   * number of symbols is determined by "size". The block can then be
   * accessed using the ISymbolProvider interface.
   *@returns 1 if the necessary memory allocations are successful, 0 otherwise.
   */
  virtual int genBlock(int size) = 0;

  /// Returns the number of symbols contained in this source block.
  int getSize() {return m_size;}

  // Implements ISymbolProvider

  ///@see ISymbolProvider::getAlphaBitsize()
  virtual int getAlphaBitsize() = 0;

  ///@see ISymbolProvider::nextSymbol()
  virtual BitBlock const& nextSymbol() = 0;

  //TODO: nextProbArray performs soft demod and returns a ProbArray object
  //will have to provide a method that specifies which of hard/soft is supported

  ///@see ISymbolProvider::hasNext()
  virtual bool hasNext() = 0;

  // End ISymbolProvider

  /**
   * Returns one of the data blocks in this Source.
   */
  virtual BitBlock const& getSym(int index) {return m_block[index];}

  /**
   * Compares the symbols in this Source with symbols from a provider
   * and returns the number of unmatched symbols.
   */
  int errors(ISymbolProvider* provider);

  /**
   * Compares the symbols in this Source with the specified symbols
   * and returns the number of unmatched symbols.
   *
   *@param symbols  An array of bit vectors.
   *@param size     ??
   */
  int errors(BitBlock* symbols, int size);

  /**
   * Compares the symbols in this Source with symbols from a provider
   * and returns the number of unmatched bits within symbols. The
   * number of symbol errors is also returned via symErrors.
   */
  int biterrors(ISymbolProvider* provider, int& symErrors);

  /**
   * Same as biterrors, but for each codeword adds the number of bit
   * errors to errorLog.
   */
  int biterrors_dbg(ISymbolProvider* provider, int& symErrors,
                    std::vector<int>& errorLog);

  virtual ~ISource() {}

 protected:
  ISource(); //prohibits direct instantiation

  /// Array of symbols (should not be accessed directly, but through getSym()).
  BitBlock* m_block;

  /// Number of elements in m_block (access through getSize())
  int  m_size;
};

#endif
