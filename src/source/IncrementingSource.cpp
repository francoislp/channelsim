//Author: Francois Leduc-Primeau

#include "IncrementingSource.hpp"

IncrementingSource::IncrementingSource(int bitsize)
  : ExtendedSource(bitsize)
{}

int IncrementingSource::genBlock(int size) {
  delete m_block;
  m_block = new BitBlock[size];
  m_size = size;

  for(uint i=0; i<size; i++) {
    m_block[i].init(m_alphabitsize, &i);
  }

  m_curIndex = 0;

  return 1;
}
