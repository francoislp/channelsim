//Author: Francois Leduc-Primeau

#ifndef _IncrementingSource_h_
#define _IncrementingSource_h_

#include "ExtendedSource.hpp"

/**
 * A deterministic, incrementing source FOR TESTING.
 */
class IncrementingSource : public ExtendedSource {
 public:
  IncrementingSource(int bitsize);

  /**
   * Note that this function always returns true.
   *@see ExtendedSource::genBlock(int)
   */
  int genBlock(int size);

};

#endif
