//Author: Francois Leduc-Primeau

#include "ZeroSource.hpp"
#include "util/BitBlock.hpp"
#include "util/Exception.hpp"

ZeroSource::ZeroSource(int bitsize)
  : ExtendedSource(bitsize)
{
  int intsize = BitBlock::getintsize(bitsize);
  uint* array = new uint[intsize];
  for(int i=0; i<intsize; i++) array[i] = 0;
  m_zeroblock.init(bitsize, array);
  delete[] array;
}

// code is almost the same as ExtendedSource::genBlock because this
// class is only temporary and we want to test similar code
// int ZeroSource::genBlock(int size) {
//   if(m_block != NULL) delete[] m_block;
//   try {
//     m_block = new BitBlock[size];
//   } catch(std::bad_alloc e) {
//     return 0;
//   }
//   m_size = size;

//   // number of integers per symbol
//   int nbint = BitBlock::getintsize(m_alphabitsize);
//   uint* intarray = new uint[nbint];
//   for(int i=0; i<size; i++) {
//     for(int j=0; j<nbint; j++) intarray[j] = 0; // always 0 !
//     m_block[i].init(m_alphabitsize, intarray);
//   }
//   delete[] intarray;

//   m_curIndex = 0;
//   return 1; //success
// }

// inline BitBlock const& ZeroSource::nextSymbol() {
//   if(!hasNext()) throw EndOfFeedException("No more symbols");
//   return m_block[m_curIndex++];
// }

inline BitBlock const& ZeroSource::nextSymbol() {
  if( hasNext() ) {
    m_curIndex++;
    return m_zeroblock;
  }
  //else:
  throw EndOfFeedException("No more symbols");
}
