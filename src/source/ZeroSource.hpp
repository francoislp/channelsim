//Author: Francois Leduc-Primeau

#ifndef _ZeroSource_H_
#define _ZeroSource_H_

#include "ExtendedSource.hpp"

/**
 * A source that only outputs zero symbols.
 */
class ZeroSource : public ExtendedSource {
 public:
  ZeroSource(int bitsize);
  ~ZeroSource() {};

  ///@see ISource::getType()
  int getType() {return SRCTYPE_ZERO;}

  ///@see ISource::getSym(int)
  BitBlock const& getSym(int index) {return m_zeroblock;}

  int genBlock(int size) {m_curIndex = 0; m_size = size; return 1;}

  BitBlock const& nextSymbol();

 private:
  BitBlock m_zeroblock;
};

#endif
