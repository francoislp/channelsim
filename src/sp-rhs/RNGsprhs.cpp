#include <assert.h>
#include "bit_operations.hpp"
#include "RNGsprhs.hpp"

RNG::RNG(uint16_t lfsr_seed_idx, bool force_seed) {
	if(force_seed) {
		assert(lfsr_seed_idx < 255);
		m_seed = lfsr_seed_idx+1;
	} else {
		assert(lfsr_seed_idx < 56);
		m_seed = getSeed(lfsr_seed_idx);
	}
    assert(lfsr_seed_idx < 255);
    reset();
}

void RNG::reset(void) {
    m_cur_val = m_seed;
	m_first_tick = true;
}

uint16_t RNG::getSeed(uint16_t seed_idx) {
    const uint16_t lfsr_seeds[] = {
        94, 156, 159, 117, 179, 103, 189, 207, 
        123, 217, 58, 150, 46, 64, 42, 33, 
        136, 195, 17, 205, 236, 224, 76, 154, 
        183, 111, 230, 97, 248, 242, 16, 68, 
        130, 85, 74, 36, 121, 109, 165, 219, 
        11, 20, 137, 5, 32, 60, 54, 193, 
        23, 48, 70, 131, 7, 34, 65, 172
    };

    return lfsr_seeds[seed_idx]+1;
}

// --tested-by: Pascal Giard (20120925)
//Fibonacci LFSR for a wordlength of 8 bits; polynomal is hardcoded to 0b10111001. Max rng_idx is 254.
void RNG::update(void) {
	if( m_first_tick ) {
		m_first_tick = false;
		return;
	}
    m_cur_val =
        ((getbit(0,m_cur_val) ^ getbit(2,m_cur_val) ^ getbit(3,m_cur_val)
		  ^ getbit(4,m_cur_val) ^ getbit(7,m_cur_val)) << 7) | (m_cur_val >> 1);
}
//Fibonacci LFSR for a wordlength of 8 bits; polynomal is hardcoded to 0b10111000. Max rng_idx is 254.
// void RNG::update(void) {
//     m_cur_val =
//         ((getbit(0,m_cur_val) ^ getbit(2,m_cur_val) ^
// 		  getbit(3,m_cur_val) ^ getbit(4,m_cur_val)) << 7) | (m_cur_val >> 1);
// }

// --tested-by: Pascal Giard (20120925)
//This method returns a (5,0) value: [0, 15]
// Distribution index:
//   0: U(0, 1/2)
//   1: U(0, 10^-4)
//   2: U(0, 10^-5)
llr_threshold_t RNG::get_rand_threshold(uint8_t distroidx/*=0*/) {
	static const uint8_t distro[3*7] =
		{ 0,  1,  2,  3,  4,  5,  6,
		  9, 10, 11,  9, 12, 13, 12,
		  13, 12, 12, 14, 12, 15, 15};
	uint8_t caseidx;
    uint16_t rn = m_cur_val;

    // output register initialized to 1
    if( m_first_tick ) return 1;

    if( ((rn>>6) & 0x3) == 0 )  caseidx = 0;
    else if( ((rn>>5)&1) == 1 ) caseidx = 1;
    else if( ((rn>>4)&1) == 1 ) caseidx = 2;
    else if( (rn&0x3f) == 0 )   caseidx = 2;
    else if( ((rn>>3)&1) == 1 ) caseidx = 3;
    else if( ((rn>>2)&1) == 1 ) caseidx = 4;
    else if( ((rn>>1)&1) == 1 ) caseidx = 5;
    else/*( (rn&1) == 1 )*/     caseidx = 6;

    return distro[7*distroidx+caseidx];
}

binary_t RNG::get_rand_fairness(void) {
    binary_t retval;

    retval = getbit(0, m_cur_val);

    return retval;
}

