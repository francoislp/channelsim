//Author: Francois Leduc-Primeau

#include "../gabi/AListParser.hpp"
#include "../gabi/AListElem.hpp"
#include <iostream>
#include <vector>

#define SRC_K  4
#define CODE_N 7

using std::cout;
using std::endl;
using std::vector;

typedef unsigned int uint;

int main(int argc, char** argv) {

  // parse arguments
  char* datafile = "matrices/AListParser_testdata";
//   if(argc != 2) {
//     cout << "Usage: test_AListParser <matrix test file>" << endl;
//     cout << " The test file must match the matrix hardcoded in the test"<<endl;
//   } else {
//     datafile = argv[1];
//   }

  AListParser p;
  p.ParseFile(datafile);
  // check size
  int m = p.GetRowCount();
  int n = p.GetColCount();
  if(m != SRC_K) {
    cout << "Invalid number of rows. Expected "<<SRC_K<<", got "<< m << endl;
    return 0;
  }
  if(n != CODE_N) {
    cout << "Invalid number of columns. Expected "<<CODE_N<<", got "<<n<<endl;
    return 0;
  }
  vector<AListElem> elemlist;
  p.GetElements(elemlist);
  // allocate array (matrix is stored in transposed form)
  char** G = new char*[n];
  for(int i=0; i<n; i++) G[i] = new char[m];
  // initialize to 0
  for(int i=0; i<n; i++) {
    for(int j=0; j<m; j++) {
      G[i][j] = 0;
    }
  }
  // copy the parsed matrix
  for(int i=0; i<elemlist.size(); i++) {
    int row = elemlist[i].GetRow();
    int col = elemlist[i].GetCol();
    G[col][row] = 1;
  }

  // the code generator matrix (transposed)
  char** G2 = new char*[CODE_N];
  //for(int i=0; i<7; i++) { G2[i] = new char[SRC_K]; }
  char tmp[SRC_K]  = {1, 0, 1, 1};
  G2[0] = tmp;
  char tmp1[SRC_K] = {1, 1, 1, 0};
  G2[1] = tmp1;
  char tmp2[SRC_K] = {0, 1, 1, 1};
  G2[2] = tmp2;
  char tmp3[SRC_K] = {1, 0, 0, 0};
  G2[3] = tmp3;
  char tmp4[SRC_K] = {0, 1, 0, 0};
  G2[4] = tmp4;
  char tmp5[SRC_K] = {0, 0, 1, 0};
  G2[5] = tmp5;
  char tmp6[SRC_K] = {0, 0, 0, 1};
  G2[6] = tmp6;

  // check if G and G2 are identical
  int mismatch=0;
  for(int i=0; i<n; i++) {
    for(int j=0; j<m; j++) {
      if(G[i][j] != G2[i][j]) mismatch++;
    }
  }
  if(mismatch>0) cout <<"(FAIL) "<< mismatch << " elements don't match" << endl;
  else cout << "(PASS) All elements match !" << endl;

//   BlockCoder coder(&src, SRC_K, CODE_N, G);

//   // try getting the parity check matrix H
//   char** H = coder.getH();

  return 0;
}
