//Author: Francois Leduc-Primeau

#include "BinarySource.h"
#include <iostream>

using std::endl;
using std::cout;

int main(int argc, char** argv) {
  BinarySource* src = new BinarySource();

  src->genBlock(100);
  int i=0;
  while(src->hasNext()) {
    cout << src->nextSymbol() << ", ";
    i++;
  }
  cout << endl;
  cout << "(" << i << " symbols)" << endl;

  src->genBlock(1000);
  int count=0;
  for(int i=0; i<1000; i++) count+= src->nextSymbol();
  float pofone = static_cast<float>(count) / 1000;
  cout << "(1000 trials), p(1)=" << pofone << endl;
}
