//Author: Francois Leduc-Primeau

#include "util/BitBlock.hpp"
#include "channel/NoiseGen.hpp"
#include <gsl/gsl_rng.h> // GNU Scientific Library Random Number Generation
#include <gsl/gsl_randist.h>

#include <iostream>
#include <vector>
#include <math.h>
#include <boost/ptr_container/ptr_vector.hpp>

using std::cout;
using std::endl;


int test_biterrorcnt(char* a1, char* a2, int size);

int main(int argc, char** argv) {

  // init random number generator
  gsl_rng* prnd = gsl_rng_alloc(gsl_rng_ranlxs0);

  for(uint blocksize=1; blocksize<=5000; blocksize++) {
    //cout << "Testing block size " << blocksize << endl;

    // create a bit array
    char* bitarray = new char[blocksize];
    for(uint i=0; i<blocksize; i++) bitarray[i] = gsl_rng_get(prnd) & 1;
    // BitBlock from bit array
    BitBlock b1(blocksize, bitarray);

    // the bit array reversed
    char* bitarray_rev = new char[blocksize];
    for(uint i=0; i<blocksize; i++) bitarray_rev[i]= bitarray[blocksize-1-i];
    // BitBlock from reversed bit array
    BitBlock b11;
    b11.init_rev(blocksize, bitarray_rev);

    // pack bits into int's
    uint intsize = ceil( static_cast<float>(blocksize) / (8*sizeof(int)) );
    uint* intarray = new uint[intsize];
    for(uint i=0; i<intsize; i++) {
      int baseindex = i*32;
      intarray[i] = 0;
      for(int j=0; j < 32 && j < (blocksize-baseindex); j++) {
        intarray[i]+= (bitarray[baseindex + j] << j);
      }
    }
    // BitBlock from int array
    BitBlock b2(blocksize, intarray);

//     cout << "Content of b1:" << endl;
//     b1.print();
//     cout << "Content of b2:" << endl;
//     b2.print();

    // check size
    if( b1.size() != blocksize ) {
      cout << "(FAIL) Invalid size for b1, line: " << __LINE__ << endl;
      return 1;
    }
    if( b11.size() != blocksize ) {
      cout << "(FAIL) Invalid size for b11, line: " << __LINE__ << endl;
      return 1;
    }
    if( b2.size() != blocksize ) {
      cout << "(FAIL) Invalid size for b2, line: " << __LINE__ << endl;
      return 1;
    }

    // the two BitBlock's should be equals
    if( !(b1==b2) ) {
      cout << "(FAIL) BitBlocks are not equal, line: " << __LINE__ << endl;
      return 1;
    }
    if( !(b1==b11) ) {
      cout << "(FAIL) BitBlocks are not equal, line: " << __LINE__ << endl;
      return 1;
    }
    if( !(b11==b2) ) {
      cout << "(FAIL) BitBlocks are not equal, line: " << __LINE__ << endl;
      return 1;
    }
    if( b1 != b2 ) {
      cout << "(FAIL) != operator does not work, line: " << __LINE__ << endl;
      return 1;
    }
    // the number of bit errors should be 0
    if( b1.biterrorcnt(b2) != 0 ) {
      cout << "(FAIL) on line " << __LINE__ << endl;
      return 1;
    }
    
    // check that we can access individual bits
    for(uint i=0; i<blocksize; i++) {
      if( b2[i] != bitarray[i] ) {
        cout << "(FAIL) Invalid bit access for bit #"<<i << endl;
        cout << static_cast<int>(b2[i]) << " != ";
        cout << static_cast<int>(bitarray[i]) << endl;
        return 1;
      }
    }
    for(uint i=0; i<blocksize; i++) {
      if( b1[i] != bitarray[i] ) {
        cout << "(FAIL) Invalid bit access for bit #"<<i << endl;
        cout << static_cast<int>(b1[i]) << " != ";
        cout << static_cast<int>(bitarray[i]) << endl;
        return 1;
      }
    }

    // some more testing for the bit error counting
    // create another bit array
    char* bitarray2 = new char[blocksize];
    for(uint i=0; i<blocksize; i++) bitarray2[i] = gsl_rng_get(prnd) & 1;
    // BitBlock from bit array
    BitBlock b1_2(blocksize, bitarray2);

    int errors = b1.biterrorcnt(b1_2);
    if( errors != test_biterrorcnt(bitarray, bitarray2, blocksize) ) {
      cout << "(FAIL) Mismatch in bit error count (line ";
      cout << __LINE__ << ")" << endl;
      return 1;
    }

    // Test: void setbit(uint, char)
    // set some bits and make sure we can read them back
    int testsize = 50;
    if(blocksize < 50) testsize = blocksize / 3;
    if(testsize == 0) testsize = 1;
    // choose "testsize" number of bit indexes for the test
    int* index_list = new int[testsize];
    int* allindexes = new int[blocksize];
    for(uint i=0; i<blocksize; i++) allindexes[i] = i;
    gsl_ran_choose(prnd, index_list, testsize, allindexes, blocksize, 
                   sizeof(int));
    for(int i=0; i<testsize; i++) {
      b1.setbit(index_list[i], i & 1);
    }
    for(int i=0; i<testsize; i++) {
      char bit = b1[index_list[i]];
      if(bit != (i&1)) {
        cout << "(FAIL) Error in bit read (line ";
        cout << __LINE__ << ")" << endl;
        return 1;
      }
    }
    

    delete[] bitarray;
    delete[] bitarray2;
    delete[] bitarray_rev;
    delete[] intarray;
    delete[] index_list;
    delete[] allindexes;

  }

  // The following tests are performed with deterministic BitBlocks

  // Make sure that blocks that are not equal are reported as such
  uint* intarray = new uint[2];
  intarray[0] = 42;
  BitBlock b1(6, intarray);
  intarray[0] = 33;
  BitBlock b2(6, intarray);
  if(b1 == b2) {
    cout << "(FAIL) Blocks are not equal, line: " << __LINE__ << endl;
    return 1;
  }
  if(b1.biterrorcnt(b2) != 3) {
    cout << "(FAIL) biterrorcnt, line: " << __LINE__ << endl;
    return 1;
  }

  intarray[0] = 89;
  intarray[1] = 1;
  BitBlock b3(33, intarray);
  intarray[1] = 0;
  BitBlock b4(33, intarray);
  if(b3 == b4) {
    cout << "(FAIL) Blocks are not equal, line: " << __LINE__ << endl;
    return 1;
  }
  if(b3.biterrorcnt(b4) != 1) {
    cout << "(FAIL) biterrorcnt, line: " << __LINE__ << endl;
    return 1;
  }

  intarray[0] = 2933617;
  intarray[1] = 33;
  BitBlock b5(38, intarray);
  intarray[1] = 3;
  BitBlock b6(38, intarray);
  if(b5 == b6) {
    cout << "(FAIL) Blocks are not equal, line: " << __LINE__ << endl;
    return 1;
  }
  if(b5.biterrorcnt(b6) != 2) {
    cout << "(FAIL) biterrorcnt, line: " << __LINE__ << endl;
    return 1;
  }

  // test dotprod(BitBlock const&)
  if(b1.dotprod(b2) != 1) {
    cout << "(FAIL) Invalid dot product, line: " << __LINE__ << endl;
    return 1;
  }
  if(b2.dotprod(b1) != 1) {
    cout << "(FAIL) Invalid dot product, line: " << __LINE__ << endl;
    return 1;
  }
  if(b3.dotprod(b4) != 0) {
    cout << "(FAIL) Invalid dot product, line: " << __LINE__ << endl;
    return 1;
  }
  if(b4.dotprod(b3) != 0) {
    cout << "(FAIL) Invalid dot product, line: " << __LINE__ << endl;
    return 1;
  }
  if(b5.dotprod(b6) != 0) {
    cout << "(FAIL) Invalid dot product, line: " << __LINE__ << endl;
    return 1;
  }
  if(b6.dotprod(b5) != 0) {
    cout << "(FAIL) Invalid dot product, line: " << __LINE__ << endl;
    return 1;
  }

  //TODO: add a check for dotprod(char*,int) and dotprod_rev(char*,int)

  // Test: bool operator<(BitBlock const& b2) const
  // Create some BitBlock objects to be sorted
  boost::ptr_vector<BitBlock> sorted_list;
  boost::ptr_vector<BitBlock> unsorted_list;
  delete[] intarray;
  intarray = new uint[20];

  for(int j=0; j<2; j++) { // try two different bit lengths
    uint length;
    if(j==0) length = 391;
    else length = 20*32;
    for(int i=0; i<20; i++) intarray[i] = 0;
    unsorted_list.clear();
    sorted_list.clear();
    
    // Element 0
    intarray[12] = 255; // if length==391, this is equivalent to 127
    unsorted_list.push_back(new BitBlock(length, intarray));
    
    // Element 1
    intarray[5] = 23;
    intarray[12] = 0;
    unsorted_list.push_back(new BitBlock(length, intarray));
    
    // Element 2
    intarray[12] = 128; // if length==391, then this bit is outside the range
    unsorted_list.push_back(new BitBlock(length, intarray));
    if(length<=391) {
      if( unsorted_list[1] != unsorted_list[2] ) {
        cout << "(FAIL) Assertion fails, line: " << __LINE__ << endl;
        return 1;
      }
    } else {
      if( unsorted_list[2] < unsorted_list[1] ) {
        cout << "(FAIL) Assertion fails, line: " << __LINE__ << endl;
        return 1;
      }
    }
    
    // Element 3
    intarray[12] = 0;
    intarray[5] = 0;
    intarray[0] = 255;
    unsorted_list.push_back(new BitBlock(length, intarray));
    if( unsorted_list[0] < unsorted_list[3] ) {
      cout << "(FAIL) Assertion fails, line: " << __LINE__ << endl;
      return 1;
    }
    if( unsorted_list[1] < unsorted_list[3] ) {
      cout << "(FAIL) Assertion fails, line: " << __LINE__ << endl;
      return 1;
    }
    
    // Element4
    intarray[5] = 1;
    unsorted_list.push_back(new BitBlock(length, intarray));

    // copy the list
    sorted_list = unsorted_list;
    // sort
    sorted_list.sort();

    // Make sure the objects are sorted properly
    if(length<=391) {
      // Elements 1 and 2 are equal and we allow the sort to be "unstable"
      if( sorted_list[0] != unsorted_list[3] ||
          sorted_list[1] != unsorted_list[4] ||
          (sorted_list[2]!=unsorted_list[1] && 
           sorted_list[2]!=unsorted_list[2]) ||
          (sorted_list[3]!=unsorted_list[2] &&
           sorted_list[3]!=unsorted_list[1]) ||
          sorted_list[4] != unsorted_list[0] ) {
        cout << "(FAIL) Invalid sort, line: " << __LINE__ << endl;
        return 1;
      }

      // try sorting in decreasing order
      sorted_list.sort( std::greater<BitBlock>() );

      // Make sure the objects are sorted properly
      if( sorted_list[4] != unsorted_list[3] ||
          sorted_list[3] != unsorted_list[4] ||
          (sorted_list[2]!=unsorted_list[1] && 
           sorted_list[2]!=unsorted_list[2]) ||
          (sorted_list[1]!=unsorted_list[1] && 
           sorted_list[1]!=unsorted_list[2]) ||
          sorted_list[0] != unsorted_list[0] ) {
        cout << "(FAIL) Invalid sort, line: " << __LINE__ << endl;
        return 1;
      }
    }
    else {
      if( sorted_list[0] != unsorted_list[3] ||
          sorted_list[1] != unsorted_list[4] ||
          sorted_list[2] != unsorted_list[1] ||
          sorted_list[3] != unsorted_list[2] ||
          sorted_list[4] != unsorted_list[0] ) {
        cout << "(FAIL) Invalid sort, line: " << __LINE__ << endl;
        return 1;
      }

      // try sorting in decreasing order
      sorted_list.sort( std::greater<BitBlock>() );

      // Make sure the objects are sorted properly
      if( sorted_list[4] != unsorted_list[3] ||
          sorted_list[3] != unsorted_list[4] ||
          sorted_list[2] != unsorted_list[1] ||
          sorted_list[1] != unsorted_list[2] ||
          sorted_list[0] != unsorted_list[0] ) {
        cout << "(FAIL) Invalid sort, line: " << __LINE__ << endl;
        return 1;
      }
    }
  }

  // Test: const BitBlock::operator+(BitBlock const& b2) const
  int length = 545;
  delete[] intarray;
  intarray = new uint[18];
  for(int i=0; i<18; i++) intarray[i]=0;
  BitBlock zeroblock(length, intarray);

  intarray[2] = 42; intarray[3] = 1; intarray[17] = (1 << 31);
  BitBlock b7(length, intarray);
  BitBlock result = zeroblock + b7;
  if( result != b7 ) {
    cout << "(FAIL) Error in operator+, line: " << __LINE__ << endl;
    return 1;
  }

  if( zeroblock != b7+b7 ) {
    cout << "(FAIL) Error in operator+, line: " << __LINE__ << endl;
    return 1;
  }

  for(int i=0; i<18; i++) intarray[i]=0;
  intarray[0] = 12; intarray[2] = 40; intarray[3] = 999;
  intarray[17] = (1 << 31) + 1;
  BitBlock b8(length, intarray);
  intarray[0] = 12; intarray[2] = 2; intarray[3] = 998;
  intarray[17] = 1;
  result = BitBlock(length, intarray);

  if( b7+b8 != result ) {
    cout << "(FAIL) Error in operator+, line: " << __LINE__ << endl;
    return 1;
  }

  // Test: void BitBlock::operator+=(BitBlock const& b2)
  // (use previously defined objects)
  b7+= b8;
  if(b8 == result || b7 != result) {
    cout << "(FAIL) Error in operator+=, line: " << __LINE__ << endl;
    return 1;
  }

  // Test: int BitBlock::getMsbIndex() const
  delete[] intarray;
  intarray = new uint[17];
  for(int i=0; i<17; i++) intarray[i]=0;

  for(int j=0; j<3; j++) {
    int intindex;
    if(j==0) intindex = 0;
    else if(j==1) intindex = 9;
    else intindex = 16;

    for(int i=0; i<32; i++) {
      intarray[intindex] = 1 << i;
      if(i>0) intarray[intindex]++;
      BitBlock b(544, intarray);
      int msb = b.getMsbIndex();
      if( msb != (i+intindex*32) ) {
        cout << "(FAIL) Incorrect MSb index for i="<<i<<", intindex=";
        cout << intindex;
        cout <<", line: "<< __LINE__ << endl;
        return 1;
      }
      if( b[msb] != 1 ) {
        cout << "(FAIL) at line: "<< __LINE__ << endl;
        return 1;
      }
      for(uint k=msb+1; k<b.size(); k++) {
        if( b[k] != 0 ) {
          cout << "(FAIL) at line: " << __LINE__ << endl;
          return 1;
        }
      }
    }
  }

  // Test: double BitBlock::euclideanDistanceSq(double const* v) const

  // generate a random real vector of size 1007
  uint vectSize = 1007;
  NoiseGen rng;
  rng.setGaussian(0,1);
  double* v = new double[vectSize];
  double* v_shift = new double[vectSize];
  char* bitarray = new char[vectSize];

  // repeat a couple times
  for(uint repeat=0; repeat<100; repeat++) {
    if( rng.getBlock(vectSize, v) == NULL ) {
      cout << "(FAIL) Error in class NoiseGen, line: " << __LINE__ << endl;
    return 1;
    }
    // compute vector L2 norm squared
    double normSq = 0;
    for(uint i=0; i<vectSize; i++) {
      normSq+= v[i] * v[i];
    }
    // shift to BPSK domain
    for(uint i=0; i<vectSize; i++) {
      v_shift[i] = v[i] - 1;
    }
    
    // create 0-block of the same size
    BitBlock b12(vectSize);
    // distance to this 0-block should be equal to the norm
    double fpDelta = 0.000001; //note: this value not related to anything
    double distance = b12.euclideanDistanceSq(v_shift);
    if( !(distance < (normSq+fpDelta) && distance > (normSq-fpDelta)) ) {
      cout <<"(FAIL) "<<distance<<"!="<<normSq<<", line: "<< __LINE__ <<endl;
      return 1;
    }

    // create a random block from an array of char
    for(uint i=0; i<vectSize; i++) bitarray[i] = gsl_rng_get(prnd) & 1;
    BitBlock b13(vectSize, bitarray);

    // create bpsk-shifted version of the noise vector
    for(uint i=0; i<vectSize; i++) {
      if(bitarray[i]==1) {
        v_shift[i] = v[i]+1;
      } else if(bitarray[i]==0) {
        v_shift[i] = v[i]-1;
      } else {
        cout << "(FAIL) Error in test, line: "<< __LINE__ << endl;
        return 1;
      }
    }

    distance = b13.euclideanDistanceSq(v_shift);
    if( !(distance < (normSq+fpDelta) && distance > (normSq-fpDelta)) ) {
      cout <<"(FAIL) "<<distance<<"!="<<normSq<<", line: "<< __LINE__ <<endl;
      return 1;
    }
  }

  delete[] v;
  delete[] v_shift;
  delete[] bitarray;

  // --------- REPEAT everything above with bit array caching turned on ---------

  for(uint blocksize=1; blocksize<=5000; blocksize++) {
    //cout << "Testing block size " << blocksize << endl;

    // create a bit array
    char* bitarray = new char[blocksize];
    for(uint i=0; i<blocksize; i++) bitarray[i] = gsl_rng_get(prnd) & 1;
    // BitBlock from bit array
    BitBlock b1(blocksize, bitarray, true);

    // the bit array reversed
    char* bitarray_rev = new char[blocksize];
    for(uint i=0; i<blocksize; i++) bitarray_rev[i]= bitarray[blocksize-1-i];
    // BitBlock from reversed bit array
    BitBlock b11;
    b11.init_rev(blocksize, bitarray_rev, true);

    // pack bits into int's
    uint intsize = ceil( static_cast<float>(blocksize) / (8*sizeof(int)) );
    uint* intarray = new uint[intsize];
    for(uint i=0; i<intsize; i++) {
      int baseindex = i*32;
      intarray[i] = 0;
      for(int j=0; j < 32 && j < (blocksize-baseindex); j++) {
        intarray[i]+= (bitarray[baseindex + j] << j);
      }
    }
    // BitBlock from int array
    BitBlock b2(blocksize, intarray);

//     cout << "Content of b1:" << endl;
//     b1.print();
//     cout << "Content of b2:" << endl;
//     b2.print();

    // check size
    if( b1.size() != blocksize ) {
      cout << "(FAIL) Invalid size for b1, line: " << __LINE__ << endl;
      return 1;
    }
    if( b11.size() != blocksize ) {
      cout << "(FAIL) Invalid size for b11, line: " << __LINE__ << endl;
      return 1;
    }
    if( b2.size() != blocksize ) {
      cout << "(FAIL) Invalid size for b2, line: " << __LINE__ << endl;
      return 1;
    }

    // the two BitBlock's should be equals
    if( !(b1==b2) ) {
      cout << "(FAIL) BitBlocks are not equal, line: " << __LINE__ << endl;
      return 1;
    }
    if( !(b1==b11) ) {
      cout << "(FAIL) BitBlocks are not equal, line: " << __LINE__ << endl;
      return 1;
    }
    if( !(b11==b2) ) {
      cout << "(FAIL) BitBlocks are not equal, line: " << __LINE__ << endl;
      return 1;
    }
    if( b1 != b2 ) {
      cout << "(FAIL) != operator does not work, line: " << __LINE__ << endl;
      return 1;
    }
    // the number of bit errors should be 0
    if( b1.biterrorcnt(b2) != 0 ) {
      cout << "(FAIL) on line " << __LINE__ << endl;
      return 1;
    }
    
    // check that we can access individual bits
    for(uint i=0; i<blocksize; i++) {
      if( b2[i] != bitarray[i] ) {
        cout << "(FAIL) Invalid bit access for bit #"<<i << endl;
        cout << static_cast<int>(b2[i]) << " != ";
        cout << static_cast<int>(bitarray[i]) << endl;
        return 1;
      }
    }
    for(uint i=0; i<blocksize; i++) {
      if( b1[i] != bitarray[i] ) {
        cout << "(FAIL) Invalid bit access for bit #"<<i << endl;
        cout << static_cast<int>(b1[i]) << " != ";
        cout << static_cast<int>(bitarray[i]) << endl;
        return 1;
      }
    }

    // some more testing for the bit error counting
    // create another bit array
    char* bitarray2 = new char[blocksize];
    for(uint i=0; i<blocksize; i++) bitarray2[i] = gsl_rng_get(prnd) & 1;
    // BitBlock from bit array
    BitBlock b1_2(blocksize, bitarray2);

    int errors = b1.biterrorcnt(b1_2);
    if( errors != test_biterrorcnt(bitarray, bitarray2, blocksize) ) {
      cout << "(FAIL) Mismatch in bit error count (line ";
      cout << __LINE__ << ")" << endl;
      return 1;
    }

    // Test: void setbit(uint, char)
    // set some bits and make sure we can read them back
    int testsize = 50;
    if(blocksize < 50) testsize = blocksize / 3;
    if(testsize == 0) testsize = 1;
    // choose "testsize" number of bit indexes for the test
    int* index_list = new int[testsize];
    int* allindexes = new int[blocksize];
    for(uint i=0; i<blocksize; i++) allindexes[i] = i;
    gsl_ran_choose(prnd, index_list, testsize, allindexes, blocksize, 
                   sizeof(int));
    for(int i=0; i<testsize; i++) {
      b1.setbit(index_list[i], i & 1);
    }
    for(int i=0; i<testsize; i++) {
      char bit = b1[index_list[i]];
      if(bit != (i&1)) {
        cout << "(FAIL) Error in bit read (line ";
        cout << __LINE__ << ")" << endl;
        return 1;
      }
    }
    

    delete[] bitarray;
    delete[] bitarray2;
    delete[] bitarray_rev;
    delete[] intarray;
    delete[] index_list;
    delete[] allindexes;

  }

  // The following tests are performed with deterministic BitBlocks
  // Using objects named b21 to b24

  // Make sure that blocks that are not equal are reported as such
  bitarray = new char[6];
  bitarray[0]=0; bitarray[1]=1; bitarray[2]=0; bitarray[3]=1;
  bitarray[4]=0; bitarray[5]=1;
  BitBlock b21(6, bitarray, true);
  bitarray[0]=1; bitarray[1]=0; bitarray[2]=0; bitarray[3]=0;
  bitarray[4]=0; bitarray[5]=1;
  BitBlock b22(6, bitarray);
  if(b21 == b22) {
    cout << "(FAIL) Blocks are not equal, line: " << __LINE__ << endl;
    return 1;
  }
  if(b21.biterrorcnt(b22) != 3) {
    cout << "(FAIL) biterrorcnt, line: " << __LINE__ << endl;
    return 1;
  }
  std::vector<int> errorlocList;
  b21.errorLoc(bitarray, errorlocList);
  if(errorlocList.size()!=3 || errorlocList[0]!=0 || errorlocList[1]!=1 ||
     errorlocList[2]!=3) {
    cout << "(FAIL) errorLoc, line: " << __LINE__ << endl;
    return 1;
  }

  delete[] bitarray;
  bitarray = new char[33];
  for(uint i=0; i<33; i++) bitarray[i] = 0;
  bitarray[0]=1; bitarray[3]=1; bitarray[4]=1; bitarray[6]=1; bitarray[32]=1;
  BitBlock b23(33, bitarray, true);
  bitarray[32]=0;
  BitBlock b24(33, bitarray, true);
  if(b23 == b24) {
    cout << "(FAIL) Blocks are not equal, line: " << __LINE__ << endl;
    return 1;
  }
  if(b23.biterrorcnt(b24) != 1) {
    cout << "(FAIL) biterrorcnt, line: " << __LINE__ << endl;
    return 1;
  }
  errorlocList.clear();
  b23.errorLoc(bitarray, errorlocList);
  if(errorlocList.size()!=1 || errorlocList[0]!=32) {
    cout << "(FAIL) errorLoc, line: " << __LINE__ <<endl;
    return 1;
  }

  // test dotprod(BitBlock const&)
  if(b21.dotprod(b22) != 1) {
    cout << "(FAIL) Invalid dot product, line: " << __LINE__ << endl;
    return 1;
  }
  if(b22.dotprod(b21) != 1) {
    cout << "(FAIL) Invalid dot product, line: " << __LINE__ << endl;
    return 1;
  }
  if(b23.dotprod(b24) != 0) {
    cout << "(FAIL) Invalid dot product, line: " << __LINE__ << endl;
    return 1;
  }
  if(b24.dotprod(b23) != 0) {
    cout << "(FAIL) Invalid dot product, line: " << __LINE__ << endl;
    return 1;
  }

  delete[] bitarray;
  //TODO: add a check for dotprod(char*,int) and dotprod_rev(char*,int)

  // Test: double BitBlock::euclideanDistanceSq(double const* v) const

  // generate a random real vector of size 1007
  vectSize = 1007;
  //NoiseGen rng;
  //rng.setGaussian(0,1);
  v = new double[vectSize];
  v_shift = new double[vectSize];
  bitarray = new char[vectSize];

  // repeat a couple times
  for(uint repeat=0; repeat<100; repeat++) {
    if( rng.getBlock(vectSize, v) == NULL ) {
      cout << "(FAIL) Error in class NoiseGen, line: " << __LINE__ << endl;
    return 1;
    }
    // compute vector L2 norm squared
    double normSq = 0;
    for(uint i=0; i<vectSize; i++) {
      normSq+= v[i] * v[i];
    }
    // shift to BPSK domain
    for(uint i=0; i<vectSize; i++) {
      v_shift[i] = v[i] - 1;
    }
    
    // create 0-block of the same size
    BitBlock b12(vectSize, true);
    // distance to this 0-block should be equal to the norm
    double fpDelta = 0.000001; //note: this value not related to anything
    double distance = b12.euclideanDistanceSq(v_shift);
    if( !(distance < (normSq+fpDelta) && distance > (normSq-fpDelta)) ) {
      cout <<"(FAIL) "<<distance<<"!="<<normSq<<", line: "<< __LINE__ <<endl;
      return 1;
    }

    // create a random block from an array of char
    for(uint i=0; i<vectSize; i++) bitarray[i] = gsl_rng_get(prnd) & 1;
    BitBlock b13(vectSize, bitarray, true);

    // create bpsk-shifted version of the noise vector
    for(uint i=0; i<vectSize; i++) {
      if(bitarray[i]==1) {
        v_shift[i] = v[i]+1;
      } else if(bitarray[i]==0) {
        v_shift[i] = v[i]-1;
      } else {
        cout << "(FAIL) Error in test, line: "<< __LINE__ << endl;
        return 1;
      }
    }

    distance = b13.euclideanDistanceSq(v_shift);
    if( !(distance < (normSq+fpDelta) && distance > (normSq-fpDelta)) ) {
      cout <<"(FAIL) "<<distance<<"!="<<normSq<<", line: "<< __LINE__ <<endl;
      return 1;
    }
  }

  delete[] v;
  delete[] v_shift;
  delete[] bitarray;

  cout << "(PASS)" << endl;
  return 0;
} // end main

// Counts the number of differences between arrays a1 and a2.
int test_biterrorcnt(char* a1, char* a2, int size) {
  int count=0;
  for(int i=0; i<size; i++) {
    if(a1[i] != a2[i]) count++;
  }
  return count;
}
