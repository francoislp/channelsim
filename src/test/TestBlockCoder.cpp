//Author: Francois Leduc-Primeau

#include "coding/BlockCoder.hpp"
#include "coding/FileInputCoder.hpp"
#include "source/IncrementingSource.hpp"
#include "parsers/AListParser.hpp"
#include "parsers/AListElem.hpp"
#include "parsers/HParser.hpp"
#include "parsers/GParser.hpp"
#include "util/BitBlock.hpp"
#include "util/Exception.hpp"

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

typedef unsigned int uint;

//char bitVectMult(uint a, uint b);
//char bitVectMult(uint a, char* vect, int size);

// define USE_CWFILE to test pre-generated codewords taken from a file
//#define USE_CWFILE
//#define CWFILE_PATH "/Users/francois/no_backup/S_CODEWORD.txt"
//#define CWFILE_PATH "/Users/francois/no_backup/mytx425dB.txt"

// define to reverse the ordering of the columns of H (with respect to
// the usual convention in this program)
//#define REVERSE_H_BITORDER

// The number of codewords that will be generated and checked (when
// USE_CWFILE is not defined).
#define TESTCW_QTY 100

int main(int argc, char** argv) {

  if( argc != 3 ) {
    cout << "Usage: test_BlockCoder <H matrix> <G matrix>" << endl;
    return 1;
  }  

  char* file_H = argv[1];
  char* file_G = argv[2];
  cout << "Loading matrices from files:" << endl;
  cout << "    H: " << file_H << endl;
  cout << "    G: " << file_G << endl;

  try {

    // the code parity-check matrix
    cout << "Expecting H in A-list format" << endl;
    AListParser alp;
    alp.ParseFile(file_H);
    // get size
    int Hn = alp.GetColCount();
    int Hm = alp.GetRowCount();
    cout << "H is "<<Hm<<" x "<<Hn << endl;
    
    vector<AListElem> hElemList;
    alp.GetElements(hElemList);
    // copy to dense format
    char** H_tmp = new char*[Hm];
    for(int i=0; i<Hm; i++) H_tmp[i] = new char[Hn];
    // initialize all elements to 0
    for(int i=0; i<Hm; i++) {
      for(int j=0; j<Hn; j++) {
        H_tmp[i][j] = 0;
      }
    }
    // set some elements to 1
    for(uint i=0; i<hElemList.size(); i++) {
      int row = hElemList[i].GetRow();
      int col = hElemList[i].GetCol();
      H_tmp[row][col] = 1;
    }
    // pack into BitBlocks
    BitBlock* H = new BitBlock[Hm];
    for(int i=0; i<Hm; i++) {
#ifdef REVERSE_H_BITORDER
      H[i].init(Hn, H_tmp[i]);
#else
      H[i].init_rev(Hn, H_tmp[i]);
#endif
      delete[] H_tmp[i];
    }
    delete[] H_tmp;
    
    // the code generator matrix (transposed)
    GParser gp;
    BitBlock* G = gp.parseFile_block(file_G);
    int n = gp.getN();
    int k = gp.getK();
    cout << "G is "<<k<<" x "<<n << endl;
    // make sure the size is consistent with H
    if( n != Hn ) {
      cout << "(FAIL) Value of n for G and H does not match" << endl;
      return 1;
    }
    // note: k is not necessarily Hn - Hm
        
#ifndef USE_CWFILE
    // number of codewords that will be tested
    int nbsymbols = TESTCW_QTY;
    
    // test random codewords
    ExtendedSource src(k);
    src.genBlock(nbsymbols);

    BlockCoder coder(&src, k, n, Hm, G, H, false);
#else
    int nbsymbols=0;
    FileInputCoder coder(k, n, H, Hm, CWFILE_PATH);
#endif
    
    vector<BitBlock> codewords;
    int i=0;
    while(coder.hasNext()) {
      codewords.push_back( coder.nextSymbol() );
#ifndef USE_CWFILE
      if(++i > nbsymbols) cout << "Too many symbols !" << endl;
#endif
    }

#ifdef USE_CWFILE
    nbsymbols = codewords.size();
#endif

    // print all codewords
//     cout << "Mapping to codewords:" << endl;
//     for(int i=0; i < nbsymbols; i++) {
//       cout << i << " -> ";
//       codewords[i].print();
//     }
    
    // check the parity of each codeword
    // get H from BlockCoder instead of using the copy we already have
    BitBlock* H2 = coder.getH();
    int errcnt = 0;
    for(int i=0; i < nbsymbols; i++) {
//       int test = errcnt;
      // do each parity check
      for(int j=0; j<Hm; j++) {
        if( codewords[i].dotprod(H2[j]) != 0) errcnt++;
      }
//       if( errcnt != test ) {
//         cout << "codeword #" << i << " fails" << endl;
//       }
    }
    int nbchecks = Hm*nbsymbols;
    if(errcnt > 0) {
      cout << errcnt << "/" << nbchecks << " parity checks failed !" << endl;
      return 1;
    }
    
    cout << "(PASS) All "<< nbchecks <<" parity checks passed" << endl;

    delete[] H;
    delete[] G;

  } catch(Exception& e) {
    cout << "An exception occurred: " << endl;
    cout << e << endl;
  }

  return 0;
} // end main

// char bitVectMult(uint a, uint b) {
//   uint sum = 0;
//   uint mask = 1;

//   for(uint i=0; i < 8*sizeof(uint); i++) {
//     uint tmp1 = a & mask;
//     uint tmp2 = b & mask;
//     tmp1>>= i;
//     tmp2>>= i;
//     sum+= tmp1 & tmp2;
//     // update the mask
//     mask<<= 1;
//   }
//   return sum % 2;
// }

// (same function as in BlockCoder.cpp)
// *** vect[size-1] is the lsb ***
// char bitVectMult(uint a, char* vect, int size) {
//   uint sum=0;
//   uint mask = 1;
//   // for each bit in vect, we isolate the corresponding bit in a and
//   // multiply the two
//   for(int i=0; i<size; i++) {
//     uint tmp = a & mask; // tmp has at most one bit = 1
//     tmp>>= i; // tmp is now 0 or 1
//     sum+= vect[size-1-i] & tmp;
//     // update the mask
//     mask<<= 1;
//   }
//   // operations are binary => addition is mod 2
//   //return sum % 2;
//   return sum & 1;
// }
