//Author: Francois Leduc-Primeau

/*
 * Test for BlockDecoder. This test must be run after TestBlockCoder.cpp
 */

#include "../GlobalDefs.h"
#include "../NoiseGen.hpp"
#include "../IncrementingSource.hpp"
#include "../BlockCoder.hpp"
#include "../BPSKMod.hpp"
#include "../BPSKDemod.hpp"
#include "../BlockDecoder.hpp"
#include "../Exception.hpp"
#include "../Logging.hpp"
#include "../gabi/AListParser.hpp"
#include "../gabi/AListElem.hpp"
#include "../GParser.hpp"

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::ofstream;
using std::vector;

int main(int argc, char** argv) {
  Logging::init(0);

  // (Expected values)
  int n = 7;
  int k = 4;

  // try with a high SNR
  //double No = 0.1;
  double No = 0.4; // not-as-good SNR

  // Load the H matrix
  AListParser parserH;
  std::string filename = "matrices/Hamming7-4_H";
#ifdef DEBUG
    ofstream* pLog = Logging::getstream();
    (*pLog) << "Reading H matrix from " << filename << endl;
#endif
  parserH.ParseFile(filename);
  vector<AListElem> hElemList;
  parserH.GetElements(hElemList);
  int Hm = parserH.GetRowCount();
  int Hn = parserH.GetColCount();

#ifdef DEBUG
    // check whether dimensions match expected values
    if(Hn != n || Hm != (n-k))
      cout << "(channelsimMPI) Assertion failed: H dimensions" << endl;
#endif

  // copy to dense format
  char** H_tmp = new char*[n-k];
  for(int i=0; i<(n-k); i++) H_tmp[i] = new char[n];
  // initialize all elements to 0
  for(int i=0; i<(n-k); i++) {
    for(int j=0; j<n; j++) {
      H_tmp[i][j] = 0;
    }
  }
  // set some elements to 1
  for(uint i=0; i<hElemList.size(); i++) {
    int row = hElemList[i].GetRow();
    int col = hElemList[i].GetCol();
    H_tmp[row][col] = 1;
  }
  // pack into BitBlocks
  BitBlock* H = new BitBlock[n-k];
  for(int i=0; i<(n-k); i++) {
    H[i].init_rev(n, H_tmp[i]);
    delete[] H_tmp[i];
  }
  delete H_tmp;

  // Load the G matrix
  GParser gp;
  char* matrixfilename = "matrices/Hamming7-4_G";
#ifdef DEBUG
    ofstream* pLog = Logging::getstream();
    (*pLog) << "Reading G matrix from " << matrixfilename << endl;
#endif
  BitBlock* G = gp.parseFile_block(matrixfilename);
#ifdef DEBUG
    // check whether dimensions match expected values
    if( gp.getN() != n || gp.getK() != k )
      cout << "(channelsimMPI) Assertion failed: G dimensions" << endl;
#endif

  //debug: print-out the H matrix
  cout << "The H matrix:" << endl;
  for(int i=0; i<(n-k); i++) {
    for(int j=0; j<n; j++) {
      cout << static_cast<int>(H[i][n-1-j]) << "  ";
    }
    cout << endl;
  }
  cout << "----------" << endl;

  //debug: print-out the G matrix
  cout << "The G matrix:" << endl;
  for(int i=0; i<k; i++) {
    for(int j=0; j<n; j++) {
      cout << static_cast<int>(G[j][k-1-i]) << "  ";
    }
    cout << endl;
  }
  cout << "----------" << endl;

  try {
    IncrementingSource src(k);    // source
    BlockCoder coder(&src, k, n, G, H); // code
    BPSKMod mod(&coder);          // modulation
    BPSKDemod demod(&mod, No);   // demodulation
    BlockDecoder decoder(&coder, &demod, MSGTYPE_LLR); // decoding(being tested)
    
    // noise
    NoiseGen ngen;
    ngen.setNormalize(true); // turn on block normalization
    // variance = No/(2R), where R=k/n is the code rate
    ngen.setGaussian(0, No/2 * n/k);    

    int nbblocks = 100;
    int nbbits = nbblocks * n; // nb of *transmitted* bits
    src.genBlock(nbblocks);
    ngen.prepareNoise(nbbits);
    double* noise = ngen.getBlock();
    // add the modulated samples to the demodulator
    for(int i=0; i<nbbits; i++) {
      demod.addSample1D( mod.nextElem1D() + noise[i] );
    }

    // decode and print all values
    BitBlock* decodedvals = new BitBlock[nbblocks];
    for(int i=0; i<nbblocks; i++) {
      decodedvals[i] = decoder.nextSymbol();
#ifdef DEBUG
      decodedvals[i].print();
#endif
    }
    cout << endl;
    
    // compare original codewords with decoded codewords
    int errors = src.errors(decodedvals, nbblocks);
    
//TODO: should also test these other functions:
    //int errors = src.errors(&decoder);
    //int biterrors = src.biterrors(&decoder);

    cout << "-- " << errors << " symbol errors" << endl;

    delete[] decodedvals;

  } catch(Exception& e) {
    cout << e << endl;
  }

  delete[] H;
  delete[] G;
  
  return 0;
}
