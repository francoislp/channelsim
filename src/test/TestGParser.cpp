//Author: Francois Leduc-Primeau

#include <iostream>
#include "../GParser.hpp"
#include "../Exception.hpp"

using std::cout;
using std::endl;

#define CODE_N 7
#define SRC_K  4

int main(int argc, char** argv) {

  char* filename = "matrices/Hamming7-4_G";
  GParser p;
  char** G;
  try {
    G = p.parseFile(filename);
  } catch(Exception* e) {
    cout << e << endl;
    return 1;
  }

  // hardcoded H matrix used to compare
  // the code generator matrix (transposed)
  char** G2 = new char*[CODE_N];
  char tmp[SRC_K]  = {1, 0, 1, 1};
  G2[0] = tmp;
  char tmp1[SRC_K] = {1, 1, 1, 0};
  G2[1] = tmp1;
  char tmp2[SRC_K] = {0, 1, 1, 1};
  G2[2] = tmp2;
  char tmp3[SRC_K] = {1, 0, 0, 0};
  G2[3] = tmp3;
  char tmp4[SRC_K] = {0, 1, 0, 0};
  G2[4] = tmp4;
  char tmp5[SRC_K] = {0, 0, 1, 0};
  G2[5] = tmp5;
  char tmp6[SRC_K] = {0, 0, 0, 1};
  G2[6] = tmp6;

  // compare the parsed matrix with the hardcoded one
  int mismatch = 0;
  for(int i=0; i<CODE_N; i++) {
    for(int j=0; j<SRC_K; j++) {
      if(G[i][j] != G2[i][j]) mismatch++;
    }
  }

  if(mismatch>0) cout <<"(FAIL) "<< mismatch << " elements don't match" << endl;
  else cout << "(PASS) All elements match !" << endl;

  return 0;
}
