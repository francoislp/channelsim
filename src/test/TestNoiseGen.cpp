#include "channel/NoiseGen.hpp"

#include <fstream>
#include <gsl/gsl_rng.h>

using std::ofstream;
using std::endl;

int main(int argc, char** argv) {

	NoiseGen ng;

	// test the uniform RNG used for the Rayleigh distribution
	ofstream ofsUni("test_NoiseGen_uniform.txt");
	gsl_rng* prng= gsl_rng_alloc(gsl_rng_ranlxd2);
	for(int i=0; i<100000; i++) {
		ofsUni<< gsl_rng_uniform_pos(prng) << endl;
	}
	ofsUni.close();
	
	// test Rayleigh distribution
	ofstream ofs("test_NoiseGen_rayleigh.txt");
	int blockSize= 1000;
	double* vect= new double[blockSize];
	for(int i=0; i<100; i++) {	
		vect= ng.getRayleighBlock(blockSize, vect);
		for(int j=0; j<blockSize; j++) ofs << vect[j] << endl;
	}
	ofs.close();
}
