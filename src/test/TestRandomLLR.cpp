//Author: Francois Leduc-Primeau

#include "IRandomLLR.h"
#include "RandomLLRFloat.h"
#include "RandomLLR_LFSR.h"

#ifdef TEST_SPRHS
#include "../ws2012/sp-rhs/src/simulator/RNG.hpp"
#endif

#include <iostream>
#include <vector>
#include <math.h>

using std::cout;
using std::endl;
using std::vector;

int main() {
  // Create objects
  vector<IRandomLLR*> rngList;
  rngList.push_back(new RandomLLRFloat(0.25));
  int seed = 0;
  for(uint i=0; i<10; i++) {
    rngList.push_back(new RandomLLR_LFSR(LfsrPrng::FIBO, 10, seed++, 
                                         false, true));
  }
#ifdef TEST_SPRHS
  seed = 0;
  vector<RNG*> sprhsRngList;
  for(uint i=0; i<55; i++) {
    sprhsRngList.push_back(new RNG(seed++));
  }
#endif

  // reset all RNGs
  for(uint i=0; i<rngList.size(); i++) rngList[i]->reset();
#ifdef TEST_SPRHS
  for(uint i=0; i<sprhsRngList.size(); i++) sprhsRngList[i]->reset();
#endif

  // print first values
  uint zeroCount;
  for(uint i=0; i<rngList.size(); i++) {

    zeroCount = 0;
    cout << "RNG #"<<i<<": ";

    for(uint j=0; j<20; j++) {
      if(j!=0) cout << ", ";
      float curThresh = rngList[i]->getCurVal();
      // if(i==0) curThresh = roundf(2.0f*curThresh)*0.5f;
      if(i==0) curThresh = roundf(curThresh);
      cout << curThresh;
      if(curThresh<0.0001) zeroCount++;
      rngList[i]->nextTick();
    }
    cout << "   ("<<zeroCount<<" zeros)";
    cout << endl;
  }
#ifdef TEST_SPRHS
  for(uint i=0; i<sprhsRngList.size(); i++) {
    zeroCount = 0;
    cout << "sp-rhs RNG #"<<i<<": ";

    for(uint j=0; j<20; j++) {
      if(j!=0) cout << ", ";
      float curThresh = sprhsRngList[i]->get_rand_threshold();
      cout << curThresh;
      if(curThresh<0.0001) zeroCount++;
      sprhsRngList[i]->update();
    }
    cout << "   ("<<zeroCount<<" zeros)";
    cout << endl;
  }
#endif
  
  return 0;
}
