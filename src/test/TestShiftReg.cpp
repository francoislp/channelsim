//Author: Francois Leduc-Primeau

#include "ShiftReg.h"
#include <iostream>
#include <stdlib.h>

using std::cout;
using std::endl;

int main(void) {

  for(int size=24; size<60; size++) {

    ShiftReg sr(size);
    
    // write an alternating sequence
    char* sequence = new char[size];
    for(int i=0; i<size; i++) {
      sequence[i] = i & 1;
      sr.push(sequence[i]);
    }
    // verify
    for(int i=0; i<size; i++) {
      if(sr[i] != sequence[size-1-i]) {
        cout << "(FAIL) Test fails at line " << __LINE__ << endl;
        return 1;
      }
    }
    
    // write another sequence
    char curBit = 1;
    for(int i=0; i<size; i++) {
      if( (i+1) % 3 ) curBit = !curBit;
      sequence[i] = curBit;
      sr.push(sequence[i]);
    }
    // verify
    for(int i=0; i<size; i++) {
      if(sr[i] != sequence[size-1-i]) {
        cout << "(FAIL) Test fails at line " << __LINE__ << endl;
        return 1;
      }
    }
    
    // write some bits
    for(int i=0; i<5; i++) sr.push(1);
    // make sure the other bits didn't change
    for(int i=5; i<(size-5); i++) {
      if(sr[i] != sequence[size-1-i+5]) {
        cout << "(FAIL) Test fails at line " << __LINE__ << endl;
        return 1;
      }
    }

  // test the ShiftReg with offset
#define OFFSET 5
    ShiftReg sr2(size, OFFSET);

    // write a random sequence
    for(int i=0; i<size; i++) {
      sequence[i] = random() & 01;
      sr2.push(sequence[i]);
    }
    // verify
    for(int i=0; i<size; i++) {
      int refindex = size-1-i-OFFSET;
      if(refindex<0) refindex = size+refindex;
      if(sr2[i] != sequence[refindex]) {
        cout << "(FAIL) Test fails at line " << __LINE__ << endl;
        return 1;
      }
    }
    
    // write another random sequence
    for(int i=0; i<size; i++) {
      sequence[i] = random() & 01;
      sr2.push(sequence[i]);
    }
    // verify
    for(int i=0; i<size; i++) {
      int refindex = size-1-i-OFFSET;
      if(refindex<0) refindex = size+refindex;
      if(sr2[i] != sequence[refindex]) {
        cout << "(FAIL) Test fails at line " << __LINE__ << endl;
        return 1;
      }
    }
    
    cout << "(PASS) test_ShiftReg" << endl;
    return 0;

  }
}
