#include "StochMixedDRE.h"

#include <iostream>
#include "Config.h"

using std::cout;
using std::endl;

typedef unsigned int uint;

int main(int argc, char** argv) {

  cout << "Note: Expecting a test configuration file \"testconf.txt\"" << endl;
  Config conf;
  try {
    conf.parseFile("testconf.txt");
    //    if(outfilename!="") conf.setOutputFilename(outfilename);
  } catch(Exception& e) {
    cout << "Exception occured while parsing config file:" << endl;
    cout << e << endl;
    return 1;
  }

  uint trials = 1000000; // for each monte-carlo test

  cout << "--- Mixed distribution test ---" << endl;
  try {
    float t = 0.01;
    StochMixedDRE* dre = new StochMixedDRE(t);
    dre->resetRNG();
    
    uint discrete_t_count = 0;
    uint discrete_tUpper_count = 0;
    uint lowerbin_count = 0;
    
    for(uint i=0; i<trials; i++) {
      float curVal = dre->getVal_f();
      dre->nextTick();
      
      if(curVal > t && curVal < (1-t)) {
        cout << "(FAIL) Value returned is in the zero-probability range";
        cout << " (TestStochMixedDRE.cpp:"<< (__LINE__ - 2) << ")" << endl;
        return 1;
      } else if(curVal <= 0 || curVal >= 1) {
        cout << "(FAIL) Value returned is in the zero-probability range";
        cout << " (TestStochMixedDRE.cpp:"<< (__LINE__ - 2) << ")" << endl;
        return 1;
      }

      if(curVal == t) discrete_t_count++;
      else if(curVal == (1-t)) discrete_tUpper_count++;
      else if(curVal>0 && curVal<t) lowerbin_count++;
    }
    
    cout << "Discrete t and (1-t) counts (expected: "<< (0.5-t)*trials << ")" <<endl;
    cout << "    " << discrete_t_count << endl;
    cout << "    " << discrete_tUpper_count << endl;
    cout << "Lower bin count (expected: " << t*trials << ")" << endl;
    cout << "    " << lowerbin_count << endl;

    cout << "--- Uniform distribution test ---" << endl;
    t = 0.5;
    dre->setThreshold(t); // this should re-create the behavior of StochDRE
    dre->resetRNG();

    uint bin1Count = 0;
    uint bin2Count = 0;
    uint bin3Count = 0;
    uint bin4Count = 0;

    for(uint i=0; i<trials; i++) {
      float curVal = dre->getVal_f();
      dre->nextTick();

      if(curVal <= 0 || curVal >= 1) {
        cout << "(FAIL) Value returned is in the zero-probability range";
        cout << " (TestStochMixedDRE.cpp:"<< (__LINE__ - 2) << ")" << endl;
        return 1;
      }
      
      if(curVal < 0.5) bin1Count++;
      if(curVal > 0.5) bin2Count++;
      if(curVal > 0.4 && curVal < 0.6) bin3Count++;
      if(curVal < 0.2) bin4Count++;
    }

    cout << ">0.5 and <0.5 counts (expected: " << trials/2 << ")" << endl;
    cout << "    "<< bin1Count << endl;
    cout << "    "<< bin2Count << endl;
    cout << "Center bin and lower bin (expected: " << trials/5 << ")" << endl;
    cout << "    "<< bin3Count << endl;
    cout << "    "<< bin4Count << endl;

  } catch(...) {
    cout << "(FAIL) Exception caught." << endl;
    return 1;
  }

  return 0;
}
