#include "ITFM.h"
#include "TFMFP.h"
#include "FeedbackTFMFP.h"
#include "StochDRE.h"

#include <iostream>

#define TESTTFM_BETA 0.03125

using std::cout;
using std::endl;

void testStaticStream(ITFM& tfm);

int main(int argc, char** argv) {

  cout << "----- Testing TFMFP -----" << endl;
  IStochDRE* pDRE = new StochDRE();
  TFMFP tfmfp(TESTTFM_BETA, pDRE);
  testStaticStream(tfmfp);

  cout << "----- Testing FeedbackTFMFP -----" << endl;
  FeedbackTFMFP fdbktfm(TESTTFM_BETA, pDRE);
  testStaticStream(fdbktfm);

  delete pDRE;
  return 1;
}

void testStaticStream(ITFM& tfm) {
  for(double prob=0; prob<=1; prob+= 0.1) {
    tfm.init(prob);

    // generate a stochastic stream using the TFM and measure its average
    int sum = 0;
    int length = 10000;
    for(int i=0; i<length; i++) {
      tfm.getDRE()->nextTick();
      sum+= tfm.getrnd();
    }
    float measuredProb = static_cast<float>(sum) / length;
    cout << "Expected = " << prob << ", Measured = " << measuredProb << endl;
  }
}
