//Author: Francois Leduc-Primeau
/**
 * Generates test vectors for a stochastic VN.
 * Currently only RHS is supported, and only k=2.
 */

#include "Node.hpp"
#include "Config.hpp"
#include "Logging.hpp"
#include "Exception.hpp"
#include "GlobalDefs.h"

#include "RandomLLR_LFSR.hpp"
#include "RandomLLR_Cycle0Adaptor.hpp"
#include "RandomLLR_CycleAdaptor.hpp"
#include "RandomLLRFloat.hpp"

#include <iostream>
#include <ostream>
#include <fstream>
#include <string>
#include <vector>
#include <bitset>
#include <stdlib.h>
#include <unistd.h>

#define MAXRAND 2147483647  // 2^31 - 1

using namespace std;

void printVNMessages(ostream& ostr, vector<SingleBitCN*>& cnList);
void sendtoVN(SingleBitVN* vnMsgInterface, char* msgList, uint d_v);
char randomMsg(float p); // generate all message bits with prob p
char randomBit(float p); // generate a random bit with prob p
float randomP(); // generate a random probability value
void readNextMsgs(ifstream* str, char* msgs, uint d_v);

void usageExit(int exitCode) {
  cout << "Usage: test_VNStoch -d<vn deg> [-L<llr_in>] [-s<seed>] [-i<nb_iter>] --config <conf file>";
  cout << " [--vectIn <filename>] [--vectOut <filename>]" << endl;
  cout << " [--readVectIn <filename>]" << endl;
  exit(exitCode);
}

int main(int argc, char** argv) {
  if(argc < 3) {
    usageExit(0);
  }

  // parse all the arguments
  char* configfile = 0;
  char* filenameVectIn = 0;
  char* filenameVectOut = 0;
  bool doGenerateVectIn = true; // whether to read input vectors or generate them
  uint d_v = 0;
  double inputLLR = 0;
  int hwRNGSeed = 0;
  uint iterLimit = 100;
  vector<float> probDist; // distributions of the VN input messages
  for(int i=1; i<argc; i++) {
    if( strcmp(argv[i], "--config") == 0 ) {
      if(++i < argc) configfile = argv[i];
      else {
        cerr << "Invalid arguments" << endl;
        return 1;
      }
    } else if( strncmp(argv[i], "-d", 2) == 0 ) {
      d_v = atoi(argv[i]+2);
    } else if( strcmp(argv[i], "--vectIn") == 0 ) {
      if( filenameVectIn!=0 ) {
        cerr << "Can't specify both --vectIn and --readVectIn" << endl;
        return 1;
      }
      if(++i < argc) filenameVectIn = argv[i];
      else {
        cerr << "Invalid arguments" << endl;
        return 1;
      }
      doGenerateVectIn = true;
    } else if( strcmp(argv[i], "--vectOut") == 0 ) {
      if(++i < argc) filenameVectOut = argv[i];
      else {
        cerr << "Invalid arguments" << endl;
        return 1;
      }
    } else if( strncmp(argv[i], "-L", 2) == 0 ) {
      inputLLR = atof(argv[i]+2);
    } else if( strncmp(argv[i], "-s", 2) == 0 ) {
      hwRNGSeed = atoi(argv[i]+2);
    } else if( strncmp(argv[i], "-i", 2) == 0 ) {
      iterLimit = atoi(argv[i]+2);
    } else if( strcmp(argv[i], "--readVectIn") == 0) {
      if( filenameVectIn!=0 ) {
        cerr << "Can't specify both --vectIn and --readVectIn" << endl;
        return 1;
      }
      if(++i < argc) filenameVectIn = argv[i];
      else {
        cerr << "Invalid arguments" << endl;
        return 1;
      }
      doGenerateVectIn = false;
      cerr << "Warning: Only the VN messages are read back." << endl; //(TODO)
    } else {
      cerr << "Unrecognized option: "<<argv[i] << endl;
      return 1;
    }
  }

  if(configfile==0) {
    cerr << "Config file not specified!" << endl;
    usageExit(1);
  }
  if(d_v==0) {
    cerr << "VN degree not specified!" << endl;
    usageExit(1);
  }

  // Output streams for the vector files
  ostream* ostrVectIn = 0;
  ifstream* ifstrVectIn = 0; // input stream if reading existing input vectors
  ostream* ostrVectOut;
  // we only need an output stream for input vectors if we are generating them
  if( doGenerateVectIn ) {
    if(filenameVectIn!=0) {
      ostrVectIn = new ofstream(filenameVectIn);
      if(!ostrVectIn->good()) {
        cerr << "Error opening output file "<<filenameVectIn<<endl;
        return 1;
      }
    } else {
      ostrVectIn = &cout;
    }
  } else { // reading existing input vectors
    ifstrVectIn = new ifstream(filenameVectIn);
    if(!ifstrVectIn->good()) {
      cerr << "Error opening existing input-vector file for reading ";
      cerr << filenameVectIn << endl;
      return 1;
    }
  }
  if(filenameVectOut!=0) {
    ostrVectOut = new ofstream(filenameVectOut);
    if(!ostrVectOut->good()) {
      cerr << "Error opening output file "<<filenameVectOut<<endl;
      return 1;
    }
  } else {
    ostrVectOut = &cout;
  }

  // Confirm parameters
  cout << "Using: degree="<<d_v<<", input LLR="<<inputLLR<<", RNG seed=";
  cout << hwRNGSeed<<", nb of iter="<<iterLimit<< endl;

  Config conf;
  try {
    conf.parseFile(configfile);
  } catch(Exception& e) {
    cerr << "Exception occured while parsing config file:" << endl;
    cerr << e << endl;
    return 1;
  }

  // log file init
  Logging::init(0);

  // Create a Variable Node (code from BlockDecoder::generateVarNodes)
  bool isRHS = false; // Is this a RHS decoder?
  try {
    if(conf.getPropertyByName<string>("decoder_subtype") == "half-stochastic") {
      isRHS = true;
    }
  } catch(InvalidKeyException&) {}
  // If it is a RHS decoder, which DRE type are we using?
  bool usingHWDRE = false;
  if(isRHS) {
    try {
      string dreType = conf.getPropertyByName<string>("stoch_dretype");
      if(dreType == "float") usingHWDRE = false;
      else if(dreType == "hwlfsr") usingHWDRE = true;
      else {
        throw Exception("BlockDecoder::generateVarNodes", "Invalid DRE type");
      }
    } catch(InvalidKeyException&) {}
  } else {
    cerr << "Non-RHS decoder not currently supported" << endl;
    return 1;
  }

  vector<IRandomLLR*> dreListRHS; // holds the DRE's for one VN
  uint drelistSize = conf.getPropertyByName<uint>("stoch_parallel_width");

  // populate the DRE list
  if(usingHWDRE) {
    IRandomLLR* basedre = new RandomLLR_LFSR(LfsrPrng::FIBO, 7, hwRNGSeed);
    // the first "cycle" needs a special adaptor
    IRandomLLR* dreinst = new RandomLLR_Cycle0Adaptor(basedre);
    dreListRHS.push_back(dreinst);
    for(uint j=1; j<drelistSize; j++) {
      // Other DREs represent the clk cycle after the previous
      // DRE in the list
      dreinst = new RandomLLR_CycleAdaptor(basedre);
      dreListRHS.push_back(dreinst);
    }
  } else {
    for(uint j=0; j<drelistSize; j++) {
      IRandomLLR* dreinst = new RandomLLRFloat();
      dreListRHS.push_back(dreinst);
    }
  }
  
  // Create the RHS VN
  VariableNode* vn = new VariableNode(0, d_v, dreListRHS, NULL);

  // Create dummy check nodes that will receive the VN messages
  vector<SingleBitCN*> cnList;
  for(uint i=0; i<d_v; i++) {
    vector<VariableNode*> curVNList;
    curVNList.push_back(vn);
    cnList.push_back(new SingleBitCN(1, conf.getMsgType(), curVNList,
                                     NULL));
  }

  // post-graph-setup VN init
  vn->baseInit();

  // Retrieve the iteration number where we switch the value of "b"
  uint gearShiftIter = conf.getPropertyByName<uint>("rhs_beta_thresh_1");


  // ----- Now we start using the VN -----

  // Reset the DREs
  for(uint i=0; i<dreListRHS.size(); i++) dreListRHS[i]->reset();
  // Set the input LLR
  vn->init(inputLLR); //note: resets internal iteration count

  // first "iteration" is special (VN receives no messages)
  bitset<4> inputLLRBits(inputLLR);

  vn->broadcast();
  // Move the DRE's to the next clock tick
  for(uint i=0; i<dreListRHS.size(); i++) dreListRHS[i]->nextTick();
  // Expected output (see rhs-vhdl/doc/VN_test_fileformat.txt)
  *ostrVectOut << static_cast<int>(vn->currentDecision()) << " ";
  printVNMessages(*ostrVectOut, cnList);

  // use "vnMsgInterface" to send messages to the VN
  AbstractVariableNode* abstrVN = vn->getImplementation();
  SingleBitVN* vnMsgInterface = dynamic_cast<SingleBitVN*>(abstrVN);

  // Iterations after first
  srandom(getpid()); // seed based on process id
  char* msgBuf = new char[d_v];

  if(doGenerateVectIn) {
    // generate random distribution parameters
    for(uint j=0; j<d_v; j++) {
      probDist.push_back(randomP());
    }
    // Confirm the distribution
    cout << "Input message distributions: ";
    cout << "{"<<probDist[0];  
    for(uint j=1; j<d_v; j++) {
      cout << ", "<<probDist[j];
    }
    cout << "}" << endl;
  }

  for(uint i=1; i<iterLimit; i++) {

    if(doGenerateVectIn) {
      // print input vector (see rhs-vhdl/doc/VN_test_fileformat.txt)
      string gearShiftTrig;
      if(i==gearShiftIter) gearShiftTrig = "1";
      else gearShiftTrig = "0";
      *ostrVectIn << inputLLRBits << " " << gearShiftTrig;
      // random input messages
      for(uint j=0; j<d_v; j++) {
        msgBuf[j] = randomMsg(probDist[j]);
        bitset<2> curMsg(msgBuf[j]);
        *ostrVectIn << " "<< curMsg;
      }
      *ostrVectIn << endl;
    } else {
      readNextMsgs(ifstrVectIn, msgBuf, d_v); // into msgBuf
    }

    sendtoVN(vnMsgInterface, msgBuf, d_v);
    vn->broadcast();
    // Move the DRE's to the next clock tick
    for(uint i=0; i<dreListRHS.size(); i++) dreListRHS[i]->nextTick();

    // print expected output
    *ostrVectOut << static_cast<int>(vn->currentDecision()) << " ";
    printVNMessages(*ostrVectOut, cnList);
  }

  // close files
  if(filenameVectIn!=0 && doGenerateVectIn) {
    ofstream* curStr = dynamic_cast<ofstream*>(ostrVectIn);
    curStr->close();
  } else if(filenameVectIn!=0) {
    ifstrVectIn->close();
  }
  if(filenameVectOut!=0) {
    ofstream* curStr = dynamic_cast<ofstream*>(ostrVectOut);
    curStr->close();
  }

  return 0;
}

void printVNMessages(ostream& ostr, vector<SingleBitCN*>& cnList) {
  // since the neighbor CNs have degree 1, their "parity" is the bit
  // that was sent (in this case it is a "parity vector")
  if(cnList.size()>0) {
    char parityVector = cnList[0]->curParity();
    bitset<2> curBits(parityVector);
    ostr << curBits;
  }
  for(uint i=1; i<cnList.size(); i++) {
    char parityVector = cnList[i]->curParity();
    bitset<2> curBits(parityVector);
    ostr << " " << curBits;
  }
  ostr << endl;
}

void sendtoVN(SingleBitVN* vnMsgInterface, char* msgList, uint d_v) {
  for(uint i=0; i<d_v; i++) {
    vnMsgInterface->receive(msgList[i], i);
  }
}

char randomMsg(float p) {
  return (randomBit(p)<<1) | randomBit(p);
}

char randomBit(float p) {
  float rndp = randomP();
  if(rndp<p) return 1;
  return 0;
}

float randomP() {
  return static_cast<float>(random()) / MAXRAND;
}

// Reads input messages from an input-vector file
void readNextMsgs(ifstream* str, char* msgBuf, uint d_v) {
  // skip <Li> and <next_b>
  int tmp1, tmp2;
  *str >> tmp1 >> tmp2;
  for(uint i=0; i<d_v; i++) {
    int x;
    *str >> x;
    msgBuf[i] = x;
  }
}
