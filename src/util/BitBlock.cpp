//Author: Francois Leduc-Primeau

#include "BitBlock.hpp"
#include "Exception.hpp"

#include <math.h>
#include <iostream> //debug

// macros for bit counting
#define TWO(c)     (0x1u << (c))
#define MASK(c)    (((unsigned int)(-1)) / (TWO(TWO(c)) + 1u))
#define COUNT(x,c) ((x) & MASK(c)) + (((x) >> (TWO(c))) & MASK(c))

using std::vector;

BitBlock::BitBlock()
  : m_bitsize(0),
    m_intsize(0),
    m_intarray(0),
    m_bitarray(0)
{}

BitBlock::BitBlock(BitBlock const& b2) 
  : m_intarray(0),
    m_bitarray(0)
{
  init(b2.size(), b2.m_intarray);
}

BitBlock::BitBlock(uint bitsize)
  : m_intarray(0),
    m_bitarray(0)
{
  init(bitsize, false);
}

BitBlock::BitBlock(uint bitsize, bool keepBitArray)
  : m_intarray(0),
    m_bitarray(0)
{
  init(bitsize, keepBitArray);
}

BitBlock::BitBlock(uint bitsize, char* bitarray) 
  : m_intarray(0),
    m_bitarray(0)
{
  init(bitsize, bitarray, false);
}

BitBlock::BitBlock(uint bitsize, char* bitarray, bool keepBitArray)
  : m_intarray(0),
    m_bitarray(0)
{
  init(bitsize, bitarray, keepBitArray);
}

BitBlock::BitBlock(uint bitsize, uint* array) 
  : m_intarray(0),
    m_bitarray(0)
{
  init(bitsize, array);
}

BitBlock::~BitBlock() {
  delete[] m_intarray;
  if(m_bitarray != 0) delete[] m_bitarray;
}

void BitBlock::operator=(BitBlock const& b2) {
  init(b2.size(), b2.m_intarray);
}

void BitBlock::init(uint bitsize, bool keepBitArray) {
  if(bitsize==0) {
    throw Exception("BitBlock::init(uint)", "bitsize cannot be 0");
  }
  m_bitsize = bitsize;
  m_intsize = getintsize(bitsize); //note: bitsize>0 => m_intsize>0
  if(m_intarray != 0) delete[] m_intarray;
  m_intarray = new uint[m_intsize];
  if(m_bitarray != 0) delete[] m_bitarray;
  for(uint i=0; i<m_intsize; i++) m_intarray[i] = 0;
  if(keepBitArray) {
    m_bitarray = new char[m_bitsize];
    for(uint i=0; i<m_bitsize; i++) m_bitarray[i] = 0;
  }
}

void BitBlock::init(uint bitsize, char* bitarray, bool keepBitArray) {
  if(bitsize==0) {
    throw Exception("BitBlock::init(uint,char*)", "bitsize cannot be 0");
  }
  m_bitsize = bitsize;
  m_intsize = getintsize(bitsize); //note: bitsize>0 => m_intsize>0
  if(m_intarray != 0) delete[] m_intarray;
  m_intarray = new uint[m_intsize];

  if(m_bitarray != 0) delete[] m_bitarray;
  if(keepBitArray) {
    m_bitarray = new char[m_bitsize];
    for(uint i=0; i<m_bitsize; i++) m_bitarray[i] = bitarray[i];
  }

  for(uint i=0; i<m_intsize; i++) {
    uint cur = 0;
    uint baseindex = 8*sizeof(uint)*i;
    for(uint j=0; j < (8*sizeof(uint)) && j < (bitsize-baseindex); j++) {
      cur+= bitarray[baseindex+j] << j;
    }
    m_intarray[i] = cur;
  }
}

void BitBlock::init_rev(uint bitsize, char* bitarray, bool keepBitArray) {
  if(bitsize==0) {
    throw Exception("BitBlock::init_rev(uint,char*)", 
                        "bitsize cannot be 0");
  }
  m_bitsize = bitsize;
  m_intsize = getintsize(bitsize); //note: bitsize>0 => m_intsize>0
  if(m_intarray != 0) delete[] m_intarray;
  m_intarray = new uint[m_intsize];

  if(m_bitarray != 0) delete[] m_bitarray;
  if(keepBitArray) {
    m_bitarray = new char[m_bitsize];
    for(uint i=0; i<m_bitsize; i++) m_bitarray[i] = bitarray[m_bitsize-1-i];
  }

  for(uint i=0; i<m_intsize; i++) {
    uint cur = 0;
    uint baseindex = bitsize - 8*sizeof(uint)*i;
    for(uint j=0; j < (8*sizeof(uint)) && j < baseindex; j++) {
      cur+= bitarray[baseindex-1-j] << j;
    }
    m_intarray[i] = cur;
  }
}

void BitBlock::init(uint bitsize, uint* array) {
  if(bitsize==0) {
    throw Exception("BitBlock::init(uint,uint*)", "bitsize cannot be 0");
  }
  m_bitsize = bitsize;
  m_intsize = getintsize(bitsize); //note: bitsize>0 => m_intsize>0
  if(m_intarray != 0) delete[] m_intarray;
  m_intarray = new uint[m_intsize];

  // never keep a char array copy when using this initialization
  if(m_bitarray != 0) delete[] m_bitarray;
  m_bitarray = 0;

  // copy all the elements but the last
  for(uint i=0; i < (m_intsize-1); i++) m_intarray[i] = array[i];

  // for the last element, make sure that the unused bits are 0  
  int valid = bitsize % (8*sizeof(uint)); // number of bits used

  if(valid==0) { // all the bits are used
    m_intarray[m_intsize-1] = array[m_intsize-1];
  } else { // not all the bits are used
    uint mask = (1 << valid) - 1; // marks all the interesting bits
    m_intarray[m_intsize-1] = array[m_intsize-1] & mask;
  }

}

void BitBlock::clear() {
  for(uint i=0; i<m_intsize; i++) {
    m_intarray[i] = 0;
  }
  // Update the bitarray cache if it's being used
  if(m_bitarray!=0) {
    for(uint i=0; i<m_bitsize; i++) m_bitarray[i] = 0;
  }
}

char BitBlock::getbit(uint index) const {
  if(index >= m_bitsize) {
    throw OutOfBoundException();
  }

  if(m_bitarray!=0) return m_bitarray[index];

  uint arrayindex = index / (8*sizeof(uint));
  uint bitindex = index % (8*sizeof(uint));

  uint bucket = m_intarray[arrayindex];
  uint mask = 1 << bitindex;
  if((bucket & mask) == 0) return 0;
  return 1;
}

void BitBlock::setbit(uint index, char value) {
  if(index >= m_bitsize) {
    throw OutOfBoundException();
  }

  if(m_bitarray!=0 && m_bitarray[index]==value) return;

  uint arrayindex = index / (8*sizeof(uint));
  uint bitindex = index % (8*sizeof(uint));

  uint mask = 1 << bitindex;
  if( value == 0 ) {
    mask = ~mask;
    m_intarray[arrayindex] &= mask;
  }
  else if( value == 1 ) {
    m_intarray[arrayindex] |= mask;
  }
  else {
    throw Exception("BitBlock::setbit", "Invalid bit value");
  }

  // Update the bitarray cache if it's being used
  if(m_bitarray!=0) m_bitarray[index] = value;

}

void BitBlock::togglebit(uint index) {
  if(index >= m_bitsize) {
    throw OutOfBoundException();
  }

  uint arrayindex = index / (8*sizeof(uint));
  uint bitindex = index % (8*sizeof(uint));

  uint mask = 1 << bitindex;
  m_intarray[arrayindex] ^= mask;

  // Update the bitarray cache if it's being used
  if(m_bitarray!=0) m_bitarray[index] ^= 1;
}

BitBlock& BitBlock::toggleAll() {
	for(uint i=0; i < m_intsize; i++) {
		m_intarray[i]= ~m_intarray[i];
	}
	// set unused bits to 0
	int valid = m_bitsize % (8*sizeof(uint));
	if(valid>0) {
		uint mask = (1 << valid) - 1; // marks all the interesting bits
		m_intarray[m_intsize-1]&= mask;
	}
	return *this;
}

bool BitBlock::operator==(BitBlock const& b2) const {
  if( size() != b2.size() ) return false;
  if( size() == 0 ) return true;

  uint* a1 = m_intarray;
  uint* a2 = b2.m_intarray;
  for(uint i=0; i < m_intsize; i++) {
    if(a1[i] != a2[i]) return false;
  }
  return true;
}

bool BitBlock::operator<(BitBlock const& b2) const {
  // (currently the operator is not defined for the case b1 and b2
  // don't have the same size)
  if( size() != b2.size() )
    throw Exception("BitBlock::operator<", "size mismatch");

  uint* a1 = m_intarray;
  uint* a2 = b2.m_intarray;

  int i = m_intsize - 1;
  if(i<0) {
    throw Exception("BitBlock::operator<", 
                        "Operation is invalid on an empty BitBlock");
  }
  // find the most significant word where b1 and b2 disagree
  while( a1[i] == a2[i] && i>=0 ) i--;
  if(i<0) return false; // b1 == b2
  return (a1[i] < a2[i]);
}

// ( almost same code as operator<() )
bool BitBlock::operator>(BitBlock const& b2) const {
  // (currently the operator is not defined for the case b1 and b2
  // don't have the same size)
  if( size() != b2.size() )
    throw Exception("BitBlock::operator>", "size mismatch");

  uint* a1 = m_intarray;
  uint* a2 = b2.m_intarray;

  int i = m_intsize - 1;
  if(i<0) {
    throw Exception("BitBlock::operator>", 
                        "Operation is invalid on an empty BitBlock");
  }
  // find the most significant word where b1 and b2 disagree
  while( a1[i] == a2[i] && i>=0 ) i--;
  if(i<0) return false; // b1 == b2
  return (a1[i] > a2[i]);
}

BitBlock const BitBlock::operator+(BitBlock const& b2) const {
  if( size() != b2.size() )
    throw Exception("BitBlock::operator+", "size mismatch");

  uint* a1 = m_intarray;
  uint* a2 = b2.m_intarray;
  uint* newarray = new uint[m_intsize];

  for(uint i=0; i<m_intsize; i++) {
    newarray[i] = a1[i] xor a2[i];
  }

  BitBlock b(size(), newarray);
  return b;
}

void BitBlock::operator+=(BitBlock const& b2) {
  if( size() != b2.size() )
    throw Exception("BitBlock::operator+=", "size mismatch");

  uint* a2 = b2.m_intarray;
  for(uint i=0; i<m_intsize; i++) {
    m_intarray[i] = m_intarray[i] xor a2[i];
  }

  // turn off bit array caching if it is being used
  if(m_bitarray!=0) {
    delete[] m_bitarray;
    m_bitarray = 0;
  }
}

void BitBlock::operator&=(BitBlock const& b2) {
  if( size() != b2.size() )
    throw Exception("BitBlock::operator&=", "size mismatch");

  uint* a2 = b2.m_intarray;
  for(uint i=0; i<m_intsize; i++) {
    m_intarray[i]&= a2[i];
  }

  // turn off bit array caching if it is being used
  if(m_bitarray!=0) {
    delete[] m_bitarray;
    m_bitarray = 0;
  }
}

int BitBlock::biterrorcnt(BitBlock const& b2) const {
  if( this->size() != b2.size() )
    throw Exception("BitBlock::biterrorcnt", "size mismatch");

  int errcnt = 0;
  uint* a1 = m_intarray;
  uint* a2 = b2.m_intarray;
  for(uint i=0; i < m_intsize; i++) {

    uint badbits = a1[i] xor a2[i];
    // count the number of '1' bits
    while(badbits != 0) {
      errcnt++;
      badbits &= badbits-1;
    }    
  }

  return errcnt;
}

uint BitBlock::smallweight() const {
  uint weight=0;
  for(uint i=0; i<m_intsize; i++) {
    uint tmp = m_intarray[i];
    // count the number of '1' bits (best for small weights)
    while(tmp != 0) {
      weight++;
      tmp&= tmp-1;
    }
  }
  return weight;
}

vector<int> BitBlock::errorLoc(char const* bitarray) {
  vector<int> errorList;
  errorLoc(bitarray, errorList);
  return errorList;
}

void BitBlock::errorLoc(BitBlock const* b2, vector<int>& list) const {
  char const* b2array = b2->getCachedBitArray();
  if(b2array!=0) return errorLoc(b2array, list);
  else {
    char* b2TmpArray = b2->toCharArray();
    errorLoc(b2TmpArray, list);
    delete[] b2TmpArray;
    return;
  }
}

void BitBlock::errorLoc(char const* b2, vector<int>& list) const {
  if(m_bitarray!=0) { // use the cached bit array if it exists
    for(uint i=0; i<size(); i++) {
      if(m_bitarray[i] != b2[i]) list.push_back(i);
    }
  } else { // create a temporary bit array
    char* tmpbitarray = toCharArray();
    for(uint i=0; i<size(); i++) {
      if(tmpbitarray[i] != b2[i]) list.push_back(i);
    }
    delete[] tmpbitarray;
  }
}

// (similar to errorLoc(char const*, vector<int>&))
void BitBlock::onesLoc(vector<int>& list) {
  // use the cached bit array if it exists, else create one
  if(m_bitarray==0) m_bitarray = toCharArray();

  for(uint i=0; i<size(); i++) {
    if(m_bitarray[i] == 1) list.push_back(i);
  }
}

// Note: Same code as BitBlock::euclideanDistanceSq(vector<double> const&)
double BitBlock::euclideanDistanceSq(double const* v) const {
  double distance=0;

  uint k=0;
  for(uint i=0; i<m_intsize; i++) {
    uint curWord = m_intarray[i];
    for(uint j=0; (j < 8*sizeof(uint)) && (k < size()); j++) {
      double bitx2 = static_cast<double>((curWord & 1) << 1); //left-shift
                                                              //for *2
      curWord>>= 1;
      distance+= pow(v[k++] - (bitx2-1), 2);
    }
  }

  return distance;
}

// Note: Same code as BitBlock::euclideanDistanceSq(double const*)
double BitBlock::euclideanDistanceSq(vector<double> const& v) const {
  double distance=0;
  if(v.size() < size()) {
	  throw Exception("BitBlock::euclideanDistanceSq(vector<double>)",
	                  "Vector size is too small");
  }

  uint k=0;
  for(uint i=0; i<m_intsize; i++) {
    uint curWord = m_intarray[i];
    for(uint j=0; (j < 8*sizeof(uint)) && (k < size()); j++) {
      double bitx2 = static_cast<double>((curWord & 1) << 1); //left-shift
                                                              //for *2
      curWord>>= 1;
      distance+= pow(v[k++] - (bitx2-1), 2);
    }
  }

  return distance;
}

char BitBlock::dotprod(BitBlock const& b2) const {
  if( this->size() != b2.size() )
    throw Exception("BitBlock::dotprod", "size mismatch");

  uint* a1 = m_intarray;
  uint* a2 = b2.m_intarray;
  int count = 0;
  // AND corresponding int's together and count the number of bits
  for(uint i=0; i < m_intsize; i++) {
    uint tmp = a1[i] & a2[i];
    // "parallel" bit counting
    //TODO: should probably use precompute-8 or precompute-16 instead
    tmp = COUNT(tmp, 0);
    tmp = COUNT(tmp, 1);
    tmp = COUNT(tmp, 2);
    tmp = COUNT(tmp, 3);
    tmp = COUNT(tmp, 4);
    count+= tmp;
  }

  return (count & 1); // mod 2
}

char BitBlock::dotprod(char* bitarray, uint size) const {
  if( this->size() != size )
    throw Exception("BitBlock::dotprod", "size mismatch");

  // AND corresponding bits together and sum them up
  uint sum=0;
  for(uint i=0; i<size; i++) {
    sum+= (getbit(i) & bitarray[i]); //Note: getbit(uint) takes advantage
                                     //   of bit array caching if enabled
  }
  // operations are binary => addition is mod 2
  return (sum & 1);
}

char BitBlock::dotprod_rev(char* bitarray, uint size) const {
  if( this->size() != size )
    throw Exception("BitBlock::dotprod_rev", "size mismatch");

  // AND corresponding bits together and sum them up
  uint sum=0;
  for(uint i=0; i<size; i++) {
    sum+= (getbit(i) & bitarray[size-1-i]); //Note: getbit(uint) takes advantage
                                            //   of bit array caching if enabled
  }
  // operations are binary => addition is mod 2
  return (sum & 1);
}

char BitBlock::dotprodOR(BitBlock const& b2) const {
  if( this->size() != b2.size() )
    throw Exception("BitBlock::dotprod", "size mismatch");

  uint* a1 = m_intarray;
  uint* a2 = b2.m_intarray;
  char retval=0;
  // AND corresponding int's together and check if resulting vector is all-zero
  for(uint i=0; i < m_intsize; i++) {
    uint tmp = a1[i] & a2[i];
    if(tmp!=0) return 1;
  }
  return 0;
}

void BitBlock::matrixprodOR(BitBlock** M, uint Mcolsize, BitBlock& retvect) const {
  if( Mcolsize==0 )
    throw Exception("BitBlock::matrixprodOR", "zero size matrix");
  if( this->size() != M[0]->size() )
    throw Exception("BitBlock::matrixprodOR", "size mismatch");

  retvect.init(this->size());
  uint* a2 = retvect.m_intarray;
  for(int i=Mcolsize-1; i>=0; i--) {
    uint intIndex = i / (8*sizeof(uint));
    a2[intIndex]<<= 1; // shift over previous bit (not harmful on first access)
    char curBit = dotprodOR(*M[i]);
    if(curBit==1) a2[intIndex] |= 1; //TODO: rewrite without branch
  }
}

int BitBlock::getMsbIndex() const {
  if(m_intsize==0) {
    throw Exception("BitBlock::getMsbIndex", 
                        "Invalid operation on empty BitBlock.");
  }
  // find most significant non-zero word
  int i = m_intsize-1;
  while( m_intarray[i] == 0 && i>=0 ) i--;
  if(i<0) return -1; // the sequence is all-zero
  uint word = m_intarray[i];

  // find highest bit
  // (this assumes that "word" has 32 bits)
  uint threshold = 65536; // 2^16
  int exponent = 16;
  int delta = 8;
  while( delta > 0 ) {
    if(word < threshold) {
      threshold>>= delta;
      exponent-= delta;
      delta>>= 1;
    }
    else {
      threshold<<= delta;
      exponent+= delta;
      delta>>= 1;
    }
  }

  if( word < threshold ) exponent--;
  return exponent + i*32;
}

//Note: This method is only called if bit array caching is not used
char* BitBlock::toCharArray() const {
  char* array = new char[size()];
  
  uint k=0;
  for(uint i=0; i < m_intsize; i++) {
    int curBucket = m_intarray[i];
    for(uint j=0; (j < 8*sizeof(uint)) && (k < size()); j++) {
      array[k++] = curBucket & 1;
      curBucket >>= 1;
    }
  }

  return array;
}

//debug
void BitBlock::print() {
  if(0<m_intsize) std::cout << m_intarray[0];
  for(uint i=1; i < m_intsize; i++) {
    std::cout << ", " << m_intarray[i];
  }
  std::cout << std::endl;
}

void BitBlock::printbits(std::ostream& outstr) {
  for(uint i=0; i < m_bitsize; i++) {
    if( getbit(i) == 0 ) outstr << "0";
    else outstr << "1";
  }
}
