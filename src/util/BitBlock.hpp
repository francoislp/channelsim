// Author: Francois Leduc-Primeau
// Copyright 2013

#ifndef BitBlock_hpp_
#define BitBlock_hpp_

//#include "GlobalDefs.h"
typedef unsigned int uint;
#include <math.h>
#include <vector>
#include <ostream>

class BitBlock {

 public:

  /// Default constructor.
  BitBlock();

  /// Copy constructor. Any 'char' array caching is not preserved.
  BitBlock(BitBlock const& b2);

  /**
   * Constructs a new all-zero Bit Block of the specified size.
   * No 'char' array copy of the data will be kept.
   */
  BitBlock(uint bitsize);

  /**
   * Same as BitBlock(uint), but allows specifying whether a 'char'
   * array is maintained in parallel with the 'int' array.
   */
  BitBlock(uint bitsize, bool keepBitArray);

  /**
   * Constructs a new Bit Block that can hold the specified number of
   * bits. Those bits are initialized from an array of bytes, each
   * byte representing one bit. The size of bitarray must be greater
   * or equal than bitsize.
   * The elements of bitarray are assumed to be 0 or 1.
   * No 'char' array copy of the data is kept.
   */
  BitBlock(uint bitsize, char* bitarray);

  /**
   * Same as BitBlock(uint, char*) but allows specifying whether a
   * 'char' array is maintained in parallel with the 'int' array, in
   * order to increase the speed of certain operations, at the cost of
   * using more memory.
   */
  BitBlock(uint bitsize, char* bitarray, bool keepBitArray);

  /**
   * Constructs a new Bit Block from an array of integers. All the
   * bits found in all the integers are used, up to bitsize. 
   * No 'char' array copy of the data is kept.
   */
  BitBlock(uint bitsize, uint* array);

  ~BitBlock();

  /// Overloaded assignment operator
  void operator=(BitBlock const& b2);

  /**
   * Re-initializes the object as when using the constructor BitBlock(uint).
   */
  void init(uint bitsize) {return init(bitsize, false);}

  /**
   * Re-initializes the object as when using the constructor BitBlock(uint, bool).
   */
  void init(uint bitsize, bool keepBitArray);

  /**
   * Re-initializes the object as when using the constructor BitBlock(uint, char*).
   */
  void init(uint bitsize, char* bitarray) {return init(bitsize, bitarray, false);}

  /**
   * Re-initializes the object as when using the constructor
   * BitBlock(uint, char*, bool).
   */
  void init(uint bitsize, char* bitarray, bool keepBitArray);

  /**
   * Same as the BitBlock(int, char*) constructor, but the array of
   * bits is read in reverse order.
   */
  void init_rev(uint bitsize, char* bitarray) {
    return init_rev(bitsize, bitarray, false); }

  /**
   * Same as the BitBlock(int, char*, bool) constructor, but the array of
   * bits is read in reverse order.
   */
  void init_rev(uint bitsize, char* bitarray, bool keepBitArray);

  /**
   * Re-initializes the object as when using the constructor
   * BitBlock(uint, uint*).
   */
  void init(uint bitsize, uint* array);

  /**
   * Set all the bits to 0.
   */
  void clear();

  /**
   * Returns the bit at the specified index (guaranteed to be 0 or 1)
   * (same as getbit(uint)).
   *@throws OutOfBoundException If index >= size().
   */
  char operator[](uint index) const {return getbit(index);}

  /**
   * Returns the bit at the specified index (guaranteed to be 0 or 1).
   *@throws OutOfBoundException If index >= size().
   */
  char getbit(uint index) const;

  /**
   * Sets the bit at the specified index to the specified value (which
   * must be 0 or 1, otherwise an exception is thrown).
   */
  void setbit(uint index, char value);

  /**
   * Toggles the bit at the specified index.
   */
  void togglebit(uint index);

	/**
	 * Toggle all bits.
	 */
	BitBlock& toggleAll();

  /**
   * Number of bits in the block.
   */
  uint size() const {return m_bitsize;}

  /**
   * Tests whether all the bits in this block and block b2 are the same.
   */
  bool operator==(BitBlock const& b2) const;

  bool operator!=(BitBlock const& b2) const { return ! operator==(b2); }

  /**
   * Tests whether the number represented by this bit sequence is
   * smaller than the bit sequence of b2.
   */
  bool operator<(BitBlock const& b2) const;

  /**
   * Tests whether the number represented by this bit sequence is
   * bigger than the bit sequence of b2.
   */  
  bool operator>(BitBlock const& b2) const;

  /**
   * XOR two BitBlocks together and returns a new BitBlock.
   */
  BitBlock const operator+(BitBlock const& b2) const;
  
  /**
   * Updates this BitBlock by XORing another BitBlock with
   * it. Automatically turns off 'char' array caching if in use.
   */
  void operator+=(BitBlock const& b2);

  /**
   * Updates this BitBlock by ANDing another BitBlock with
   * it. Automatically turns off 'char' array caching if in use.
   */
  void operator&=(BitBlock const& b2);

  /**
   * Counts the number of bit mismatches between this BitBlock and b2.
   */
  int biterrorcnt(BitBlock const& b2) const;

  /**
   * Count the number of '1' bits, with code optimized for small
   * numbers (similar to biterrorcnt(...)).
   */
  uint smallweight() const;

  /**
   * Identifies the index of all the bit mismatches between this
   * BitBlock and the bit array provided. As a side effect, creates a
   * bit array cache if it doesn't exist.
   *@return A list of bit indexes.
   */
  std::vector<int> errorLoc(char const* bitarray);

  /**
   * Calls errorLoc(char*, std::vector<int>&) after extracting the bit
   * array representation of b2. The cached bit array of b2 is used if
   * it exists, otherwise b2->toCharArray() is used to create a bit
   * array, which is less efficient. As a side effect, creates a bit
   * array cache in this BitBlock if it doesn't exist.
   */
  void errorLoc(BitBlock const* b2, std::vector<int>& list) const;

  /// Same as errorLoc(char*) but the indexes are added to "list".
  void errorLoc(char const* bitarray, std::vector<int>& list) const;

  /**
   * Similar to errorLoc(), but returns the position of '1' bits. As a
   * side effect, creates a bit array cache if it doesn't exist.
   */
  void onesLoc(std::vector<int>& list);

  /**
   * Computes the square of Euclidean distance between this BitBlock
   * (interpreted in BPSK space) and the real vector provided.
   * "Interpreted in BPSK space" means that 0 bits are replaced
   * with -1, and 1 bits remain 1.
   *
   *@param v Real vector, which is assumed to be of length equal to this 
   *         BitBlock.
   */
  double euclideanDistanceSq(double const* v) const;

  /**
   * Computes the square of Euclidean distance between this BitBlock
   * (interpreted in BPSK space) and the real vector provided.
   * "Interpreted in BPSK space" means that 0 bits are replaced
   * with -1, and 1 bits remain 1.
   *
   *@param v Real vector, which must be at least as long as this BitBlock.
   */
	double euclideanDistanceSq(std::vector<double> const& v) const;
	

  /**
   * Performs the dot product of two bit blocks, and returns the
   * resulting bit.
   */
  char dotprod(BitBlock const& b2) const;

  /**
   * Performs the dot product of this bit block with the specified bit
   * array and returns the resulting bit. bitarray[0] is taken to be
   * the lsb.
   */
  char dotprod(char* bitarray, uint size) const;

  /**
   * Performs the dot product of this bit block with the specified bit
   * array and returns the resulting bit. bitarray[0] is taken to be
   * the msb.
   */
  char dotprod_rev(char* bitarray, uint size) const;

  /**
   * Performs the bitwise AND of the two bit vectors, and returns 1 if
   * the resulting vector contains at least one non-zero bit, and 0
   * otherwise. This corresponds to a dot product where multiplication
   * is replaced by AND, and addition by OR.
   */
  char dotprodOR(BitBlock const& b2) const;

  /**
   * Product of this vector with a matrix of BitBlocks, using the
   * vector product dotprodOR().
   *@param M  An array of BitBlock* (i.e. BitBlock pointers), where each 
   *          BitBlock represents one column of the matrix.
   *@param Mcolsize  The number of BitBlock columns in the matrix.
   *@param retvect  A BitBlock object that is overwritten with the result.
   */
  void matrixprodOR(BitBlock** M, uint Mcolsize, BitBlock& retvect) const;

  /**
   * Returns the index of the most significant non-zero bit, or -1 if
   * the bit sequence is all zero.
   */
  int getMsbIndex() const;

  /**
   * Whether we keep a copy of the data in the form of a 'char' array
   * (to increase the speed of certain operations).
   */
  bool getKeepBitArray() const {return (m_bitarray!=0);}

  /**
   * Returns the cached bit array (where each bit is represented by a
   * 'char' type), or NULL if getKeepBitArray() is false.
   */
  char const* getCachedBitArray() const {return m_bitarray;}

  /**
   * Converts this BitBlock to an array of individual bits. The caller
   * is responsible for deleting the array.
   */
  char* toCharArray() const;

  // debug
  void print();

  /**
   * Print the bit data to the specified stream. A sequence of "0" and
   * "1" characters is printed. No new line character is appended.
   */
  void printbits(std::ostream&);

  // static
  /**
   * Returns the number of uint's required to store the specified
   * amount of bits.
   */
  static uint getintsize(uint nbbits) {
    return ceil( static_cast<double>(nbbits)/(8*sizeof(uint)) );
  }

 private:

  /// Number of bits in the block
  uint m_bitsize;

  /// Number of ints used as storage
  uint m_intsize;

  /// Storage for the bits
  uint* m_intarray;

  /// Parallel bit-by-bit storage (can be NULL if not used).
  char* m_bitarray;
};
 

#endif
