/*
 *  Exception.cpp
 *
 *  Created by Francois Leduc-Primeau on 15/03/07.
 *
 */

#include "Exception.hpp"

// class Exception ---------------------------------------------------

/* Constructor 1 */
Exception::Exception(std::string type) 
{
  m_message = type;
}

/* Constructor 2 */
Exception::Exception(std::string type, std::string message) {
  m_message = type + ": " + message;
}

// (not in class Exception)
ostream& operator<<(ostream& stream, Exception const & e) {
  return stream << e.message();
}

ostream& operator<<(ostream& stream, Exception const * e) {
  return stream << e->message();
}

// class InvalidSymbolException --------------------------------------
InvalidSymbolException::InvalidSymbolException(std::string msg)
  : Exception("InvalidSymbolException", msg)
{}

// class SourceException ---------------------------------------------
// SourceException::SourceException(std::string msg)
//   : Exception("SourceException", msg)
// {}

// SourceException::SourceException(std::string msg, ISymbolProvider* src)
//   : Exception("SourceException", msg),
//     m_src(src)
// {}

// class OutOfBoundException -----------------------------------------
OutOfBoundException::OutOfBoundException()
  : Exception("OutOfBoundException")
{}

OutOfBoundException::OutOfBoundException(std::string msg)
  : Exception("OutOfBoundException", msg)
{}

// class EndOfFeedException ------------------------------------------
EndOfFeedException::EndOfFeedException(std::string msg)
  : Exception("EndOfFeedException", msg)
{}

// class MaxTrialsException ------------------------------------------
MaxTrialsException::MaxTrialsException(std::string msg)
  : Exception("MaxTrialsException", msg)
{}

BufferEmptyException::BufferEmptyException()
  : Exception("BufferEmptyException", "")
{}

BufferFullException::BufferFullException()
  : Exception("BufferFullException", "")
{}
