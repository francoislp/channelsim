/*
 *  Exception.hpp
 *
 *  Created by Francois Leduc-Primeau on 15/03/07.
 *
 */

#ifndef _Exception_hpp_
#define _Exception_hpp_

#include <string>
#include <ostream>
//#include "ISymbolProvider.h"

using std::ostream;

/*
 * The parent of all exception classes.
 */
class Exception
{
public:
  Exception(std::string type);
  Exception(std::string type, std::string message);
  virtual ~Exception() {}
    
  std::string message() const {return m_message;}
    
private:
  std::string m_message;
};

ostream& operator<<(ostream&, Exception const &);

ostream& operator<<(ostream&, Exception const *);

//thrown by: Implementers of ICoder
class InvalidSymbolException : public Exception {
 public:
  InvalidSymbolException(std::string msg);
};

// class SourceException : public Exception {
// public:
//   SourceException(std::string msg);
//   SourceException(std::string msg, ISymbolProvider* src);
// private:
//   ISymbolProvider* m_src; //for future functionality
// };

class OutOfBoundException : public Exception {
 public:
  OutOfBoundException();
  OutOfBoundException(std::string msg);
};

class EndOfFeedException : public Exception {
 public:
  EndOfFeedException(std::string msg);
};

class MaxTrialsException : public Exception {
 public:
  MaxTrialsException(std::string msg);
};

/// Exception thrown by implementers of INodeOutputBuffer.
class BufferEmptyException : public Exception {
public:
  BufferEmptyException();
};

/// Exception thrown by implementers of INodeOutputBuffer.
class BufferFullException : public Exception {
public:
  BufferFullException();
};

#endif

