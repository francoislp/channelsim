#ifndef _TYPE_CONVERTER_HPP
#define _TYPE_CONVERTER_HPP

#include <string>
#include <sstream>
//#include <iostream>
#include <stdexcept>


namespace TypeConverter {

 /**
  *Converts value from string to another type
  *\tparam T destination type.
  *\param val Property value.
  */
 template<typename T> void convert(T* dst, const std::string& val) {

   std::stringstream ss;
   T ret;

   ss << val;
   ss >> ret;
   if( !ss.eof() ) { //FLP
     std::string msg;
     msg = "Parsing Error (value = " + val + ")";
     throw std::runtime_error(msg.c_str());
   }

   *dst = ret;

 }

 template<> inline void convert<std::string>(std::string *dst, const std::string& val) {

   *dst = val;

 }

 template<> inline void convert<bool>(bool *dst, const std::string& val) {

   bool ret;
   if ((val == "true") || (val == "1") || (val == "yes")) {
     ret = true;
   }
   else if ((val == "false") || (val == "0") || (val == "no")) {
     ret = false;
   }
   else {
     std::string msg;
     msg = "Invalid value for a bool " + val + "!";
     throw std::runtime_error(msg.c_str());
   }

   *dst = ret;

 }

}

#endif
